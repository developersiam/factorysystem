﻿using FactoryBL.Helper;
using FactoryDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.Report
{
    public interface IPickingReportBL
    {
        void DetailReport(string rcno);
        void SummaryReport(string rcno);
    }
    public class PickingReportBL : IPickingReportBL
    {
        StecDBMSUnitOfWork uow;
        public PickingReportBL()
        {
            uow = new StecDBMSUnitOfWork();
        }
        public void DetailReport(string rcno)
        {
            try
            {
                if (string.IsNullOrEmpty(rcno))
                    throw new ArgumentException("โปรดระบุ Rcno");


                if (string.IsNullOrEmpty(rcno))
                    throw new ArgumentException("โปรดระบุ Rc no.");

                var matList = uow.matRepo.Get(x => x.rcno == rcno).ToList();
                var matHeader = uow.matRepo.GetSingle(x => x.rcno == rcno);

                var matRcHeader = uow.matrcRepo.GetSingle(x => x.rcno == rcno);


                h_Excel.Open(@"C:\Receiving\Template\SupplierDetail.xls");
                h_Excel.PutDataToCell("C5", matHeader.type);
                h_Excel.PutDataToCell("C6", matHeader.frompddate.ToString());
                h_Excel.PutDataToCell("C7", matHeader.frompdno);
                h_Excel.PutDataToCell("C8", matHeader.fromprgrade);

                h_Excel.PutDataToCell("E5", matRcHeader.classifier);
                h_Excel.PutDataToCell("G3", rcno);

                h_Excel.PutDataToCell("G5", matList.Count().ToString());
                h_Excel.PutDataToCell("G6", ((decimal)matList.Sum(x => x.weight)).ToString("N2") + "  Kg.");
                h_Excel.PutDataToCell("G7", ((DateTime)matRcHeader.starttime).ToShortTimeString());
                h_Excel.PutDataToCell("G8", ((DateTime)matRcHeader.finishtime).ToShortDateString());


                int row = 11;
                int rowNumber = 1;
                foreach (var item in matList.OrderBy(x => x.baleno))
                {
                    h_Excel.PutDataToCell("B" + row, "BD");
                    h_Excel.PutDataToCell("C" + row, item.baleno.ToString());
                    h_Excel.PutDataToCell("D" + row, "'" + item.bc);
                    h_Excel.PutDataToCell("F" + row, item.classify);
                    h_Excel.PutDataToCell("G" + row, item.weight.ToString());

                    row++;
                    rowNumber++;
                }

                //Print Summary
                h_Excel.PutDataToCell("D" + row, "Bale Total " + matList.Count().ToString());
                h_Excel.PutDataToCell("F" + row, "Weight Total " + ((decimal)matList.Sum(x => x.weight)).ToString("N2") + "  Kg.");


                //h_Excel.DeleteRow("A" + row, "A1000");
                h_Excel.Print(1);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void SummaryReport(string rcno)
        {
            try
            {
                if (string.IsNullOrEmpty(rcno))
                    throw new ArgumentException("โปรดระบุ Rc no.");

                var matList = uow.matRepo.Get(x => x.rcno == rcno).ToList();
                var matHeader = uow.matRepo.GetSingle(x => x.rcno == rcno);

                var matRcHeader = uow.matrcRepo.GetSingle(x => x.rcno == rcno); 


                h_Excel.Open(@"C:\Receiving\Template\SupplierSummary.xls");
                h_Excel.PutDataToCell("C5",  matHeader.type);
                h_Excel.PutDataToCell("C6",  matHeader.frompddate.ToString());
                h_Excel.PutDataToCell("C7",  matHeader.frompdno);
                h_Excel.PutDataToCell("C8",  matHeader.fromprgrade);

                h_Excel.PutDataToCell("E5",  matRcHeader.classifier);
                h_Excel.PutDataToCell("G3",  rcno);

                h_Excel.PutDataToCell("G5", matList.Count().ToString());
                h_Excel.PutDataToCell("G6", ((decimal)matList.Sum(x => x.weight)).ToString("N2") + "  Kg.");
                h_Excel.PutDataToCell("G7", ((DateTime)matRcHeader.starttime).ToShortTimeString());
                h_Excel.PutDataToCell("G8", ((DateTime)matRcHeader.finishtime).ToShortDateString());

                var classifyIn = matList
                    .GroupBy(x => x.classify)
                    .Select(x => new
                    {
                        classify = x.Key,
                        bale = x.Count(),
                        weight = x.Sum(y => y.weight)
                    })
                    .ToList();


                int row = 11;
                int rowNumber = 1;
                foreach (var item in classifyIn.OrderBy(x => x.classify))
                {
                    h_Excel.PutDataToCell("B" + row, "BD");
                    h_Excel.PutDataToCell("C" + row, item.classify);
                    h_Excel.PutDataToCell("F" + row, item.bale.ToString());
                    h_Excel.PutDataToCell("G" + row, item.weight.ToString());

                    row++;
                    rowNumber++;
                }

                //Print Summary
                h_Excel.PutDataToCell("B" + row, "Grand Total   " + classifyIn.Count().ToString());
                h_Excel.PutDataToCell("D" + row, "Bale Total   " + matList.Count().ToString());
                h_Excel.PutDataToCell("F" + row, "Weight Total   " + ((decimal)matList.Sum(x => x.weight)).ToString("N2"));


                //h_Excel.DeleteRow("A" + row, "A1000");
                h_Excel.Print(1);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
