﻿using FactoryBL;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.Model;
using FactorySystemWPF.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace FactorySystemWPF.ViewModel.Processing.ProductionControl
{
    public class vm_BarcodeSetting : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_BarcodeSetting()
        {
            _packedGradeList = Facade.packedgradeBL()
                .GetAll()
                .Where(x => x.crop > user_setting.currentCrop - 7)
                .OrderBy(x => x.packedgrade1)
                .ToList();
            _templateList = Facade.BarcodeTemplateBL()
                .GetAll()
                .OrderBy(x => x.FileName)
                .ToList();
            _printerList = Facade.BarcodePrinterBL()
                .GetAll()
                .OrderBy(x => x.PrinterName)
                .ToList();
            RaisePropertyChangedEvent(nameof(PackedGradeList));
            RaisePropertyChangedEvent(nameof(TemplateList));
            RaisePropertyChangedEvent(nameof(PrinterList));
            Clear();
        }


        #region Properties
        private Guid _templateID;

        public Guid TemplateID
        {
            get { return _templateID; }
            set { _templateID = value; }
        }

        private string _packedgrade;

        public string packedgrade
        {
            get { return _packedgrade; }
            set { _packedgrade = value; }
        }

        private int _printerID;

        public int PrinterID
        {
            get { return _printerID; }
            set { _printerID = value; }
        }

        private int _fromCaseNo;

        public int FromCaseNo
        {
            get { return _fromCaseNo; }
            set { _fromCaseNo = value; }
        }

        private int _toCaseNo;

        public int ToCaseNo
        {
            get { return _toCaseNo; }
            set { _toCaseNo = value; }
        }

        private short _copies;

        public short Copies
        {
            get { return _copies; }
            set { _copies = value; }
        }

        private BarcodeSetting _dataGridSelectedRow;

        public BarcodeSetting DataGridSelectedRow
        {
            get { return _dataGridSelectedRow; }
            set { _dataGridSelectedRow = value; }
        }

        private Visibility _addButtonVisibility;

        public Visibility AddButtonVisibility
        {
            get { return _addButtonVisibility; }
            set { _addButtonVisibility = value; }
        }

        private Visibility _editButtonVisibility;

        public Visibility EditButtonVisibility
        {
            get { return _editButtonVisibility; }
            set { _editButtonVisibility = value; }
        }
        #endregion



        #region List
        private List<BarcodeSetting> _barcodeSettingList;

        public List<BarcodeSetting> BarcodeSettingList
        {
            get { return _barcodeSettingList; }
            set { _barcodeSettingList = value; }
        }

        private List<BarcodeTemplate> _templateList;

        public List<BarcodeTemplate> TemplateList
        {
            get { return _templateList; }
            set { _templateList = value; }
        }

        private List<packedgrade> _packedGradeList;

        public List<packedgrade> PackedGradeList
        {
            get { return _packedGradeList; }
            set { _packedGradeList = value; }
        }

        private List<BarcodePrinter> _printerList;

        public List<BarcodePrinter> PrinterList
        {
            get { return _printerList; }
            set { _printerList = value; }
        }
        #endregion



        #region Command
        private ICommand _addCommand;

        public ICommand AddCommand
        {
            get { return _addCommand ?? (_addCommand = new RelayCommand(Add)); }
            set { _addCommand = value; }
        }

        private void Add(object obj)
        {
            Add();
        }

        private ICommand _editCommand;

        public ICommand EditCommand
        {
            get { return _editCommand ?? (_editCommand = new RelayCommand(Edit)); }
            set { _editCommand = value; }
        }

        private void Edit(object obj)
        {
            Edit();
        }

        private ICommand _deleteCommand;

        public ICommand DeleteCommand
        {
            get { return _deleteCommand ?? (_deleteCommand = new RelayCommand(Delete)); }
            set { _deleteCommand = value; }
        }

        private void Delete(object obj)
        {
            Delete();
        }

        private ICommand _clearCommand;

        public ICommand ClearCommand
        {
            get { return _clearCommand ?? (_clearCommand = new RelayCommand(Clear)); }
            set { _clearCommand = value; }
        }

        private void Clear(object obj)
        {
            Clear();
        }

        private ICommand _selectedEditCommand;

        public ICommand SelectedEditCommand
        {
            get { return _selectedEditCommand ?? (_selectedEditCommand = new RelayCommand(SelectedEdit)); }
            set { _selectedEditCommand = value; }
        }

        private void SelectedEdit(object obj)
        {
            try
            {
                _dataGridSelectedRow = (BarcodeSetting)obj;
                if (_dataGridSelectedRow == null)
                    return;

                _templateID = _dataGridSelectedRow.TemplateID;
                _packedgrade = _dataGridSelectedRow.PackedGrade;
                _printerID = _dataGridSelectedRow.PrinterID;
                _fromCaseNo = _dataGridSelectedRow.FromCaseNo;
                _toCaseNo = _dataGridSelectedRow.ToCaseNo;
                _copies = _dataGridSelectedRow.Copies;
                _editButtonVisibility = Visibility.Visible;
                _addButtonVisibility = Visibility.Collapsed;
                RaisePropertyChangedEvent(nameof(TemplateID));
                RaisePropertyChangedEvent(nameof(packedgrade));
                RaisePropertyChangedEvent(nameof(PrinterID));
                RaisePropertyChangedEvent(nameof(FromCaseNo));
                RaisePropertyChangedEvent(nameof(ToCaseNo));
                RaisePropertyChangedEvent(nameof(Copies));
                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
                RaisePropertyChangedEvent(nameof(EditButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _refreshPackedGradeCommand;

        public ICommand RefreshPackedGradeCommand
        {
            get { return _refreshPackedGradeCommand ?? (_refreshPackedGradeCommand = new RelayCommand(RefreshPackedGrade)); }
            set { _refreshPackedGradeCommand = value; }
        }

        private void RefreshPackedGrade(object obj)
        {
            try
            {
                _packedGradeList = Facade.packedgradeBL().GetByCrop(user_setting.currentCrop);
                RaisePropertyChangedEvent(nameof(PackedGradeList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _addTemplateCommand;

        public ICommand AddTemplateCommand
        {
            get { return _addTemplateCommand ?? (_addTemplateCommand = new RelayCommand(AddTemplate)); }
            set { _addTemplateCommand = value; }
        }

        private void AddTemplate(object obj)
        {
            try
            {
                var window = new View.Processing.ProductionControl.BarcodeTemplate();
                var vm = new vm_BarcodeTemplate();
                window.DataContext = vm;
                window.ShowDialog();
                _templateList = Facade.BarcodeTemplateBL()
                    .GetAll()
                    .OrderBy(x => x.FileName)
                    .ToList();
                RaisePropertyChangedEvent(nameof(TemplateList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _addPrinterCommand;

        public ICommand AddPrinterCommand
        {
            get { return _addPrinterCommand ?? (_addPrinterCommand = new RelayCommand(AddPrinter)); }
            set { _addPrinterCommand = value; }
        }

        private void AddPrinter(object obj)
        {
            try
            {
                var window = new View.Processing.ProductionControl.BarcodePrinter();
                var vm = new vm_BarcodePrinter();
                window.DataContext = vm;
                window.ShowDialog();
                _printerList = Facade.BarcodePrinterBL()
                    .GetAll()
                    .OrderBy(x => x.PrinterName)
                    .ToList();
                RaisePropertyChangedEvent(nameof(PrinterList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion



        #region Function
        private void Add()
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการเพิ่มข้อมูลการตั้งค่าบาร์โค้ตใหม่ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                Facade.BarcodeSettingBL()
                    .Add(_packedgrade,
                    _templateID,
                    _printerID,
                    _fromCaseNo,
                    _toCaseNo,
                    _copies,
                    user_setting.security.uname);
                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Edit()
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                Facade.BarcodeSettingBL()
                    .Edit(
                    _dataGridSelectedRow.ID,
                    _packedgrade,
                    _templateID,
                    _printerID,
                    _fromCaseNo,
                    _toCaseNo,
                    _copies,
                    user_setting.security.uname);
                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Delete()
        {
            try
            {
                if (_dataGridSelectedRow == null)
                    return;
                
                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                Facade.BarcodeSettingBL().Delete(_dataGridSelectedRow.ID);
                MessageBoxHelper.Info("ลบข้อมูลสำเร็จ");
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Clear()
        {
            _templateID = Guid.NewGuid();
            _packedgrade = null;
            _fromCaseNo = 0;
            _toCaseNo = 0;
            _copies = 0;
            _dataGridSelectedRow = null;
            _addButtonVisibility = Visibility.Visible;
            _editButtonVisibility = Visibility.Collapsed;
            RaisePropertyChangedEvent(nameof(TemplateID));
            RaisePropertyChangedEvent(nameof(packedgrade));
            RaisePropertyChangedEvent(nameof(FromCaseNo));
            RaisePropertyChangedEvent(nameof(ToCaseNo));
            RaisePropertyChangedEvent(nameof(Copies));
            RaisePropertyChangedEvent(nameof(AddButtonVisibility));
            RaisePropertyChangedEvent(nameof(EditButtonVisibility));
            RaisePropertyChangedEvent(nameof(DataGridSelectedRow));
            BarcodeSettingListBinding();
        }

        private void BarcodeSettingListBinding()
        {
            try
            {
                _barcodeSettingList = Facade.BarcodeSettingBL()
                      .GetAll()
                      .OrderBy(x => x.BarcodeTemplate.FileName)
                      .ThenBy(x => x.PackedGrade)
                      .ToList();
                RaisePropertyChangedEvent(nameof(BarcodeSettingList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
