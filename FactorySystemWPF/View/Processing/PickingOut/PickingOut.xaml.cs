﻿using FactorySystemWPF.MVVM;
using FactorySystemWPF.ViewModel.Processing.PickingOut;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FactorySystemWPF.View.Processing.PickingOut
{
    /// <summary>
    /// Interaction logic for Feeding.xaml
    /// </summary>
    public partial class PickingOut : Page
    {
        public PickingOut()
        {
            InitializeComponent();
            DataContext = new vm_PickingOut();
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_PickingOut)DataContext;
            switch (e.PropertyName)
            {
               
                case nameof(vm.classifier):
                    classifierComboBox.Focus();
                    break;
                case nameof(vm.remark):
                    remarkTextBox.SelectAll();
                    remarkTextBox.Focus();
                    break;
                case nameof(vm.bc):
                    bcTextBox.SelectAll();
                    bcTextBox.Focus();
                    break;
                case nameof(vm.baleno):
                    baleTextBox.SelectAll();
                    baleTextBox.Focus();
                    break;
                default:
                    break;
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }
    }
}
