﻿using FactoryBL;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.MVVM;
using FactorySystemWPF.View.Processing.Packing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace FactorySystemWPF.ViewModel.Processing.Packing
{
    public class vm_ConfirmToPrintInternalBarcode : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_ConfirmToPrintInternalBarcode()
        {
            _isValid = false;
            _loginFormVisibility = Visibility.Visible;
            _printerListVisibility = Visibility.Collapsed;
            _barcodePrinterList = Facade.BarcodePrinterBL()
                .GetAll()
                .OrderBy(x => x.PrinterName)
                .ToList();
            RaisePropertyChangedEvent(nameof(IsValid));
            RaisePropertyChangedEvent(nameof(LoginFormVisibility));
            RaisePropertyChangedEvent(nameof(PrinterListVisibility));
            RaisePropertyChangedEvent(nameof(BarcodePrinterList));
        }


        #region Properties
        private string _username;

        public string username
        {
            get { return _username; }
            set { _username = value; }
        }

        private string _password;

        public string password
        {
            get { return _password; }
            set { _password = value; }
        }

        private security _security;

        public security security
        {
            get { return _security; }
            set { _security = value; }
        }

        private ConfirmToPrintInternalBarcode _window;

        public ConfirmToPrintInternalBarcode window
        {
            get { return _window; }
            set { _window = value; }
        }

        private bool _isValid;

        public bool IsValid
        {
            get { return _isValid; }
            set { _isValid = value; }
        }

        private int _printerID;

        public int PrinterID
        {
            get { return _printerID; }
            set { _printerID = value; }
        }

        private Visibility _loginFormVisibility;

        public Visibility LoginFormVisibility
        {
            get { return _loginFormVisibility; }
            set { _loginFormVisibility = value; }
        }

        private Visibility _printerListVisibility;

        public Visibility PrinterListVisibility
        {
            get { return _printerListVisibility; }
            set { _printerListVisibility = value; }
        }

        #endregion



        #region List
        private List<BarcodePrinter> _barcodePrinterList;

        public List<BarcodePrinter> BarcodePrinterList
        {
            get { return _barcodePrinterList; }
            set { _barcodePrinterList = value; }
        }

        #endregion



        #region Command
        private ICommand _submitCommand;

        public ICommand SubmitCommand
        {
            get { return _submitCommand ?? (_submitCommand = new RelayCommand(Submit)); }
            set { _submitCommand = value; }
        }

        private void Submit(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_username))
                {
                    MessageBoxHelper.Warning("โปรดระบุ username");
                    OnFocusRequested(nameof(username));
                }

                if (string.IsNullOrEmpty(_password))
                {
                    MessageBoxHelper.Warning("โปรดระบุ password");
                    OnFocusRequested(nameof(password));
                }

                _security = Facade.securityBL().GetSingle(username);
                if (_security == null)
                    throw new ArgumentException("ไม่พบบัญชีผู้ใช้นี้ในระบบ");

                if (_security.productioncontrol == false)
                    throw new ArgumentException("บัญชีผู้ใช้งานของท่านไม่สามารถเข้าถึงฟังก์ชันนี้ได้ (productioncontrol = true)");

                var encode = Facade.securityBL().DecodePassword(_security.pwd);
                if (encode != _password)
                    throw new ArgumentException("password ไม่ถูกต้อง");

                _isValid = true;
                _loginFormVisibility = _isValid == true ? Visibility.Collapsed : Visibility.Visible;
                _printerListVisibility = _isValid == true ? Visibility.Visible : Visibility.Collapsed;
                RaisePropertyChangedEvent(nameof(IsValid));
                RaisePropertyChangedEvent(nameof(LoginFormVisibility));
                RaisePropertyChangedEvent(nameof(PrinterListVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _clearCommand;

        public ICommand ClearCommand
        {
            get { return _clearCommand ?? (_clearCommand = new RelayCommand(Clear)); }
            set { _clearCommand = value; }
        }

        private void Clear(object obj)
        {
            _username = null;
            _password = null;
            RaisePropertyChangedEvent(nameof(username));
            RaisePropertyChangedEvent(nameof(password));
            OnFocusRequested(nameof(username));
        }

        private ICommand _closeCommand;

        public ICommand CloseCommand
        {
            get { return _closeCommand ?? (_closeCommand = new RelayCommand(Close)); }
            set { _closeCommand = value; }
        }

        private void Close(object obj)
        {
            try
            {
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _confirmPrinterCommand;

        public ICommand ConfirmPrinterCommand
        {
            get { return _confirmPrinterCommand ?? (_confirmPrinterCommand = new RelayCommand(ConfirmPrinter)); }
            set { _confirmPrinterCommand = value; }
        }

        private void ConfirmPrinter(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการใช้เครื่องพิมพ์บาร์โค้ตนี้เพื่อพิมพ์ internal barcode ใช่หรือไม่?")
                    == MessageBoxResult.No)
                    return;

                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _passwordChangedCommand;

        public ICommand PasswordChangedCommand
        {
            get { return _passwordChangedCommand ?? (_passwordChangedCommand = new RelayCommand(PasswordChanged)); }
            set { _passwordChangedCommand = value; }
        }

        private void PasswordChanged(object obj)
        {
            _password = ((System.Windows.Controls.PasswordBox)obj).Password;
        }

        #endregion



        #region Function

        #endregion
    }
}
