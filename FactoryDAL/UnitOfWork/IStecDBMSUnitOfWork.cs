﻿using FactoryDAL;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryDAL.UnitOfWork
{
    public interface IStecDBMSUnitOfWork
    {
        IGenericDataRepository<BarcodePrinter> barcodePrinterRepo { get; }
        IGenericDataRepository<BarcodeTemplate> barcodeTemplateRepo { get; }
        IGenericDataRepository<BarcodeSetting> barcodeSettingRepo { get; }
        IGenericDataRepository<blendingstatu> blendingstatusRepo { get; }
        IGenericDataRepository<classify> classifyRepo { get; }
        IGenericDataRepository<company> companyRepo { get; }
        IGenericDataRepository<expert> expertRepo { get; }
        IGenericDataRepository<mat> matRepo { get; }
        IGenericDataRepository<mati> matisRepo { get; }
        IGenericDataRepository<matisno> matisnoRepo { get; }
        IGenericDataRepository<matrc> matrcRepo { get; }
        IGenericDataRepository<matrcno> matrcnoRepo { get; }
        IGenericDataRepository<matrgno> matrgnoRepo { get; }
        IGenericDataRepository<matwh> matwhRepo { get; }
        IGenericDataRepository<packedgrade> packedgradeRepo { get; }
        IGenericDataRepository<packingstatu> packingstatusRepo { get; }
        IGenericDataRepository<pd> pdRepo { get; }
        IGenericDataRepository<pdbc> pdbcRepo { get; }
        IGenericDataRepository<PDCustomer> pdCustomeRepo { get; }
        IGenericDataRepository<PDCustomerPackedPrint> pdCusPackedPrintRepo { get; }
        IGenericDataRepository<pdsetup> pdsetupRepo { get; }
        IGenericDataRepository<security> securityRepo { get; }
        IGenericDataRepository<sfsetup> sfsetupRepo { get; }
        IGenericDataRepository<ShippingMovement> shippingMovementRepo { get; }
        IGenericDataRepository<subtype> subtypeRepo { get; }
        IGenericDataRepository<supplier> supplierRepo { get; }
        IGenericDataRepository<type> typeRepo { get; }
        IGenericDataRepository<pdsp> pdspRepo { get; }
        IGenericDataRepository<ShippingOrder> shippingOrderRepo { get; }
        IGenericDataRepository<ShippingTruckDriver> ShippingTruckDriverRepo { get; }
        IGenericDataRepository<ShippingTruck> ShippingTruckRepo { get; }
        IGenericDataRepository<ShippingLocation> ShippingLocationRepo { get; }
        IGenericDataRepository<ShippingMovementTransaction> ShippingMovementTransactionRepo { get; }
        void Save();
    }
}
