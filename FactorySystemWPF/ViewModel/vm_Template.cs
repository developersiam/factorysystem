﻿using FactorySystemWPF.Helper;
using FactorySystemWPF.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FactorySystemWPF.ViewModel
{
    public class vm_Template : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_Template()
        {
        }


        #region Properties

        #endregion



        #region List

        #endregion



        #region Command
        private ICommand _command;

        public ICommand Command
        {
            get { return _command ?? (_command = new RelayCommand(CommandMethod)); }
            set { _command = value; }
        }

        private void CommandMethod(object obj)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion



        #region Function

        #endregion
    }
}
