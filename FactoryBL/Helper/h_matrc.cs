﻿using FactoryBL.Model;
using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.Helper
{
    public static class h_matrc
    {
        public static List<m_matrc> GetByType(int crop, string type, string rcfrom)
        {
            try
            {
                using (var uow = new StecDBMSUnitOfWork())
                {
                    var matGroupList = uow.matRepo
                        .Get(x => x.crop == crop &&
                        x.type == type &&
                        x.rcfrom == rcfrom)
                        .GroupBy(x => x.rcno)
                        .Select(x => new
                        {
                            rcno = x.Key,
                            total = x.Count()
                        }).ToList();

                    var matrcList = uow.matrcRepo
                        .Get(x => x.crop == crop &&
                        x.type == type &&
                        x.rcfrom == rcfrom);

                    var result = (from a in matrcList
                                  join b in matGroupList
                                  on a.rcno equals b.rcno into c
                                  from r in c.DefaultIfEmpty()
                                  select new m_matrc
                                  {
                                      rgno = a.rgno,
                                      hsno = a.hsno,
                                      tfno = a.tfno,
                                      rcno = a.rcno,
                                      crop = a.crop,
                                      type = a.type,
                                      date = a.date,
                                      place = a.place,
                                      truckno = a.truckno,
                                      buyer = a.buyer,
                                      classifier = a.classifier,
                                      starttime = a.starttime,
                                      finishtime = a.finishtime,
                                      userlocked = a.userlocked,
                                      userlockedtime = a.userlockedtime,
                                      xlocked = a.xlocked,
                                      xlockedtimes = a.xlockedtimes,
                                      machine = a.machine,
                                      rcfrom = a.rcfrom,
                                      remark = a.remark,
                                      dtrecord = a.dtrecord,
                                      user = a.user,
                                      checker = a.checker,
                                      leafuser = a.leafuser,
                                      checkerapproved = a.checkerapproved,
                                      checkerapproveddate = a.checkerapproveddate,
                                      checkerapprovedtime = a.checkerapprovedtime,
                                      leaflocked = a.leaflocked,
                                      stem = a.stem,
                                      labours = a.labours,
                                      hsmaingrade = a.hsmaingrade,
                                      laboursall = a.laboursall,
                                      labourscollect = a.labourscollect,
                                      laboursrg = a.laboursrg,
                                      TransportationDocumentCode = a.TransportationDocumentCode,
                                      RemarkCancleRcNo = a.RemarkCancleRcNo,
                                      CancleRcNoStatus = a.CancleRcNoStatus,
                                      CancleRcNoDate = a.CancleRcNoDate,
                                      InvoiceNo = a.InvoiceNo,
                                      PriceID = a.PriceID,
                                      TotalBale = c == null ? 0 : c.Sum(y => y.total),
                                      LockedStatus = a.userlocked == true ? "Locked" : "UnLocked"
                                  }).ToList();
                    return result;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
