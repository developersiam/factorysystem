﻿using FactoryBL;
using FactoryBL.Model;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace FactorySystemWPF.ViewModel.Processing.ProductionControl
{
    public class vm_ByProductSetup : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_ByProductSetup()
        {
        }



        #region Properties
        private m_pdsetup _pdsetup;

        public m_pdsetup pdsetup
        {
            get { return _pdsetup; }
            set
            {
                _pdsetup = value;
                StemFindListBinding();
                StemFineSetupBinding();
            }
        }

        private sfsetup _sfsetup;

        private string _stlGrade;

        public string STLGrade
        {
            get { return _stlGrade; }
            set
            {
                _stlGrade = value;
                _sfsetup.slgrade = value;
                _stl = _packedgradeList.SingleOrDefault(x => x.packedgrade1 == value);
                RaisePropertyChangedEvent(nameof(STLGrade));
                RaisePropertyChangedEvent(nameof(STL));
                RaisePropertyChangedEvent(nameof(sfsetup));
            }
        }

        private string _stl2Grade;

        public string STL2Grade
        {
            get { return _stl2Grade; }
            set
            {
                _stl2Grade = value;
                _sfsetup.sl2grade = value;
                _stl2 = _packedgradeList.SingleOrDefault(x => x.packedgrade1 == value);
                RaisePropertyChangedEvent(nameof(STL2Grade));
                RaisePropertyChangedEvent(nameof(STL2));
                RaisePropertyChangedEvent(nameof(sfsetup));
            }
        }

        private string _stsGrade;

        public string STSGrade
        {
            get { return _stsGrade; }
            set
            {
                _stsGrade = value;
                _sfsetup.ssgrade = value;
                _sts = _packedgradeList.SingleOrDefault(x => x.packedgrade1 == value);
                RaisePropertyChangedEvent(nameof(STSGrade));
                RaisePropertyChangedEvent(nameof(STS));
                RaisePropertyChangedEvent(nameof(sfsetup));
            }
        }

        private string _flGrade;

        public string FLGrade
        {
            get { return _flGrade; }
            set
            {
                _flGrade = value;
                _sfsetup.flgrade = value;
                _fl = _packedgradeList.SingleOrDefault(x => x.packedgrade1 == value);
                RaisePropertyChangedEvent(nameof(FLGrade));
                RaisePropertyChangedEvent(nameof(FL));
                RaisePropertyChangedEvent(nameof(sfsetup));
            }
        }

        private string _fsGrade;

        public string FSGrade
        {
            get { return _fsGrade; }
            set
            {
                _fsGrade = value;
                _sfsetup.fsgrade = value;
                _fs = _packedgradeList.SingleOrDefault(x => x.packedgrade1 == value);
                RaisePropertyChangedEvent(nameof(FSGrade));
                RaisePropertyChangedEvent(nameof(FS));
                RaisePropertyChangedEvent(nameof(sfsetup));
            }
        }

        public sfsetup sfsetup
        {
            get { return _sfsetup; }
            set { _sfsetup = value; }
        }

        private packedgrade _stl;

        public packedgrade STL
        {
            get { return _stl; }
            set { _stl = value; }
        }

        private packedgrade _stl2;

        public packedgrade STL2
        {
            get { return _stl2; }
            set { _stl2 = value; }
        }

        private packedgrade _sts;

        public packedgrade STS
        {
            get { return _sts; }
            set { _sts = value; }
        }

        private packedgrade _fl;

        public packedgrade FL
        {
            get { return _fl; }
            set { _fl = value; }
        }

        private packedgrade _fs;

        public packedgrade FS
        {
            get { return _fs; }
            set { _fs = value; }
        }
        #endregion



        #region List
        private List<packedgrade> _packedgradeList;

        public List<packedgrade> packedgradeList
        {
            get { return _packedgradeList; }
            set { _packedgradeList = value; }
        }

        private List<packedgrade> _stlList;

        public List<packedgrade> STLList
        {
            get { return _stlList; }
            set { _stlList = value; }
        }

        private List<packedgrade> _stl2List;

        public List<packedgrade> STL2List
        {
            get { return _stl2List; }
            set { _stl2List = value; }
        }

        private List<packedgrade> _stsList;

        public List<packedgrade> STSList
        {
            get { return _stsList; }
            set { _stsList = value; }
        }

        private List<packedgrade> _flList;

        public List<packedgrade> FLList
        {
            get { return _flList; }
            set { _flList = value; }
        }

        private List<packedgrade> _fsList;

        public List<packedgrade> FSList
        {
            get { return _fsList; }
            set { _fsList = value; }
        }
        #endregion



        #region Command
        private ICommand _onSTLClearCommand;

        public ICommand OnSTLClearCommand
        {
            get { return _onSTLClearCommand ?? (_onSTLClearCommand = new RelayCommand(STLClear)); }
            set { _onSTLClearCommand = value; }
        }

        private void STLClear(object obj)
        {
            _stl = null;
            _stlGrade = null;
            _sfsetup.slgrade = null;
            RaisePropertyChangedEvent(nameof(STL));
            RaisePropertyChangedEvent(nameof(STLGrade));
            RaisePropertyChangedEvent(nameof(sfsetup));
        }

        private ICommand _onSTL2ClearCommand;

        public ICommand OnSTL2ClearCommand
        {
            get { return _onSTL2ClearCommand ?? (_onSTL2ClearCommand = new RelayCommand(STL2Clear)); }
            set { _onSTL2ClearCommand = value; }
        }

        private void STL2Clear(object obj)
        {
            _stl2 = null;
            _stl2Grade = null;
            _sfsetup.slgrade = null;
            RaisePropertyChangedEvent(nameof(STL2));
            RaisePropertyChangedEvent(nameof(STL2Grade));
            RaisePropertyChangedEvent(nameof(sfsetup));
        }

        private ICommand _onSTSClearCommand;

        public ICommand OnSTSClearCommand
        {
            get { return _onSTSClearCommand ?? (_onSTSClearCommand = new RelayCommand(STSClear)); }
            set { _onSTSClearCommand = value; }
        }

        private void STSClear(object obj)
        {
            _sts = null;
            _stsGrade = null;
            _sfsetup.ssgrade = null;
            RaisePropertyChangedEvent(nameof(STS));
            RaisePropertyChangedEvent(nameof(STSGrade));
            RaisePropertyChangedEvent(nameof(sfsetup));
        }

        private ICommand _onFLClearCommand;

        public ICommand OnFLClearCommand
        {
            get { return _onFLClearCommand ?? (_onFLClearCommand = new RelayCommand(FLClear)); }
            set { _onFLClearCommand = value; }
        }

        private void FLClear(object obj)
        {
            _fl = null;
            _flGrade = null;
            _sfsetup.flgrade = null;
            RaisePropertyChangedEvent(nameof(FL));
            RaisePropertyChangedEvent(nameof(FLGrade));
            RaisePropertyChangedEvent(nameof(sfsetup));
        }

        private ICommand _onFSClearCommand;

        public ICommand OnFSClearCommand
        {
            get { return _onFSClearCommand ?? (_onFSClearCommand = new RelayCommand(FSClear)); }
            set { _onFSClearCommand = value; }
        }

        private void FSClear(object obj)
        {
            _fs = null;
            _fsGrade = null;
            _sfsetup.fsgrade = null;
            RaisePropertyChangedEvent(nameof(FS));
            RaisePropertyChangedEvent(nameof(FSGrade));
            RaisePropertyChangedEvent(nameof(sfsetup));
        }

        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(Save)); }
            set { _onSaveCommand = value; }
        }

        private void Save(object obj)
        {
            if (_sfsetup == null)
                throw new ArgumentException("sfsetup not define.");

            if (MessageBoxHelper.Question("การบันทึกข้อมูลนี้อาจส่งผลต่อการบันทึกข้อมูลของโปรแกรม ByProduct .+" +
                ".ที่กำลังทำงานอยู่ท่านแน่ใจที่จะบันทึกข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                return;

            Facade.sfsetupBL().Save(_sfsetup);
            MessageBoxHelper.Info("ตั้งค่า stem/fine เรียบร้อย");
        }
        #endregion



        #region Function
        private void StemFindListBinding()
        {
            try
            {
                _packedgradeList = Facade.packedgradeBL()
                    .GetByCrop(DateTime.Now.Year)
                    .Where(x => x.type == _pdsetup.type)
                    .ToList();

                _stlList = _packedgradeList.Where(x => x.form.Contains("STL")).ToList();
                _stl2List = _packedgradeList.Where(x => x.form.Contains("STM")).ToList();
                _stsList = _packedgradeList.Where(x => x.form.Contains("STS")).ToList();
                _flList = _packedgradeList.Where(x => x.form.Contains("#")).ToList();
                _fsList = _packedgradeList.Where(x => x.form.Contains("#")).ToList();

                RaisePropertyChangedEvent(nameof(STLList));
                RaisePropertyChangedEvent(nameof(STL2List));
                RaisePropertyChangedEvent(nameof(STSList));
                RaisePropertyChangedEvent(nameof(FLList)); ;
                RaisePropertyChangedEvent(nameof(FSList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void StemFineSetupBinding()
        {
            try
            {
                _sfsetup = Facade.sfsetupBL().GetSingle();
                if (_sfsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล sfsetup ในระบบ โปรดแจ้งแผนกไอทีเพื่อทำการตรวจสอบ");

                StemFindListBinding();

                _stl = _stlList.SingleOrDefault(x => x.packedgrade1 == _sfsetup.slgrade);
                _stl2 = _stl2List.SingleOrDefault(x => x.packedgrade1 == _sfsetup.sl2grade);
                _sts = _stsList.SingleOrDefault(x => x.packedgrade1 == _sfsetup.ssgrade);
                _fl = _flList.SingleOrDefault(x => x.packedgrade1 == _sfsetup.flgrade);
                _fs = _fsList.SingleOrDefault(x => x.packedgrade1 == _sfsetup.fsgrade);

                _stlGrade = _sfsetup.slgrade;
                _stl2Grade = _sfsetup.slgrade;
                _stsGrade = _sfsetup.ssgrade;
                _flGrade = _sfsetup.flgrade;
                _fsGrade = _sfsetup.fsgrade;

                RaisePropertyChangedEvent(nameof(sfsetup));
                RaisePropertyChangedEvent(nameof(STL));
                RaisePropertyChangedEvent(nameof(STS));
                RaisePropertyChangedEvent(nameof(FL));
                RaisePropertyChangedEvent(nameof(FS));
                RaisePropertyChangedEvent(nameof(STLGrade));
                RaisePropertyChangedEvent(nameof(STL2Grade));
                RaisePropertyChangedEvent(nameof(STSGrade));
                RaisePropertyChangedEvent(nameof(FLGrade));
                RaisePropertyChangedEvent(nameof(FSGrade));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
