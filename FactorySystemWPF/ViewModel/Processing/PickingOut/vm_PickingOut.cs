﻿using FactoryBL;
using FactoryBL.Model;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.Model;
using FactorySystemWPF.MVVM;
using FactorySystemWPF.View.Processing.PickingOut;
using FactorySystemWPF.View.Shared;
using FactorySystemWPF.ViewModel.Processing.PickingOut;
using FactorySystemWPF.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace FactorySystemWPF.ViewModel.Processing.PickingOut
{
    public class vm_PickingOut : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_PickingOut()
        {
            try
            {
                _classifierList = Facade.expertBL().GetAll();
                _companyList = Facade.companyBL().GetAll();
                _typeList = Facade.typeBL().GetAll();
                _subtypeList = Facade.subtypeBL().GetAll();
                _productionDate = DateTime.Now.Date;
                _crop = DateTime.Now.Year;

                RaisePropertyChangedEvent(nameof(classifierList));
                RaisePropertyChangedEvent(nameof(companyList));
                RaisePropertyChangedEvent(nameof(subtypeList));
                RaisePropertyChangedEvent(nameof(typeList));
                RaisePropertyChangedEvent(nameof(ProductionDate));

                _deleteEnabled = true;
                _saveEnabled = true;
                _reprintEnabled = true;
                _senddataEnabled = true;

                ResetEnabledButton();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }

        }

        #region Properties
        private matrc _matrc;

        public matrc matrc
        {
            get { return _matrc; }
            set { _matrc = value; }
        }

        private string _documentStatus;

        public string DocumentStatus
        {
            get { return _documentStatus; }
            set { _documentStatus = value; }
        }

        private Brush _documentStatusBGColor;

        public Brush DocumentStatusBGColor
        {
            get { return _documentStatusBGColor; }
            set { _documentStatusBGColor = value; }
        }

        private int _totalBale;

        public int TotalBale
        {
            get { return _totalBale; }
            set { _totalBale = value; }
        }

        private decimal _totalWeight;

        public decimal TotalWeight
        {
            get { return _totalWeight; }
            set { _totalWeight = value; }
        }

        private mati _matis;

        public mati matis
        {
            get { return _matis; }
            set { _matis = value; }
        }

        private int _crop;

        public int crop
        {
            get { return _crop; }
            set { _crop = value; }
        }

        private string _subtype;

        public string subtype
        {
            get { return _subtype; }
            set { _subtype = value; }
        }

        private string _company;

        public string company
        {
            get { return _company; }
            set { _company = value; }
        }

        private string _companyName;

        public string companyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        private string _place;

        public string place
        {
            get { return _place; }
            set { _place = value; }
        }

        private string _classifier;

        public string classifier
        {
            get { return _classifier; }
            set { _classifier = value; }
        }

        private string _remark;

        public string remark
        {
            get { return _remark; }
            set { _remark = value; }
        }

        private mat _matInput;

        public mat matInput
        {
            get { return _matInput; }
            set
            {
                _matInput = value;
                if (_matInput == null)
                    _deleteEnabled = false;
                else
                    _deleteEnabled = true;
                RaisePropertyChangedEvent(nameof(DeleteEnabled));
            }
        }


        private bool _deleteEnabled;

        public bool DeleteEnabled
        {
            get { return _deleteEnabled; }
            set { _deleteEnabled = value; }
        }

        private bool _saveEnabled;

        public bool SaveEnabled
        {
            get { return _saveEnabled; }
            set { _saveEnabled = value; }
        }

        private bool _reprintEnabled;

        public bool ReprintEnabled
        {
            get { return _reprintEnabled; }
            set { _reprintEnabled = value; }
        }

        private bool _finishEnabled;

        public bool FinishEnabled
        {
            get { return _finishEnabled; }
            set { _finishEnabled = value; }
        }

        private bool _senddataEnabled;

        public bool SendDataEnabled
        {
            get { return _senddataEnabled; }
            set { _senddataEnabled = value; }
        }

        private bool _headerEnabled;

        public bool HeaderEnabled
        {
            get { return _headerEnabled; }
            set { _headerEnabled = value; }
        }


        private bool _autoSave;

        public bool autoSave
        {
            get { return _autoSave; }
            set { _autoSave = value; }
        }

        private bool _picking;

        public bool picking
        {
            get { return _picking; }
            set { _picking = value; }
        }

        private bool _brazilianChk;
        public bool brazilianChk
        {
            get { return _brazilianChk; }
            set { _brazilianChk = value; }
        }

        private string _bc;

        public string bc
        {
            get { return _bc; }
            set { _bc = value; }
        }

        private string _classify;

        public string classify
        {
            get { return _classify; }
            set { _classify = value; }
        }
        private string _packedgrade;

        public string packedgrade
        {
            get { return _packedgrade; }
            set { _packedgrade = value; }
        }

        private decimal _weight;

        public decimal weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        private int _baleno;

        public int baleno
        {
            get { return _baleno; }
            set { _baleno = value; }
        }

        private string _mark;

        public string mark
        {
            get { return _mark; }
            set { _mark = value; }
        }

        private decimal _totalBuyingWeight;

        public decimal TotalBuyingWeight
        {
            get { return _totalBuyingWeight; }
            set { _totalBuyingWeight = value; }
        }

        private decimal _totalReceiveWeight;

        public decimal TotalReceiveWeight
        {
            get { return _totalReceiveWeight; }
            set { _totalReceiveWeight = value; }
        }

        private DateTime _productionDate;

        public DateTime ProductionDate
        {
            get { return _productionDate; }
            set
            {
                _productionDate = value;
                _pdnoList = Facade.pdsetupBL()
                        .GetByDate(_productionDate)
                        .OrderBy(x => x.pdno)
                        .ToList();

                RaisePropertyChangedEvent(nameof(pdnoList));
            }
        }
        private string _pdno;
        public string pdno
        {
            get { return _pdno; }
            set
            {
                _pdno = value;

                if (_pdno != null)
                    DataProductionBlinding();
            }
        }

        private string _type;

        public string type
        {
            get { return _type; }
            set
            {
                _type = value;
                if (_type != null)
                    _classifyList = Facade.pickingBL().GetbyType(_type);

                RaisePropertyChangedEvent(nameof(classifyList));
            }
        }

        private string _form;
        public string form
        {
            get { return _form; }
            set { _form = value; }
        }

        private bool _userlocked;
        public bool userlocked
        {
            get { return _userlocked; }
            set { _userlocked = value; }
        }

        private decimal _price;
        private decimal _unitprice;


        #endregion

        #region List
        private List<expert> _classifierList;

        public List<expert> classifierList
        {
            get { return _classifierList; }
            set { _classifierList = value; }
        }

        private List<picking> _classifyList;

        public List<picking> classifyList
        {
            get { return _classifyList; }
            set {
                _classifyList = value;
            }
        }

        private List<mat> _matList;

        public List<mat> matList
        {
            get { return _matList; }
            set { _matList = value; }
        }

        private List<company> _companyList;

        public List<company> companyList
        {
            get { return _companyList; }
            set { _companyList = value; }
        }

        private List<type> _typeList;

        public List<type> typeList
        {
            get { return _typeList; }
            set { _typeList = value; }
        }

        private List<subtype> _subtypeList;

        public List<subtype> subtypeList
        {
            get { return _subtypeList; }
            set { _subtypeList = value; }
        }
        private List<mat> _pickingList;

        public List<mat> pickingList
        {
            get { return _pickingList; }
            set { _pickingList = value; }
        }

        private List<pdsetup> _pdnoList;
        public List<pdsetup> pdnoList
        {
            get { return _pdnoList; }
            set { _pdnoList = value; }
        }
        private bool _printX2;

        public bool PrintX2
        {
            get { return _printX2; }
            set { _printX2 = value; }
        }

        private bool _Print;

        public bool Print
        {
            get { return _Print; }
            set
            {
                _Print = value;
                if (_Print == true)
                    _printX2 = false;
                else
                    _printX2 = true;
                RaisePropertyChangedEvent(nameof(PrintX2));
            }
        }


        private bool _useDigitalScale;

        public bool UseDigitalScale
        {
            get { return _useDigitalScale; }
            set
            {
                _useDigitalScale = value;

                if (_useDigitalScale == true)
                {
                    if (DigitalScaleHelper.CheckPort() == false)
                    {
                        _useDigitalScale = false;
                        MessageBoxHelper.Warning("ไม่พบ port เครื่องชั่งที่เชื่อมต่ออยู่กับเครื่องคอมพิวเตอร์");
                    }
                    else
                        _useDigitalScale = true;
                }
            }
        }
        #endregion


        #region Command
        private ICommand _newCommand;

        public ICommand NewCommand
        {
            get { return _newCommand ?? (_newCommand = new RelayCommand(New)); }
            set { _newCommand = value; }
        }

        private void New(object obj)
        {
            try
            {
                var window = new SelectType();
                var vm = new vm_SelectType();
                window.DataContext = vm;
                vm.Window = window;
                window.ShowDialog();
                if (string.IsNullOrEmpty(vm.type))
                    throw new ArgumentException("โปรดระบุ type");

                _type = vm.type;
                //_subtype = Facade.subtypeBL().GetAll().Select(x => x.subtype1);
                _productionDate = DateTime.Now;
                _crop = vm.crop;
                _classifyList = Facade.pickingBL().GetbyType(_type);
                _matrc = new matrc();
                _matrc.date = DateTime.Now;
                _matrc.rcfrom = "Blending";
                _matrc.starttime = DateTime.Now;
                _matrc.type = vm.type;
                _matis = new mati();
                _company = null;
                _companyName = null;
                _totalBale = 0;
                _totalWeight = (decimal)0.0;
                _place = null;
                _classifier = null;
                _remark = null;
                _matList = null;
                _totalBale = 0;
                _totalBuyingWeight = (decimal)0.0;
                _totalReceiveWeight = (decimal)0.0;
                _totalWeight = (decimal)0.0;
                _headerEnabled = true;
                _pickingList = null;


                _deleteEnabled = true;
                _saveEnabled = true;
                _reprintEnabled = true;
                _senddataEnabled = true;

                RaisePropertyChangedEvent(nameof(type));
                RaisePropertyChangedEvent(nameof(subtype));
                RaisePropertyChangedEvent(nameof(crop));
                RaisePropertyChangedEvent(nameof(matrc));
                RaisePropertyChangedEvent(nameof(matis));
                RaisePropertyChangedEvent(nameof(company));
                RaisePropertyChangedEvent(nameof(companyName));
                RaisePropertyChangedEvent(nameof(TotalBale));
                RaisePropertyChangedEvent(nameof(TotalWeight));
                RaisePropertyChangedEvent(nameof(place));
                //RaisePropertyChangedEvent(nameof(classifier));
                RaisePropertyChangedEvent(nameof(classifyList));
                RaisePropertyChangedEvent(nameof(remark));
                RaisePropertyChangedEvent(nameof(matList));
                RaisePropertyChangedEvent(nameof(TotalBale));
                RaisePropertyChangedEvent(nameof(TotalBuyingWeight));
                RaisePropertyChangedEvent(nameof(TotalReceiveWeight));
                RaisePropertyChangedEvent(nameof(TotalWeight));
                RaisePropertyChangedEvent(nameof(HeaderEnabled));
                RaisePropertyChangedEvent(nameof(ProductionDate));
                RaisePropertyChangedEvent(nameof(pickingList));


                ClearInputForm();
                ResetEnabledButton();
                OnFocusRequested(nameof(ProductionDate));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _callDataCommand;

        public ICommand CallDataCommand
        {
            get { return _callDataCommand ?? (_callDataCommand = new RelayCommand(CallData)); }
            set { _callDataCommand = value; }
        }

        private void CallData(object obj)
        {
            CallData();
        }

        public void CallData()
        {
            try
            {
                var window = new CallData();
                var vm = new vm_CallData();
                window.DataContext = vm;
                vm.window = window;
                window.ShowDialog();
                if (vm.selectedRow == null)
                    throw new ArgumentException("ท่านไม่ได้เลือก Picking Out no ที่จะแสดงผลในหน้าจอนี้");


                ClearInputForm();

                _matrc = new matrc();
                _matrc.rcno = vm.selectedRow.rcno;
                _matrc.rcfrom = vm.selectedRow.rcfrom;
                _matrc.type = vm.type;

                HeaderBinding();
                InputDetailsBinding();
                ResetEnabledButton();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        private ICommand _finishCommand;

        public ICommand FinishCommand
        {
            get { return _finishCommand ?? (_finishCommand = new RelayCommand(Finish)); }
            set { _finishCommand = value; }
        }

        private void Finish(object obj)
        {
            try
            {
                if (matrc.rcno == null) return;
                //update matrc
                Facade.matrcBL().FinishPicking(_matrc.rcno, user_setting.security.uname);


                _finishEnabled = false;
                RaisePropertyChangedEvent(nameof(FinishEnabled));
            }
            catch(Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }          
        }

        private ICommand _reloadCommand;

        public ICommand ReloadCommand
        {
            get { return _reloadCommand ?? (_reloadCommand = new RelayCommand(Reload)); }
            set { _reloadCommand = value; }
        }

        private void Reload(object obj)
        {
            if (_matrc == null)
                return;

            HeaderBinding();
            InputDetailsBinding();
        }

        private ICommand _addCommand;

        public ICommand AddCommand
        {
            get { return _addCommand ?? (_addCommand = new RelayCommand(AddInput)); }
            set { _addCommand = value; }
        }

        private void AddInput(object obj)
        {
             AddPicking();
        }

        private void AddPicking()
        {
            try
            {
                if (_matrc == null)
                    throw new ArgumentException("โปรดกด New เพื่อเริ่มต้นการทำงาน");

                if (string.IsNullOrEmpty(_classifier))
                    throw new ArgumentException("โปรดระบุ Classifier");
                if (string.IsNullOrEmpty(_type))
                    throw new ArgumentException("โปรดระบุ Type");
                if (string.IsNullOrEmpty(_pdno))
                    throw new ArgumentException("โปรดระบุ Production No.");
                if (string.IsNullOrEmpty(_subtype))
                    throw new ArgumentException("โปรดระบุ Subtype");

                //check subtype 
                if (_type == "BU" && (_subtype == "F" || _subtype == "OR" || _subtype == "TM"))
                    throw new ArgumentException("โปรดระบุ Subtype ให้ถูกต้องกับ Type");
                if (_type == "FC" && (_subtype == "BE" || _subtype == "BM" || _subtype == "OR" || _subtype == "TM"))
                    throw new ArgumentException("โปรดระบุ Subtype ให้ถูกต้องกับ Type");
                if (_type == "OR" && (_subtype == "BE" || _subtype == "BM" || _subtype == "F" || _subtype == "TM"))
                    throw new ArgumentException("โปรดระบุ Subtype ให้ถูกต้องกับ Type");
                if (_type == "TM" && (_subtype == "BE" || _subtype == "BM" || _subtype == "F" || _subtype == "OR"))
                    throw new ArgumentException("โปรดระบุ Subtype ให้ถูกต้องกับ Type");


                _bc = "";
                if (_Print) _bc = Facade.matBL().GetPickingBarcode((short)_crop, _type);            
                _classify = "";
                _weight = (decimal)0;
                _matInput = null;
                _remark = "";
                _saveEnabled = true;
                _deleteEnabled = false;
                _baleno = 0;
                _price = 0;
                _unitprice = 0;

                RaisePropertyChangedEvent(nameof(bc));
                RaisePropertyChangedEvent(nameof(baleno));
                RaisePropertyChangedEvent(nameof(classifyList));
                RaisePropertyChangedEvent(nameof(classify));
                RaisePropertyChangedEvent(nameof(weight));
                RaisePropertyChangedEvent(nameof(matInput));
                RaisePropertyChangedEvent(nameof(remark));
                RaisePropertyChangedEvent(nameof(SaveEnabled));
                RaisePropertyChangedEvent(nameof(DeleteEnabled));
                OnFocusRequested(nameof(bc));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }

        }

        private ICommand _deleteCommand;

        public ICommand DeleteCommand
        {
            get { return _deleteCommand ?? (_deleteCommand = new RelayCommand(DeleteInput)); }
            set { _deleteCommand = value; }
        }

        private void DeleteInput(object obj)
        {
            DeleteInput();
        }


        private ICommand _reprintCommand;

        public ICommand ReprintCommand
        {
            get { return _reprintCommand ?? (_reprintCommand = new RelayCommand(Reprint)); }
            set { _reprintCommand = value; }
        }

        private void Reprint(object obj)
        {
            try
            {
                ///Reprint function.
                _reprintEnabled = false;
                RaisePropertyChangedEvent(nameof(ReprintEnabled));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _sendDataCommand;

        public ICommand SendDataCommand
        {
            get { return _sendDataCommand ?? (_sendDataCommand = new RelayCommand(SendData)); }
            set { _sendDataCommand = value; }
        }

        private void SendData(object obj)
        {

            List<mat> ex = Facade.matBL().GetByRcno(_matrc.rcno).ToList();
            if (ex.Count() <= 0)
                throw new ArgumentException("Picking เบอร์นี้ ยังไม่มีห่อยา, ไม่สามารถส่งข้อมูลได้!!");

            if (MessageBoxHelper.Question("คุณตรวจสอบข้อมูล ถูกต้อง ครบถ้วนแล้ว ใช่่หรือไม่, " + Environment.NewLine + 
                                           "หลังจากส่งข้อมูลแล้ว คุณไม่สามารถแก้ไขข้อมูลยา Picking ชุดนี้ได้อีก" + Environment.NewLine + 
                                           "แต่หากพบข้อผิดพลาดภายหลัง กรุณาแจ้งแผนกบัญชีเพื่อปลดล็อกแล้วทำการแก้ไขภายหลัง")
                        == MessageBoxResult.No) return;

            //update matrc
            Facade.matrcBL().FinishPicking(_matrc.rcno, user_setting.security.uname);
            Facade.matrcBL().UserLockedPicking(_matrc.rcno);

            //update mat
            Facade.matBL().UpdateDocno(_crop, _matrc.rcno, "BD", _subtype, _company);
            Facade.matBL().UpdateCheckerLocked(_matrc.rcno, user_setting.security.uname);

            HeaderBinding();
            InputDetailsBinding();

            MessageBoxHelper.Info("ส่งข้อมูลเรียบร้อยแล้ว");
        }

        private ICommand _f1Command;

        public ICommand F1Command
        {
            get { return _f1Command ?? (_f1Command = new RelayCommand(F1)); }
            set { _f1Command = value; }
        }

        private void F1(object obj)
        {
            if (_matrc == null)
                return;

            HeaderBinding();
            InputDetailsBinding();
        }

        private ICommand _f2Command;

        public ICommand F2Command
        {
            get { return _f2Command ?? (_f2Command = new RelayCommand(F2)); }
            set { _f2Command = value; }
        }

        private void F2(object obj)
        {
            CallData();
        }
        private ICommand _f5Command;

        public ICommand F5Command
        {
            get { return _f5Command ?? (_f5Command = new RelayCommand(F5)); }
            set { _f5Command = value; }
        }

        private void F5(object obj)
        {
            ReWeight();
        }

        private ICommand _f9Command;

        public ICommand F9Command
        {
            get { return _f9Command ?? (_f9Command = new RelayCommand(F9)); }
            set { _f9Command = value; }
        }
        private void F9(object obj)
        {
            PrintPicking();
        }

        private ICommand _f8Command;

        public ICommand F8Command
        {
            get { return _f8Command ?? (_f8Command = new RelayCommand(F8)); }
            set { _f8Command = value; }
        }

        private void F8(object obj)
        {
            AddPicking();
        }

        private ICommand _f12Command;

        public ICommand F12Command
        {
            get { return _f12Command ?? (_f12Command = new RelayCommand(F12)); }
            set { _f12Command = value; }
        }

        private void F12(object obj)
        {
            SavePicking();
        }

        private ICommand _barcodeInputEnterCommand;

        public ICommand BarcodeInputEnterCommand
        {
            get { return _barcodeInputEnterCommand ?? (_barcodeInputEnterCommand = new RelayCommand(BarcodeInputEnter)); }
            set { _barcodeInputEnterCommand = value; }
        }

        private void BarcodeInputEnter(object obj)
        {
            try
            {
                //check duplicate
                var existedBC = Facade.matBL().GetSingle(_bc);
                if (existedBC != null)
                    throw new ArgumentException("Barcode นี้มีอยู่ในระบบแล้ว ไม่สามารถใช้ช้ำได้ : " + _bc);

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _selectedRowCommand;

        public ICommand SelectedRowCommand
        {
            get { return _selectedRowCommand ?? (_selectedRowCommand = new RelayCommand(MatInputSelectedRow)); }
            set { _selectedRowCommand = value; }
        }

        private void MatInputSelectedRow(object obj)
        {
            try
            {
                if (obj == null)
                    return;

                _matInput = (mat)obj;
                _bc = _matInput.bc;
                _company = _matInput.company;
                _classify = _matInput.classify;
                _weight = (decimal)_matInput.weightbuy;
                _deleteEnabled = true;
                _baleno = (int)matInput.baleno;

                RaisePropertyChangedEvent(nameof(matInput));
                RaisePropertyChangedEvent(nameof(bc));
                RaisePropertyChangedEvent(nameof(company));
                RaisePropertyChangedEvent(nameof(classifyList));
                RaisePropertyChangedEvent(nameof(classify));
                RaisePropertyChangedEvent(nameof(weight));
                RaisePropertyChangedEvent(nameof(baleno));
                RaisePropertyChangedEvent(nameof(DeleteEnabled));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion

        #region Function
        private void CompanyToCompanyName()
        {
            try
            {
                if (string.IsNullOrEmpty(_company))
                    return;

                var company = _companyList.SingleOrDefault(x => x.code == _company);
                if (company == null)
                    throw new ArgumentException("ไม่พบ company " + _company + " ในระบบ");

                _companyName = company.name;
                RaisePropertyChangedEvent(nameof(companyName));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void HeaderBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_matrc.rcno))
                    throw new ArgumentException("ไม่พบข้อมูล rcno นี้ในระบบ");

                _matrc = Facade.matrcBL().GetSingle(_matrc.rcno);
                if (_matrc == null)
                    throw new ArgumentException("ไม่พบข้อมูล matrc ในระบบ");


                _crop = (int)_matrc.crop;
                _type = _matrc.type;
                _place = _matrc.place;
                _classifier = _matrc.classifier;
                _remark = _matrc.remark;
                _userlocked = (bool)_matrc.userlocked;
                _senddataEnabled = !_userlocked;

                _documentStatus = !_userlocked ? "UnLocked" : "Locked";
                _documentStatusBGColor = !_userlocked ? Brushes.LightGreen : Brushes.Salmon;                   
                _finishEnabled = _matrc.finishtime == null ? true : false;

                if (_senddataEnabled)
                {
                    _deleteEnabled = true;
                    _saveEnabled = true;
                    _reprintEnabled = true;
                }

                RaisePropertyChangedEvent(nameof(matrc));
                RaisePropertyChangedEvent(nameof(crop));
                RaisePropertyChangedEvent(nameof(type));
                RaisePropertyChangedEvent(nameof(place));
                RaisePropertyChangedEvent(nameof(classifier));
                RaisePropertyChangedEvent(nameof(classify));
                RaisePropertyChangedEvent(nameof(remark));
                RaisePropertyChangedEvent(nameof(DocumentStatus));
                RaisePropertyChangedEvent(nameof(DocumentStatusBGColor));
                RaisePropertyChangedEvent(nameof(FinishEnabled));
                RaisePropertyChangedEvent(nameof(SendDataEnabled));

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InputDetailsBinding()
        {
            try
            {
                //if (string.IsNullOrEmpty(_matis.isno))
                //    throw new ArgumentException("ไม่พบข้อมูล isno นี้ในระบบ");

                _pickingList = Facade.matBL().GetByRcno(_matrc.rcno)
                    .OrderByDescending(x => x.issueddate)
                    .ThenByDescending(x => x.issuedtime)
                    .ToList();
                _totalBale = _pickingList.Count();
                _totalWeight = _pickingList.Count() > 0 ?
                    (decimal)_pickingList.Sum(x => x.weightbuy) : (decimal)0.0;
                _totalReceiveWeight = _pickingList.Count() > 0 ?
                    (decimal)_pickingList.Sum(x => x.weight) : (decimal)0.0;
             
                _subtype = _pickingList.Select(x => x.subtype).FirstOrDefault();
                _company = _pickingList.Select(x => x.company).FirstOrDefault();
                _productionDate = _pickingList.Count() > 0 ? (DateTime)_pickingList.Select(x => x.frompddate).FirstOrDefault() : DateTime.Now;
                _pdno = _pickingList.Select(x => x.frompdno).FirstOrDefault();
                _packedgrade = _pickingList.Select(x => x.fromprgrade).FirstOrDefault();


                RaisePropertyChangedEvent(nameof(pickingList));
                RaisePropertyChangedEvent(nameof(TotalBale));
                RaisePropertyChangedEvent(nameof(TotalWeight));
                RaisePropertyChangedEvent(nameof(TotalBuyingWeight));
                RaisePropertyChangedEvent(nameof(TotalReceiveWeight));
                RaisePropertyChangedEvent(nameof(ProductionDate));
                RaisePropertyChangedEvent(nameof(pdnoList));
                RaisePropertyChangedEvent(nameof(pdno));
                RaisePropertyChangedEvent(nameof(subtype));
                RaisePropertyChangedEvent(nameof(packedgrade));

                if (_pickingList.Count() > 0)
                {
                    _company = _pickingList.FirstOrDefault().company;
                    var company = _companyList.SingleOrDefault(x => x.code == _company);
                    if (company == null)
                        throw new ArgumentException("This company does not exits in the system. Please check with IT department.");
                    _companyName = company.name;
                    RaisePropertyChangedEvent(nameof(company));
                    RaisePropertyChangedEvent(nameof(companyName));

                    PickingGradeListBlinding();
                    DataProductionBlinding();
                }

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PickingGradeListBlinding()
        {
            if (_type != null)
                _classifyList = Facade.pickingBL().GetbyType(_type);

            RaisePropertyChangedEvent(nameof(classifyList));
        }

        private void ClearInputForm()
        {
            _userlocked = false;
            _documentStatus = !_userlocked ? "UnLocked" : "Locked";
            _documentStatusBGColor = !_userlocked ? Brushes.LightGreen : Brushes.Salmon;
            if (_matrc != null)
            _finishEnabled = _matrc.finishtime == null ? true : false;


            _classify = "";
            _weight = (decimal)0.0;
            _matInput = null;
            _saveEnabled = true;
            _deleteEnabled = false;
            _senddataEnabled = true;
            _bc = "";
            _baleno = 0;

            RaisePropertyChangedEvent(nameof(bc));
            RaisePropertyChangedEvent(nameof(baleno));
            RaisePropertyChangedEvent(nameof(classifyList));
            RaisePropertyChangedEvent(nameof(classify));
            RaisePropertyChangedEvent(nameof(weight));
            RaisePropertyChangedEvent(nameof(matInput));
            RaisePropertyChangedEvent(nameof(SaveEnabled));
            RaisePropertyChangedEvent(nameof(DeleteEnabled));
            RaisePropertyChangedEvent(nameof(SendDataEnabled));
            RaisePropertyChangedEvent(nameof(DocumentStatus));
            RaisePropertyChangedEvent(nameof(DocumentStatusBGColor));
            RaisePropertyChangedEvent(nameof(FinishEnabled));
            OnFocusRequested(nameof(bc));
        }

        private ICommand _weightEnterCommand;

        public ICommand WeightEnterCommand
        {
            get { return _weightEnterCommand ?? (_weightEnterCommand = new RelayCommand(WeightEnter)); }
            set { _weightEnterCommand = value; }
        }

        private void WeightEnter(object obj)
        {
            try
            {
                if (_weight < 1)
                    throw new ArgumentException("น้ำหนักห่อยาจะต้องมากกว่า 0");

                if (_weight > 100)
                    if (MessageBoxHelper.Question("น้ำหนักห่อยาเกิน 100 กก. ท่านต้องการบันทึกห่อยานี้ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;

                SavePicking();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        private ICommand _printCommand;

        public ICommand PrintCommand
        {
            get { return _printCommand ?? (_printCommand = new RelayCommand(PrintButton)); }
            set { _printCommand = value; }
        }

        private void PrintButton(object obj)
        {
            PrintPicking();
        }

        private ICommand _saveCommand;

        public ICommand SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(SaveInput)); }
            set { _saveCommand = value; }
        }

        private void SaveInput(object obj)
        {
            SavePicking();
        }


        private void SavePicking()
        {
            try
            {

                if (string.IsNullOrEmpty(_bc))
                    throw new ArgumentException("กรุณา ระบุ Barcode!!!");
                if (string.IsNullOrEmpty(_classifier))
                    throw new ArgumentException("โปรดระบุ classifier");
                if (string.IsNullOrEmpty(_type))
                    throw new ArgumentException("โปรดระบุ type");
                if (string.IsNullOrEmpty(_subtype))
                    throw new ArgumentException("โปรดระบุ subtype");
                if (string.IsNullOrEmpty(_baleno.ToString()))
                    throw new ArgumentException("โปรดระบุ Case no.");
                if (string.IsNullOrEmpty(_classify))
                    throw new ArgumentException("โปรดระบุเกรด classify");
                if (string.IsNullOrEmpty(_company))
                    throw new ArgumentException("โปรดระบุ Company");
                if (_weight == 0)
                    throw new ArgumentException("โปรดระบุ น้ำหนักยา");

                //check duplicate
                var existedBC = Facade.matBL().GetSingle(_bc);
                if (existedBC != null)
                    throw new ArgumentException("Barcode นี้มีอยู่ในระบบแล้ว ไม่สามารถใช้ช้ำได้ : " + _bc);

                var matrc = Facade.matrcBL().GetSingle(_matrc.rcno);
                if (matrc == null)
                {
                    //matrcno
                    _matrc.rcno = Facade.matrcnoBL().Add(_crop, user_setting.machine, user_setting.security.uname);
                    //matrc
                    Facade.matrcBL().Add(null, null, null, _matrc.rcno, _crop, _type, DateTime.Now,
                                "Blending", "Leaf(BD,RG,HS)", _classifier, _remark, user_setting.security.uname,"","");
                }
                else
                {
                    Facade.matrcBL().UpdateMatrcPicking(_matrc.rcno, _classifier, _remark, user_setting.security.uname, _type);
                }

                if (!_picking && (_form == "CTB" || _form == "RYO"))
                {
                    CalculatePicking();
                }
                else if (!_picking)
                {
                    //---- TEST PRICE IS NULL -----
                    _unitprice = (decimal)Facade.pickingBL().GetSingle(_classify, _type).price;
                    _price = _unitprice * _weight;
                }

                //Add mat
                List<mat> comp_last = Facade.matBL().GetByRcno(_matrc.rcno).ToList();
                if (comp_last.Count() > 0)
                {
                    if (_company != comp_last.FirstOrDefault().company)
                        throw new ArgumentException("Company ของ Picking ห่อนี้ไม่เหมือนห่อก่อนหน้า, กรุณาเลือก document ใหม่อีกครั้ง");
                    if(_type != comp_last.FirstOrDefault().type)
                        throw new ArgumentException("Type ของ Picking ห่อนี้ไม่เหมือนห่อก่อนหน้า, กรุณาเลือก document ใหม่อีกครั้ง");
                    if(_subtype != comp_last.FirstOrDefault().subtype)
                        throw new ArgumentException("Subtype ของ Picking ห่อนี้ไม่เหมือนห่อก่อนหน้า, กรุณาเลือก document ใหม่อีกครั้ง");
                }


                Facade.matBL().PickingReceive(_matrc.rcno,_bc,_baleno,_classify, _weight, user_setting.security.uname, _mark, _productionDate, _pdno, _packedgrade, _price, _unitprice, _subtype, _company, _brazilianChk);

                //if (_Print)
                //    Facade.matBL().PrintPickingBarcode(_bc, _printX2 == true ? 2 : 1);


                mat matEx = Facade.matBL().GetSingle(_bc);

                _saveEnabled = false;
                RaisePropertyChangedEvent(nameof(SaveEnabled));

                MessageBoxHelper.Info("ข้อมูล" + Environment.NewLine +
                    "Bale No. : " + matEx.baleno + Environment.NewLine +
                    "classify: " + matEx.classify + Environment.NewLine +
                    "weight: " + Convert.ToDecimal(matEx.weight).ToString("N1") + Environment.NewLine +
                    "ทำการบันทึกเรียบร้อยแล้ว");

                HeaderBinding();
                InputDetailsBinding();
                AddPicking();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CalculatePicking()
        {
            decimal priceUnit = 0;
            decimal price = 0;
            decimal packedPrice = 0;
            double greenPrice = 0;
            string tmpPickingGrade;

            if (_classify.Length <= 3)
            {
                priceUnit = (decimal)Facade.pickingBL().GetSingle(_classify, _type).price;
                price = priceUnit * _weight;
            }
            else
            {
                tmpPickingGrade = _classify.Remove(0, 3);

                //Find Average Repacked 
                List<pd> Repacked = Facade.pdBL().GetByToPdno(_pdno).Where(x => x.grade == tmpPickingGrade).ToList();

                if (Repacked.Count() != 0)
                {
                    var sumNet = Repacked.Sum(x => x.netdef);
                    var sumPrice = Repacked.Sum(x => x.price);
                    packedPrice = Convert.ToDecimal(sumPrice) / (decimal)sumNet;
                }
                //Find Average Picking
                List<mat> Green = Facade.matBL().GetByToPdno(_pdno).Where(x => x.classify == _classify).ToList();
                if (Green.Count() != 0) greenPrice = (double)Green.Average(x => x.priceunit);


                //เช็คว่าเกรด Picking ที่เหลือ ตรงกับหัวเกรดรึป่าว
                if (tmpPickingGrade == _packedgrade)
                {
                    //หาราคาเฉลี่ย ยา repacked ทั้งหมดใน run นี้
                    List<pd> Curr_Repacked = Facade.pdBL().GetByToPdno(_pdno).ToList();
                    var sumNet = Repacked.Sum(x => x.netdef);
                    var sumPrice = Repacked.Sum(x => x.price);
                    priceUnit = Convert.ToDecimal(sumPrice) / (decimal)sumNet;
                    price = priceUnit * _weight;

                    //update picking price
                    Facade.pickingBL().UpdatePickingPrice(_classify, _type, priceUnit, user_setting.security.uname);
                }else if(packedPrice != 0)
                {
                    priceUnit = packedPrice;
                    price = priceUnit * _weight;

                    //update picking price
                    Facade.pickingBL().UpdatePickingPrice(_classify, _type, priceUnit, user_setting.security.uname);
                }else if(greenPrice != 0)
                {
                    priceUnit = Convert.ToDecimal(greenPrice);
                    price = priceUnit * _weight;
                }
                else
                {
                    priceUnit = (decimal)Facade.pickingBL().GetSingle(_classify, _type).price;
                    price = priceUnit * _weight;
                }
            }
            
            _price = price;
            _unitprice = priceUnit;

        }

        private void DeleteInput()
        {
            try
            {
                if (_matInput == null)
                    throw new ArgumentException("โปรดคลิกเลือกแถวข้อมูลที่ต้องการลบจากตาราง regrade input");

                if (_userlocked)
                    throw new ArgumentException("ข้อมูลนี้ Locked แล้วท่านไม่สามารถลบห่อยานี้ได้");

                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?" + Environment.NewLine +
                    "===============" + Environment.NewLine +
                    "bc: " + _matInput.bc + Environment.NewLine +
                    "company: " + _matInput.company + Environment.NewLine +
                    "classify: " + _matInput.classify + Environment.NewLine +
                    "weight: " + Convert.ToDecimal(_matInput.weight).ToString("N1") + Environment.NewLine +
                    "supplier: " + _matInput.supplier + Environment.NewLine) == MessageBoxResult.No)
                    return;

                Facade.matBL().DeletePicking(_matInput.bc);

                //List<mat> m = Facade.matBL().GetByRcno(_matInput.rcno);
                //if (m.Count() == 0)
                //{
                //    Facade.matrcBL().DeleteByRcno(_matInput.rcno);
                //    Facade.matrcnoBL().DeleteByRcno(_crop, _matInput.rcno);
                //}
                   
                _deleteEnabled = false;
                RaisePropertyChangedEvent(nameof(DeleteEnabled));
                AddPicking();
                InputDetailsBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ResetEnabledButton()
        {
            //_finishEnabled = true;
            //_deleteEnabled = true;
            //_saveEnabled = true;
            //_reprintEnabled = true;
            //_senddataEnabled = true;

            RaisePropertyChangedEvent(nameof(FinishEnabled));
            RaisePropertyChangedEvent(nameof(DeleteEnabled));
            RaisePropertyChangedEvent(nameof(SaveEnabled));
            RaisePropertyChangedEvent(nameof(ReprintEnabled));
            RaisePropertyChangedEvent(nameof(SendDataEnabled));
        }
        private void DataProductionBlinding()
        {
            try
            {
                pdsetup t = new pdsetup();
                t = Facade.pdsetupBL().GetSingle(_pdno);
                _packedgrade = t.packedgrade;
                _form = Facade.packedgradeBL().GetSingle(_packedgrade).form;

                RaisePropertyChangedEvent(nameof(packedgrade));
                RaisePropertyChangedEvent(nameof(form));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        private ICommand _summarySheetClickCommand;

        public ICommand SummarySheetClickCommand
        {
            get { return _summarySheetClickCommand ?? (_summarySheetClickCommand = new RelayCommand(SummaryReport)); }
            set { _summarySheetClickCommand = value; }
        }
        private void SummaryReport(object obj)
        {
            try
            {

                Facade.PickingReportBL().SummaryReport(matrc.rcno);
                MessageBoxHelper.Info("พิมพ์รายงานเรียบร้อยแล้ว");

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _detailSheetClickCommand;

        public ICommand DetailSheetClickCommand
        {
            get { return _detailSheetClickCommand ?? (_detailSheetClickCommand = new RelayCommand(DetailsReport)); }
            set { _detailSheetClickCommand = value; }
        }
        private void DetailsReport(object obj)
        {
            try
            {
                Facade.PickingReportBL().DetailReport(matrc.rcno);
                MessageBoxHelper.Info("พิมพ์รายงานเรียบร้อยแล้ว");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        public void PrintPicking()
        {
            try
            {
                if (String.IsNullOrEmpty(_bc))
                    throw new ArgumentException("โปรดคลิกเลือกแถวที่ต้องการปริ้นท์จากตาราง");

                Facade.matBL().PrintPickingBarcode(_bc, _printX2 == true ? 2 : 1);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        public void ReWeight()
        {
            try
            {
                if (_useDigitalScale == false)
                    return;

                if (DigitalScaleHelper.CheckPort() == false)
                    {
                    MessageBoxHelper.Warning("ไม่พบพอร์ตเชื่อมต่อเครื่องชั่งดิจิตอลบนเครื่องคอมพิวเตอร์นี้");
                    _useDigitalScale = false;
                    RaisePropertyChangedEvent(nameof(UseDigitalScale));
                    return;
                }

                if(_userlocked)
                    throw new ArgumentException("ข้อมูลนี้ Locked แล้วท่านไม่สามารถ Reweight และ แก้ไขข้อมูลได้"); 

                if (String.IsNullOrEmpty(_bc))
                    throw new ArgumentException("โปรดคลิกเลือกแถวที่ต้องการปริ้นท์จากตาราง");


                if (MessageBoxHelper.Question("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่") == MessageBoxResult.No)
                    return;

                var window = new ScalePanel();
                var vm = new vm_ScalePanel();
                vm.Window = window;
                window.DataContext = vm;
                window.ShowDialog();

                if (vm.Weight == null)
                    return;

                _weight = (decimal)vm.Weight;
                RaisePropertyChangedEvent(nameof(weight));
                OnFocusRequested(nameof(weight));

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion


    }
}
