﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IexpertBL
    {
        List<expert> GetAll();
    }

    public class expertBL : IexpertBL
    {
        IStecDBMSUnitOfWork uow;
        public expertBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public List<expert> GetAll()
        {
            return uow.expertRepo.Get();
        }
    }
}
