﻿using FactoryBL;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.Model;
using FactorySystemWPF.MVVM;
using FactorySystemWPF.View.Shared;
using FactorySystemWPF.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace FactorySystemWPF.ViewModel.Processing.ByProduct
{
    public class vm_ByProduct : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_ByProduct()
        {
            OnLoad();
            ClearForm();
            _printerList = Facade.BarcodePrinterBL()
                .GetAll()
                .OrderBy(x => x.PrinterName)
                .ToList();
            RaisePropertyChangedEvent(nameof(PrinterList));
        }



        #region Properties
        private string _pdno;

        public string pdno
        {
            get { return _pdno; }
            set { _pdno = value; }
        }

        private pdsetup _pdsetup;

        public pdsetup pdsetup
        {
            get { return _pdsetup; }
            set { _pdsetup = value; }
        }

        private packedgrade _packedgrade;

        public packedgrade packedgrade
        {
            get { return _packedgrade; }
            set { _packedgrade = value; }
        }

        private packedgrade _byProductPackedGrade;

        public packedgrade ByProductPackedGrade
        {
            get { return _byProductPackedGrade; }
            set { _byProductPackedGrade = value; }
        }

        private packedgrade _slpackedgrade;

        public packedgrade SLpackedgrade
        {
            get { return _slpackedgrade; }
            set { _slpackedgrade = value; }
        }

        private packedgrade _sl2packedgrade;

        public packedgrade SL2packedgrade
        {
            get { return _sl2packedgrade; }
            set { _sl2packedgrade = value; }
        }

        private packedgrade _sspackedgrade;

        public packedgrade SSpackedgrade
        {
            get { return _sspackedgrade; }
            set { _sspackedgrade = value; }
        }

        private packedgrade _flpackedgrade;

        public packedgrade FLpackedgrade
        {
            get { return _flpackedgrade; }
            set { _flpackedgrade = value; }
        }

        private packedgrade _fspackedgrade;

        public packedgrade FSpackedgrade
        {
            get { return _fspackedgrade; }
            set { _fspackedgrade = value; }
        }

        private sfsetup _sfsetup;

        public sfsetup sfsetup
        {
            get { return _sfsetup; }
            set { _sfsetup = value; }
        }

        private PDCustomerPackedPrint _pdCusPackedPrint;

        public PDCustomerPackedPrint PDCusPackedPrint
        {
            get { return _pdCusPackedPrint; }
            set { _pdCusPackedPrint = value; }
        }

        private string _bc;

        public string bc
        {
            get { return _bc; }
            set { _bc = value; }
        }

        private int _caseno;

        public int caseno
        {
            get { return _caseno; }
            set { _caseno = value; }
        }

        private decimal _gross;

        public decimal gross
        {
            get { return _gross; }
            set
            {
                _gross = value;
                _netreal = _gross - _boxtare;
                RaisePropertyChangedEvent(nameof(netreal));
            }
        }

        private decimal _boxtare;

        public decimal boxtare
        {
            get { return _boxtare; }
            set
            {
                _boxtare = value;
                _netreal = _gross - _boxtare;
                RaisePropertyChangedEvent(nameof(netreal));
            }
        }

        private decimal _taredef;

        public decimal taredef
        {
            get { return _taredef; }
            set { _taredef = value; }
        }

        private decimal _netreal;

        public decimal netreal
        {
            get { return _netreal; }
            set { _netreal = value; }
        }

        private DateTime _time;

        public DateTime time
        {
            get { return _time; }
            set { _time = value; }
        }

        private string _box;

        public string box
        {
            get { return _box; }
            set { _box = value; }
        }

        private Brush _boxBGColor;

        public Brush BoxBGColor
        {
            get { return _boxBGColor; }
            set { _boxBGColor = value; }
        }

        private bool _useDigitalScale;

        public bool UseDigitalScale
        {
            get { return _useDigitalScale; }
            set
            {
                _useDigitalScale = value;

                if (_useDigitalScale == true)
                {
                    if (DigitalScaleHelper.CheckPort() == false)
                    {
                        _useDigitalScale = false;
                        MessageBoxHelper.Warning("ไม่พบ port เครื่องชั่งที่เชื่อมต่ออยู่กับเครื่องคอมพิวเตอร์");
                    }
                    else
                        _useDigitalScale = true;
                }
            }
        }

        private bool _scanFromBoxTare;

        public bool ScanFromBoxTare
        {
            get { return _scanFromBoxTare; }
            set
            {
                try
                {
                    _scanFromBoxTare = value;

                    if (_scanFromBoxTare == false)
                    {
                        if (string.IsNullOrEmpty(_pdno))
                            throw new ArgumentException("โปรดคลิกปุ่ม New Grade เพื่อเลือกเกรดก่อน");

                        _boxtare = (decimal)_packedgrade.taredef;
                    }

                    RaisePropertyChangedEvent(nameof(boxtare));
                    RaisePropertyChangedEvent(nameof(ScanFromBoxTare));
                    OnFocusRequested(nameof(boxtare));
                }
                catch (Exception ex)
                {
                    MessageBoxHelper.Exception(ex);
                }
            }
        }

        private bool _checkNetReal;

        public bool CheckNetReal
        {
            get { return _checkNetReal; }
            set { _checkNetReal = value; }
        }

        private bool _printX2;

        public bool PrintX2
        {
            get { return _printX2; }
            set { _printX2 = value; }
        }

        private bool _notPrint;

        public bool NotPrint
        {
            get { return _notPrint; }
            set
            {
                _notPrint = value;
                if (_notPrint == true)
                    _printX2 = false;
                else
                    _printX2 = true;
                RaisePropertyChangedEvent(nameof(PrintX2));
            }
        }

        private FactoryEntities.BarcodePrinter _printer;

        public FactoryEntities.BarcodePrinter Printer
        {
            get { return _printer; }
            set { _printer = value; }
        }

        private int? _cusRunNo;

        public int? CusRunNo
        {
            get { return _cusRunNo; }
            set { _cusRunNo = value; }
        }

        private int? _caseRunNo;

        public int? CaseRunNo
        {
            get { return _caseRunNo; }
            set { _caseRunNo = value; }
        }

        private int? _accuRunno;

        public int? AccuRunno
        {
            get { return _accuRunno; }
            set { _accuRunno = value; }
        }

        private string _customerBarcode;

        public string CustomerBarcode
        {
            get { return _customerBarcode; }
            set { _customerBarcode = value; }
        }

        private Visibility _commandButtonVisibility;

        public Visibility CommandButtonVisibility
        {
            get { return _commandButtonVisibility; }
            set { _commandButtonVisibility = value; }
        }

        private Visibility _printButtonVisibility;

        public Visibility PrintButtonVisibility
        {
            get { return _printButtonVisibility; }
            set { _printButtonVisibility = value; }
        }

        private pd _dataGridSelectedRow;

        public pd DataGridSelectedRow
        {
            get { return _dataGridSelectedRow; }
            set
            {
                _dataGridSelectedRow = value;
                _printButtonVisibility = _dataGridSelectedRow == null ?
                    Visibility.Collapsed : Visibility.Visible;
                RaisePropertyChangedEvent(nameof(PrintButtonVisibility));
            }
        }

        private string _packingend;

        public string packingend
        {
            get { return _packingend; }
            set { _packingend = value; }
        }

        private string _machine;

        public string machine
        {
            get { return _machine; }
            set { _machine = value; }
        }

        private string _mode;

        public string Mode
        {
            get { return _mode; }
            set { _mode = value; }
        }

        private Brush _slBgColor;

        public Brush SLBgColor
        {
            get { return _slBgColor; }
            set { _slBgColor = value; }
        }

        private Brush _sl2BgColor;

        public Brush SL2BgColor
        {
            get { return _sl2BgColor; }
            set { _sl2BgColor = value; }
        }

        private Brush _ssBgColor;

        public Brush SSBgColor
        {
            get { return _ssBgColor; }
            set { _ssBgColor = value; }
        }

        private Brush _flBgColor;

        public Brush FLBgColor
        {
            get { return _flBgColor; }
            set { _flBgColor = value; }
        }

        private Brush _fsBgColor;

        public Brush FSBgColor
        {
            get { return _fsBgColor; }
            set { _fsBgColor = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private int _slCase;

        public int SLCase
        {
            get { return _slCase; }
            set { _slCase = value; }
        }

        private int _sl2Case;

        public int SL2Case
        {
            get { return _sl2Case; }
            set { _sl2Case = value; }
        }

        private int _ssCase;

        public int SSCase
        {
            get { return _ssCase; }
            set { _ssCase = value; }
        }

        private int _flCase;

        public int FLCase
        {
            get { return _flCase; }
            set { _flCase = value; }
        }

        private int _fsCase;

        public int FSCase
        {
            get { return _fsCase; }
            set { _fsCase = value; }
        }

        private decimal _slNet;

        public decimal SLNet
        {
            get { return _slNet; }
            set { _slNet = value; }
        }

        private decimal _sl2Net;

        public decimal SL2Net
        {
            get { return _sl2Net; }
            set { _sl2Net = value; }
        }

        private decimal _ssNet;

        public decimal SSNet
        {
            get { return _ssNet; }
            set { _ssNet = value; }
        }

        private decimal _flNet;

        public decimal FLNet
        {
            get { return _flNet; }
            set { _flNet = value; }
        }

        private decimal _fsNet;

        public decimal FSNet
        {
            get { return _fsNet; }
            set { _fsNet = value; }
        }
        #endregion



        #region List
        private List<string> _packingEndList;

        public List<string> PackingEndList
        {
            get { return _packingEndList; }
            set { _packingEndList = value; }
        }

        private List<string> _machineList;

        public List<string> MachineList
        {
            get { return _machineList; }
            set { _machineList = value; }
        }

        private List<pd> _pdList;

        public List<pd> pdList
        {
            get { return _pdList; }
            set { _pdList = value; }
        }

        private List<pd> _byProductList;

        public List<pd> ByProductList
        {
            get { return _byProductList; }
            set { _byProductList = value; }
        }

        private List<FactoryEntities.BarcodePrinter> _printerList;

        public List<FactoryEntities.BarcodePrinter> PrinterList
        {
            get { return _printerList; }
            set { _printerList = value; }
        }
        #endregion



        #region Command

        private ICommand _finishGradeCommand;

        public ICommand FinishGradeCommand
        {
            get { return _finishGradeCommand ?? (_finishGradeCommand = new RelayCommand(FinishGrade)); }
            set { _finishGradeCommand = value; }
        }

        private void FinishGrade(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_pdno))
                    throw new ArgumentException("โปรดเลือกเกรดที่จะทำการ finish ก่อน โดยคลิกเลือกจากปุ่ม New Grade");

                if (MessageBoxHelper.Question("หลังจากที่ทำการ finish เกรดนี้แล้วจะไม่สามารถแก้ไขข้อมูลได้อีก" +
                    " ท่านยืรยันที่จะ finish เกรดนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                Facade.pdsetupBL().FinishPacking(_pdno);
                ProductionChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _newGradeCommand;

        public ICommand NewGradeCommand
        {
            get { return _newGradeCommand ?? (_newGradeCommand = new RelayCommand(NewGrade_)); }
            set { _newGradeCommand = value; }
        }

        private void NewGrade_(object obj)
        {
            try
            {
                var window = new View.Processing.NewGrade();
                var vm = new vm_NewGrade();
                window.DataContext = vm;
                vm.Window = window;
                window.ShowDialog();

                if (vm.SelectedItem == null)
                    return;

                _pdno = vm.SelectedItem.pdno;
                _pdsetup = Facade.pdsetupBL().GetSingle(_pdno);
                _packedgrade = Facade.packedgradeBL().GetSingle(_pdsetup.packedgrade);

                RaisePropertyChangedEvent(nameof(pdno));
                RaisePropertyChangedEvent(nameof(pdsetup));
                RaisePropertyChangedEvent(nameof(packedgrade));

                ProductionChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _reWeightCommand;

        public ICommand ReWeightCommand
        {
            get { return _reWeightCommand ?? (_reWeightCommand = new RelayCommand(ReWeight)); }
            set { _reWeightCommand = value; }
        }

        private void ReWeight(object obj)
        {
            ReWeight();
        }

        private ICommand _addCommand;

        public ICommand AddCommand
        {
            get { return _addCommand ?? (_addCommand = new RelayCommand(Add)); }
            set { _addCommand = value; }
        }

        private void Add(object obj)
        {
            Add();
        }

        private ICommand _saveCommand;

        public ICommand SaveCommnd
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(Save)); }
            set { _saveCommand = value; }
        }

        private void Save(object obj)
        {
            Save();
        }

        private ICommand _printCommand;

        public ICommand PrintCommand
        {
            get { return _printCommand ?? (_printCommand = new RelayCommand(Print)); }
            set { _printCommand = value; }
        }

        private void Print(object obj)
        {
            Print();
        }

        private ICommand _printTestCommand;

        public ICommand PrintTestCommand
        {
            get { return _printTestCommand ?? (_printTestCommand = new RelayCommand(PrintTest)); }
            set { _printTestCommand = value; }
        }

        private void PrintTest(object obj)
        {
            try
            {
                if (_printer == null)
                    throw new ArgumentException("โปรดระบุเครื่องปริ้นท์");

                var copy = 0;
                if (_notPrint == false)
                {
                    if (_printX2 == true)
                        copy = 2;
                    else
                        copy = 1;
                }

                Facade.pdBL()
                    .PrintSTECBarcode(new pd
                    {
                        bc = "LN-0009999",
                        caseno = 9999,
                        grade = "Print Test",
                        grossdef = (decimal)999.9,
                        netdef = (decimal)999.9,
                        boxtare = 99.9,
                        netreal = (decimal)999.9

                    }, _printer.PrinterName,
                    (short)copy);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _f1Command;

        public ICommand F1Command
        {
            get { return _f1Command ?? (_f1Command = new RelayCommand(F1)); }
            set { _f1Command = value; }
        }

        private void F1(object obj)
        {
            _mode = "SL";
            ChangeMode();
        }

        private ICommand _f2Command;

        public ICommand F2Command
        {
            get { return _f2Command ?? (_f2Command = new RelayCommand(F2)); }
            set { _f2Command = value; }
        }

        private void F2(object obj)
        {
            _mode = "SS";
            ChangeMode();
        }

        private ICommand _f3Command;

        public ICommand F3Command
        {
            get { return _f3Command ?? (_f3Command = new RelayCommand(F3)); }
            set { _f3Command = value; }
        }

        private void F3(object obj)
        {
            _mode = "FL";
            ChangeMode();
        }

        private ICommand _f4Command;

        public ICommand F4Command
        {
            get { return _f4Command ?? (_f4Command = new RelayCommand(F4)); }
            set { _f4Command = value; }
        }

        private void F4(object obj)
        {
            _mode = "FS";
            ChangeMode();
        }

        private ICommand _f5Command;

        public ICommand F5Command
        {
            get { return _f5Command ?? (_f5Command = new RelayCommand(F5)); }
            set { _f5Command = value; }
        }

        private void F5(object obj)
        {
            _mode = "SL2";
            ChangeMode();
        }

        private ICommand _f6Command;

        public ICommand F6Command
        {
            get { return _f6Command ?? (_f6Command = new RelayCommand(F6)); }
            set { _f6Command = value; }
        }

        private void F6(object obj)
        {
            if (_box.Contains("NewBox"))
            {
                _box = "OldBox";
                _boxBGColor = Brushes.LimeGreen;
            }
            else
            {
                _box = "NewBox";
                _boxBGColor = Brushes.Gold;
            }
            RaisePropertyChangedEvent(nameof(box));
            RaisePropertyChangedEvent(nameof(BoxBGColor));
        }

        private ICommand _f7Command;

        public ICommand F7Command
        {
            get { return _f7Command ?? (_f7Command = new RelayCommand(F7)); }
            set { _f7Command = value; }
        }

        private void F7(object obj)
        {
            Add();
            _caseno = 0;
            _gross = (decimal)0.0;
            RaisePropertyChangedEvent(nameof(gross));
            RaisePropertyChangedEvent(nameof(caseno));
            OnFocusRequested(nameof(gross));
        }

        private ICommand _f8Command;

        public ICommand F8Command
        {
            get { return _f8Command ?? (_f8Command = new RelayCommand(F8)); }
            set { _f8Command = value; }
        }

        private void F8(object obj)
        {
            Add();
        }

        private ICommand _f9Command;

        public ICommand F9Command
        {
            get { return _f9Command ?? (_f9Command = new RelayCommand(F9)); }
            set { _f9Command = value; }
        }

        private void F9(object obj)
        {
            Print();
        }

        private ICommand _f10Command;

        public ICommand F10Command
        {
            get { return _f10Command ?? (_f10Command = new RelayCommand(F10)); }
            set { _f10Command = value; }
        }

        private void F10(object obj)
        {
            ReWeight();
        }

        private ICommand _f12Command;

        public ICommand F12Command
        {
            get { return _f12Command ?? (_f12Command = new RelayCommand(F12)); }
            set { _f12Command = value; }
        }

        private void F12(object obj)
        {
            Save();
        }

        private ICommand _selectedRowCommand;

        public ICommand SelectedRowCommand
        {
            get { return _selectedRowCommand ?? (_selectedRowCommand = new RelayCommand(SelectedRow)); }
            set { _selectedRowCommand = value; }
        }

        private void SelectedRow(object obj)
        {
            try
            {
                if (obj == null)
                    return;

                var selectedItem = (pd)obj;

                _bc = selectedItem.bc;
                _caseno = (int)selectedItem.caseno;
                _gross = (decimal)selectedItem.grossreal;
                _boxtare = (decimal)selectedItem.boxtare;
                _taredef = (decimal)selectedItem.taredef;
                _netreal = (decimal)selectedItem.netreal;
                _time = (DateTime)selectedItem.packingtime;
                _box = selectedItem.box == true ? "NewBox" : "OldBox";
                _boxBGColor = _box == "NewBox" ? Brushes.Gold : Brushes.LimeGreen;
                _printButtonVisibility = Visibility.Visible;

                if (selectedItem.caseno == 0)
                {
                    _caseRunNo = null;
                    _accuRunno = null;
                    _cusRunNo = null;
                    _customerBarcode = null;
                    RaisePropertyChangedEvent(nameof(AccuRunno));
                    RaisePropertyChangedEvent(nameof(CaseRunNo));
                    RaisePropertyChangedEvent(nameof(CusRunNo));
                    RaisePropertyChangedEvent(nameof(CustomerBarcode));
                }

                if (_pdCusPackedPrint != null)
                {
                    if (selectedItem.AccuRunno != null)
                    {
                        _caseRunNo = (int)selectedItem.CaseRunno;
                        _accuRunno = (int)selectedItem.AccuRunno;
                        _cusRunNo = (int)selectedItem.CusRunno;
                        RaisePropertyChangedEvent(nameof(CaseRunNo));
                        RaisePropertyChangedEvent(nameof(AccuRunno));
                        RaisePropertyChangedEvent(nameof(CusRunNo));
                        SetCustomerBarcode((int)selectedItem.AccuRunno);
                    }
                }

                RaisePropertyChangedEvent(nameof(bc));
                RaisePropertyChangedEvent(nameof(caseno));
                RaisePropertyChangedEvent(nameof(gross));
                RaisePropertyChangedEvent(nameof(boxtare));
                RaisePropertyChangedEvent(nameof(taredef));
                RaisePropertyChangedEvent(nameof(netreal));
                RaisePropertyChangedEvent(nameof(time));
                RaisePropertyChangedEvent(nameof(box));
                RaisePropertyChangedEvent(nameof(BoxBGColor));
                RaisePropertyChangedEvent(nameof(PrintButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _grossGotFocusCommand;

        public ICommand GrossGotFocusCommand
        {
            get { return _grossGotFocusCommand ?? (_grossGotFocusCommand = new RelayCommand(GrossGotFocus)); }
            set { _grossGotFocusCommand = value; }
        }

        private void GrossGotFocus(object obj)
        {
            try
            {
                if (_useDigitalScale == true)
                {
                    if (DigitalScaleHelper.CheckPort() == false)
                    {
                        MessageBoxHelper.Warning("ไม่พบพอร์ตเชื่อมต่อเครื่องชั่งดิจิตอลบนเครื่องคอมพิวเตอร์นี้");
                        _useDigitalScale = false;
                        RaisePropertyChangedEvent(nameof(UseDigitalScale));
                        return;
                    }

                    var window = new ScalePanel();
                    var vm = new vm_ScalePanel();
                    vm.Window = window;
                    window.DataContext = vm;
                    window.ShowDialog();

                    if (vm.Weight == null)
                        return;

                    _gross = (decimal)vm.Weight;
                    RaisePropertyChangedEvent(nameof(gross));
                    OnFocusRequested(nameof(boxtare));
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        public void ClearForm()
        {
            _bc = "";
            _caseno = 0;
            _gross = 0;
            _taredef = (decimal)(_packedgrade != null ? _packedgrade.taredef : _taredef); ;
            _netreal = 0;
            _time = DateTime.Now;
            _dataGridSelectedRow = null;

            RaisePropertyChangedEvent(nameof(bc));
            RaisePropertyChangedEvent(nameof(caseno));
            RaisePropertyChangedEvent(nameof(gross));
            RaisePropertyChangedEvent(nameof(taredef));
            RaisePropertyChangedEvent(nameof(netreal));
            RaisePropertyChangedEvent(nameof(time));
            RaisePropertyChangedEvent(nameof(DataGridSelectedRow));
        }

        public void OnLoad()
        {
            try
            {
                _useDigitalScale = true;
                _checkNetReal = true;
                _printX2 = true;
                _box = "NewBox";
                _boxBGColor = Brushes.Gold;
                _commandButtonVisibility = Visibility.Collapsed;
                _printButtonVisibility = Visibility.Collapsed;

                _machineList = new List<string>();
                _packingEndList = new List<string>();

                _machineList.Add("Nikom");
                _machineList.Add("Prasit");

                _packingEndList.Add("Chatmongkon");
                _packingEndList.Add("Thodsapol");

                _slBgColor = Brushes.Lavender;
                _sl2BgColor = Brushes.Lavender;
                _ssBgColor = Brushes.Lavender;
                _flBgColor = Brushes.Lavender;
                _fsBgColor = Brushes.Lavender;

                RaisePropertyChangedEvent(nameof(UseDigitalScale));
                RaisePropertyChangedEvent(nameof(CheckNetReal));
                RaisePropertyChangedEvent(nameof(PrintX2));
                RaisePropertyChangedEvent(nameof(box));
                RaisePropertyChangedEvent(nameof(BoxBGColor));
                RaisePropertyChangedEvent(nameof(CommandButtonVisibility));
                RaisePropertyChangedEvent(nameof(PrintButtonVisibility));

                RaisePropertyChangedEvent(nameof(MachineList));
                RaisePropertyChangedEvent(nameof(PackingEndList));

                RaisePropertyChangedEvent(nameof(SLBgColor));
                RaisePropertyChangedEvent(nameof(SL2BgColor));
                RaisePropertyChangedEvent(nameof(SSBgColor));
                RaisePropertyChangedEvent(nameof(FLBgColor));
                RaisePropertyChangedEvent(nameof(FSBgColor));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void ProductionChanged()
        {
            try
            {
                if (_pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                _packedgrade = Facade.packedgradeBL().GetSingle(_pdsetup.packedgrade);
                if (_packedgrade == null)
                    throw new ArgumentException("ไม่พบข้อมูล packedgrade โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                _sfsetup = Facade.sfsetupBL().GetSingle();
                if (_sfsetup == null)
                    return;

                _slpackedgrade = Facade.packedgradeBL().GetSingle(_sfsetup.slgrade);
                _sl2packedgrade = Facade.packedgradeBL().GetSingle(_sfsetup.sl2grade);
                _sspackedgrade = Facade.packedgradeBL().GetSingle(_sfsetup.ssgrade);
                _flpackedgrade = Facade.packedgradeBL().GetSingle(_sfsetup.flgrade);
                _fspackedgrade = Facade.packedgradeBL().GetSingle(_sfsetup.fsgrade);

                PdListBinding();

                if (_scanFromBoxTare == false)
                {
                    _boxtare = (decimal)_packedgrade.taredef;
                    RaisePropertyChangedEvent(nameof(boxtare));
                }

                if (_pdsetup.byproductlocked == true)
                {
                    _commandButtonVisibility = Visibility.Collapsed;
                    _printButtonVisibility = Visibility.Collapsed;
                }
                else
                {
                    _commandButtonVisibility = Visibility.Visible;
                    _printButtonVisibility = Visibility.Visible;
                }

                _mode = "";
                ChangeMode();

                RaisePropertyChangedEvent(nameof(SLpackedgrade));
                RaisePropertyChangedEvent(nameof(SL2packedgrade));
                RaisePropertyChangedEvent(nameof(SSpackedgrade));
                RaisePropertyChangedEvent(nameof(FLpackedgrade));
                RaisePropertyChangedEvent(nameof(FSpackedgrade));
                RaisePropertyChangedEvent(nameof(pdList));
                RaisePropertyChangedEvent(nameof(CommandButtonVisibility));
                RaisePropertyChangedEvent(nameof(PrintButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void Save()
        {
            try
            {
                if (_printer == null)
                    throw new ArgumentException("โปรดระบุเครื่องปริ้นท์");

                if (user_setting.security == null)
                    throw new ArgumentException("โปรดล็อคอินเข้าใช้งานก่อน");

                if (_dataGridSelectedRow != null)
                {
                    /// หากมีการคลิกเลือก row ใน datagrid ก่อนหน้านี้ โปรแกรมจะเข้าสู่โหมด re-weight
                    /// เพื่อป้องกันไม่ให้เป็นการ Add ข้อมูลใหม่เข้าไปในจังหวะที่มีการกด F12 เพื่อ Save ข้อมูลใหม่
                    ReWeight();
                    Add();
                    return;
                }

                var copy = 0;
                if (_notPrint == false)
                {
                    if (_printX2 == true)
                        copy = 2;
                    else
                        copy = 1;
                }

                Facade.pdBL().AddByProduct(
                    _bc,
                    _caseno,
                    _gross,
                    _packingend,
                    _machine,
                    _boxtare,
                    _box.Contains("New") ? true : false,
                    _byProductPackedGrade.packedgrade1,
                    _checkNetReal,
                    _printer.PrinterName,
                    (short)copy,
                    _pdno,
                    user_setting.security.uname);

                ChangeMode();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void ReWeight()
        {
            try
            {
                if (_dataGridSelectedRow == null)
                    throw new ArgumentException("โปรดเลือกข้อมูลที่จะทำการแก้จากแถวในตารางข้อมูล");

                if (MessageBoxHelper.Question("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่") == MessageBoxResult.No)
                    return;

                Facade.pdBL()
                    .ReWeightByProduct(_dataGridSelectedRow.bc,
                    _gross,
                    _box == "NewBox" ? true : false,
                    _boxtare,
                    _checkNetReal,
                    user_setting.security.uname);

                PdListBinding();
                ChangeMode();
                Add();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void Add()
        {
            try
            {
                ClearForm();

                if (string.IsNullOrEmpty(_machine))
                    throw new ArgumentException("โปรดระบุ machine");

                if (string.IsNullOrEmpty(_packingend))
                    throw new ArgumentException("โปรดระบุ packing end");

                if (string.IsNullOrEmpty(_pdno))
                    throw new ArgumentException("โปรดคลิก New Grade ก่อนเพื่อ pdno ที่จะทำ");

                switch (_mode)
                {
                    case "SL":
                        if (_slpackedgrade == null)
                            throw new ArgumentException("ไม่มีเกรด by product => Stem Long ในระบบ โปรดเลือกเป็นประเภทอื่น");
                        break;
                    case "SL2":
                        if (_sl2packedgrade == null)
                            throw new ArgumentException("ไม่มีเกรด by product => Stem Long 2 ในระบบ โปรดเลือกเป็นประเภทอื่น");
                        break;
                    case "SS":
                        if (_sspackedgrade == null)
                            throw new ArgumentException("ไม่มีเกรด by product => Stem Short ในระบบ โปรดเลือกเป็นประเภทอื่น");
                        break;
                    case "FL":
                        if (_flpackedgrade == null)
                            throw new ArgumentException("ไม่มีเกรด by product => Fines Large ในระบบ โปรดเลือกเป็นประเภทอื่น");
                        break;
                    case "FS":
                        if (_fspackedgrade == null)
                            throw new ArgumentException("ไม่มีเกรด by product => Fines Small ในระบบ โปรดเลือกเป็นประเภทอื่น");
                        break;
                    default:
                        throw new ArgumentException("โปรดระบุเกรด by product ก่อน => " +
                            "Stem Long, Stem Long 2, Stem Short, Fines Large, Fines Small");
                }

                _bc = Facade.pdBL().GetNewByProductBarcode(_mode, (short)_pdsetup.crop);
                _caseno = Facade.pdBL().GetNewByProductCaseNo(_byProductPackedGrade.packedgrade1);
                _printButtonVisibility = Visibility.Collapsed;

                RaisePropertyChangedEvent(nameof(bc));
                RaisePropertyChangedEvent(nameof(caseno));
                OnFocusRequested(nameof(gross));
                RaisePropertyChangedEvent(nameof(PrintButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void Print()
        {
            try
            {
                if (_notPrint == true)
                    throw new ArgumentException("โปรดนำเครื่องหมายถูกหน้าข้อความ Not Print ออกก่อน");

                if (_dataGridSelectedRow == null)
                    throw new ArgumentException("โปรดคลิกเลือกแถวที่ต้องการปริ้นท์จากตารางข้อมูล");

                if (_printer == null)
                    throw new ArgumentException("โปรดระบุเครื่องปริ้นท์");

                var copy = _printX2 == true ? 2 : 1;
                if (_pdCusPackedPrint != null)
                    Facade.pdBL()
                        .PrintCustomerBarcode(_pdCusPackedPrint,
                        _dataGridSelectedRow,
                        _printer.PrinterName,
                        (short)copy);
                else
                    Facade.pdBL()
                        .PrintSTECBarcode(_dataGridSelectedRow,
                        _printer.PrinterName,
                        (short)copy);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void ChangeMode()
        {
            try
            {
                if (string.IsNullOrEmpty(_machine))
                    throw new ArgumentException("โปรดระบุ machine");

                if (string.IsNullOrEmpty(_packingend))
                    throw new ArgumentException("โปรดระบุ packing end");

                if (string.IsNullOrEmpty(_pdno))
                    throw new ArgumentException("โปรดคลิก New Grade ก่อนเพื่อ pdno ที่จะทำ");

                _pdList = null;
                _byProductList = null;
                _pdCusPackedPrint = null;
                _caseRunNo = null;
                _cusRunNo = null;
                _accuRunno = null;
                _customerBarcode = null;

                RaisePropertyChangedEvent(nameof(pdList));
                RaisePropertyChangedEvent(nameof(ByProductList));
                RaisePropertyChangedEvent(nameof(PDCusPackedPrint));
                RaisePropertyChangedEvent(nameof(CaseRunNo));
                RaisePropertyChangedEvent(nameof(CusRunNo));
                RaisePropertyChangedEvent(nameof(AccuRunno));
                RaisePropertyChangedEvent(nameof(CustomerBarcode));

                _slBgColor = Brushes.Lavender;
                _sl2BgColor = Brushes.Lavender;
                _ssBgColor = Brushes.Lavender;
                _flBgColor = Brushes.Lavender;
                _fsBgColor = Brushes.Lavender;

                PdListBinding();
                ClearForm();
                switch (_mode)
                {
                    case "SL":
                        _slBgColor = Brushes.SpringGreen;
                        if (_slpackedgrade != null)
                        {
                            _byProductPackedGrade = _slpackedgrade;
                            ByProductListBinding(_byProductPackedGrade.packedgrade1);
                        }
                        break;
                    case "SL2":
                        _sl2BgColor = Brushes.SpringGreen;
                        if (_sl2packedgrade != null)
                        {
                            _byProductPackedGrade = _sl2packedgrade;
                            ByProductListBinding(_byProductPackedGrade.packedgrade1);
                        }
                        break;
                    case "SS":
                        _ssBgColor = Brushes.SpringGreen;
                        if (_sspackedgrade != null)
                        {
                            _byProductPackedGrade = _sspackedgrade;
                            ByProductListBinding(_byProductPackedGrade.packedgrade1);
                        }
                        break;
                    case "FL":
                        _flBgColor = Brushes.SpringGreen;
                        if (_flpackedgrade != null)
                        {
                            _byProductPackedGrade = _flpackedgrade;
                            ByProductListBinding(_byProductPackedGrade.packedgrade1);
                        }
                        break;
                    case "FS":
                        _fsBgColor = Brushes.SpringGreen;
                        if (_fspackedgrade != null)
                        {
                            _byProductPackedGrade = _fspackedgrade;
                            ByProductListBinding(_byProductPackedGrade.packedgrade1);
                        }
                        break;
                    default:
                        break;
                }

                RaisePropertyChangedEvent(nameof(SLBgColor));
                RaisePropertyChangedEvent(nameof(SL2BgColor));
                RaisePropertyChangedEvent(nameof(SSBgColor));
                RaisePropertyChangedEvent(nameof(FLBgColor));
                RaisePropertyChangedEvent(nameof(FSBgColor));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void ByProductListBinding(string byProductPackedGrade)
        {
            try
            {
                if (_byProductPackedGrade == null)
                    return;

                _byProductList = _pdList
                            .Where(x => x.grade == byProductPackedGrade)
                            .OrderByDescending(x => x.packingdate)
                            .ThenByDescending(x => x.packingtime)
                            .ToList();
                _totalRecord = _byProductList.Count();
                _taredef = (decimal)_byProductPackedGrade.taredef;
                _boxtare = _taredef;

                RaisePropertyChangedEvent(nameof(ByProductPackedGrade));
                RaisePropertyChangedEvent(nameof(ByProductList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
                RaisePropertyChangedEvent(nameof(taredef));
                RaisePropertyChangedEvent(nameof(boxtare));

                /// *************************************
                /// Show customer label.
                /// *************************************
                //_pdCusPackedPrint = Facade.pdCustomerPackedPrintBL()
                //    .GetByPackedGradeAndCustomer(_packedgrade.packedgrade1,
                //    _packedgrade.customer);
                //RaisePropertyChangedEvent(nameof(PDCusPackedPrint));

                if (_pdCusPackedPrint != null)
                    PDCustomerPackedPrintBinding(_pdCusPackedPrint);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void PdListBinding()
        {
            if (_pdsetup == null)
                return;

            _pdList = Facade.pdBL()
                    .GetByFromPdno(_pdsetup.pdno)
                    .Where(x => x.pdtype != "Lamina" && x.pdtype != "Remnant")
                    .ToList();

            if (_pdList.Count() > 0)
            {
                if (_slpackedgrade != null)
                {
                    _slCase = _pdList
                        .Where(x => x.grade == _slpackedgrade.packedgrade1)
                        .Count();
                    _slNet = (decimal)(_slCase * _slpackedgrade.netdef);
                }
                if (_sl2packedgrade != null)
                {
                    _sl2Case = _pdList
                            .Where(x => x.grade == _sl2packedgrade.packedgrade1)
                            .Count();
                    _sl2Net = (decimal)(_sl2Case * _sl2packedgrade.netdef);
                }
                if (_sspackedgrade != null)
                {
                    _ssCase = _pdList
                            .Where(x => x.grade == _sspackedgrade.packedgrade1)
                            .Count();
                    _ssNet = (decimal)(_ssCase * _sspackedgrade.netdef);
                }
                if (_flpackedgrade != null)
                {
                    _flCase = _pdList
                            .Where(x => x.grade == _flpackedgrade.packedgrade1)
                            .Count();
                    _flNet = (decimal)(_flCase * _flpackedgrade.netdef);
                }
                if (_fspackedgrade != null)
                {
                    _fsCase = _pdList
                            .Where(x => x.grade == _packedgrade.packedgrade1)
                            .Count();
                    _fsNet = (decimal)(_fsCase * _packedgrade.netdef);
                }
            }

            RaisePropertyChangedEvent(nameof(pdList));
            RaisePropertyChangedEvent(nameof(SLCase));
            RaisePropertyChangedEvent(nameof(SL2Case));
            RaisePropertyChangedEvent(nameof(SSCase));
            RaisePropertyChangedEvent(nameof(FLCase));
            RaisePropertyChangedEvent(nameof(FSCase));
            RaisePropertyChangedEvent(nameof(SLNet));
            RaisePropertyChangedEvent(nameof(SL2Net));
            RaisePropertyChangedEvent(nameof(SSNet));
            RaisePropertyChangedEvent(nameof(FLNet));
            RaisePropertyChangedEvent(nameof(FSNet));
        }

        private void PDCustomerPackedPrintBinding(PDCustomerPackedPrint model)
        {
            try
            {
                if (model == null)
                {
                    _pdCusPackedPrint = null;
                    _cusRunNo = null;
                    _accuRunno = null;
                    _caseRunNo = null;
                    _customerBarcode = "";
                }
                else
                {
                    var byProductList = _pdList
                            .Where(x => x.grade == _byProductPackedGrade.packedgrade1 &&
                            x.caseno != 0)
                            .ToList();

                    var caseInPdnoList = byProductList
                        .Where(x => x.frompdno == pdsetup.pdno)
                        .ToList();
                    _caseRunNo = caseInPdnoList.Count() > 0 ? (int)(caseInPdnoList.Max(x => x.CaseRunno) + 1) : 1;
                    _accuRunno = byProductList.Count() > 0 ? (int)(byProductList.Max(x => x.AccuRunno) + 1) : 1;

                    var cusRunnoList = byProductList
                        .GroupBy(x => new { x.CusRunno, x.frompdno })
                        .Select(x => new
                        {
                            pdno = x.Key.frompdno,
                            CusRunno = x.Key.CusRunno
                        }).ToList();

                    _cusRunNo = 1;
                    if (cusRunnoList.Count() > 0)
                    {
                        if (caseInPdnoList.Count() > 0)
                            /// Not first box of this pdno.
                            _cusRunNo = (int)caseInPdnoList.FirstOrDefault().CusRunno;
                        else
                            /// First box of this pdno.
                            _cusRunNo = (int)caseInPdnoList.Max(x => x.CusRunno) + 1;
                    }
                }

                RaisePropertyChangedEvent(nameof(CaseRunNo));
                RaisePropertyChangedEvent(nameof(AccuRunno));
                RaisePropertyChangedEvent(nameof(CusRunNo));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void SetCustomerBarcode(int? accuRunno)
        {
            try
            {
                _customerBarcode = _pdCusPackedPrint.CusPrefixFormat + accuRunno.ToString().PadLeft(5, '0');
                RaisePropertyChangedEvent(nameof(CustomerBarcode));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
