﻿using FactoryBL;
using FactoryBL.Helper;
using FactoryBL.Model;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.MVVM;
using FactorySystemWPF.View.GreenLeaf.Regrade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FactorySystemWPF.ViewModel.GreenLeaf.Regrade
{
    public class vm_CallData : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_CallData()
        {
            _crop = DateTime.Now.Year;
            _typeList = Facade.typeBL().GetAll();
            RaisePropertyChangedEvent(nameof(crop));
            RaisePropertyChangedEvent(nameof(TypeList));
        }


        #region Properties
        private int _crop;

        public int crop
        {
            get { return _crop; }
            set
            {
                _crop = value;
                RegradeNoListBinding();
            }
        }

        private string _type;

        public string type
        {
            get { return _type; }
            set
            {
                _type = value;
                RegradeNoListBinding();
            }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private m_matrc _selectedRow;

        public m_matrc selectedRow
        {
            get { return _selectedRow; }
            set { _selectedRow = value; }
        }

        private CallData _window;

        public CallData window
        {
            get { return _window; }
            set { _window = value; }
        }
        #endregion



        #region List
        private List<type> _typeList;

        public List<type> TypeList
        {
            get { return _typeList; }
            set { _typeList = value; }
        }

        private List<m_matrc> _regradeNoList;

        public List<m_matrc> RegradeNoList
        {
            get { return _regradeNoList; }
            set { _regradeNoList = value; }
        }
        #endregion



        #region Command
        private ICommand _selectedRowCommand;

        public ICommand SelectedRowCommand
        {
            get { return _selectedRowCommand ?? (_selectedRowCommand = new RelayCommand(SelectedRow)); }
            set { _selectedRowCommand = value; }
        }

        private void SelectedRow(object obj)
        {
            try
            {
                if (obj == null)
                    return;

                _selectedRow = (m_matrc)obj;
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void RegradeNoListBinding()
        {
            try
            {
                _regradeNoList = h_matrc.GetByType(_crop, _type, "Regrade")
                    .OrderByDescending(x => x.rgno)
                    .ToList();
                _totalRecord = _regradeNoList.Count();
                RaisePropertyChangedEvent(nameof(RegradeNoList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
