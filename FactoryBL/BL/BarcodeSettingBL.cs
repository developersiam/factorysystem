﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IBarcodeSettingBL
    {
        void Add(string packedgrade, Guid templateID, int printerID, int fromCaseno, int toCaseno, short copies, string createUser);
        void Delete(Guid id);
        void Edit(Guid id, string packedgrade, Guid templateID, int printerID, int fromCaseno, int toCaseno, short copies, string editUser);
        BarcodeSetting GetSingle(Guid id);
        BarcodeSetting GetByCurrentCaseNo(string packedgrade, int caseno);
        List<BarcodeSetting> GetAll();
        List<BarcodeSetting> GetByGrade(string packedgrade);
        List<BarcodeSetting> GetByTemplate(Guid templateID);
        List<BarcodeSetting> GetByTemplateName(string fileName);
    }

    public class BarcodeSettingBL : IBarcodeSettingBL
    {
        StecDBMSUnitOfWork uow;

        public BarcodeSettingBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public void Add(string packedgrade, Guid templateID, int printerID, int fromCaseno, int toCaseno, short copies, string createUser)
        {
            try
            {
                if (fromCaseno > toCaseno)
                    throw new ArgumentException("from case no ต้องน้อยกว่า to case no");

                if (toCaseno < fromCaseno)
                    throw new ArgumentException("to case no ต้องมากกว่า from case no");

                if (fromCaseno <= 0)
                    throw new ArgumentException("from case no ไม่ควรน้อยกว่า 1");

                if (copies <= 0)
                    throw new ArgumentException("โปรดระบุจำนวนที่จะพิมพ์");

                var settingList = uow.barcodeSettingRepo.Get(x => x.PackedGrade == packedgrade);
                if (settingList
                        .Where(x => fromCaseno >= x.FromCaseNo &&
                        fromCaseno <= x.ToCaseNo)
                        .Count() > 0)
                    throw new ArgumentException("ในยาเกรดเดียวกัน ช่วงของ caseno ไม่ควรมีค่าที่ทับซ้อนกัน " +
                        "หากต้องการกำหนดช่วงของ caseno ใหม่ให้ลบของเดิมออกก่อนตามลำดับ");

                if (settingList
                    .Where(x => toCaseno >= x.FromCaseNo &&
                    toCaseno <= x.ToCaseNo)
                    .Count() > 0)
                    throw new ArgumentException("ในยาเกรดเดียวกัน ช่วงของ caseno ไม่ควรมีค่าที่ทับซ้อนกัน " +
                        "หากต้องการกำหนดช่วงของ caseno ใหม่ให้ลบของเดิมออกก่อนตามลำดับ");

                uow.barcodeSettingRepo.Add(new BarcodeSetting
                {
                    ID = Guid.NewGuid(),
                    PackedGrade = packedgrade,
                    TemplateID = templateID,
                    PrinterID = printerID,
                    FromCaseNo = fromCaseno,
                    ToCaseNo = toCaseno,
                    Copies = copies,
                    CreateBy = createUser,
                    CreateDate = DateTime.Now,
                    ModifiedBy = createUser,
                    ModifiedDate = DateTime.Now
                });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var item = GetSingle(id);
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                uow.barcodeSettingRepo.Remove(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(Guid id, string packedgrade, Guid templateID, int printerID, int fromCaseno, int toCaseno,short copies, string editUser)
        {
            try
            {
                if (fromCaseno > toCaseno)
                    throw new ArgumentException("from case no ต้องน้อยกว่า to case no");

                if (toCaseno < fromCaseno)
                    throw new ArgumentException("to case no ต้องมากกว่า from case no");

                if (fromCaseno <= 0)
                    throw new ArgumentException("from case no ไม่ควรน้อยกว่า 1");

                if (copies <= 0)
                    throw new ArgumentException("โปรดระบุจำนวนที่จะพิมพ์");

                var item = GetSingle(id);
                if (item == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (item.FromCaseNo != fromCaseno || item.ToCaseNo != toCaseno)
                {
                    ///กรณีแก้ไข from case no และ to case no ให้ตรวจสอบความทัยซ้อนของช่วง case no
                    ///
                    var settingList = uow.barcodeSettingRepo.Get(x => x.PackedGrade == packedgrade);
                    if (settingList
                        .Where(x => fromCaseno >= x.FromCaseNo &&
                        fromCaseno <= x.ToCaseNo)
                        .Count() > 0)
                        throw new ArgumentException("ในยาเกรดเดียวกัน ช่วงของ caseno ไม่ควรมีค่าที่ทับซ้อนกัน " +
                            "หากต้องการกำหนดช่วงของ caseno ใหม่ให้ลบของเดิมออกก่อนตามลำดับ");

                    if (settingList
                        .Where(x => toCaseno >= x.FromCaseNo &&
                        toCaseno <= x.ToCaseNo)
                        .Count() > 0)
                        throw new ArgumentException("ในยาเกรดเดียวกัน ช่วงของ caseno ไม่ควรมีค่าที่ทับซ้อนกัน " +
                            "หากต้องการกำหนดช่วงของ caseno ใหม่ให้ลบของเดิมออกก่อนตามลำดับ");
                }

                item.PackedGrade = packedgrade;
                item.TemplateID = templateID;
                item.PrinterID = printerID;
                item.FromCaseNo = fromCaseno;
                item.ToCaseNo = toCaseno;
                item.Copies = copies;
                item.ModifiedBy = editUser;
                item.ModifiedDate = DateTime.Now;

                uow.barcodeSettingRepo.Update(item);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BarcodeSetting> GetAll()
        {
            return uow.barcodeSettingRepo
                .Get(null,
                null,
                x => x.BarcodeTemplate,
                x => x.BarcodePrinter);
        }

        public BarcodeSetting GetByCurrentCaseNo(string packedgrade, int caseno)
        {
            return uow.barcodeSettingRepo
                .GetSingle(x => x.PackedGrade == packedgrade &&
                caseno >= x.FromCaseNo &&
                caseno <= x.ToCaseNo,
                x => x.BarcodePrinter,
                x => x.BarcodeTemplate);
        }

        public List<BarcodeSetting> GetByGrade(string packedgrade)
        {
            return uow.barcodeSettingRepo.Get(x => x.PackedGrade == packedgrade, null, x => x.BarcodeTemplate);
        }

        public List<BarcodeSetting> GetByTemplate(Guid templateID)
        {
            return uow.barcodeSettingRepo.Get(x => x.TemplateID == templateID, null, x => x.BarcodeTemplate);
        }

        public List<BarcodeSetting> GetByTemplateName(string fileName)
        {
            return uow.barcodeSettingRepo.Get(x => x.BarcodeTemplate.FileName == fileName, null, x => x.BarcodeTemplate);
        }

        public BarcodeSetting GetSingle(Guid id)
        {
            return uow.barcodeSettingRepo.GetSingle(x => x.ID == id, x => x.BarcodeTemplate);
        }
    }
}
