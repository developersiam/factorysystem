﻿using FactoryBL;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FactorySystemWPF.ViewModel.Processing.ByProduct
{
    public class vm_Setup : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_Setup()
        {

        }


        #region Properties
        private string _pdno;

        public string pdno
        {
            get { return _pdno; }
            set { _pdno = value; }
        }

        private pdsetup _pdsetup;

        public pdsetup pdsetup
        {
            get { return _pdsetup; }
            set { _pdsetup = value; }
        }

        private sfsetup _sfsetup;

        public sfsetup sfsetup
        {
            get { return _sfsetup; }
            set { _sfsetup = value; }
        }

        private packedgrade _stlpackedgrade;

        public packedgrade STLpackedgrade
        {
            get { return _stlpackedgrade; }
            set { _stlpackedgrade = value; }
        }

        private packedgrade _stl2packedgrade;

        public packedgrade STL2packedgrade
        {
            get { return _stl2packedgrade; }
            set { _stl2packedgrade = value; }
        }

        private packedgrade _stspackedgrade;

        public packedgrade STSpackedgrade
        {
            get { return _stspackedgrade; }
            set { _stspackedgrade = value; }
        }

        private packedgrade _flpackedgrade;

        public packedgrade FLpackedgrade
        {
            get { return _flpackedgrade; }
            set { _flpackedgrade = value; }
        }

        private packedgrade _fspackedgrade;

        public packedgrade FSpackedgrade
        {
            get { return _fspackedgrade; }
            set { _fspackedgrade = value; }
        }
        #endregion



        #region List

        #endregion



        #region Command
        private ICommand _okCommand;

        public ICommand OKCommand
        {
            get { return _okCommand ?? (_okCommand = new RelayCommand(OK)); }
            set { _okCommand = value; }
        }

        private void OK(object obj)
        {
            throw new NotImplementedException();
        }

        private ICommand _cancelCommand;

        public ICommand CancelCommand
        {
            get { return _cancelCommand ?? (_cancelCommand = new RelayCommand(Cancel)); }
            set { _cancelCommand = value; }
        }

        private void Cancel(object obj)
        {
            OnExit();
        }

        private ICommand _enterCommand;

        public ICommand EnterCommand
        {
            get { return _enterCommand ?? (_enterCommand = new RelayCommand(Enter)); }
            set { _enterCommand = value; }
        }

        private void Enter(object obj)
        {
            OnEnter();
        }

        private ICommand _escCommand;

        public ICommand ESCCommand
        {
            get { return _escCommand ?? (_escCommand = new RelayCommand(ESC)); }
            set { _escCommand = value; }
        }

        private void ESC(object obj)
        {
            OnExit();
        }
        #endregion



        #region Function
        private void OnLoad()
        {
            try
            {
                if (string.IsNullOrEmpty(_pdno))
                    return;

                _pdsetup = Facade.pdsetupBL().GetSingle(_pdno);
                if (_pdsetup == null)
                    throw new ArgumentException("pdsetup not found.");

                _sfsetup = Facade.sfsetupBL().GetSingle();
                if (_sfsetup == null)
                    return;

                _stlpackedgrade = Facade.packedgradeBL().GetSingle(_sfsetup.slgrade);
                if (_stlpackedgrade == null)
                    return;

                _stl2packedgrade = Facade.packedgradeBL().GetSingle(_sfsetup.sl2grade);
                if (_stl2packedgrade == null)
                    return;

                _stspackedgrade = Facade.packedgradeBL().GetSingle(_sfsetup.ssgrade);
                if (_stspackedgrade == null)
                    return;

                _flpackedgrade = Facade.packedgradeBL().GetSingle(_sfsetup.flgrade);
                if (_flpackedgrade == null)
                    return;

                _fspackedgrade = Facade.packedgradeBL().GetSingle(_sfsetup.fsgrade);
                if (_fspackedgrade == null)
                    return;

                RaisePropertyChangedEvent(nameof(pdsetup));
                RaisePropertyChangedEvent(nameof(sfsetup));
                RaisePropertyChangedEvent(nameof(STLpackedgrade));
                RaisePropertyChangedEvent(nameof(STL2packedgrade));
                RaisePropertyChangedEvent(nameof(STSpackedgrade));
                RaisePropertyChangedEvent(nameof(FLpackedgrade));
                RaisePropertyChangedEvent(nameof(FSpackedgrade));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OnEnter()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OnCancel()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OnExit()
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
