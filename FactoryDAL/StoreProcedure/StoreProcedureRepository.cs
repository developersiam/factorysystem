﻿using FactoryDAL.EDMX;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryDAL.StoreProcedure
{
    public static class StoreProcedureRepository
    {
        public static string sp_Receiving_DecodePassword(string password)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_Receiving_DecodePassword(password).FirstOrDefault().ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<sp_PD_SEL_PackedCasesByGrade_Result> sp_PD_SEL_PackedCasesByGrade(string grade)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_PD_SEL_PackedCasesByGrade(grade).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void sp_Receiving_UPD_CheckerLocked(string rcno, string checker, bool checkerapproved, bool checkerlocked)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    _context.sp_Receiving_UPD_CheckerLocked(rcno, checker, checkerapproved, checkerlocked);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void sp_Receiving_UPD_RCBFinished(string rcno, bool locked)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    _context.sp_Receiving_UPD_RCBFinished(rcno, locked);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void sp_Receiving_UPD_DocNo(int crop, string rcno, string supplier, string subtype, string company)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    _context.sp_Receiving_UPD_DocNo(crop, rcno, supplier, subtype, company);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static List<sp_Shipping_SEL_ShippingMovementV2023_Result> sp_Shipping_SEL_ShippingMovementV2023(int crop, string fromLocation)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_Shipping_SEL_ShippingMovementV2023(crop, fromLocation).ToList();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static sp_Shipping_SEL_ShippingMovementByMovementNo_Result sp_Shipping_SEL_ShippingMovementByMovementNo(string movementNo)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_Shipping_SEL_ShippingMovementByMovementNo(movementNo).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static sp_Shipping_SEL_BarcodeDetailFromPC_Result sp_Shipping_SEL_BarcodeDetailFromPC(string bc)
        {
            try
            {
                using (StecDBMSEntities _context = new StecDBMSEntities())
                {
                    return _context.sp_Shipping_SEL_BarcodeDetailFromPC(bc).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
