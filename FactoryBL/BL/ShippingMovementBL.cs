﻿using FactoryDAL.StoreProcedure;
using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IShippingMovementBL
    {
        void Add(int crop, string truckID, string personID, string fromLocation, string toLocation, string username);
        void ChangeDriverAndTruck(string movementNo, string personID, string truckID, string user);
        List<sp_Shipping_SEL_ShippingMovementV2023_Result> GetShippingMovementV2023(int crop, string fromLocation);
        ShippingMovement GetSingle(string movementNo);
        sp_Shipping_SEL_ShippingMovementByMovementNo_Result GetShippingMovementByMovementNo(string movementNo);
        sp_Shipping_SEL_BarcodeDetailFromPC_Result GetBarcodeInformation(string bc);
    }
    public class ShippingMovementBL : IShippingMovementBL
    {
        IStecDBMSUnitOfWork uow;
        public ShippingMovementBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public void Add(int crop, string truckID, string personID, string fromLocation, string toLocation, string username)
        {
            try
            {
                if (string.IsNullOrEmpty(truckID))
                    throw new Exception("Truck ID cannot be empty.");

                if (string.IsNullOrEmpty(personID))
                    throw new Exception("Person ID cannot be empty.");

                if (string.IsNullOrEmpty(fromLocation))
                    throw new Exception("From location cannot be empty.");

                if (string.IsNullOrEmpty(toLocation))
                    throw new Exception("To location cannot be empty.");

                if (string.IsNullOrEmpty(username))
                    throw new Exception("Username cannot be empty.");

                var maxDocumentNo = 0;
                var runningList = uow.shippingMovementRepo
                    .Get(x => x.Crop == crop)
                    .Select(x => new
                    {
                        RuningNo = Convert.ToInt16(x.MovementNo.Substring(x.MovementNo.Length - 4, 4))
                    })
                    .ToList();
                if (runningList.Count() > 0)
                    maxDocumentNo = runningList.Max(x => x.RuningNo) + 1;
                else
                    maxDocumentNo = maxDocumentNo + 1;

                var movementNo = crop.ToString().Substring(2, 2) + "-" + maxDocumentNo.ToString().PadLeft(4, '0');
                uow.shippingMovementRepo
                    .Add(new ShippingMovement
                    {
                        Crop = crop,
                        DateStart = DateTime.Now,
                        MovementFrom = fromLocation,
                        MovementTo = toLocation,
                        MovementFromUser = username,
                        MovementNo = movementNo,
                        TruckID = truckID,
                        PersonID = personID,
                        MovementFromFinished = false,
                        MovementStatusID = false
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ChangeDriverAndTruck(string movementNo, string personID, string truckID, string user)
        {
            try
            {
                if (string.IsNullOrEmpty(movementNo))
                    throw new Exception("Movement no cannot be empty.");

                if (string.IsNullOrEmpty(truckID))
                    throw new Exception("Truck no cannot be empty.");

                if (string.IsNullOrEmpty(personID))
                    throw new Exception("Truck driver cannot be empty.");

                if (string.IsNullOrEmpty(user))
                    throw new Exception("Username cannot be empty. Please login.");

                var editRow = GetSingle(movementNo);
                if (editRow == null)
                    throw new Exception("ไม่พบข้อมูล movement no นี้ในระบบ");

                if (editRow.DateReceived != null)
                    throw new Exception("Movement no นี้ถูก received แล้วจึงไม่สามารถแก้ไขข้อมูลได้");

                editRow.TruckID = truckID;
                editRow.PersonID = personID;
                editRow.ReceivedUser = user;

                uow.shippingMovementRepo.Update(editRow);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public sp_Shipping_SEL_BarcodeDetailFromPC_Result GetBarcodeInformation(string bc)
        {
            return StoreProcedureRepository.sp_Shipping_SEL_BarcodeDetailFromPC(bc);
        }

        public sp_Shipping_SEL_ShippingMovementByMovementNo_Result GetShippingMovementByMovementNo(string movementNo)
        {
            return StoreProcedureRepository.sp_Shipping_SEL_ShippingMovementByMovementNo(movementNo);
        }

        public List<sp_Shipping_SEL_ShippingMovementV2023_Result> GetShippingMovementV2023(int crop, string fromLocation)
        {
            return StoreProcedureRepository.sp_Shipping_SEL_ShippingMovementV2023(crop, fromLocation);
        }

        public ShippingMovement GetSingle(string movementNo)
        {
            return uow.shippingMovementRepo.GetSingle(x => x.MovementNo == movementNo);
        }
    }
}
