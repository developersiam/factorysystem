﻿using FactoryBL;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.Model;
using FactorySystemWPF.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace FactorySystemWPF.ViewModel.Processing.ProductionControl
{
    public class vm_BarcodePrinter : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_BarcodePrinter()
        {
            Clear();
        }


        #region Properties
        private string printerName;

        public string PrinterName
        {
            get { return printerName; }
            set { printerName = value; }
        }

        private BarcodePrinter barcodePrinter;

        public BarcodePrinter BarcodePrinter
        {
            get { return barcodePrinter; }
            set { barcodePrinter = value; }
        }

        private Visibility addButtonVisibility;

        public Visibility AddButtonVisibility
        {
            get { return addButtonVisibility; }
            set { addButtonVisibility = value; }
        }

        private Visibility editButtonVisibility;

        public Visibility EditButtonVisibility
        {
            get { return editButtonVisibility; }
            set { editButtonVisibility = value; }
        }

        #endregion



        #region List
        private List<BarcodePrinter> barcodePrinters;

        public List<BarcodePrinter> BarcodePrinters
        {
            get { return barcodePrinters; }
            set { barcodePrinters = value; }
        }
        #endregion



        #region Command
        private ICommand addCommand;

        public ICommand AddCommand
        {
            get { return addCommand ?? (addCommand = new RelayCommand(Add)); }
            set { addCommand = value; }
        }

        private void Add(object obj)
        {
            Add();
        }

        private ICommand editCommand;

        public ICommand EditCommand
        {
            get { return editCommand ?? (editCommand = new RelayCommand(Edit)); }
            set { editCommand = value; }
        }

        private void Edit(object obj)
        {
            if (barcodePrinter == null)
                throw new ArgumentException("โปรดคลิกเลือกเครื่องพิมพ์ที่ต้องการลบจากตารางด้านล่าง");

            Edit();
        }

        private ICommand clearFormCommand;

        public ICommand ClearFormCommand
        {
            get { return clearFormCommand ?? (clearFormCommand = new RelayCommand(ClearForm)); }
            set { clearFormCommand = value; }
        }

        private void ClearForm(object obj)
        {
            Clear();
        }

        private ICommand selectedEditCommand;

        public ICommand SelectedEditCommand
        {
            get { return selectedEditCommand ?? (selectedEditCommand = new RelayCommand(SelectedEdit)); }
            set { selectedEditCommand = value; }
        }

        private void SelectedEdit(object obj)
        {
            try
            {
                if (obj == null)
                    return;

                barcodePrinter = (BarcodePrinter)obj;
                printerName = barcodePrinter.PrinterName;
                addButtonVisibility = Visibility.Collapsed;
                editButtonVisibility = Visibility.Visible;
                RaisePropertyChangedEvent(nameof(BarcodePrinter));
                RaisePropertyChangedEvent(nameof(PrinterName));
                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
                RaisePropertyChangedEvent(nameof(EditButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand deleteCommand;

        public ICommand DeleteCommand
        {
            get { return deleteCommand ?? (deleteCommand = new RelayCommand(Delete)); }
            set { deleteCommand = value; }
        }

        private void Delete(object obj)
        {
            if (obj == null)
                return;

            barcodePrinter = (BarcodePrinter)obj;
            Delete();
        }


        #endregion



        #region Function
        private void Add()
        {
            try
            {
                if (string.IsNullOrEmpty(printerName))
                {
                    MessageBoxHelper.Warning("โปรดใส่ชื่อเครื่องพิมพ์บาร์โค้ต");
                    OnFocusRequested(nameof(PrinterName));
                    return;
                }
                Facade.BarcodePrinterBL()
                    .Add(PrinterName, user_setting.security.uname);
                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Edit()
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการแก้ไขชื่อเครื่องพิมพ์นี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;
                Facade.BarcodePrinterBL()
                    .Update(barcodePrinter.PrinterID, printerName, user_setting.security.uname);
                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Delete()
        {
            try
            {
                if (barcodePrinter == null)
                    throw new ArgumentException("โปรดคลิกเลือกเครื่องพิมพ์ที่ต้องการลบจากตารางด้านล่าง");

                if (MessageBoxHelper.Question("ท่านต้องการลบเครื่องพิมพ์นี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                Facade.BarcodePrinterBL().Delete(barcodePrinter.PrinterID);
                MessageBoxHelper.Info("ลบข้อมูลสำเร็จ");
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Clear()
        {
            try
            {
                printerName = "";
                barcodePrinter = null;
                addButtonVisibility = Visibility.Visible;
                editButtonVisibility = Visibility.Collapsed;
                RaisePropertyChangedEvent(nameof(PrinterName));
                RaisePropertyChangedEvent(nameof(BarcodePrinter));
                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
                RaisePropertyChangedEvent(nameof(EditButtonVisibility));
                OnFocusRequested(nameof(PrinterName));
                PrinterListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PrinterListBinding()
        {
            try
            {
                barcodePrinters = Facade.BarcodePrinterBL()
                    .GetAll()
                    .OrderBy(x => x.PrinterName)
                    .ToList();
                RaisePropertyChangedEvent(nameof(BarcodePrinters));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
