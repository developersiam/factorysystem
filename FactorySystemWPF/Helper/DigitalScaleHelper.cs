﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace FactorySystemWPF.Helper
{
    public struct ComportParameter
    {
        public string PortName { get; set; }
        public int BaudRate { get; set; }
        public Parity Parity { get; set; }
        public short DataBits { get; set; }
        public StopBits StopBits { get; set; }
        public short StrLength { get; set; }
        public string Model { get; set; }
    }

    public static class DigitalScaleHelper
    {
        public static SerialPort serialPort = new SerialPort();
        static string _lineReadIn;
        static short _strLength;
        static int _threadSleep;

        //private delegate void preventCrossThreading(string x);
        //private static preventCrossThreading accessControlFromCentralThread;

        public static void Setup()
        {
            try
            {
                if (string.IsNullOrEmpty(Properties.Settings.Default.Model))
                    throw new ArgumentException("โปรดระบุยี่ห้อ/รุ่นเครื่องชั่งที่จะใช้งาน");

                var ports = SerialPort.GetPortNames();
                if (ports.Count() <= 0)
                    throw new ArgumentException("ไม่พบพอร์ตสำหรับการเขื่อมต่อเครื่องชั่งบนเครื่องคอมพิวเตอร์ของท่าน");

                var hasPortName = false;
                for (int i = 0; i < ports.Length; i++)
                {
                    if (ports[i] == Properties.Settings.Default.PortName)
                    {
                        hasPortName = true;
                        break;
                    }
                }

                if (!hasPortName)
                    throw new ArgumentException("ไม่พบพอร์ต " + Properties.Settings.Default.PortName +
                        " เชื่อมต่อกับเครื่องคอมพิวเตอร์ของคุณ โปรดตรวจสอบการตั้งค่าเครื่องชั่งอีกครั้ง");

                serialPort.PortName = Properties.Settings.Default.PortName;
                serialPort.BaudRate = Properties.Settings.Default.BaudRate;
                serialPort.Parity = Properties.Settings.Default.Parity;
                serialPort.DataBits = Properties.Settings.Default.DataBit;
                serialPort.StopBits = Properties.Settings.Default.StopBits;

                _threadSleep = Properties.Settings.Default.ThreadSleep;
                _strLength = Properties.Settings.Default.SubStringLength;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetWeightFromTiger_Commando()
        {
            try
            {
                _lineReadIn += serialPort.ReadExisting();
                Thread.Sleep(_threadSleep);

                /// display what we've acquired.
                string result = _lineReadIn;

                if (result.Length <= _strLength)
                    return "0";

                string[] splitString;
                string[] stringSeparators = new string[] { "\r\n" };

                splitString = result.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                if (splitString.Count() < 2)
                    return "0";

                result = splitString[1];

                result = result.Replace(" ", string.Empty);
                result = result.Replace("", string.Empty);
                
                /// replace charctor ทีละตัว ตัวไหนที่ไม่ใช่ตัวเลข จุด และเครื่องหมายลบ ให้ทำการ replace ด้วย ""
                /// 
                char[] replaceOperator = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', '.', '-' };
                foreach (var str in result)
                {
                    bool flag = true;
                    foreach (var opr in replaceOperator)
                    {
                        if (str == opr)
                        {
                            flag = false;
                            break;
                        }
                    }

                    if (flag == true)
                        result = result.Replace(str.ToString(), string.Empty);
                }

                /// ตรวจสอบว่าข้อมูลที่ตัดออกมากไม่อยู่ในรูปแบบ decimal ให้ return เลข Error ออกไปแสดงที่หน้าจอ
                decimal resultWeight = 0;
                if (result != "")
                {
                    decimal value;
                    if (Decimal.TryParse(result, out value))
                    {
                        resultWeight = Convert.ToDecimal(result);
                        result = resultWeight.ToString("N1");
                    }
                    else
                    {
                        return "Scale Error";
                    }
                }

                /// Clear string from digital scale sended.
                _lineReadIn = "";
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetWeightFromMETTLER_TOLEDO_IND221()
        {
            try
            {
                _lineReadIn += serialPort.ReadExisting();
                Thread.Sleep(_threadSleep);

                if (_lineReadIn.Length <= 100)
                    return "0";

                string[] splitString;
                string[] stringSeparators = new string[] { "\r" };

                splitString = _lineReadIn.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                if (splitString.Count() < 5)
                    return "0";

                var array = splitString[3].Split(' ');
                array = array.Where(x => x != "").ToArray();
                if (array.Count() != 3)
                    return "0";

                var prefix = array[0];
                var val = array[1];
                var tare = array[2];

                if (string.IsNullOrEmpty(prefix) || string.IsNullOrEmpty(val) || string.IsNullOrEmpty(tare))
                    return "0";

                /// display what we've acquired.
                string result = (Convert.ToInt32(val) / 10) + "." + (Convert.ToInt32(val) % 10);

                if (prefix.Contains("+3"))
                    result = "-" + result;
                else if (prefix.Contains("+;"))
                    result = "-" + result;

                /// ตรวจสอบว่าข้อมูลที่ตัดออกมากไม่อยู่ในรูปแบบ decimal ให้ return เลข Error ออกไปแสดงที่หน้าจอ
                decimal resultWeight = 0;
                if (result != "")
                {
                    decimal value;
                    if (Decimal.TryParse(result, out value))
                    {
                        resultWeight = Convert.ToDecimal(result);
                        result = resultWeight.ToString("N1");
                    }
                    else
                    {
                        return "Scale Error";
                    }
                }

                /// Clear string from digital scale sended.
                _lineReadIn = "";
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static string GetWeightFromMETTLER_TOLEDO_Spider2()
        {
            try
            {
                _lineReadIn += serialPort.ReadExisting();
                Thread.Sleep(_threadSleep);

                string[] splitString;
                string[] stringSeparators = new string[] { "\r\n" };

                splitString = _lineReadIn.Split(stringSeparators, StringSplitOptions.RemoveEmptyEntries);

                if (splitString.Count() < 3)
                    return "0";

                if (splitString[1].Length < 17)
                    return "0";

                var array = splitString[1].Split(' ');
                string result = array[array.Count() - 3];

                /// ตรวจสอบว่าข้อมูลที่ตัดออกมากไม่อยู่ในรูปแบบ decimal ให้ return เลข Error ออกไปแสดงที่หน้าจอ
                decimal resultWeight = 0;
                if (result != "")
                {
                    decimal value;
                    if (Decimal.TryParse(result, out value))
                    {
                        resultWeight = Convert.ToDecimal(result);
                        result = resultWeight.ToString("N1");
                    }
                    else
                    {
                        return "Scale Error";
                    }
                }

                /// Clear string from digital scale sended.
                _lineReadIn = "";
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static bool CheckPort()
        {
            var ports = SerialPort.GetPortNames();
            if (ports.Count() <= 0)
                return false;
            else
                return true;
        }
    }
}
