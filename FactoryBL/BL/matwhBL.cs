﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface ImatwhBL
    {
        List<matwh> GetAll();
    }

    public class matwhBL : ImatwhBL
    {
        IStecDBMSUnitOfWork uow;
        public matwhBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public List<matwh> GetAll()
        {
            return uow.matwhRepo.Get();
        }
    }
}
