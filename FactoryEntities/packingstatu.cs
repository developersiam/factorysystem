//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FactoryEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class packingstatu
    {
        public Nullable<int> crop { get; set; }
        public string pdno { get; set; }
        public Nullable<System.DateTime> pddate { get; set; }
        public int packinghour { get; set; }
        public Nullable<bool> locked { get; set; }
    }
}
