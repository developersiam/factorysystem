﻿using FactoryBL;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.Model;
using FactorySystemWPF.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace FactorySystemWPF.ViewModel.Shared
{
    public class vm_BarcodePrinter : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_BarcodePrinter()
        {
            _addButtonVisibility = Visibility.Visible;
            _editButtonVisibility = Visibility.Collapsed;

            RaisePropertyChangedEvent(nameof(AddButtonVisibility));
            RaisePropertyChangedEvent(nameof(EditButtonVisibility));

            PrinterListBinding();
        }


        #region Properties
        private int _printerID;

        public int PrinterID
        {
            get { return _printerID; }
            set { _printerID = value; }
        }

        private string _printerName;

        public string PrinterName
        {
            get { return _printerName; }
            set { _printerName = value; }
        }

        private Visibility _addButtonVisibility;

        public Visibility AddButtonVisibility
        {
            get { return _addButtonVisibility; }
            set { _addButtonVisibility = value; }
        }

        private Visibility _editButtonVisibility;

        public Visibility EditButtonVisibility
        {
            get { return _editButtonVisibility; }
            set { _editButtonVisibility = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        #endregion



        #region List
        private List<BarcodePrinter> _printerList;

        public List<BarcodePrinter> PrinterList
        {
            get { return _printerList; }
            set { _printerList = value; }
        }
        #endregion



        #region Command
        private ICommand _addCommand;

        public ICommand AddCommand
        {
            get { return _addCommand ?? (_addCommand = new RelayCommand(Add)); }
            set { _addCommand = value; }
        }

        private void Add(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_printerName))
                {
                    MessageBoxHelper.Warning("โปรดระบุชื่อเครื่องพิมพ์");
                    OnFocusRequested(nameof(PrinterName));
                    return;
                }

                Facade.BarcodePrinterBL()
                    .Add(_printerName, user_setting.security.uname);
                PrinterListBinding();
                OnFocusRequested(nameof(PrinterName));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _editCommand;

        public ICommand EditCommand
        {
            get { return _editCommand ?? (_editCommand = new RelayCommand(Edit)); }
            set { _editCommand = value; }
        }

        private void Edit(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_printerName))
                {
                    MessageBoxHelper.Warning("โปรดระบุชื่อเครื่องพิมพ์");
                    OnFocusRequested(nameof(PrinterName));
                    return;
                }

                if (MessageBoxHelper.Question("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                Facade.BarcodePrinterBL()
                    .Update(_printerID, _printerName, user_setting.security.uname);
                PrinterListBinding();

                _editButtonVisibility = Visibility.Collapsed;
                _addButtonVisibility = Visibility.Visible;
                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
                RaisePropertyChangedEvent(nameof(EditButtonVisibility));
                OnFocusRequested(nameof(PrinterName));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _deleteCommand;

        public ICommand DeleteCommand
        {
            get { return _deleteCommand ?? (_deleteCommand = new RelayCommand(Delete)); }
            set { _deleteCommand = value; }
        }

        private void Delete(object obj)
        {
            try
            {
                var model = (BarcodePrinter)obj;
                if (model == null)
                    return;

                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                Facade.BarcodePrinterBL().Delete(model.PrinterID);
                OnFocusRequested(nameof(PrinterName));
                PrinterListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _refreshCommand;

        public ICommand RefreshCommand
        {
            get { return _refreshCommand ?? (_refreshCommand = new RelayCommand(Refresh)); }
            set { _refreshCommand = value; }
        }

        private void Refresh(object obj)
        {
            _printerName = "";
            _editButtonVisibility = Visibility.Collapsed;
            _addButtonVisibility = Visibility.Visible;

            RaisePropertyChangedEvent(nameof(PrinterName));
            RaisePropertyChangedEvent(nameof(AddButtonVisibility));
            RaisePropertyChangedEvent(nameof(EditButtonVisibility));
            OnFocusRequested(nameof(PrinterName));

            PrinterListBinding();
        }

        private ICommand _editSelectedCommand;

        public ICommand EditSelectedCommand
        {
            get { return _editSelectedCommand ?? (_editSelectedCommand = new RelayCommand(EditSelected)); }
            set { _editSelectedCommand = value; }
        }

        private void EditSelected(object obj)
        {
            try
            {
                var model = (BarcodePrinter)obj;
                if (model == null)
                    return;

                _printerID = model.PrinterID;
                _printerName = model.PrinterName;
                _editButtonVisibility = Visibility.Visible;
                _addButtonVisibility = Visibility.Collapsed;

                RaisePropertyChangedEvent(nameof(PrinterID));
                RaisePropertyChangedEvent(nameof(PrinterName));
                RaisePropertyChangedEvent(nameof(EditButtonVisibility));
                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion



        #region Function
        public void PrinterListBinding()
        {
            _printerList = Facade.BarcodePrinterBL().GetAll()
                .OrderBy(x => x.PrinterName)
                .ToList();
            _totalRecord = _printerList.Count();
            RaisePropertyChangedEvent(nameof(PrinterList));
            RaisePropertyChangedEvent(nameof(TotalRecord));
        }
        #endregion
    }
}
