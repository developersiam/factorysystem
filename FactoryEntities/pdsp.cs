//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FactoryEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class pdsp
    {
        public string spno { get; set; }
        public string customerref { get; set; }
        public string customer { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string stecgrade { get; set; }
        public string type { get; set; }
        public string truckno { get; set; }
        public string containerno { get; set; }
        public string sealedno { get; set; }
        public string transportcompany { get; set; }
        public string destination { get; set; }
        public string remark { get; set; }
        public Nullable<bool> locked { get; set; }
        public Nullable<System.DateTime> dtrecord { get; set; }
        public string user { get; set; }
        public string customergrade { get; set; }
        public string OrderNo { get; set; }
    }
}
