﻿using FactoryBL.Helper;
using FactoryDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.Report
{
    public interface IBlendingReportBL
    {
        void DetailReport(string pdno, int blendinghour, short copies);
        void PeriodReport(string pdno, int blendinghour, short copies);
        void DailyReport(string grade, DateTime pddate, short copies);
        void SummaryReport(string grade, short copies);
    }

    public class BlendingReportBL : IBlendingReportBL
    {
        StecDBMSUnitOfWork uow;
        public BlendingReportBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public void DetailReport(string pdno, int blendinghour, short copies)
        {
            try
            {
                if (string.IsNullOrEmpty(pdno))
                    throw new ArgumentException("โปรดระบุ grade");

                if (copies <= 0)
                    throw new ArgumentException("โปรดระบุจำนวนที่ต้องการพิมพ์");

                var blendingstatus = uow.blendingstatusRepo
                    .GetSingle(x => x.pdno == pdno &&
                    x.blendinghour == blendinghour);
                if (blendingstatus == null)
                    throw new ArgumentException("blendingstatus not found.");

                if (blendingstatus.locked == false)
                    throw new ArgumentException("ชั่วโมงนี้ยังไม่ถูก locked ไม่สามารถพิมพ์รายการได้");

                var pdsetup = uow.pdsetupRepo
                    .GetSingle(x => x.pdno == pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                var packedgrade = uow.packedgradeRepo
                    .GetSingle(x => x.packedgrade1 == pdsetup.packedgrade);
                if (packedgrade == null)
                    throw new ArgumentException("packedgrade not found.");

                var greenInput = uow.matRepo
                    .Get(x => x.topddate == pdsetup.date &&
                    x.topdhour == blendinghour &&
                    x.topdno == pdsetup.pdno)
                    .ToList();

                var pdInput = uow.pdRepo
                    .Get(x => x.topddate == pdsetup.date &&
                    x.topdhour == blendinghour &&
                    x.topdno == pdsetup.pdno)
                    .ToList();

                h_Excel.Open(@"C:\Blending\Template\DetailReport.xlt");
                h_Excel.PutDataToCell("D5", packedgrade.packedgrade1);
                h_Excel.PutDataToCell("D6", packedgrade.type);
                h_Excel.PutDataToCell("D7", packedgrade.customer);
                h_Excel.PutDataToCell("D8", packedgrade.form);
                h_Excel.PutDataToCell("D9", packedgrade.packingmat);
                h_Excel.PutDataToCell("H5", ((DateTime)pdsetup.date).ToString("dd/MM/yyyy"));
                h_Excel.PutDataToCell("I4", pdsetup.pdno);
                h_Excel.PutDataToCell("H7", blendinghour.ToString());

                var company = new string[] { "1", "2" };
                int row = 12;
                int rowNumber = 1;
                foreach (var item in greenInput.OrderBy(x => x.feedingtime))
                {
                    // Show green feed in detail.
                    var weight = (company.Contains(item.company) ? item.weightbuy : item.weight);
                    h_Excel.PutDataToCell("B" + row, rowNumber.ToString());
                    h_Excel.PutDataToCell("C" + row, item.bc);
                    h_Excel.PutDataToCell("E" + row, item.supplier);
                    h_Excel.PutDataToCell("F" + row, item.baleno.ToString());
                    h_Excel.PutDataToCell("G" + row, item.classify);
                    h_Excel.PutDataToCell("H" + row, weight.ToString());
                    h_Excel.PutDataToCell("J" + row, ((DateTime)item.feedingtime).ToLongTimeString());
                    row++;
                    rowNumber++;
                }

                rowNumber = 1;
                foreach (var item in pdInput.OrderBy(x => x.feedingtime))
                {
                    h_Excel.PutDataToCell("B" + row, rowNumber.ToString());
                    h_Excel.PutDataToCell("C" + row, item.bc);
                    h_Excel.PutDataToCell("F" + row, item.caseno.ToString());
                    h_Excel.PutDataToCell("H" + row, item.netreal.ToString());
                    h_Excel.PutDataToCell("J" + row, ((DateTime)item.feedingtime).ToLongTimeString());
                    row++;
                    rowNumber++;
                }

                row++;
                h_Excel.DeleteRow("A" + row, "A1000");
                h_Excel.Print(copies);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PeriodReport(string pdno, int blendinghour, short copies)
        {
            try
            {
                if (string.IsNullOrEmpty(pdno))
                    throw new ArgumentException("โปรดระบุ pdno");

                if (copies <= 0)
                    throw new ArgumentException("โปรดระบุจำนวนที่ต้องการพิมพ์");

                var blendingstatus = uow.blendingstatusRepo
                    .GetSingle(x => x.pdno == pdno &&
                    x.blendinghour == blendinghour);
                if (blendingstatus == null)
                    throw new ArgumentException("blendingstatus not found.");

                if (blendingstatus.locked == false)
                    throw new ArgumentException("ชั่วโมงนี้ยังไม่ถูก locked ไม่สามารถพิมพ์รายการได้");

                var pdsetup = Facade.pdsetupBL().GetSingle(pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                var packedgrade = uow.packedgradeRepo
                    .GetSingle(x => x.packedgrade1 == pdsetup.packedgrade);
                if (packedgrade == null)
                    throw new ArgumentException("packedgrade not found.");

                var greenInput = uow.matRepo
                    .Get(x => x.topddate == pdsetup.date &&
                    x.topdhour <= blendinghour &&
                    x.topdno == pdsetup.pdno)
                    .ToList();

                var pdInput = uow.pdRepo
                    .Get(x => x.topddate == pdsetup.date &&
                    x.topdhour <= blendinghour &&
                    x.topdno == pdsetup.pdno)
                    .ToList();

                var pdOutput = uow.pdRepo
                    .Get(x => x.frompddate == pdsetup.date &&
                    x.frompdhour <= blendinghour &&
                    x.frompdno == pdsetup.pdno)
                    .ToList();

                h_Excel.Open(@"C:\Blending\Template\PeriodReport.xlt");
                h_Excel.PutDataToCell("D5", " : "+ packedgrade.packedgrade1);
                h_Excel.PutDataToCell("D6", " : " + packedgrade.type);
                h_Excel.PutDataToCell("D7", " : " + packedgrade.customer);
                h_Excel.PutDataToCell("D8", " : " + packedgrade.form);
                h_Excel.PutDataToCell("D9", " : " + packedgrade.packingmat);
                h_Excel.PutDataToCell("P4", pdsetup.pdno);
                h_Excel.PutDataToCell("O5", ((DateTime)pdsetup.date).ToString("dd-MM-yyyy"));
                h_Excel.PutDataToCell("O6", blendinghour.ToString());

                var company = new string[] { "1", "2" };

                // Show accumulative summary.
                var pickingIn = greenInput.Where(x => x.picking == true && x.topdhour <= blendinghour).ToList();
                var pickingInWeight = pickingIn.Sum(x => company.Contains(x.company) && x.crop >= 2019 ? x.weightbuy : x.weight);
                var repacked = pdInput.Where(x => x.pdtype == "Lamina");
                var repackedWeight = repacked.Sum(x => x.netdef);
                var remnantIn = pdInput.Where(x => x.pdtype == "Remnant");
                var remnantInWeight = remnantIn.Sum(x => x.netreal);
                var packedOut = pdOutput.Where(x => x.pdtype == "Lamina");
                var packedOutWeight = packedOut.Sum(x => x.netdef);

                h_Excel.PutDataToCell("O1003", pickingIn.Count().ToString());
                h_Excel.PutDataToCell("P1003", pickingInWeight.ToString());
                h_Excel.PutDataToCell("O1004", repacked.Count().ToString());
                h_Excel.PutDataToCell("P1004", repackedWeight.ToString());
                h_Excel.PutDataToCell("O1005", remnantIn.Count().ToString());
                h_Excel.PutDataToCell("P1005", remnantInWeight.ToString());
                h_Excel.PutDataToCell("O1007", packedOut.Count().ToString());
                h_Excel.PutDataToCell("P1007", packedOutWeight.ToString());

                // Show period summary.
                var prPickingIn = pickingIn.Where(x => x.topdhour == blendinghour).ToList();
                var prPickingInWeight = prPickingIn.Sum(x => company.Contains(x.company) && x.crop >= 2019 ? x.weightbuy : x.weight);
                var prRepacked = pdInput.Where(x => x.pdtype == "Lamina" && x.topdhour == blendinghour).ToList();
                var prRepackedWeight = prRepacked.Sum(x => x.netreal);
                var prRemnantIn = pdInput.Where(x => x.pdtype == "Remnant" && x.topdhour == blendinghour);
                var prRemnantInWeight = prRemnantIn.Sum(x => x.netreal);
                var prPackedOut = pdOutput.Where(x => x.pdtype == "Lamina" && x.frompdhour == blendinghour);
                var prPackedOutWeight = prPackedOut.Sum(x => x.netdef);

                var count = prRepacked.Count();
                h_Excel.PutDataToCell("G1003", prPickingIn.Count().ToString());
                h_Excel.PutDataToCell("H1003", prPickingInWeight.ToString());
                h_Excel.PutDataToCell("G1004", prRepacked.Count().ToString());
                h_Excel.PutDataToCell("H1004", prRepackedWeight.ToString());
                h_Excel.PutDataToCell("G1005", prRemnantIn.Count().ToString());
                h_Excel.PutDataToCell("H1005", prRemnantInWeight.ToString());
                h_Excel.PutDataToCell("G1007", prPackedOut.Count().ToString());
                h_Excel.PutDataToCell("H1007", prPackedOutWeight.ToString());

                var classifyInAccumulative = greenInput
                    .GroupBy(x => x.classify)
                    .Select(x => new
                    {
                        classify = x.Key,
                        bale = x.Count(),
                        weight = x.Sum(y =>
                        company.Contains(y.company) && y.crop >= 2019 ?
                        y.weightbuy : y.weight)
                    })
                    .ToList();

                var classifyInPeriod = greenInput
                    .Where(x => x.topdhour == blendinghour)
                    .GroupBy(x => x.classify)
                    .Select(x => new
                    {
                        classify = x.Key,
                        bale = x.Count(),
                        weight = x.Sum(y =>
                        company.Contains(y.company) && y.crop >= 2019 ?
                        y.weightbuy : y.weight)
                    })
                    .ToList();

                int row = 13;
                int rowNumber = 1;
                foreach (var item in classifyInAccumulative.OrderBy(x => x.classify))
                {
                    // Show period feed in.
                    h_Excel.PutDataToCell("B" + row, rowNumber.ToString());
                    h_Excel.PutDataToCell("C" + row, item.classify);
                    h_Excel.PutDataToCell("O" + row, item.bale.ToString());
                    h_Excel.PutDataToCell("P" + row, item.weight.ToString());

                    // Show accumulative green feed in.
                    if (classifyInPeriod.Where(x => x.classify == item.classify).Count() > 0)
                    {
                        var prBale = classifyInPeriod
                            .SingleOrDefault(x => x.classify == item.classify).bale;
                        var prWeight = classifyInPeriod
                            .SingleOrDefault(x => x.classify == item.classify).weight;

                        h_Excel.PutDataToCell("G" + row, prBale.ToString());
                        h_Excel.PutDataToCell("H" + row, prWeight.ToString());
                    }

                    row++;
                    rowNumber++;
                }

                row++;
                h_Excel.DeleteRow("A" + row, "A1000");
                h_Excel.Print(copies);

                #region SQL Query
                /*
                 * Picking input.
                 * SELECT COUNT(bc) AS bt, 
                 * case when crop >= 2019 and company in (1,2) then sum(weightbuy) 
                 * else SUM(weight) end wt,SUM(feedingweight) AS rwt 
                 * FROM mat 
                 * WHERE (toprgrade = ?) 
                 * AND (topddate = ?) 
                 * AND (CONVERT(decimal(10), topdhour) <= ?) 
                 * AND topdno = ? 
                 * AND (picking = 1) group by crop, company
                 */

                /*
                 * Lamina input.
                 SELECT COUNT(bc) AS ct, SUM(netdef) AS nt 
                 FROM pd 
                 WHERE (pdtype = 'Lamina') 
                 AND (toprgrade = ?) 
                 AND (topddate = ?) 
                 AND (topdhour <= ?) 
                 AND topdno = ?
                 */

                /*
                 * Remnant input.
                 * SELECT COUNT(bc) AS ct, SUM(netreal) AS nt 
                 * FROM pd 
                 * WHERE (pdtype = 'Remnant') 
                 * AND (toprgrade = ?) 
                 * AND (topddate = ?) 
                 * AND (topdhour <= ?) 
                 * AND topdno = ?
                 */

                /*
                 * Lamina output.
                 * SELECT COUNT(bc) AS ct, SUM(netdef) AS nt
                 * FROM pd
                 * WHERE (fromprgrade = ?) 
                 * AND (frompddate = ?) 
                 * AND frompdhour <= ?) 
                 * AND (pdtype = 'Lamina')
                 * AND frompdno = ?
                 */

                /*
                * Packed grade.
                * SELECT pd.frompdhour
	                ,pd.frompdno
	                ,pdsetup.packedgrade
	                ,packedgrade.type
	                ,packedgrade.customer
	                ,packedgrade.form
	                ,packedgrade.packingmat
                FROM pd
                INNER JOIN pdsetup ON pd.frompdno = pdsetup.pdno
                INNER JOIN packedgrade ON pdsetup.packedgrade = packedgrade.packedgrade
                WHERE (pd.fromprgrade = '21-AB4-CP')
	                AND (pd.frompddate = '2021-01-20')
	                AND (pd.frompdhour = 1)
	                AND pd.frompdno = '21-010'
                 */
                #endregion
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DailyReport(string grade, DateTime pddate, short copies)
        {
            try
            {
                if (string.IsNullOrEmpty(grade))
                    throw new ArgumentException("โปรดระบุ grade");

                if (copies <= 0)
                    throw new ArgumentException("โปรดระบุจำนวนที่ต้องการพิมพ์");

                var packedgrade = uow.packedgradeRepo
                    .GetSingle(x => x.packedgrade1 == grade);
                if (packedgrade == null)
                    throw new ArgumentException("packedgrade not found.");

                var greenInput = uow.matRepo
                    .Get(x => x.topddate == pddate &&
                    x.toprgrade == packedgrade.packedgrade1)
                    .ToList();

                var pdInput = uow.pdRepo
                    .Get(x => x.topddate == pddate &&
                    x.toprgrade == packedgrade.packedgrade1)
                    .ToList();

                var pdOutput = uow.pdRepo
                    .Get(x => x.frompddate == pddate &&
                    x.grade == packedgrade.packedgrade1)
                    .ToList();

                var pdOut = pdOutput.FirstOrDefault();
                if (pdOut == null)
                    throw new ArgumentException("ยังไม่มียาออกทางท้ายเครื่อง ยังไม่สามารถดูรายงานนี้ได้");

                var pdsetup = uow.pdsetupRepo.GetSingle(x => x.pdno == pdOut.frompdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (pdsetup.blendinglocked == false)
                    throw new ArgumentException("blendinglocked ยังไม่ถูก locked ยังไม่สามารถดูรายงานนี้ได้");

                if (pdsetup.packinglocked == false)
                    throw new ArgumentException("packinglocked ยังไม่ถูก locked ยังไม่สามารถดูรายงานนี้ได้");

                if (pdsetup.byproductlocked == false)
                    throw new ArgumentException("byproductlocked ยังไม่ถูก locked ยังไม่สามารถดูรายงานนี้ได้");

                if (pdsetup.pickinglocked == false)
                    throw new ArgumentException("pickinglocked ยังไม่ถูก locked ยังไม่สามารถดูรายงานนี้ได้");

                h_Excel.Open(@"C:\Blending\Template\DailyReport.xlt");
                h_Excel.PutDataToCell("D5", packedgrade.packedgrade1);
                h_Excel.PutDataToCell("D6", packedgrade.type);
                h_Excel.PutDataToCell("D7", packedgrade.customer);
                h_Excel.PutDataToCell("D8", packedgrade.form);
                h_Excel.PutDataToCell("D9", packedgrade.packingmat);
                h_Excel.PutDataToCell("I5", pddate.ToString("dd/MM/yyyy"));

                var company = new string[] { "1", "2" };
                var classifyIn = greenInput
                    .Where(x => x.picking == false)
                    .GroupBy(x => x.classify)
                    .Select(x => new
                    {
                        classify = x.Key,
                        bale = x.Count(),
                        weight = x.Sum(y =>
                        company.Contains(y.company) && y.crop >= 2019 ?
                        y.weightbuy : y.weight)
                    })
                    .ToList();

                int row = 12;
                int rowNumber = 1;
                foreach (var item in classifyIn.OrderBy(x => x.classify))
                {
                    // Show period feed in.
                    h_Excel.PutDataToCell("B" + row, rowNumber.ToString());
                    h_Excel.PutDataToCell("D" + row, item.classify);
                    h_Excel.PutDataToCell("F" + row, item.bale.ToString());
                    h_Excel.PutDataToCell("H" + row, item.weight.ToString());

                    row++;
                    rowNumber++;
                }

                // Show accumulative summary.
                var pickingIn = greenInput.Where(x => x.picking == true).ToList();
                var pickingInWeight = pickingIn.Sum(x => company.Contains(x.company) && x.crop >= 2019 ? x.weightbuy : x.weight);
                var repacked = pdInput.Where(x => x.pdtype == "Lamina");
                var repackedWeight = repacked.Sum(x => x.netdef);
                var remnantIn = pdInput.Where(x => x.pdtype == "Remnant");
                var remnantInWeight = remnantIn.Sum(x => x.netdef);
                var packedOut = pdOutput.Where(x => x.pdtype == "Lamina");
                var packedOutWeight = packedOut.Sum(x => x.netdef);

                h_Excel.PutDataToCell("F1002", pickingIn.Count().ToString());
                h_Excel.PutDataToCell("H1002", pickingInWeight.ToString());
                h_Excel.PutDataToCell("F1003", repacked.Count().ToString());
                h_Excel.PutDataToCell("H1003", repackedWeight.ToString());
                h_Excel.PutDataToCell("F1004", remnantIn.Count().ToString());
                h_Excel.PutDataToCell("H1004", remnantInWeight.ToString());
                h_Excel.PutDataToCell("F1006", packedOut.Count().ToString());
                h_Excel.PutDataToCell("H1006", packedOutWeight.ToString());

                row++;
                h_Excel.DeleteRow("A" + row, "A1000");
                h_Excel.Print(copies);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SummaryReport(string grade, short copies)
        {
            try
            {
                if (string.IsNullOrEmpty(grade))
                    throw new ArgumentException("โปรดระบุ grade");

                if (copies <= 0)
                    throw new ArgumentException("โปรดระบุจำนวนที่ต้องการพิมพ์");

                var packedgrade = uow.packedgradeRepo
                    .GetSingle(x => x.packedgrade1 == grade);
                if (packedgrade == null)
                    throw new ArgumentException("packedgrade not found.");

                var greenInput = uow.matRepo
                    .Get(x => x.toprgrade == packedgrade.packedgrade1)
                    .ToList();

                var pdInput = uow.pdRepo
                    .Get(x => x.toprgrade == packedgrade.packedgrade1)
                    .ToList();

                var pdOutput = uow.pdRepo
                    .Get(x => x.grade == packedgrade.packedgrade1)
                    .ToList();

                h_Excel.Open(@"C:\Blending\Template\GradeAccReport.xlt");
                h_Excel.PutDataToCell("D5", ": " + packedgrade.packedgrade1);
                h_Excel.PutDataToCell("D6", ": " + packedgrade.type);
                h_Excel.PutDataToCell("D7", ": " + packedgrade.customer);
                h_Excel.PutDataToCell("D8", ": " + packedgrade.form);
                h_Excel.PutDataToCell("D9", ": " + packedgrade.packingmat);

                var company = new string[] { "1", "2" };
                var classifyIn = greenInput
                    .GroupBy(x => x.classify)
                    .Select(x => new
                    {
                        classify = x.Key,
                        bale = x.Count(),
                        weight = x.Sum(y =>
                        company.Contains(y.company) && y.crop >= 2019 ?
                        y.weightbuy : y.weight)
                    })
                    .ToList();

                int row = 13;
                int rowNumber = 1;
                foreach (var item in classifyIn.OrderBy(x => x.classify))
                {
                    // Show period feed in.
                    h_Excel.PutDataToCell("B" + row, rowNumber.ToString());
                    h_Excel.PutDataToCell("C" + row, item.classify);
                    h_Excel.PutDataToCell("G" + row, item.bale.ToString());
                    h_Excel.PutDataToCell("H" + row, item.weight.ToString());

                    row++;
                    rowNumber++;
                }

                // Show accumulative summary.
                var pickingIn = greenInput.Where(x => x.picking == true).ToList();
                var pickingInWeight = pickingIn
                    .Sum(x => company.Contains(x.company) &&
                    x.picking == true &&
                    x.crop >= 2019 ? x.weightbuy : x.weight);
                var repacked = pdInput.Where(x => x.pdtype == "Lamina");
                var repackedWeight = repacked.Sum(x => x.netdef);
                var remnantIn = pdInput.Where(x => x.pdtype == "Remnant");
                var remnantInWeight = remnantIn.Sum(x => x.netreal);
                var packedOut = pdOutput.Where(x => x.pdtype == "Lamina");
                var packedOutWeight = packedOut.Sum(x => x.netdef);

                h_Excel.PutDataToCell("G1003", pickingIn.Count().ToString());
                h_Excel.PutDataToCell("H1003", pickingInWeight.ToString());
                h_Excel.PutDataToCell("G1004", repacked.Count().ToString());
                h_Excel.PutDataToCell("H1004", repackedWeight.ToString());
                h_Excel.PutDataToCell("G1005", remnantIn.Count().ToString());
                h_Excel.PutDataToCell("H1005", remnantInWeight.ToString());
                h_Excel.PutDataToCell("G1007", packedOut.Count().ToString());
                h_Excel.PutDataToCell("H1007", packedOutWeight.ToString());

                row++;
                h_Excel.DeleteRow("A" + row, "A1000");
                h_Excel.Print(copies);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
