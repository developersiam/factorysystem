﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IShippingTruckDriverBL
    {
        void Add(string personID, string createBy);
        void Delete(string personID);
        void SetActiveStatus(string personID, bool status, string modifiedBy);
        List<ShippingTruckDriver> GetAll();
        List<ShippingTruckDriver> GetByActiveStatus(bool status);
        ShippingTruckDriver GetSingle(string personID);
    }

    public class ShippingTruckDriverBL : IShippingTruckDriverBL
    {
        IStecDBMSUnitOfWork uow;
        public ShippingTruckDriverBL()
        {
            uow = new StecDBMSUnitOfWork();
        }
        public void Add(string personID, string createBy)
        {
            try
            {
                if (string.IsNullOrEmpty(personID))
                    throw new Exception("Person ID cannot be empty.");

                if (string.IsNullOrEmpty(createBy))
                    throw new Exception("Create By cannot be empty.");

                if (GetSingle(personID) != null)
                    throw new Exception("มีข้อมูล Person ID นี้ซ้ำในระบบ");

                uow.ShippingTruckDriverRepo
                    .Add(new ShippingTruckDriver
                    {
                        PersonID = personID,
                        ActiveStatus = true,
                        CreateBy = createBy,
                        CreateDate = DateTime.Now,
                        ModifiedBy = createBy,
                        ModifiedDate = DateTime.Now
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string personID)
        {
            try
            {
                if (string.IsNullOrEmpty(personID))
                    throw new Exception("Person ID cannot be empty.");

                var model = GetSingle(personID);
                if (model == null)
                    throw new Exception("ไม่พบข้อมูล Truck Driver รายนี้ในระบบ");

                uow.ShippingTruckDriverRepo.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ShippingTruckDriver> GetAll()
        {
            return uow.ShippingTruckDriverRepo.Get();
        }

        public List<ShippingTruckDriver> GetByActiveStatus(bool status)
        {
            return uow.ShippingTruckDriverRepo.Get(x => x.ActiveStatus == status);
        }

        public ShippingTruckDriver GetSingle(string personID)
        {
            return uow.ShippingTruckDriverRepo.GetSingle(x => x.PersonID == personID);
        }

        public void SetActiveStatus(string personID, bool status, string modifiedBy)
        {
            try
            {
                if (string.IsNullOrEmpty(personID))
                    throw new Exception("Person ID cannot be empty.");

                if (string.IsNullOrEmpty(modifiedBy))
                    throw new Exception("Modified By cannot be empty.");

                var model = GetSingle(personID);
                if (model == null)
                    throw new Exception("ไม่พบข้อมูล Truck Driver รายนี้ในระบบ");

                model.ActiveStatus = status;
                model.ModifiedDate = DateTime.Now;
                model.ModifiedBy = modifiedBy;
                uow.ShippingTruckDriverRepo.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
