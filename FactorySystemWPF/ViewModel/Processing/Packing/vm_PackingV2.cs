﻿using FactoryBL;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.Model;
using FactorySystemWPF.MVVM;
using FactorySystemWPF.View.Processing.Packing;
using FactorySystemWPF.View.Shared;
using FactorySystemWPF.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace FactorySystemWPF.ViewModel.Processing.Packing
{
    public class vm_PackingV2 : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_PackingV2()
        {
            PageLoad();
            ClearForm();
        }


        #region Properties
        private string _pdno;

        public string pdno
        {
            get { return _pdno; }
            set { _pdno = value; }
        }

        private pdsetup _pdsetup;

        public pdsetup pdsetup
        {
            get { return _pdsetup; }
            set { _pdsetup = value; }
        }

        private string _type;

        public string type
        {
            get { return _type; }
            set { _type = value; }
        }

        private string _customer;

        public string customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        private packedgrade _packedgrade;

        public packedgrade packedgrade
        {
            get { return _packedgrade; }
            set { _packedgrade = value; }
        }

        private int _packinghour;

        public int packinghour
        {
            get { return _packinghour; }
            set
            {
                _packinghour = value;
                PeriodChanged();
            }
        }

        private packingstatu _packingstatus;

        public packingstatu packingstatus
        {
            get { return _packingstatus; }
            set { _packingstatus = value; }
        }

        private string _periodLockedText;

        public string PeriodLockedText
        {
            get { return _periodLockedText; }
            set { _periodLockedText = value; }
        }

        private Brush _periodLockedBGColor;

        public Brush PeriodLockedBGColor
        {
            get { return _periodLockedBGColor; }
            set { _periodLockedBGColor = value; }
        }

        private DateTime _starttime;

        public DateTime starttime
        {
            get { return _starttime; }
            set { _starttime = value; }
        }

        private DateTime _finishtime;

        public DateTime finishtime
        {
            get { return _finishtime; }
            set { _finishtime = value; }
        }

        private int _periodPackedOut;

        public int PeriodPackedOut
        {
            get { return _periodPackedOut; }
            set { _periodPackedOut = value; }
        }

        private int _totalPackedCase;

        public int TotalPackedCase
        {
            get { return _totalPackedCase; }
            set { _totalPackedCase = value; }
        }

        private string _thr;

        public string thr
        {
            get { return _thr; }
            set { _thr = value; }
        }

        private string _rdy;

        public string rdy
        {
            get { return _rdy; }
            set { _rdy = value; }
        }

        private string _pend;

        public string pend
        {
            get { return _pend; }
            set { _pend = value; }
        }

        private string _bc;

        public string bc
        {
            get { return _bc; }
            set { _bc = value; }
        }

        private int _caseno;

        public int caseno
        {
            get { return _caseno; }
            set { _caseno = value; }
        }

        private int _caseRunno;

        public int CaseRunno
        {
            get { return _caseRunno; }
            set { _caseRunno = value; }
        }

        private int _cusRunno;

        public int CusRunno
        {
            get { return _cusRunno; }
            set { _cusRunno = value; }
        }

        private int _accuRunno;

        public int AccuRunno
        {
            get { return _accuRunno; }
            set { _accuRunno = value; }
        }

        private string _pdtype;

        public string pdtype
        {
            get { return _pdtype; }
            set { _pdtype = value; }
        }

        private Brush _pdtypeBGColor;

        public Brush pdtypeBGColor
        {
            get { return _pdtypeBGColor; }
            set { _pdtypeBGColor = value; }
        }

        private decimal _gross;

        public decimal gross
        {
            get { return _gross; }
            set
            {
                _gross = value;
                _netreal = _gross - _boxtare;
                RaisePropertyChangedEvent(nameof(netreal));
            }
        }

        private decimal _boxtare;

        public decimal boxtare
        {
            get { return _boxtare; }
            set
            {
                _boxtare = value;
                _netreal = _gross - _boxtare;
                RaisePropertyChangedEvent(nameof(netreal));
            }
        }

        private decimal _taredef;

        public decimal taredef
        {
            get { return _taredef; }
            set { _taredef = value; }
        }

        private decimal _netreal;

        public decimal netreal
        {
            get { return _netreal; }
            set { _netreal = value; }
        }

        private DateTime _time;

        public DateTime time
        {
            get { return _time; }
            set { _time = value; }
        }

        private string _box;

        public string box
        {
            get { return _box; }
            set { _box = value; }
        }

        private Brush _boxBGColor;

        public Brush BoxBGColor
        {
            get { return _boxBGColor; }
            set { _boxBGColor = value; }
        }

        private bool _useDigitalScale;

        public bool UseDigitalScale
        {
            get { return _useDigitalScale; }
            set
            {
                _useDigitalScale = value;

                if (_useDigitalScale == true)
                {
                    if (DigitalScaleHelper.CheckPort() == false)
                    {
                        _useDigitalScale = false;
                        MessageBoxHelper.Warning("ไม่พบ port เครื่องชั่งที่เชื่อมต่ออยู่กับเครื่องคอมพิวเตอร์");
                    }
                    else
                        _useDigitalScale = true;
                }
            }
        }

        private bool _scanBoxTare;

        public bool ScanBoxTare
        {
            get { return _scanBoxTare; }
            set
            {
                try
                {
                    _scanBoxTare = value;

                    if (_scanBoxTare == false)
                    {
                        if (string.IsNullOrEmpty(_pdno))
                            throw new ArgumentException("โปรดคลิกปุ่ม New Grade เพื่อเลือกเกรดก่อน");
                        _boxtare = (decimal)_packedgrade.taredef;
                    }

                    RaisePropertyChangedEvent(nameof(boxtare));
                    RaisePropertyChangedEvent(nameof(ScanBoxTare));
                    OnFocusRequested(nameof(boxtare));
                }
                catch (Exception ex)
                {
                    MessageBoxHelper.Exception(ex);
                }
            }
        }

        private bool _checkNetReal;

        public bool CheckNetReal
        {
            get { return _checkNetReal; }
            set { _checkNetReal = value; }
        }

        private bool _notPrint;

        public bool NotPrint
        {
            get { return _notPrint; }
            set
            {
                _notPrint = value;
                _printF9ButtonVisibility = _notPrint == true ? Visibility.Collapsed : Visibility.Visible;
                RaisePropertyChangedEvent(nameof(PrintF9ButtonVisibility));
            }
        }

        private BarcodeSetting _barcodeSetting;

        public BarcodeSetting BarcodeSetting
        {
            get { return _barcodeSetting; }
            set { _barcodeSetting = value; }
        }

        private Visibility _finishGradeButtonVisibility;

        public Visibility FinishGradeButtonVisibility
        {
            get { return _finishGradeButtonVisibility; }
            set { _finishGradeButtonVisibility = value; }
        }

        private Visibility _periodFinishButtonVisibility;

        public Visibility PeriodFinishButtonVisibility
        {
            get { return _periodFinishButtonVisibility; }
            set { _periodFinishButtonVisibility = value; }
        }

        private Visibility _periodNewButtonVisibility;

        public Visibility PeriodNewButtonVisibility
        {
            get { return _periodNewButtonVisibility; }
            set { _periodNewButtonVisibility = value; }
        }

        private Visibility _periodDeleteButtonVisibility;

        public Visibility PeriodDeleteButtonVisibility
        {
            get { return _periodDeleteButtonVisibility; }
            set { _periodDeleteButtonVisibility = value; }
        }

        private Visibility _commandButtonVisibility;

        public Visibility CommandButtonVisibility
        {
            get { return _commandButtonVisibility; }
            set { _commandButtonVisibility = value; }
        }

        private Visibility _printF9ButtonVisibility;

        public Visibility PrintF9ButtonVisibility
        {
            get { return _printF9ButtonVisibility; }
            set { _printF9ButtonVisibility = value; }
        }

        private pd _dataGridSelectedRow;

        public pd DataGridSelectedRow
        {
            get { return _dataGridSelectedRow; }
            set
            {
                _dataGridSelectedRow = value;

                if (_dataGridSelectedRow == null)
                    _printF9ButtonVisibility = Visibility.Collapsed;
                else
                    _printF9ButtonVisibility = Visibility.Visible;

                RaisePropertyChangedEvent(nameof(PrintF9ButtonVisibility));
            }
        }
        #endregion



        #region List
        private List<pd> _pdListByPeriod;

        public List<pd> pdListByPeriod
        {
            get { return _pdListByPeriod; }
            set { _pdListByPeriod = value; }
        }

        private List<pd> _pdListByGrade;

        public List<pd> pdListByGrade
        {
            get { return _pdListByGrade; }
            set { _pdListByGrade = value; }
        }

        private List<pd> _pdListOnDataGrid;

        public List<pd> pdListOnDataGrid
        {
            get { return _pdListOnDataGrid; }
            set { _pdListOnDataGrid = value; }
        }

        private List<string> _thresherList;

        public List<string> ThresherList
        {
            get { return _thresherList; }
            set { _thresherList = value; }
        }

        private List<string> _redryerList;

        public List<string> RedryerList
        {
            get { return _redryerList; }
            set { _redryerList = value; }
        }

        private List<string> _packingEndList;

        public List<string> PackingEndList
        {
            get { return _packingEndList; }
            set { _packingEndList = value; }
        }

        private List<packingstatu> _packingstatusList;

        public List<packingstatu> packingstatusList
        {
            get { return _packingstatusList; }
            set { _packingstatusList = value; }
        }
        #endregion



        #region Command

        private ICommand _newGradeCommand;

        public ICommand NewGradeCommand
        {
            get { return _newGradeCommand ?? (_newGradeCommand = new RelayCommand(NewGrade)); }
            set { _newGradeCommand = value; }
        }

        private void NewGrade(object obj)
        {
            try
            {
                var window = new View.Processing.NewGrade();
                var vm = new vm_NewGrade();
                window.DataContext = vm;
                vm.Window = window;
                window.ShowDialog();

                if (vm.SelectedItem == null)
                    return;

                _pdno = vm.SelectedItem.pdno;
                RaisePropertyChangedEvent(nameof(pdno));
                ProductionChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _finishGradeCommand;

        public ICommand FinishGradeCommand
        {
            get { return _finishGradeCommand ?? (_finishGradeCommand = new RelayCommand(FinishGrade)); }
            set { _finishGradeCommand = value; }
        }

        private void FinishGrade(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("หลังจากที่ทำการ finish เกรดนี้แล้วจะไม่สามารถแก้ไขข้อมูลได้อีก" +
                    " ท่านต้องการ finish เกรดนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                Facade.pdsetupBL().FinishPacking(_pdno);
                ProductionChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _newPeriodCommand;

        public ICommand NewPeriodCommand
        {
            get { return _newPeriodCommand ?? (_newPeriodCommand = new RelayCommand(NewPeriod)); }
            set { _newPeriodCommand = value; }
        }

        private void NewPeriod(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_pdno))
                    throw new ArgumentException("โปรดระบุ pdno โดยคลิกปุ่ม New Grade");

                if (string.IsNullOrEmpty(_thr))
                    throw new ArgumentException("โปรดระบุ thresher");

                if (string.IsNullOrEmpty(_pend))
                    throw new ArgumentException("โปรดระบุ packing end");

                if (string.IsNullOrEmpty(_rdy))
                    throw new ArgumentException("โปรดระบุ redry");

                _packinghour = Facade.packingstatusBL().Add(_pdno);
                PackingStatusListBinding();
                RaisePropertyChangedEvent(nameof(packinghour));
                PeriodChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _finishPeriodCommand;

        public ICommand FinishPeriodCommand
        {
            get { return _finishPeriodCommand ?? (_finishPeriodCommand = new RelayCommand(FinishPeriod)); }
            set { _finishPeriodCommand = value; }
        }

        private void FinishPeriod(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("โปรดตรวจสอบข้อมูลให้ถูกต้อง " +
                "โดยหลังจากนี้ชั่วโมงถูกเปลี่ยนสถานะ locked จะไม่สามารถแก้ไขข้อมูลได้อีก")
                    == MessageBoxResult.No)
                    return;

                Facade.packingstatusBL().Finish(_pdno, _packinghour);
                PackingStatusListBinding();
                RaisePropertyChangedEvent(nameof(packinghour));
                PeriodChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _deletePeriodCommand;

        public ICommand DeletePeriodCommand
        {
            get { return _deletePeriodCommand ?? (_deletePeriodCommand = new RelayCommand(DeletePeriod)); }
            set { _deletePeriodCommand = value; }
        }

        private void DeletePeriod(object obj)
        {
            try
            {
                Facade.packingstatusBL().Delete(_pdno, _packinghour);
                PackingStatusListBinding();
                _packinghour = _packingstatusList.Count() > 0 ?
                    _packingstatusList.Max(x => x.packinghour) : 0;
                RaisePropertyChangedEvent(nameof(packinghour));
                PeriodChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _reWeightCommand;

        public ICommand ReWeightCommand
        {
            get { return _reWeightCommand ?? (_reWeightCommand = new RelayCommand(ReWeight)); }
            set { _reWeightCommand = value; }
        }

        private void ReWeight(object obj)
        {
            ReWeight();
        }

        private ICommand _addCommand;

        public ICommand AddCommand
        {
            get { return _addCommand ?? (_addCommand = new RelayCommand(Add)); }
            set { _addCommand = value; }
        }

        private void Add(object obj)
        {
            Add();
        }

        private ICommand _saveCommand;

        public ICommand SaveCommnd
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(Save)); }
            set { _saveCommand = value; }
        }

        private void Save(object obj)
        {
            Save();
        }

        private ICommand _printCommand;

        public ICommand PrintCommand
        {
            get { return _printCommand ?? (_printCommand = new RelayCommand(Print)); }
            set { _printCommand = value; }
        }

        private void Print(object obj)
        {
            if (_dataGridSelectedRow == null)
                throw new ArgumentException("โปรดคลิกเลือกแถวที่ต้องการปริ้นท์จากตารางข้อมูล");

            Print(_dataGridSelectedRow);
        }

        private ICommand _f1Command;

        public ICommand F1Command
        {
            get { return _f1Command ?? (_f1Command = new RelayCommand(F1)); }
            set { _f1Command = value; }
        }

        private void F1(object obj)
        {
            try
            {
                _pdtype = "Lamina";
                _pdtypeBGColor = Brushes.SaddleBrown;
                RaisePropertyChangedEvent(nameof(pdtype));
                RaisePropertyChangedEvent(nameof(pdtypeBGColor));

                if (_packingstatus == null)
                    return;

                PeriodChanged();

                if (_packingstatus.locked == true)
                    return;

                _bc = Facade.pdBL().GetNewPackedBarcode(_pdtype, (short)_pdsetup.crop, _packedgrade.form);
                _caseno = Facade.pdBL().GetNewPackedCaseNo(_packedgrade.packedgrade1);
                RaisePropertyChangedEvent(nameof(bc));
                RaisePropertyChangedEvent(nameof(caseno));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _f2Command;

        public ICommand F2Command
        {
            get { return _f2Command ?? (_f2Command = new RelayCommand(F2)); }
            set { _f2Command = value; }
        }

        private void F2(object obj)
        {
            try
            {
                _pdtype = "Remnant";
                _pdtypeBGColor = Brushes.BlueViolet;

                RaisePropertyChangedEvent(nameof(pdtype));
                RaisePropertyChangedEvent(nameof(pdtypeBGColor));

                if (_packingstatus == null)
                    return;

                PeriodChanged();

                if (_packingstatus.locked == true)
                    return;

                _bc = Facade.pdBL().GetNewPackedBarcode(_pdtype, (short)_pdsetup.crop, _packedgrade.form);
                _caseno = 0;

                RaisePropertyChangedEvent(nameof(bc));
                RaisePropertyChangedEvent(nameof(caseno));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _f5Command;

        public ICommand F5Command
        {
            get { return _f5Command ?? (_f5Command = new RelayCommand(F5)); }
            set { _f5Command = value; }
        }

        private void F5(object obj)
        {
            ReWeight();
        }

        private ICommand _f6Command;

        public ICommand F6Command
        {
            get { return _f6Command ?? (_f6Command = new RelayCommand(F6)); }
            set { _f6Command = value; }
        }

        private void F6(object obj)
        {
            if (_box.Contains("NewBox"))
            {
                _box = "OldBox";
                _boxBGColor = Brushes.LimeGreen;
            }
            else
            {
                _box = "NewBox";
                _boxBGColor = Brushes.Gold;
            }
            RaisePropertyChangedEvent(nameof(box));
            RaisePropertyChangedEvent(nameof(BoxBGColor));
        }

        private ICommand _f8Command;

        public ICommand F8Command
        {
            get { return _f8Command ?? (_f8Command = new RelayCommand(F8)); }
            set { _f8Command = value; }
        }

        private void F8(object obj)
        {
            Add();
        }

        private ICommand _f9Command;

        public ICommand F9Command
        {
            get { return _f9Command ?? (_f9Command = new RelayCommand(F9)); }
            set { _f9Command = value; }
        }

        private void F9(object obj)
        {
            if (_dataGridSelectedRow == null)
                throw new ArgumentException("โปรดคลิกเลือกแถวที่ต้องการปริ้นท์จากตารางข้อมูล");

            Print(_dataGridSelectedRow);
        }

        private ICommand _f12Command;

        public ICommand F12Command
        {
            get { return _f12Command ?? (_f12Command = new RelayCommand(F12)); }
            set { _f12Command = value; }
        }

        private void F12(object obj)
        {
            Save();
        }

        private ICommand _selectedRowCommand;

        public ICommand SelectedRowCommand
        {
            get { return _selectedRowCommand ?? (_selectedRowCommand = new RelayCommand(SelectedRow)); }
            set { _selectedRowCommand = value; }
        }

        private void SelectedRow(object obj)
        {
            try
            {
                if (obj == null)
                    return;

                var selectedItem = (pd)obj;

                _bc = selectedItem.bc;
                _caseno = (int)selectedItem.caseno;
                _gross = (decimal)selectedItem.grossreal;
                _boxtare = (decimal)selectedItem.boxtare;
                _taredef = (decimal)selectedItem.taredef;
                _netreal = (decimal)selectedItem.netreal;
                _time = (DateTime)selectedItem.packingtime;
                _thr = selectedItem.thr;
                _rdy = selectedItem.rdy;
                _pend = selectedItem.pend;
                _pdtype = selectedItem.pdtype;
                _pdtypeBGColor = selectedItem.pdtype == "Lamina" ? Brushes.SaddleBrown : Brushes.BlueViolet;
                _box = selectedItem.box == true ? "NewBox" : "OldBox";
                _boxBGColor = _box == "NewBox" ? Brushes.Gold : Brushes.LimeGreen;
                _printF9ButtonVisibility = _notPrint == true ? Visibility.Collapsed : Visibility.Visible;
                RaisePropertyChangedEvent(nameof(bc));
                RaisePropertyChangedEvent(nameof(caseno));
                RaisePropertyChangedEvent(nameof(gross));
                RaisePropertyChangedEvent(nameof(boxtare));
                RaisePropertyChangedEvent(nameof(taredef));
                RaisePropertyChangedEvent(nameof(netreal));
                RaisePropertyChangedEvent(nameof(time));
                RaisePropertyChangedEvent(nameof(thr));
                RaisePropertyChangedEvent(nameof(rdy));
                RaisePropertyChangedEvent(nameof(pend));
                RaisePropertyChangedEvent(nameof(pdtype));
                RaisePropertyChangedEvent(nameof(pdtypeBGColor));
                RaisePropertyChangedEvent(nameof(box));
                RaisePropertyChangedEvent(nameof(BoxBGColor));
                RaisePropertyChangedEvent(nameof(PrintF9ButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _grossGotFocusCommand;

        public ICommand GrossGotFocusCommand
        {
            get { return _grossGotFocusCommand ?? (_grossGotFocusCommand = new RelayCommand(GrossGotFocus)); }
            set { _grossGotFocusCommand = value; }
        }

        private void GrossGotFocus(object obj)
        {
            try
            {
                if (_useDigitalScale == false)
                    return;

                if (_useDigitalScale == true)
                {
                    if (DigitalScaleHelper.CheckPort() == false)
                    {
                        MessageBoxHelper.Warning("ไม่พบพอร์ตเชื่อมต่อเครื่องชั่งดิจิตอลบนเครื่องคอมพิวเตอร์นี้");
                        _useDigitalScale = false;
                        RaisePropertyChangedEvent(nameof(UseDigitalScale));
                        return;
                    }

                    var window = new ScalePanel();
                    var vm = new vm_ScalePanel();
                    vm.Window = window;
                    window.DataContext = vm;
                    window.ShowDialog();

                    if (vm.Weight == null)
                        return;

                    _gross = (decimal)vm.Weight;
                    RaisePropertyChangedEvent(nameof(gross));
                    OnFocusRequested(nameof(boxtare));
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _grossEnterCommand;

        public ICommand GrossEnterCommand
        {
            get { return _grossEnterCommand ?? (_grossEnterCommand = new RelayCommand(GrossEnter)); }
            set { _grossEnterCommand = value; }
        }

        private void GrossEnter(object obj)
        {
            try
            {
                if (_gross <= 0)
                    throw new ArgumentException("gross ต้องมากกว่า 0");

                OnFocusRequested(nameof(boxtare));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _boxTareEnterCommand;

        public ICommand BoxTareEnterCommand
        {
            get { return _boxTareEnterCommand ?? (_boxTareEnterCommand = new RelayCommand(BoxTareEnter)); }
            set { _boxTareEnterCommand = value; }
        }

        private void BoxTareEnter(object obj)
        {
            try
            {
                if (_boxtare <= 0)
                    throw new ArgumentException("boxtare ต้องมากกว่า 0");

                Save();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion



        #region Function
        public void ClearForm()
        {
            _bc = "";
            _caseno = 0;
            _gross = 0;
            _taredef = (decimal)(_packedgrade != null ? _packedgrade.taredef : _taredef); ;
            _netreal = 0;
            _time = DateTime.Now;
            _dataGridSelectedRow = null;

            RaisePropertyChangedEvent(nameof(bc));
            RaisePropertyChangedEvent(nameof(caseno));
            RaisePropertyChangedEvent(nameof(gross));
            RaisePropertyChangedEvent(nameof(taredef));
            RaisePropertyChangedEvent(nameof(netreal));
            RaisePropertyChangedEvent(nameof(time));
            RaisePropertyChangedEvent(nameof(DataGridSelectedRow));
        }

        public void PageLoad()
        {
            _useDigitalScale = true;
            _checkNetReal = true;
            _box = "NewBox";
            _boxBGColor = Brushes.Gold;
            RaisePropertyChangedEvent(nameof(UseDigitalScale));
            RaisePropertyChangedEvent(nameof(CheckNetReal));
            RaisePropertyChangedEvent(nameof(box));
            RaisePropertyChangedEvent(nameof(BoxBGColor));

            _finishGradeButtonVisibility = Visibility.Collapsed;
            _periodFinishButtonVisibility = Visibility.Collapsed;
            _periodNewButtonVisibility = Visibility.Collapsed;
            _periodDeleteButtonVisibility = Visibility.Collapsed;
            _commandButtonVisibility = Visibility.Collapsed;
            _printF9ButtonVisibility = Visibility.Collapsed;
            RaisePropertyChangedEvent(nameof(FinishGradeButtonVisibility));
            RaisePropertyChangedEvent(nameof(PeriodNewButtonVisibility));
            RaisePropertyChangedEvent(nameof(PeriodFinishButtonVisibility));
            RaisePropertyChangedEvent(nameof(PeriodDeleteButtonVisibility));
            RaisePropertyChangedEvent(nameof(CommandButtonVisibility));
            RaisePropertyChangedEvent(nameof(PrintF9ButtonVisibility));

            _thresherList = new List<string>();
            _redryerList = new List<string>();
            _packingEndList = new List<string>();
            _thresherList.Add("Sanun");
            _thresherList.Add("Teerapong");
            _redryerList.Add("Noraset");
            _redryerList.Add("Prasit");
            _redryerList.Add("Sarawut");
            _redryerList.Add("Thanakorn");
            _redryerList.Add("Veerasak");
            _packingEndList.Add("Chaipat");
            _packingEndList.Add("Chaiphat");
            _packingEndList.Add("Patcharinc");
            _packingEndList.Add("Pichai");
            _packingEndList.Add("Prajol");
            _packingEndList.Add("Wilaipron");
            RaisePropertyChangedEvent(nameof(ThresherList));
            RaisePropertyChangedEvent(nameof(RedryerList));
            RaisePropertyChangedEvent(nameof(PackingEndList));

            _pdtype = "Lamina";
            _pdtypeBGColor = Brushes.SaddleBrown;
            RaisePropertyChangedEvent(nameof(pdtype));
            RaisePropertyChangedEvent(nameof(pdtypeBGColor));
        }

        public void PdPeriodListBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_pdno))
                    return;

                _pdListByPeriod = Facade.pdBL()
                    .GetByFromPdHour(_pdno, _packinghour)
                    .OrderByDescending(x => x.caseno)
                    .ToList();
                _periodPackedOut = _pdListByPeriod.Count();
                _pdListOnDataGrid = _pdListByPeriod.Where(x => x.pdtype == _pdtype).ToList();

                if (_pdListByPeriod.Count() > 0)
                {
                    _starttime = (DateTime)_pdListByPeriod.Min(x => x.dtrecord);
                    _finishtime = (DateTime)_pdListByPeriod.Max(x => x.dtrecord);
                }
                else
                {
                    _starttime = DateTime.Now;
                    _finishtime = DateTime.Now;
                }



                RaisePropertyChangedEvent(nameof(starttime));
                RaisePropertyChangedEvent(nameof(finishtime));
                RaisePropertyChangedEvent(nameof(pdListByPeriod));
                RaisePropertyChangedEvent(nameof(PeriodPackedOut));
                RaisePropertyChangedEvent(nameof(pdListOnDataGrid));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void PackingStatusListBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_pdno))
                    return;

                _packingstatusList = Facade.packingstatusBL().GetByPdno(_pdno).ToList();
                if (_packingstatusList.Count() > 0)
                {
                    if (_packingstatusList
                        .Where(x => x.locked == false).Count() > 0)
                    {
                        _periodNewButtonVisibility = Visibility.Collapsed;
                        _periodDeleteButtonVisibility = Visibility.Collapsed;
                        _periodFinishButtonVisibility = Visibility.Collapsed;
                    }
                    else
                    {
                        _periodNewButtonVisibility = Visibility.Visible;
                        _periodDeleteButtonVisibility = Visibility.Collapsed;
                        _periodFinishButtonVisibility = Visibility.Collapsed;
                    }
                }
                else
                {
                    _periodNewButtonVisibility = Visibility.Visible;
                    _periodDeleteButtonVisibility = Visibility.Collapsed;
                    _periodFinishButtonVisibility = Visibility.Collapsed;
                    _commandButtonVisibility = Visibility.Collapsed;
                }

                RaisePropertyChangedEvent(nameof(packingstatusList));
                RaisePropertyChangedEvent(nameof(PeriodNewButtonVisibility));
                RaisePropertyChangedEvent(nameof(PeriodDeleteButtonVisibility));
                RaisePropertyChangedEvent(nameof(PeriodFinishButtonVisibility));
                RaisePropertyChangedEvent(nameof(CommandButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void ReWeight()
        {
            try
            {
                if (_dataGridSelectedRow == null)
                    throw new ArgumentException("โปรดเลือกข้อมูลที่จะทำการแก้จากแถวในตารางข้อมูล");

                if (_packingstatus == null)
                    throw new ArgumentException("โปรดเลือกชั่วโมงการทำงานก่อน");

                if (_packingstatus.locked == true)
                    throw new ArgumentException("ชั่วโมงนี้ถูกล็อคแล้ว ไม่สามารถบันทึกข้อมูลได้");

                if (MessageBoxHelper.Question("ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่") == MessageBoxResult.No)
                    return;

                Facade.pdBL()
                    .ReWeightPacked(_dataGridSelectedRow.bc,
                    _gross,
                    _box == "NewBox" ? true : false,
                    _boxtare,
                    _checkNetReal,
                    user_setting.security.uname);

                PeriodChanged();
                Add();

                if (MessageBoxHelper.Question("ท่านต้องการพิมพ์ป้ายบาร์โค้ตใหม่หรือไม่") == MessageBoxResult.Yes)
                {
                    if (_notPrint == true)
                        throw new ArgumentException("โปรดนำเครื่องหมายถูกหน้าช่อง not print ออกก่อน");

                    _dataGridSelectedRow.grossreal = _gross;
                    _dataGridSelectedRow.box = _box == "NewBox" ? true : false;
                    _dataGridSelectedRow.boxtare = (double)_boxtare;
                    Print(_dataGridSelectedRow);
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void Save()
        {
            try
            {
                ///หาก user คลิกเลือกแถวข้อมูลค้างไว้แล้วกด F12 Save 
                ///จะถือเป็นการแก้ไขข้อมูลหรือ reweight ทันที
                if (_dataGridSelectedRow != null)
                {
                    ReWeight();
                    return;
                }

                if (user_setting.security == null)
                    throw new ArgumentException("โปรดล็อคอินเข้าใช้งานก่อน");

                if (_pdtype == "Remnant")
                    if (MessageBoxHelper.Question("ท่านต้องการบันทึกข้อมูลยา Remnant ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;

                if (_notPrint == false && _barcodeSetting == null)
                    throw new ArgumentException("ไม่พบการตั้งค่าการพิมพ์บาร์โค้ตในระบบ โปรดแจ้งแผนก QC เพื่อตั้งค่า" +
                        " จากนั้นกดปุ่ม New grade เพื่อเริ่มต้นใหม่อีกครั้ง");

                var box = _box == "NewBox" ? true : false;
                var newpd = Facade.pdBL()
                    .AddPacked(_bc,
                    _caseno,
                    _gross,
                    _thr,
                    _rdy,
                    _pend,
                    _boxtare,
                    box,
                    _pdtype,
                    _pdno,
                    _packinghour,
                    user_setting.security.uname,
                   _checkNetReal);

                if (_notPrint == false)
                    Print(newpd);

                PeriodChanged();
                Add();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void Add()
        {
            try
            {
                ClearForm();

                if (string.IsNullOrEmpty(_pdno))
                    throw new ArgumentException("โปรดคลิกปุ่ม New Grade เพื่อเลือกเกรดก่อน");

                if (string.IsNullOrEmpty(_thr))
                    throw new ArgumentException("โปรดระบุ thresher");

                if (string.IsNullOrEmpty(_pend))
                    throw new ArgumentException("โปรดระบุ packing end");

                if (string.IsNullOrEmpty(_rdy))
                    throw new ArgumentException("โปรดระบุ redry");

                if (_packingstatus == null)
                    throw new ArgumentException("โปรดเลือกชั่วโมงการทำงานก่อน");

                if (_packingstatus.locked == true)
                    throw new ArgumentException("ชั่วโมงนี้ถูกล็อคแล้ว ไม่สามารถบันทึกข้อมูลได้");

                _bc = Facade.pdBL()
                    .GetNewPackedBarcode(_pdtype,
                    (short)_pdsetup.crop,
                    _packedgrade.form);

                if (pdtype == "Lamina")
                    _caseno = Facade.pdBL().GetNewPackedCaseNo(_packedgrade.packedgrade1);
                else
                    _caseno = 0;

                _printF9ButtonVisibility = Visibility.Collapsed;
                _boxtare = _scanBoxTare == true ? (decimal)0.0 : _taredef;
                RaisePropertyChangedEvent(nameof(bc));
                RaisePropertyChangedEvent(nameof(caseno));
                RaisePropertyChangedEvent(nameof(boxtare));
                RaisePropertyChangedEvent(nameof(PrintF9ButtonVisibility));
                OnFocusRequested(nameof(gross));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void Print(pd pd)
        {
            try
            {
                if (_notPrint == true)
                    throw new ArgumentException("เมื่อต้องการพิมพ์บาร์โค้ต โปรดคลิกเอาเครื่องหมายหน้า Not Print ออกก่อน");

                Facade.pdBL().PrintBarcodeWithBartender(pd);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void ProductionChanged()
        {
            try
            {
                _pdsetup = Facade.pdsetupBL().GetSingle(_pdno);
                RaisePropertyChangedEvent(nameof(pdsetup));

                if (_pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                var packedgrade = Facade.packedgradeBL().GetSingle(pdsetup.packedgrade);
                if (packedgrade == null)
                    throw new ArgumentException("ไม่พบข้อมูล packedgrade โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                _type = packedgrade.type;
                _packedgrade = packedgrade;
                _customer = packedgrade.customer + " / " + packedgrade.form;
                _pdListByGrade = Facade.pdBL().GetByGrade(_packedgrade.packedgrade1);
                RaisePropertyChangedEvent(nameof(type));
                RaisePropertyChangedEvent(nameof(packedgrade));
                RaisePropertyChangedEvent(nameof(customer));
                RaisePropertyChangedEvent(nameof(pdListByGrade));

                PackingStatusListBinding();
                if (_packingstatusList.Count() > 0)
                {
                    _packinghour = _packingstatusList.Max(x => x.packinghour);
                    RaisePropertyChangedEvent(nameof(packinghour));
                    PeriodChanged();
                }
                else
                {
                    ClearForm();
                    CommandButtonChanged();
                    FinishPeriodButtonChanged();
                    NewPeriodButtonChanged();
                    DeletePeriodButtonChanged();
                    FinishGradeChanged();
                }

                _bc = null;
                OnFocusRequested(nameof(bc));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        public void PeriodChanged()
        {
            try
            {
                ClearForm();
                _periodLockedText = null;
                _periodLockedBGColor = null;
                RaisePropertyChangedEvent(nameof(PeriodLockedText));
                RaisePropertyChangedEvent(nameof(PeriodLockedBGColor));

                _packingstatus = _packingstatusList.SingleOrDefault(x => x.packinghour == _packinghour);
                RaisePropertyChangedEvent(nameof(packingstatus));

                if (_packingstatus != null)
                {
                    _periodLockedText = _packingstatus.locked == true ? "Locked" : "Unlocked";
                    _periodLockedBGColor = _packingstatus.locked == true ? Brushes.Salmon : Brushes.LightGreen;
                    _commandButtonVisibility = _packingstatus.locked == true ? Visibility.Collapsed : Visibility.Visible;
                    RaisePropertyChangedEvent(nameof(PeriodLockedText));
                    RaisePropertyChangedEvent(nameof(PeriodLockedBGColor));
                    RaisePropertyChangedEvent(nameof(CommandButtonVisibility));
                }

                PdPeriodListBinding();
                FinishPeriodButtonChanged();
                NewPeriodButtonChanged();
                DeletePeriodButtonChanged();
                FinishGradeChanged();

                ///=============================================================================================
                ///Show barcode setting info.
                ///ถ้าเป็นยากล่องแรกของ packedgrade ให้ currentCaseNo = 1
                ///ถ้าเป็นยากล่องแรกของชั่วโมงแต่ไม่ใช่ยากล่องแรกของ packedgrade ให้ currentCaseNo = maxCaseNo of packedgrade
                ///ถ้าเป็นยาที่ไม่ใช่กล่องแรกของชั่วโมง ให้ currentCaseNo = maxCaseNo of period.
                _caseno = 1;
                if (_pdListByGrade.Count() > 0)
                    if (_pdListByPeriod.Count() > 0)
                        _caseno = (int)_pdListByPeriod.Max(x => x.caseno) + 1;
                    else
                        _caseno = (int)_pdListByGrade.Max(x => x.caseno) + 1;

                _accuRunno = _caseno;
                RaisePropertyChangedEvent(nameof(caseno));
                RaisePropertyChangedEvent(nameof(AccuRunno));
                BarcodeSettingChanged(_caseno);

                var pdListByPdno = _pdListByGrade.Where(x => x.frompdno == _pdsetup.pdno).ToList();
                if (pdListByPdno.Count() > 0)
                {
                    _cusRunno = (int)pdListByPdno.FirstOrDefault().CusRunno;
                }
                else
                {
                    if (_pdListByGrade.Count() > 0)
                    {
                        if (pdsetup.mode == "Continue")
                            _cusRunno = (int)_pdListByGrade.Max(x => x.CusRunno);
                        else
                            _cusRunno = (int)_pdListByGrade.Max(x => x.CusRunno) + 1;
                    }
                }
                RaisePropertyChangedEvent(nameof(CusRunno));

                if (pdListByPdno.Count() > 0)
                    _caseRunno = (int)pdListByPdno.Max(x => x.CaseRunno) + 1;
                else
                    _caseRunno = 1;
                RaisePropertyChangedEvent(nameof(CaseRunno));

                ///=============================================================================================
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void FinishPeriodButtonChanged()
        {
            try
            {
                _periodFinishButtonVisibility = Visibility.Collapsed;
                RaisePropertyChangedEvent(nameof(PeriodFinishButtonVisibility));

                if (_pdsetup.packinglocked == true)
                    return;

                if (_packingstatusList.Count() <= 0)
                    return;

                if (_packingstatusList.Where(x => x.locked == false).Count() <= 0)
                    return;

                if (packingstatus != null)
                    if (_packingstatus.locked == true)
                        return;

                if (_pdListByPeriod.Count() <= 0)
                    return;

                _periodFinishButtonVisibility = Visibility.Visible;
                RaisePropertyChangedEvent(nameof(PeriodFinishButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void NewPeriodButtonChanged()
        {
            try
            {
                _periodNewButtonVisibility = Visibility.Collapsed;
                RaisePropertyChangedEvent(nameof(PeriodNewButtonVisibility));

                if (_pdsetup.packinglocked == true)
                    return;

                if (_packingstatusList.Where(x => x.locked == false).Count() > 0)
                    return;

                _periodNewButtonVisibility = Visibility.Visible;
                RaisePropertyChangedEvent(nameof(PeriodNewButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeletePeriodButtonChanged()
        {
            try
            {
                _periodDeleteButtonVisibility = Visibility.Collapsed;
                RaisePropertyChangedEvent(nameof(PeriodDeleteButtonVisibility));

                if (_pdsetup.packinglocked == true)
                    return;

                if (_packingstatus == null)
                    return;

                if (_packingstatus.locked == true)
                    return;

                if (_pdListByPeriod.Count() > 0)
                    return;

                _periodDeleteButtonVisibility = Visibility.Visible;
                RaisePropertyChangedEvent(nameof(PeriodDeleteButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void FinishGradeChanged()
        {
            try
            {
                _finishGradeButtonVisibility = Visibility.Collapsed;
                RaisePropertyChangedEvent(nameof(FinishGradeButtonVisibility));

                if (_pdsetup.packinglocked == true)
                    return;

                if (_packingstatusList.Count() <= 0)
                    return;

                if (_packingstatusList.Where(x => x.locked == true).Count() <= 0)
                    return;

                _finishGradeButtonVisibility = Visibility.Visible;
                RaisePropertyChangedEvent(nameof(FinishGradeButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CommandButtonChanged()
        {
            try
            {
                _commandButtonVisibility = Visibility.Collapsed;

                if (_pdsetup.packinglocked == false)
                    if (_packingstatus != null)
                        if (_packingstatus.locked == false)
                            _commandButtonVisibility = Visibility.Visible;

                RaisePropertyChangedEvent(nameof(CommandButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BarcodeSettingChanged(int currentCaseNo)
        {
            try
            {
                _barcodeSetting = Facade.BarcodeSettingBL()
                    .GetByCurrentCaseNo(_packedgrade.packedgrade1, currentCaseNo);
                RaisePropertyChangedEvent(nameof(BarcodeSetting));

                if (_barcodeSetting == null)
                    throw new ArgumentException("ไม่พบการตั้งค่าการพิมพ์บาร์โค้ต โปรดติดต่อแผนก QC เพื่อตั้งค่าดังกล่าว");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
