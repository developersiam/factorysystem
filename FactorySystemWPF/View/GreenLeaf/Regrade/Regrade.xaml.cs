﻿using FactorySystemWPF.MVVM;
using FactorySystemWPF.ViewModel.GreenLeaf.Regrade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FactorySystemWPF.View.GreenLeaf.Regrade
{
    /// <summary>
    /// Interaction logic for Regrade.xaml
    /// </summary>
    public partial class Regrade : Page
    {
        public Regrade()
        {
            InitializeComponent();
            DataContext = new vm_Regrade();
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_Regrade)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.remark):
                    remarkTextBox.SelectAll();
                    remarkTextBox.Focus();
                    break;
                case nameof(vm.bcInput):
                    bcInputTextBox.SelectAll();
                    bcInputTextBox.Focus();
                    break;
                case nameof(vm.bcOutput):
                    bcOutputTextBox.SelectAll();
                    bcOutputTextBox.Focus();
                    break;
                case nameof(vm.balenoOutput):
                    balenoOutputTextBox.SelectAll();
                    balenoOutputTextBox.Focus();
                    break;
                case nameof(vm.classifyOutput):
                    classifyOutputTextBox.SelectAll();
                    classifyOutputTextBox.Focus();
                    break;
                case nameof(vm.weightOutput):
                    weightOutputTextBox.SelectAll();
                    weightOutputTextBox.Focus();
                    break;
                default:
                    break;
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }
    }
}
