﻿using FactoryBL.Report;
using FactoryBL.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL
{
    public static class Facade
    {
        public static IclassifyBL classifyBL()
        {
            IclassifyBL obj = new classifyBL();
            return obj;
        }

        public static IcompanyBL companyBL()
        {
            IcompanyBL obj = new companyBL();
            return obj;
        }

        public static IexpertBL expertBL()
        {
            IexpertBL obj = new expertBL();
            return obj;
        }

        public static ImatwhBL matwhBL()
        {
            ImatwhBL obj = new matwhBL();
            return obj;
        }

        public static ImatBL matBL()
        {
            ImatBL obj = new matBL();
            return obj;
        }

        public static ImatrcBL matrcBL()
        {
            ImatrcBL obj = new matrcBL();
            return obj;
        }

        public static ImatrcnoBL matrcnoBL()
        {
            ImatrcnoBL obj = new matrcnoBL();
            return obj;
        }

        public static ImatrgnoBL matrgnoBL()
        {
            ImatrgnoBL obj = new matrgnoBL();
            return obj;
        }

        public static ImatisnoBL matisnoBL()
        {
            ImatisnoBL obj = new matisnoBL();
            return obj;
        }

        public static ImatisBL matisBL()
        {
            ImatisBL obj = new matisBL();
            return obj;
        }

        public static IpdBL pdBL()
        {
            IpdBL obj = new pdBL();
            return obj;
        }

        public static IpdbcBL pdbcBL()
        {
            IpdbcBL obj = new pdbcBL();
            return obj;
        }

        public static IpdCustomerPackedPrintBL pdCustomerPackedPrintBL()
        {
            IpdCustomerPackedPrintBL obj = new pdCustomerPackedPrintBL();
            return obj;
        }

        public static IpdsetupBL pdsetupBL()
        {
            IpdsetupBL obj = new pdsetupBL();
            return obj;
        }

        public static IpackedgradeBL packedgradeBL()
        {
            IpackedgradeBL obj = new packedgradeBL();
            return obj;
        }

        public static IpackingstatusBL packingstatusBL()
        {
            IpackingstatusBL obj = new packingstatusBL();
            return obj;
        }

        public static IblendingstatusBL blendingstatusBL()
        {
            IblendingstatusBL obj = new blendingstatusBL();
            return obj;
        }

        public static IsecurityBL securityBL()
        {
            IsecurityBL obj = new securityBL();
            return obj;
        }

        public static IShippingMovementBL ShippingMovementBL()
        {
            IShippingMovementBL obj = new ShippingMovementBL();
            return obj;
        }

        public static IBlendingReportBL BlendingReportBL()
        {
            IBlendingReportBL obj = new BlendingReportBL();
            return obj;
        }

        public static IRegradeReportBL RegradeReportBL()
        {
            IRegradeReportBL obj = new RegradeReportBL();
            return obj;
        }

        public static IpdCustomerBL pdCustomerBL()
        {
            IpdCustomerBL obj = new pdCustomerBL();
            return obj;
        }

        public static IsfsetupBL sfsetupBL()
        {
            IsfsetupBL obj = new sfsetupBL();
            return obj;
        }

        public static ItypeBL typeBL()
        {
            ItypeBL obj = new typeBL();
            return obj;
        }

        public static IsubtypeBL subtypeBL()
        {
            IsubtypeBL obj = new subtypeBL();
            return obj;
        }

        public static IpickingBL pickingBL()
        {
            IpickingBL obj = new pickingBL();
            return obj;
        }

        public static IPickingReportBL PickingReportBL()
        {
            IPickingReportBL obj = new PickingReportBL();
            return obj;
        }

        public static IBarcodePrinterBL BarcodePrinterBL()
        {
            IBarcodePrinterBL obj = new BarcodePrinterBL();
            return obj;
        }

        public static IBarcodeSettingBL BarcodeSettingBL()
        {
            IBarcodeSettingBL obj = new BarcodeSettingBL();
            return obj;
        }

        public static IBarcodeTemplateBL BarcodeTemplateBL()
        {
            IBarcodeTemplateBL obj = new BarcodeTemplateBL();
            return obj;
        }

        public static IShippingTruckBL ShippingTruckBL()
        {
            IShippingTruckBL obj = new ShippingTruckBL();
            return obj;
        }

        public static IShippingTruckDriverBL ShippingTruckDriverBL()
        {
            IShippingTruckDriverBL obj = new ShippingTruckDriverBL();
            return obj;
        }

        public static IShippingLocationBL ShippingLocationBL()
        {
            IShippingLocationBL obj = new ShippingLocationBL();
            return obj;
        }
    }
}
