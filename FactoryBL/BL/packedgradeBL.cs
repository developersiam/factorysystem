﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IpackedgradeBL
    {
        packedgrade GetSingle(string packedgrade);
        List<packedgrade> GetByCrop(int crop);
        List<packedgrade> GetAll();
        List<int> GetPackedCrop();
    }

    public class packedgradeBL : IpackedgradeBL
    {
        StecDBMSUnitOfWork uow;
        public packedgradeBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public List<packedgrade> GetAll()
        {
            return uow.packedgradeRepo.Get();
        }

        public List<packedgrade> GetByCrop(int crop)
        {
            return uow.packedgradeRepo.Get(x => x.crop == crop);
        }

        public List<int> GetPackedCrop()
        {
            var list = new List<int>();
            foreach (var item in uow.packedgradeRepo
                .Get()
                .GroupBy(x => x.crop)
                .Select(x => new { crop = x.Key }))
            {
                list.Add(Convert.ToInt16(item.crop));
            }
            return list;
        }

        public packedgrade GetSingle(string packedgrade)
        {
            return uow.packedgradeRepo.GetSingle(x => x.packedgrade1 == packedgrade);
        }
    }
}
