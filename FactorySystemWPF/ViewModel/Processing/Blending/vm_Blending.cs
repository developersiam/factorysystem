﻿using FactorySystemWPF.Helper;
using FactorySystemWPF.Model;
using FactorySystemWPF.MVVM;
using FactoryBL;
using FactoryBL.Helper;
using FactoryBL.Model;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using FactorySystemWPF.ViewModel.Shared;

namespace FactorySystemWPF.ViewModel.Processing.Blending
{
    public class vm_Blending : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        private void OnTimedEvent(object sender, System.Timers.ElapsedEventArgs e)
        {
            PackingStatusListBinding();
        }

        int _minute;

        public vm_Blending()
        {
            _autoRefreshIntervalList = new List<int>();
            _autoRefreshIntervalList.Add(30);
            _autoRefreshIntervalList.Add(60);

            _autoRefreshInterval = 30;
            _minute = 60000;

            _timer = new Timer(_autoRefreshInterval * _minute);
            _timer.Elapsed += OnTimedEvent;
            _timer.AutoReset = true;
            _timer.Enabled = false;
            _isAutomatically = false;
            _refreshButtonVisibility = Visibility.Visible;
            _periodNewButtonVisibility = Visibility.Collapsed;
            _periodDeleteButtonVisibility = Visibility.Collapsed;
            _periodFinishButtonVisibility = Visibility.Collapsed;


            _copiesList = new List<short>();
            _copiesList.Add(1);
            _copiesList.Add(2);
            _copiesList.Add(3);
            _copiesList.Add(4);

            _feedInType = "F1";
            _pddate = DateTime.Now;
            _greenInListVisible = Visibility.Visible;
            _packedInListVisible = Visibility.Collapsed;
            _finishGradeButtonVisibility = Visibility.Collapsed;

            RaisePropertyChangedEvent(nameof(Timer));
            RaisePropertyChangedEvent(nameof(IsAutomatically));
            RaisePropertyChangedEvent(nameof(CopiesList));
            RaisePropertyChangedEvent(nameof(FeedInType));
            RaisePropertyChangedEvent(nameof(pddate));
            RaisePropertyChangedEvent(nameof(GreenInListVisible));
            RaisePropertyChangedEvent(nameof(PackedInListVisible));
            RaisePropertyChangedEvent(nameof(AutoRefreshInterval));
            RaisePropertyChangedEvent(nameof(RefreshButtonVisibility));
            RaisePropertyChangedEvent(nameof(AutoRefreshIntervalVisibility));
            RaisePropertyChangedEvent(nameof(AutoRefreshIntervalList));
            RaisePropertyChangedEvent(nameof(PeriodNewButtonVisibility));
            RaisePropertyChangedEvent(nameof(PeriodDeleteButtonVisibility));
            RaisePropertyChangedEvent(nameof(PeriodFinishButtonVisibility));
            RaisePropertyChangedEvent(nameof(FinishGradeButtonVisibility));
        }


        #region Properties
        private Timer _timer;

        public Timer Timer
        {
            get { return _timer; }
            set { _timer = value; }
        }

        private int _autoRefreshInterval;

        public int AutoRefreshInterval
        {
            get { return _autoRefreshInterval; }
            set
            {
                _autoRefreshInterval = value;
                _timer.Interval = _autoRefreshInterval * _minute;
                RaisePropertyChangedEvent(nameof(Timer));
            }
        }

        private bool _isAutomatically;

        public bool IsAutomatically
        {
            get { return _isAutomatically; }
            set
            {
                _isAutomatically = value;
                _timer.Enabled = _isAutomatically;

                if (_isAutomatically == false)
                {
                    _refreshButtonVisibility = Visibility.Visible;
                    _AutoRefreshIntervalVisibility = Visibility.Collapsed;
                }
                else
                {
                    _refreshButtonVisibility = Visibility.Collapsed;
                    _AutoRefreshIntervalVisibility = Visibility.Visible;
                }

                RaisePropertyChangedEvent(nameof(Timer));
                RaisePropertyChangedEvent(nameof(RefreshButtonVisibility));
                RaisePropertyChangedEvent(nameof(AutoRefreshIntervalVisibility));
            }
        }

        private pdsetup _pdsetup;

        public pdsetup pdsetup
        {
            get { return _pdsetup; }
            set { _pdsetup = value; }
        }

        private string _pdno;

        public string pdno
        {
            get { return _pdno; }
            set { _pdno = value; }
        }

        private DateTime _pddate;

        public DateTime pddate
        {
            get { return _pddate; }
            set { _pddate = value; }
        }

        private string _type;

        public string type
        {
            get { return _type; }
            set { _type = value; }
        }

        private string _packedgrade;

        public string packedgrade
        {
            get { return _packedgrade; }
            set { _packedgrade = value; }
        }

        private string _customer;

        public string customer
        {
            get { return _customer; }
            set { _customer = value; }
        }


        private int _blendinghour;

        public int blendinghour
        {
            get { return _blendinghour; }
            set
            {
                _blendinghour = value;
                PeriodChanged();
            }
        }

        private blendingstatu _blendingstatus;

        public blendingstatu blendingstatus
        {
            get { return _blendingstatus; }
            set { _blendingstatus = value; }
        }

        private string _periodLockedText;

        public string PeriodLockedText
        {
            get { return _periodLockedText; }
            set { _periodLockedText = value; }
        }

        private Brush _periodLockedBGColor;

        public Brush PeriodLockedBGColor
        {
            get { return _periodLockedBGColor; }
            set { _periodLockedBGColor = value; }
        }

        private Visibility _periodFinishButtonVisibility;

        public Visibility PeriodFinishButtonVisibility
        {
            get { return _periodFinishButtonVisibility; }
            set { _periodFinishButtonVisibility = value; }
        }

        private int _greenIn;

        public int GreenIn
        {
            get { return _greenIn; }
            set { _greenIn = value; }
        }

        private int _packedStockIn;

        public int PackedStockIn
        {
            get { return _packedStockIn; }
            set { _packedStockIn = value; }
        }

        private int _feedingBarcode;

        public int FeedingBarcode
        {
            get { return _feedingBarcode; }
            set { _feedingBarcode = value; }
        }

        private decimal _feedingWeight;

        public decimal FeedingWeight
        {
            get { return _feedingWeight; }
            set { _feedingWeight = value; }
        }

        private int _packedOutCases;

        public int PackedOutCases
        {
            get { return _packedOutCases; }
            set { _packedOutCases = value; }
        }

        private decimal _packedOutWeight;

        public decimal PackedOutWeight
        {
            get { return _packedOutWeight; }
            set { _packedOutWeight = value; }
        }

        private decimal _periodYield;

        public decimal PeriodYield
        {
            get { return _periodYield; }
            set { _periodYield = value; }
        }

        private short _copies;

        public short Copies
        {
            get { return _copies; }
            set { _copies = value; }
        }

        private Visibility _refreshButtonVisibility;

        public Visibility RefreshButtonVisibility
        {
            get { return _refreshButtonVisibility; }
            set { _refreshButtonVisibility = value; }
        }

        private Visibility _AutoRefreshIntervalVisibility;

        public Visibility AutoRefreshIntervalVisibility
        {
            get { return _AutoRefreshIntervalVisibility; }
            set { _AutoRefreshIntervalVisibility = value; }
        }

        private string _feedInType;

        public string FeedInType
        {
            get { return _feedInType; }
            set
            {
                _feedInType = value;
                if (_feedInType == "F1")
                {
                    _greenInListVisible = Visibility.Visible;
                    _packedInListVisible = Visibility.Collapsed;
                }
                else
                {
                    _greenInListVisible = Visibility.Collapsed;
                    _packedInListVisible = Visibility.Visible;
                }
                RaisePropertyChangedEvent(nameof(GreenInListVisible));
                RaisePropertyChangedEvent(nameof(PackedInListVisible));
            }
        }

        private string _bc;

        public string bc
        {
            get { return _bc; }
            set { _bc = value; }
        }

        private decimal _pdFeedIn;

        public decimal pdFeedIn
        {
            get { return _pdFeedIn; }
            set { _pdFeedIn = value; }
        }

        private decimal _pdPackedOut;

        public decimal pdPackedOut
        {
            get { return _pdPackedOut; }
            set { _pdPackedOut = value; }
        }

        private decimal _pdYield;

        public decimal pdYield
        {
            get { return _pdYield; }
            set { _pdYield = value; }
        }

        private decimal _packedgradeFeedIn;

        public decimal packedgradeFeedIn
        {
            get { return _packedgradeFeedIn; }
            set { _packedgradeFeedIn = value; }
        }

        private decimal _packedgradePackedOut;

        public decimal packedgradePackedOut
        {
            get { return _packedgradePackedOut; }
            set { _packedgradePackedOut = value; }
        }

        private decimal _packedgradeYield;

        public decimal packedgradeYield
        {
            get { return _packedgradeYield; }
            set { _packedgradeYield = value; }
        }

        private Visibility _greenInListVisible;

        public Visibility GreenInListVisible
        {
            get { return _greenInListVisible; }
            set { _greenInListVisible = value; }
        }

        private Visibility _packedInListVisible;

        public Visibility PackedInListVisible
        {
            get { return _packedInListVisible; }
            set { _packedInListVisible = value; }
        }

        private m_packingstatus _packingstatus;

        public m_packingstatus packingstatus
        {
            get { return _packingstatus; }
            set { _packingstatus = value; }
        }

        private Visibility _finishGradeButtonVisibility;

        public Visibility FinishGradeButtonVisibility
        {
            get { return _finishGradeButtonVisibility; }
            set { _finishGradeButtonVisibility = value; }
        }

        private Visibility _periodNewButtonVisibility;

        public Visibility PeriodNewButtonVisibility
        {
            get { return _periodNewButtonVisibility; }
            set { _periodNewButtonVisibility = value; }
        }

        private Visibility _periodDeleteButtonVisibility;

        public Visibility PeriodDeleteButtonVisibility
        {
            get { return _periodDeleteButtonVisibility; }
            set { _periodDeleteButtonVisibility = value; }
        }
        #endregion




        #region List
        private List<mat> _greenInList;

        public List<mat> GreenInList
        {
            get
            {
                if (_greenInList == null)
                    _greenInList = new List<mat>();
                return _greenInList;
            }
            set { _greenInList = value; }
        }

        private List<pd> _packedInList;

        public List<pd> PackedInList
        {
            get
            {
                if (_packedInList == null)
                    _packedInList = new List<pd>();
                return _packedInList;
            }
            set { _packedInList = value; }
        }

        private List<pd> _packedOutList;

        public List<pd> PackedOutList
        {
            get
            {
                if (_packedOutList == null)
                    _packedOutList = new List<pd>();
                return _packedOutList;
            }
            set { _packedOutList = value; }
        }

        private List<short> _copiesList;

        public List<short> CopiesList
        {
            get { return _copiesList; }
            set { _copiesList = value; }
        }

        private List<blendingstatu> _blendingstatusList;

        public List<blendingstatu> blendingstatusList
        {
            get { return _blendingstatusList; }
            set { _blendingstatusList = value; }
        }

        private List<m_packingstatus> _packingstatusList;

        public List<m_packingstatus> packingstatusList
        {
            get { return _packingstatusList; }
            set { _packingstatusList = value; }
        }

        private List<int> _autoRefreshIntervalList;

        public List<int> AutoRefreshIntervalList
        {
            get { return _autoRefreshIntervalList; }
            set { _autoRefreshIntervalList = value; }
        }
        #endregion




        #region Command
        private ICommand _onNewGradeCommand;

        public ICommand OnNewGradeCommand
        {
            get { return _onNewGradeCommand ?? (_onNewGradeCommand = new RelayCommand(OnNewGrade)); }
            set { _onNewGradeCommand = value; }
        }

        private void OnNewGrade(object obj)
        {
            try
            {
                var window = new View.Processing.NewGrade();
                var vm = new vm_NewGrade();
                window.DataContext = vm;
                vm.Window = window;
                window.ShowDialog();

                if (vm.SelectedItem == null)
                    return;

                _pdno = vm.SelectedItem.pdno;
                RaisePropertyChangedEvent(nameof(pdno));
                ProductionChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onFinishGradeCommand;

        public ICommand OnFinishGradeCommand
        {
            get { return _onFinishGradeCommand ?? (_onFinishGradeCommand = new RelayCommand(FinishGrade)); }
            set { _onFinishGradeCommand = value; }
        }

        private void FinishGrade(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("หลังจากที่ทำการ finish เกรดนี้แล้วจะไม่สามารถแก้ไขข้อมูลได้อีก" +
                    " ท่านต้องการ finish เกรดนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                Facade.pdsetupBL().FinishBlending(_pdno);
                ProductionChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onNewPeriodCommand;

        public ICommand NewPeriodCommand
        {
            get { return _onNewPeriodCommand ?? (_onNewPeriodCommand = new RelayCommand(NewPeriod)); }
            set { _onNewPeriodCommand = value; }
        }

        private void NewPeriod(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_pdno))
                    throw new ArgumentException("โปรดระบุ pdno โดยคลิกปุ่ม New Grade");

                _blendinghour = Facade.blendingstatusBL().Add(_pdno);
                BlendingStatusListBinding();
                PeriodChanged();
                RaisePropertyChangedEvent(nameof(blendinghour));
                FinishGradeChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _fnishPeriodCommand;

        public ICommand FinishPeriodCommand
        {
            get { return _fnishPeriodCommand ?? (_onFinishGradeCommand = new RelayCommand(FinishPeriod)); }
            set { _fnishPeriodCommand = value; }
        }

        private void FinishPeriod(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("โปรดตรวจสอบข้อมูลให้ถูกต้อง " +
                "โดยหลังจากนี้ชั่วโมงถูกเปลี่ยนสถานะ locked จะไม่สามารถแก้ไขข้อมูลได้อีก")
                    == MessageBoxResult.No)
                    return;

                Facade.blendingstatusBL().Finish(_pdno, _blendinghour);
                BlendingStatusListBinding();
                RaisePropertyChangedEvent(nameof(blendinghour));
                PeriodChanged();
                FinishGradeChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _deletePeriodCommand;

        public ICommand DeletePeriodCommand
        {
            get { return _deletePeriodCommand ?? (_deletePeriodCommand = new RelayCommand(DeletePeriod)); }
            set { _deletePeriodCommand = value; }
        }

        private void DeletePeriod(object obj)
        {
            try
            {
                Facade.blendingstatusBL().Delete(_pdno, _blendinghour);
                BlendingStatusListBinding();
                _blendinghour = _blendingstatusList.Max(x => x.blendinghour);
                RaisePropertyChangedEvent(nameof(blendinghour));
                PeriodChanged();
                FinishGradeChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onBarcodeEnterCommand;

        public ICommand OnBarcodeEnterCommand
        {
            get { return _onBarcodeEnterCommand ?? (_onBarcodeEnterCommand = new RelayCommand(BarcodeEnter)); }
            set { _onBarcodeEnterCommand = value; }
        }

        private void BarcodeEnter(object obj)
        {
            try
            {
                if (_feedInType == "F1")
                {
                    Facade.matBL().FeedIn(_bc,
                        DateTime.Now,
                        DateTime.Now,
                        _pdno,
                        _blendinghour,
                        DateTime.Now,
                        DateTime.Now,
                        user_setting.security.uname,
                        _pddate,
                        _packedgrade);
                }
                else
                {
                    Facade.pdBL().FeedIn(_bc,
                        DateTime.Now,
                        DateTime.Now,
                        _pdno,
                        _blendinghour,
                        DateTime.Now,
                        DateTime.Now,
                        user_setting.security.uname,
                        _pddate,
                        _packedgrade);
                }

                PeriodChanged();
                SummaryPeriodBinding();
                //SummaryProductionBinding(); performance poor.

                _bc = "";
                RaisePropertyChangedEvent(nameof(bc));
                OnFocusRequested(nameof(bc));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onF1Command;

        public ICommand OnF1Command
        {
            get { return _onF1Command ?? (_onF1Command = new RelayCommand(F1)); }
            set { _onF1Command = value; }
        }

        private void F1(object obj)
        {
            _feedInType = "F1";
            _greenInListVisible = Visibility.Visible;
            _packedInListVisible = Visibility.Collapsed;

            RaisePropertyChangedEvent(nameof(FeedInType));
            RaisePropertyChangedEvent(nameof(GreenInListVisible));
            RaisePropertyChangedEvent(nameof(PackedInListVisible));
        }

        private ICommand _onF2Command;

        public ICommand OnF2Command
        {
            get { return _onF2Command ?? (_onF2Command = new RelayCommand(F2)); }
            set { _onF2Command = value; }
        }

        private void F2(object obj)
        {
            _feedInType = "F2";
            _greenInListVisible = Visibility.Collapsed;
            _packedInListVisible = Visibility.Visible;

            RaisePropertyChangedEvent(nameof(FeedInType));
            RaisePropertyChangedEvent(nameof(GreenInListVisible));
            RaisePropertyChangedEvent(nameof(PackedInListVisible));
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(Clear)); }
            set { _onClearCommand = value; }
        }

        private void Clear(object obj)
        {
            try
            {
                _bc = "";
                RaisePropertyChangedEvent(nameof(bc));
                OnFocusRequested(nameof(bc));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onPeriodReportClickCommand;

        public ICommand OnPeriodReportClickCommand
        {
            get { return _onPeriodReportClickCommand ?? (_onPeriodReportClickCommand = new RelayCommand(PeriodReportRender)); }
            set { _onPeriodReportClickCommand = value; }
        }

        private void PeriodReportRender(object obj)
        {
            try
            {
                if (_copies < 1)
                    throw new ArgumentException("โปรดระบุจำนวนที่จะพิมพ์");

                if (_packingstatus == null)
                    throw new ArgumentException("โปรดเลือกชั่วโมงที่จะพิมพ์รายงานจากตาราง packing status ด้านซ้ายมือ");

                if (_packingstatus.locked == false)
                    throw new ArgumentException("ท้ายเครื่องยังไม่ได้ locked ชั่วโมงนี้ ยังไม่สามารถพิมพ์รายงานได้");

                Facade.BlendingReportBL().PeriodReport(_pdno, _blendinghour, _copies);
                MessageBoxHelper.Info("พิมพ์รายงานเรียบร้อยแล้ว");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDetailReportClickCommand;

        public ICommand OnDetailReportClickCommand
        {
            get { return _onDetailReportClickCommand ?? (_onDetailReportClickCommand = new RelayCommand(DetailReportRender)); }
            set { _onDetailReportClickCommand = value; }
        }

        private void DetailReportRender(object obj)
        {
            try
            {
                if (_copies < 1)
                    throw new ArgumentException("โปรดระบุจำนวนที่จะพิมพ์");

                if (_packingstatus == null)
                    throw new ArgumentException("โปรดเลือกชั่วโมงที่จะพิมพ์รายงานจากตาราง packing status ด้านซ้ายมือ");

                if (_packingstatus.locked == false)
                    throw new ArgumentException("ท้ายเครื่องยังไม่ได้ locked ชั่วโมงนี้ ยังไม่สามารถพิมพ์รายงานได้");

                Facade.BlendingReportBL().DetailReport(_pdno, _blendinghour, _copies);
                MessageBoxHelper.Info("พิมพ์รายงานเรียบร้อยแล้ว");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDailyReportClickCommand;

        public ICommand OnDailyReportClickCommand
        {
            get { return _onDailyReportClickCommand ?? (_onDailyReportClickCommand = new RelayCommand(DailyReportRender)); }
            set { _onDailyReportClickCommand = value; }
        }

        private void DailyReportRender(object obj)
        {
            try
            {
                if (_copies < 1)
                    throw new ArgumentException("โปรดระบุจำนวนที่จะพิมพ์");

                Facade.BlendingReportBL().DailyReport(_packedgrade, _pddate, _copies);
                MessageBoxHelper.Info("พิมพ์รายงานเรียบร้อยแล้ว");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onAccumulativeReportClickCommand;

        public ICommand OnAccumulativeReportClickCommand
        {
            get { return _onAccumulativeReportClickCommand ?? (_onAccumulativeReportClickCommand = new RelayCommand(AccumulativeReportClick)); }
            set { _onAccumulativeReportClickCommand = value; }
        }

        private void AccumulativeReportClick(object obj)
        {
            try
            {
                if (_copies < 1)
                    throw new ArgumentException("โปรดระบุจำนวนที่จะพิมพ์");

                Facade.BlendingReportBL().SummaryReport(_packedgrade, _copies);
                MessageBoxHelper.Info("พิมพ์รายงานเรียบร้อยแล้ว");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onRefershPackingStatusCommand;

        public ICommand OnRefershPackingStatusCommand
        {
            get { return _onRefershPackingStatusCommand ?? (_onRefershPackingStatusCommand = new RelayCommand(RefreshPackingStatus)); }
            set { _onRefershPackingStatusCommand = value; }
        }

        private void RefreshPackingStatus(object obj)
        {
            PackingStatusListBinding();
        }
        #endregion




        #region Function
        private void GreenInListBinding()
        {
            try
            {
                _greenInList = Facade.matBL()
                    .GetByToPdHour(_pdno, _blendinghour)
                    .OrderByDescending(x => x.feedingdate)
                    .ThenByDescending(x => x.feedingtime)
                    .ToList();
                _greenIn = _greenInList.Count();
                RaisePropertyChangedEvent(nameof(GreenInList));
                RaisePropertyChangedEvent(nameof(GreenIn));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PackedInListBinding()
        {
            try
            {
                _packedInList = Facade.pdBL()
                    .GetByToPdHour(_pdno, _blendinghour)
                    .OrderByDescending(x => x.feedingdate)
                    .ThenByDescending(x => x.feedingtime)
                    .ToList();
                _packedStockIn = _packedInList.Count();
                RaisePropertyChangedEvent(nameof(PackedInList));
                RaisePropertyChangedEvent(nameof(PackedStockIn));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void BlendingStatusListBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_pdno))
                    return;

                _blendingstatusList = Facade.blendingstatusBL().GetByPdno(_pdno);
                RaisePropertyChangedEvent(nameof(blendingstatusList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PackingStatusListBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_pdno))
                    throw new ArgumentException("โปรดกดปุ่ม New Grade เพื่อเลือกเกรดที่ทำการอบก่อน");

                _packingstatusList = h_packingstatus.GetByPdno(_pdno);
                RaisePropertyChangedEvent(nameof(packingstatusList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SummaryPeriodBinding()
        {
            try
            {
                PackingStatusListBinding();

                _feedingBarcode = _greenIn + _packedStockIn;
                _feedingWeight = (decimal)(_greenInList.Sum(x => x.weightbuy) + _packedInList.Sum(x => x.netdef));
                _packedOutList = Facade.pdBL()
                    .GetByFromPdHour(_pdno, _blendinghour)
                    .Where(x => x.pdtype == "Lamina")
                    .ToList();
                _packedOutCases = _packedOutList.Count();
                _packedOutWeight = (decimal)_packedOutList.Sum(x => x.netdef);
                _periodYield = 0;

                if (_feedingWeight > 0 && _packedOutWeight > 0)
                    _periodYield = _packedOutWeight / _feedingWeight * 100;

                RaisePropertyChangedEvent(nameof(PeriodDeleteButtonVisibility));
                RaisePropertyChangedEvent(nameof(FeedingBarcode));
                RaisePropertyChangedEvent(nameof(FeedingWeight));
                RaisePropertyChangedEvent(nameof(PackedOutCases));
                RaisePropertyChangedEvent(nameof(PackedOutWeight));
                RaisePropertyChangedEvent(nameof(PeriodYield));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SummaryProductionBinding()
        {
            try
            {
                var greenFeedIn = (decimal)Facade.matBL()
                    .GetByToPdno(_pdno)
                    .Sum(x => x.weightbuy);
                var pdFeedIn = (decimal)Facade.pdBL()
                    .GetByToPdno(_pdno)
                    .Where(x => x.pdtype == "Lamina")
                    .Sum(x => x.netdef);

                _pdFeedIn = greenFeedIn + pdFeedIn;
                _pdPackedOut = (decimal)Facade.pdBL()
                    .GetByFromPdno(_pdno)
                    .Where(x => x.pdtype == "Lamina")
                    .Sum(x => x.netdef);

                if (_pdFeedIn > 0 && _pdPackedOut > 0)
                    _pdYield = _pdPackedOut / _pdFeedIn * 100;

                RaisePropertyChangedEvent(nameof(pdFeedIn));
                RaisePropertyChangedEvent(nameof(pdPackedOut));
                RaisePropertyChangedEvent(nameof(pdYield));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ProductionChanged()
        {
            try
            {
                _pdsetup = Facade.pdsetupBL().GetSingle(_pdno);
                RaisePropertyChangedEvent(nameof(pdsetup));

                if (_pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                _greenInList = null;
                _packedInList = null;
                _packedOutList = null;
                _blendingstatusList = null;
                _packingstatusList = null;
                _greenIn = 0;
                _packedStockIn = 0;
                RaisePropertyChangedEvent(nameof(GreenInList));
                RaisePropertyChangedEvent(nameof(PackedOutList));
                RaisePropertyChangedEvent(nameof(blendingstatusList));
                RaisePropertyChangedEvent(nameof(packingstatusList));
                RaisePropertyChangedEvent(nameof(GreenIn));
                RaisePropertyChangedEvent(nameof(PackedStockIn));

                _feedingBarcode = 0;
                _feedingWeight = 0;
                _packedOutCases = 0;
                _packedOutWeight = 0;
                _periodYield = 0;
                RaisePropertyChangedEvent(nameof(FeedingBarcode));
                RaisePropertyChangedEvent(nameof(FeedingWeight));
                RaisePropertyChangedEvent(nameof(PackedOutCases));
                RaisePropertyChangedEvent(nameof(PackedOutWeight));
                RaisePropertyChangedEvent(nameof(PeriodYield));

                _periodLockedBGColor = null;
                _periodLockedText = null;
                RaisePropertyChangedEvent(nameof(PeriodLockedText));
                RaisePropertyChangedEvent(nameof(PeriodLockedBGColor));

                var packedgrade = Facade.packedgradeBL().GetSingle(pdsetup.packedgrade);
                if (packedgrade == null)
                    throw new ArgumentException("ไม่พบข้อมูล packedgrade โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                _type = packedgrade.type;
                _packedgrade = packedgrade.packedgrade1;
                _customer = packedgrade.customer + " / " + packedgrade.form;

                RaisePropertyChangedEvent(nameof(type));
                RaisePropertyChangedEvent(nameof(packedgrade));
                RaisePropertyChangedEvent(nameof(customer));

                //SummaryProductionBinding(); performance poor.

                BlendingStatusListBinding();
                if (_blendingstatusList.Count() > 0)
                {
                    _blendinghour = _blendingstatusList.Max(x => x.blendinghour);
                    RaisePropertyChangedEvent(nameof(blendinghour));
                    PeriodChanged();
                }

                FinishPeriodButtonChanged();
                NewPeriodButtonChanged();
                DeletePeriodButtonChanged();
                FinishGradeChanged();

                _bc = null;
                OnFocusRequested(nameof(bc));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PeriodChanged()
        {
            try
            {
                _feedingBarcode = 0;
                _feedingWeight = 0;
                _packedOutCases = 0;
                _packedOutWeight = 0;
                _periodYield = 0;
                RaisePropertyChangedEvent(nameof(FeedingBarcode));
                RaisePropertyChangedEvent(nameof(FeedingWeight));
                RaisePropertyChangedEvent(nameof(PackedOutCases));
                RaisePropertyChangedEvent(nameof(PackedOutWeight));
                RaisePropertyChangedEvent(nameof(PeriodYield));

                _periodLockedBGColor = null;
                _periodLockedText = null;
                RaisePropertyChangedEvent(nameof(PeriodLockedText));
                RaisePropertyChangedEvent(nameof(PeriodLockedBGColor));

                _blendingstatus = _blendingstatusList
                    .SingleOrDefault(x => x.blendinghour == _blendinghour);
                RaisePropertyChangedEvent(nameof(blendingstatus));

                if (_blendingstatus == null)
                    return;

                _periodLockedText = _blendingstatus.locked == true ? "Locked" : "Unlocked";
                _periodLockedBGColor = _blendingstatus.locked == true ? Brushes.Salmon : Brushes.LightGreen;
                RaisePropertyChangedEvent(nameof(PeriodLockedText));
                RaisePropertyChangedEvent(nameof(PeriodLockedBGColor));

                GreenInListBinding();
                PackedInListBinding();
                SummaryPeriodBinding();
                FinishPeriodButtonChanged();
                NewPeriodButtonChanged();
                DeletePeriodButtonChanged();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void FinishPeriodButtonChanged()
        {
            try
            {
                _periodFinishButtonVisibility = Visibility.Collapsed;
                RaisePropertyChangedEvent(nameof(PeriodFinishButtonVisibility));

                if (_pdsetup.blendinglocked == true)
                    return;

                if (_blendingstatusList.Count() <= 0)
                    return;

                if (_blendingstatusList.Where(x => x.locked == false).Count() <= 0)
                    return;

                if (_blendingstatus.locked == true)
                    return;

                if (_greenInList.Count() <= 0)
                    return;

                _periodFinishButtonVisibility = Visibility.Visible;
                RaisePropertyChangedEvent(nameof(PeriodFinishButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void NewPeriodButtonChanged()
        {
            try
            {
                _periodNewButtonVisibility = Visibility.Collapsed;
                RaisePropertyChangedEvent(nameof(PeriodNewButtonVisibility));

                if (_pdsetup.blendinglocked == true)
                    return;

                if (_blendingstatusList.Where(x => x.locked == false).Count() > 0)
                    return;

                _periodNewButtonVisibility = Visibility.Visible;
                RaisePropertyChangedEvent(nameof(PeriodNewButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeletePeriodButtonChanged()
        {
            try
            {
                _periodDeleteButtonVisibility = Visibility.Collapsed;
                RaisePropertyChangedEvent(nameof(PeriodDeleteButtonVisibility));

                if (_pdsetup.blendinglocked == true)
                    return;

                if (_blendingstatus == null)
                    return;

                if (_blendingstatus.locked == true)
                    return;

                if (_greenInList.Count() > 0)
                    return;

                _periodDeleteButtonVisibility = Visibility.Visible;
                RaisePropertyChangedEvent(nameof(PeriodDeleteButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void FinishGradeChanged()
        {
            try
            {
                _finishGradeButtonVisibility = Visibility.Collapsed;
                RaisePropertyChangedEvent(nameof(FinishGradeButtonVisibility));

                if (_pdsetup.blendinglocked == true)
                    return;

                if (_blendingstatusList.Count() <= 0)
                    return;

                if (_blendingstatusList.Where(x => x.locked == true).Count() <= 0)
                    return;

                _finishGradeButtonVisibility = Visibility.Visible;
                RaisePropertyChangedEvent(nameof(FinishGradeButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}