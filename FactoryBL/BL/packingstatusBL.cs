﻿using FactoryBL.Model;
using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IpackingstatusBL
    {
        int Add(string pdno);
        void Finish(string pdno, int packinghour);
        void Delete(string pdno, int packinghour);
        List<packingstatu> GetByPdno(string pdno);
        packingstatu GetSingle(string pdno, int packinghour);
    }

    public class packingstatusBL : IpackingstatusBL
    {
        StecDBMSUnitOfWork uow;
        public packingstatusBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public int Add(string pdno)
        {
            try
            {
                var pdsetup = uow.pdsetupRepo.GetSingle(x => x.pdno == pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (pdsetup.packinglocked == true)
                    throw new ArgumentException("(packinglocked) เกรดนี้ท้ายเครื่อง locked แล้ว" +
                        " หากต้องการปลดล็อคให้ติดต่อหัวหน้างานเพื่อขอปลดล็อค");

                var packingstatusList = GetByPdno(pdno);
                if (packingstatusList.Where(x => x.locked == false).Count() > 0)
                    throw new ArgumentException("ชั่วโมงที่ " +
                            packingstatusList.FirstOrDefault(x => x.locked == false).packinghour +
                            " ยังไม่ถูกล็อค ไม่สามารถเพิ่มชั่วโมงใหม่ได้");

                var nextHour = 1;
                if (packingstatusList.Count() > 0)
                    nextHour = packingstatusList.Max(x => x.packinghour) + 1;

                uow.packingstatusRepo.Add(new packingstatu
                {
                    crop = pdsetup.crop,
                    pdno = pdno,
                    pddate = (DateTime)pdsetup.date,
                    packinghour = nextHour,
                    locked = false
                });
                uow.Save();

                return nextHour;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string pdno, int packinghour)
        {
            try
            {
                if (string.IsNullOrEmpty(pdno))
                    throw new ArgumentException("โปรดเลือกเกรดที่จะลบชั่วโมง โดยคลิกที่ปุ่ม New Grade");

                var pdsetup = uow.pdsetupRepo.GetSingle(x => x.pdno == pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (pdsetup.packinglocked == true)
                    throw new ArgumentException("pdno นี้ถูกล็อคแล้วไม่สามารถเพิ่มชั่วโมงได้ (packinglocked = ture)" +
                        " โปรดติดต่อแผนก Leaf Account เพื่อตรวจสอบหรือปลดล็อค");

                var pdOutList = Facade.pdBL().GetByFromPdHour(pdno, packinghour);
                if (pdOutList.Count() >= 1)
                    throw new ArgumentException("ชั่วโมงนี้มียาออกแล้วที่ท้ายเครื่องแล้ว ไม่สามารถลบชั่วโมงได้ โปรดตรวจสอบกับท้ายเครื่อง");

                var model = GetSingle(pdno, packinghour);
                if (model == null)
                    throw new ArgumentException("ไม่พบชั่วโมงดังกล่าวในระบบ");

                if (model.locked == true)
                    throw new ArgumentException("ชั่วโมงนี้อยู่ในสถานะ locked แล้ว ไม่สามารถลบได้");

                uow.packingstatusRepo.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Finish(string pdno, int packinghour)
        {
            try
            {
                var model = GetSingle(pdno, packinghour);
                if (model == null)
                    throw new ArgumentException("ไม่พบชั่วโมงดังกล่าวในระบบ");

                model.locked = true;
                uow.packingstatusRepo.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<packingstatu> GetByPdno(string pdno)
        {
            return uow.packingstatusRepo.Get(x => x.pdno == pdno);
        }

        public packingstatu GetSingle(string pdno, int packinghour)
        {
            return uow.packingstatusRepo
                .GetSingle(x => x.pdno == pdno && x.packinghour == packinghour);
        }
    }
}
