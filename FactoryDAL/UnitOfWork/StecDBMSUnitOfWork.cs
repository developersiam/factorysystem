﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using FactoryDAL;
using FactoryDAL.EDMX;
using FactoryEntities;

namespace FactoryDAL.UnitOfWork
{
    public class StecDBMSUnitOfWork : IStecDBMSUnitOfWork, System.IDisposable
    {
        private readonly StecDBMSEntities _context;

        private IGenericDataRepository<BarcodePrinter> _barcodePrinterRepo;
        private IGenericDataRepository<BarcodeTemplate> _barcodeTemplateRepo;
        private IGenericDataRepository<BarcodeSetting> _barcodeSettingRepo;
        private IGenericDataRepository<blendingstatu> _blendingstatusRepo;
        private IGenericDataRepository<classify> _classifyRepo;
        private IGenericDataRepository<company> _companyRepo;
        private IGenericDataRepository<expert> _expertRepo;
        private IGenericDataRepository<mat> _matRepo;
        private IGenericDataRepository<mati> _matisRepo;
        private IGenericDataRepository<matisno> _matisnoRepo;
        private IGenericDataRepository<matrc> _matrcRepo;
        private IGenericDataRepository<matrcno> _matrcnoRepo;
        private IGenericDataRepository<matrgno> _matrgnoRepo;
        private IGenericDataRepository<matwh> _matwhRepo;
        private IGenericDataRepository<packedgrade> _packedgradeRepo;
        private IGenericDataRepository<packingstatu> _packingstatusRepo;
        private IGenericDataRepository<pdbc> _pdbcRepo;
        private IGenericDataRepository<pd> _pdRepo;
        private IGenericDataRepository<PDCustomer> _pdCustomerRepo;
        private IGenericDataRepository<PDCustomerPackedPrint> _pdCusPackedPrintRepo;
        private IGenericDataRepository<pdsetup> _pdsetupRepo;
        private IGenericDataRepository<security> _securityRepo;
        private IGenericDataRepository<sfsetup> _sfsetupRepo;
        private IGenericDataRepository<ShippingMovement> _shippingMovementRepo;
        private IGenericDataRepository<subtype> _subtypeRepo;
        private IGenericDataRepository<supplier> _supplierRepo;
        private IGenericDataRepository<type> _typeRepo;
        private IGenericDataRepository<picking> _pickingRepo;
        private IGenericDataRepository<pdsp> _pdspRepo;
        private IGenericDataRepository<ShippingOrder> _shippingOrderRepo;
        private IGenericDataRepository<ShippingTruck> _shippingTruckRepo;
        private IGenericDataRepository<ShippingTruckDriver> _shippingTruckDriverRepo;
        private IGenericDataRepository<ShippingLocation> _shippingLocationRepo;
        private IGenericDataRepository<ShippingMovementTransaction> _shippingMovementTransactionRepo;

        public StecDBMSUnitOfWork()
        {
            _context = new StecDBMSEntities();
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        public IGenericDataRepository<BarcodePrinter> barcodePrinterRepo
        {
            get { return _barcodePrinterRepo ?? (_barcodePrinterRepo = new GenericDataRepository<BarcodePrinter>(_context)); }
        }

        public IGenericDataRepository<BarcodeTemplate> barcodeTemplateRepo
        {
            get { return _barcodeTemplateRepo ?? (_barcodeTemplateRepo = new GenericDataRepository<BarcodeTemplate>(_context)); }
        }

        public IGenericDataRepository<BarcodeSetting> barcodeSettingRepo
        {
            get { return _barcodeSettingRepo ?? (_barcodeSettingRepo = new GenericDataRepository<BarcodeSetting>(_context)); }
        }

        public IGenericDataRepository<blendingstatu> blendingstatusRepo
        {
            get { return _blendingstatusRepo ?? (_blendingstatusRepo = new GenericDataRepository<blendingstatu>(_context)); }
        }

        public IGenericDataRepository<classify> classifyRepo
        {
            get { return _classifyRepo ?? (_classifyRepo = new GenericDataRepository<classify>(_context)); }
        }

        public IGenericDataRepository<company> companyRepo
        {
            get { return _companyRepo ?? (_companyRepo = new GenericDataRepository<company>(_context)); }
        }

        public IGenericDataRepository<expert> expertRepo
        {
            get { return _expertRepo ?? (_expertRepo = new GenericDataRepository<expert>(_context)); }
        }

        public IGenericDataRepository<mat> matRepo
        {
            get { return _matRepo ?? (_matRepo = new GenericDataRepository<mat>(_context)); }
        }

        public IGenericDataRepository<mati> matisRepo
        {
            get { return _matisRepo ?? (_matisRepo = new GenericDataRepository<mati>(_context)); }
        }

        public IGenericDataRepository<matisno> matisnoRepo
        {
            get { return _matisnoRepo ?? (_matisnoRepo = new GenericDataRepository<matisno>(_context)); }
        }

        public IGenericDataRepository<matrc> matrcRepo
        {
            get { return _matrcRepo ?? (_matrcRepo = new GenericDataRepository<matrc>(_context)); }
        }

        public IGenericDataRepository<matrcno> matrcnoRepo
        {
            get { return _matrcnoRepo ?? (_matrcnoRepo = new GenericDataRepository<matrcno>(_context)); }
        }

        public IGenericDataRepository<matrgno> matrgnoRepo
        {
            get { return _matrgnoRepo ?? (_matrgnoRepo = new GenericDataRepository<matrgno>(_context)); }
        }

        public IGenericDataRepository<matwh> matwhRepo
        {
            get { return _matwhRepo ?? (_matwhRepo = new GenericDataRepository<matwh>(_context)); }
        }

        public IGenericDataRepository<packedgrade> packedgradeRepo
        {
            get { return _packedgradeRepo ?? (_packedgradeRepo = new GenericDataRepository<packedgrade>(_context)); }
        }

        public IGenericDataRepository<packingstatu> packingstatusRepo
        {
            get { return _packingstatusRepo ?? (_packingstatusRepo = new GenericDataRepository<packingstatu>(_context)); }
        }

        public IGenericDataRepository<pd> pdRepo
        {
            get { return _pdRepo ?? (_pdRepo = new GenericDataRepository<pd>(_context)); }
        }

        public IGenericDataRepository<pdbc> pdbcRepo
        {
            get { return _pdbcRepo ?? (_pdbcRepo = new GenericDataRepository<pdbc>(_context)); }
        }

        public IGenericDataRepository<PDCustomer> pdCustomeRepo
        {
            get { return _pdCustomerRepo ?? (_pdCustomerRepo = new GenericDataRepository<PDCustomer>(_context)); }
        }

        public IGenericDataRepository<PDCustomerPackedPrint> pdCusPackedPrintRepo
        {
            get { return _pdCusPackedPrintRepo ?? (_pdCusPackedPrintRepo = new GenericDataRepository<PDCustomerPackedPrint>(_context)); }
        }

        public IGenericDataRepository<pdsetup> pdsetupRepo
        {
            get { return _pdsetupRepo ?? (_pdsetupRepo = new GenericDataRepository<pdsetup>(_context)); }
        }

        public IGenericDataRepository<security> securityRepo
        {
            get { return _securityRepo ?? (_securityRepo = new GenericDataRepository<security>(_context)); }
        }

        public IGenericDataRepository<sfsetup> sfsetupRepo
        {
            get { return _sfsetupRepo ?? (_sfsetupRepo = new GenericDataRepository<sfsetup>(_context)); }
        }

        public IGenericDataRepository<ShippingMovement> shippingMovementRepo
        {
            get { return _shippingMovementRepo ?? (_shippingMovementRepo = new GenericDataRepository<ShippingMovement>(_context)); }
        }

        public IGenericDataRepository<subtype> subtypeRepo
        {
            get { return _subtypeRepo ?? (_subtypeRepo = new GenericDataRepository<subtype>(_context)); }
        }

        public IGenericDataRepository<supplier> supplierRepo
        {
            get { return _supplierRepo ?? (_supplierRepo = new GenericDataRepository<supplier>(_context)); }
        }

        public IGenericDataRepository<type> typeRepo
        {
            get { return _typeRepo ?? (_typeRepo = new GenericDataRepository<type>(_context)); }
        }

        public IGenericDataRepository<picking> pickingRepo
        {
            get { return _pickingRepo ?? (_pickingRepo = new GenericDataRepository<picking>(_context)); }
        }

        public IGenericDataRepository<pdsp> pdspRepo
        {
            get { return _pdspRepo ?? (_pdspRepo = new GenericDataRepository<pdsp>(_context)); }
        }

        public IGenericDataRepository<ShippingOrder> shippingOrderRepo
        {
            get { return _shippingOrderRepo ?? (_shippingOrderRepo = new GenericDataRepository<ShippingOrder>(_context)); }
        }

        public IGenericDataRepository<ShippingTruckDriver> ShippingTruckDriverRepo
        {
            get { return _shippingTruckDriverRepo ?? (_shippingTruckDriverRepo = new GenericDataRepository<ShippingTruckDriver>(_context)); }
        }

        public IGenericDataRepository<ShippingTruck> ShippingTruckRepo
        {
            get { return _shippingTruckRepo ?? (_shippingTruckRepo = new GenericDataRepository<ShippingTruck>(_context)); }
        }

        public IGenericDataRepository<ShippingLocation> ShippingLocationRepo
        {
            get { return _shippingLocationRepo ?? (_shippingLocationRepo = new GenericDataRepository<ShippingLocation>(_context)); }
        }

        public IGenericDataRepository<ShippingMovementTransaction> ShippingMovementTransactionRepo
        {
            get { return _shippingMovementTransactionRepo ?? (_shippingMovementTransactionRepo = new GenericDataRepository<ShippingMovementTransaction>(_context)); }
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            System.GC.SuppressFinalize(this);
        }
    }
}