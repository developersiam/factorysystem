﻿using FactorySystemWPF.Helper;
using FactorySystemWPF.MVVM;
using FactoryBL;
using FactoryBL.Model;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using FactorySystemWPF.View.Processing;

namespace FactorySystemWPF.ViewModel.Processing
{
    public class vm_NewGrade : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_NewGrade()
        {
            //_productionDate = DateTime.Now.Date;
            //_productionDate = Convert.ToDateTime("2022-05-20"); //use for testing in a blending system.
            // _productionDate = Convert.ToDateTime("2021-07-13"); //use for testing in a packing system.
            _productionDate = Convert.ToDateTime("2017-05-08"); //use for customer label print testing in a packing system.
            //_productionDate = DateTime.Now;
            RaisePropertyChangedEvent(nameof(ProductionDate));
            DataGridBinding();
        }


        #region Properties
        private DateTime _productionDate;

        public DateTime ProductionDate
        {
            get { return _productionDate; }
            set
            {
                _productionDate = value;
                DataGridBinding();
            }
        }

        private pdsetup _selectedItem;

        public pdsetup SelectedItem
        {
            get { return _selectedItem; }
            set { _selectedItem = value; }
        }

        private NewGrade _window;

        public NewGrade Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        #endregion



        #region List
        private List<pdsetup> _pdsetupList;

        public List<pdsetup> pdsetupList
        {
            get { return _pdsetupList; }
            set { _pdsetupList = value; }
        }
        #endregion



        #region Command
        private ICommand _onSelectedCommand;

        public ICommand OnSelectedCommand
        {
            get { return _onSelectedCommand ?? (_onSelectedCommand = new RelayCommand(OnSelected)); }
            set { _onSelectedCommand = value; }
        }

        private void OnSelected(object obj)
        {
            try
            {
                var item = (pdsetup)obj;
                if (item == null)
                    return;

                _selectedItem = item;
                RaisePropertyChangedEvent(nameof(SelectedItem));
                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onRefreshCommand;

        public ICommand OnRefreshCommand
        {
            get { return _onRefreshCommand ?? (_onRefreshCommand = new RelayCommand(OnRefresh)); }
            set { _onRefreshCommand = value; }
        }

        private void OnRefresh(object obj)
        {
            DataGridBinding();
        }
        #endregion



        #region Function
        private void DataGridBinding()
        {
            try
            {
                _pdsetupList = Facade.pdsetupBL()
                        .GetByDateAndDefault(_productionDate)
                        .OrderBy(x => x.pdno)
                        .ToList();
                _totalRecord = _pdsetupList.Count();
                RaisePropertyChangedEvent(nameof(pdsetupList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
