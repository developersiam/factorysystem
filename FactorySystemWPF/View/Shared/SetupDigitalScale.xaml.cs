﻿using FactorySystemWPF.MVVM;
using FactorySystemWPF.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FactorySystemWPF.View.Shared
{
    /// <summary>
    /// Interaction logic for SetupDigitalScale.xaml
    /// </summary>
    public partial class SetupDigitalScale : Window
    {
        public SetupDigitalScale()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_SetupDigitalScale)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.Model):
                    modelComboBox.Focus();
                    break;
                case nameof(vm.PortName):
                    portNameComboBox.Focus();
                    break;
                case nameof(vm.BuardRate):
                    buardRateComboBox.Focus();
                    break;
                case nameof(vm.DataBit):
                    dataBitComboBox.Focus();
                    break;
                case nameof(vm.Parity):
                    parityComboBox.Focus();
                    break;
                case nameof(vm.StopBit):
                    stopBitComboBox.Focus();
                    break;
                case nameof(vm.ThreadSleep):
                    threadSleepComboBox.Focus();
                    break;
            }
        }
    }
}
