﻿using DomainModelHris;
using FactoryBL.Model;
using HRISSystemBL.BL.HRISBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.Helper
{
    public static class h_TruckDriver
    {
        public static List<m_TruckDriver> GetAll()
        {
            try
            {
                var truckDrivers = Facade.ShippingTruckDriverBL().GetAll();
                var personIDs = new List<string>();
                foreach (var item in truckDrivers)
                    personIDs.Add(item.PersonID);
                var employees = HRISBLServices.EmployeeBL().GetByPersonIDList(personIDs)
                    .GroupBy(x => new
                    {
                        x.Person_ID,
                        x.Person.TitleName.TitleNameInitialTH,
                        x.Person.FirstNameTH,
                        x.Person.LastNameTH
                    })
                    .Select(x => new m_TruckDriver
                    {
                        PersonID = x.Key.Person_ID,
                        Prefix = x.Key.TitleNameInitialTH,
                        FirstName = x.Key.FirstNameTH,
                        LastName = x.Key.LastNameTH
                    })
                    .ToList();
                var returnList = (from a in truckDrivers
                                  from b in employees
                                  where a.PersonID == b.PersonID
                                  select new m_TruckDriver
                                  {
                                      PersonID = a.PersonID,
                                      ActiveStatus = a.ActiveStatus,
                                      Prefix = b.Prefix,
                                      FirstName = b.FirstName,
                                      LastName = b.LastName,
                                      CreateBy = a.CreateBy,
                                      CreateDate = a.CreateDate,
                                      ModifiedBy = a.ModifiedBy,
                                      ModifiedDate = a.ModifiedDate
                                  })
                                  .ToList();
                return returnList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<m_TruckDriver> GetByStatus(bool status)
        {
            try
            {
                var truckDrivers = Facade.ShippingTruckDriverBL().GetByActiveStatus(status);
                var personIDs = new List<string>();
                foreach (var item in truckDrivers)
                    personIDs.Add(item.PersonID);
                var employees = HRISBLServices.EmployeeBL().GetByPersonIDList(personIDs)
                    .GroupBy(x => new
                    {
                        x.Person_ID,
                        x.Person.TitleName.TitleNameInitialTH,
                        x.Person.FirstNameTH,
                        x.Person.LastNameTH
                    })
                    .Select(x => new m_TruckDriver
                    {
                        PersonID = x.Key.Person_ID,
                        Prefix = x.Key.TitleNameInitialTH,
                        FirstName = x.Key.FirstNameTH,
                        LastName = x.Key.LastNameTH
                    })
                    .ToList();
                var returnList = (from a in truckDrivers
                                  from b in employees
                                  where a.PersonID == b.PersonID
                                  select new m_TruckDriver
                                  {
                                      PersonID = a.PersonID,
                                      ActiveStatus = a.ActiveStatus,
                                      Prefix = b.Prefix,
                                      FirstName = b.FirstName,
                                      LastName = b.LastName,
                                      CreateBy = a.CreateBy,
                                      CreateDate = a.CreateDate,
                                      ModifiedBy = a.ModifiedBy,
                                      ModifiedDate = a.ModifiedDate
                                  })
                                  .ToList();
                return returnList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static m_TruckDriver GetSingle(string personID)
        {
            var truckDriver = Facade.ShippingTruckDriverBL().GetSingle(personID);
            var employee = HRISBLServices.EmployeeBL().GetSingle(personID);
            return new m_TruckDriver
            {
                PersonID = personID,
                Prefix = employee.Person.TitleName.TitleNameInitialTH,
                FirstName = employee.Person.FirstNameTH,
                LastName = employee.Person.LastNameTH,
                ActiveStatus = truckDriver.ActiveStatus,
                CreateBy = truckDriver.CreateBy,
                CreateDate = truckDriver.CreateDate,
                ModifiedBy = truckDriver.ModifiedBy,
                ModifiedDate = truckDriver.ModifiedDate
            };
        }

        public static List<m_TruckDriver> GetByDistinct()
        {
            try
            {
                var fromHRISList = HRISBLServices.EmployeeBL()
                    .GetByDepartmentList(new List<string> { "TRD", "FLD" })
                    .Where(x => x.Staff_status == 1)
                    .Select(x => new m_TruckDriver
                    {
                        PersonID = x.Person_ID
                    })
                    .ToList();

                var truckDrivers = Facade.ShippingTruckDriverBL()
                    .GetAll()
                    .Select(x => new m_TruckDriver
                    {
                        PersonID = x.PersonID
                    })
                    .ToList();

                var a = new List<string>();
                var b = new List<string>();
                foreach (var item in fromHRISList)
                    a.Add(item.PersonID);

                foreach (var item in truckDrivers)
                    b.Add(item.PersonID);

                var joinList = a.Except(b).ToList();

                var resultList = HRISBLServices.EmployeeBL()
                    .GetByPersonIDList(joinList)
                    .GroupBy(x => new
                    {
                        x.Person_ID,
                        x.Person.TitleName.TitleNameTH,
                        x.Person.FirstNameTH,
                        x.Person.LastNameTH
                    })
                    .Select(x => new m_TruckDriver
                    {
                        PersonID = x.Key.Person_ID,
                        Prefix = x.Key.TitleNameTH,
                        FirstName = x.Key.FirstNameTH,
                        LastName = x.Key.LastNameTH
                    })
                    .ToList();

                return resultList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
