﻿using FactoryBL;
using FactoryBL.Model;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.Model;
using FactorySystemWPF.MVVM;
using FactorySystemWPF.View.Processing.ProductionControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using m_mode = FactoryBL.Model.m_mode;

namespace FactorySystemWPF.ViewModel.Processing.ProductionControl
{
    public class vm_ProductionSetup : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_ProductionSetup()
        {
            _crop = DateTime.Now.Year;
            _modeList = new List<m_mode>();
            _modeList.Add(new m_mode { Value = "Normal", Text = "อบปกติ" });
            _modeList.Add(new m_mode { Value = "Fix", Text = "อบซ่อม" });
            _modeList.Add(new m_mode { Value = "Continue", Text = "อบต่อ (Try run)" });
            _packedgradeList = Facade.packedgradeBL().GetByCrop(_crop);
            _pdnoStatusBGColor = Brushes.Honeydew;

            RaisePropertyChangedEvent(nameof(crop));
            RaisePropertyChangedEvent(nameof(mode));
            RaisePropertyChangedEvent(nameof(packedgradeList));
            RaisePropertyChangedEvent(nameof(pdnoStatusBGColor));

            pdsetupListBinding();
            ClearForm();
        }


        #region Properties
        private int _crop;

        public int crop
        {
            get { return _crop; }
            set { _crop = value; }
        }

        private m_pdsetup _selectedEditItem;

        public m_pdsetup SelectedEditItem
        {
            get { return _selectedEditItem; }
            set { _selectedEditItem = value; }
        }

        private string _pdno;

        public string pdno
        {
            get { return _pdno; }
            set { _pdno = value; }
        }

        private string _packedgrade;

        public string packedgrade
        {
            get { return _packedgrade; }
            set
            {
                _packedgrade = value;
                packedgradeChanged();
            }
        }

        private string _type;

        public string type
        {
            get { return _type; }
            set { _type = value; }
        }

        private string _customer;

        public string customer
        {
            get { return _customer; }
            set { _customer = value; }
        }

        private string _form;

        public string form
        {
            get { return _form; }
            set { _form = value; }
        }

        private string _packingmat;

        public string packingmat
        {
            get { return _packingmat; }
            set { _packingmat = value; }
        }

        private decimal _netdef;

        public decimal netdef
        {
            get { return _netdef; }
            set { _netdef = value; }
        }

        private decimal _taredef;

        public decimal taredef
        {
            get { return _taredef; }
            set { _taredef = value; }
        }

        private decimal _grossdef;

        public decimal grossdef
        {
            get { return _grossdef; }
            set { _grossdef = value; }
        }

        private DateTime _date;

        public DateTime date
        {
            get { return _date; }
            set { _date = value; }
        }

        private string _mode;

        public string mode
        {
            get { return _mode; }
            set { _mode = value; }
        }

        private string _remark;

        public string remark
        {
            get { return _remark; }
            set { _remark = value; }
        }

        private string _remarkparent;

        public string remarkparent
        {
            get { return _remarkparent; }
            set { _remarkparent = value; }
        }

        private bool _def;

        public bool def
        {
            get { return _def; }
            set { _def = value; }
        }

        private Brush _pdnoStatusBGColor;

        public Brush pdnoStatusBGColor
        {
            get { return _pdnoStatusBGColor; }
            set { _pdnoStatusBGColor = value; }
        }

        private Visibility _addbuttonVisibility;

        public Visibility AddButtonVisibility
        {
            get { return _addbuttonVisibility; }
            set { _addbuttonVisibility = value; }
        }

        private Visibility _editButtonVisibility;

        public Visibility EditButtonVisibility
        {
            get { return _editButtonVisibility; }
            set { _editButtonVisibility = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }
        #endregion



        #region List
        private List<packedgrade> _packedgradeList;

        public List<packedgrade> packedgradeList
        {
            get { return _packedgradeList; }
            set { _packedgradeList = value; }
        }

        private List<m_mode> _modeList;

        public List<m_mode> ModeList
        {
            get { return _modeList; }
            set { _modeList = value; }
        }

        private List<m_pdsetup> _pdsetupList;

        public List<m_pdsetup> pdsetupList
        {
            get { return _pdsetupList; }
            set { _pdsetupList = value; }
        }

        #endregion



        #region Command
        private ICommand _onAddCommand;

        public ICommand OnAddCommand
        {
            get { return _onAddCommand ?? (_onAddCommand = new RelayCommand(Add)); }
            set { _onAddCommand = value; }
        }

        private void Add(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการเพิ่ม production no. ใหม่ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                Facade.pdsetupBL()
                    .Add(_packedgrade,
                    _mode,
                    user_setting.security.uname,
                    _remark,
                    _remarkparent,
                    _date,
                    _def);
                ClearForm();
                MessageBoxHelper.Info("เพิ่มข้อมูลสำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onEditCommand;

        public ICommand OnEditCommand
        {
            get { return _onEditCommand ?? (_onEditCommand = new RelayCommand(Edit)); }
            set { _onEditCommand = value; }
        }

        private void Edit(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("การแก้ไขข้อมูลอาจส่งผลกระทบต่อข้อมูลการผลิต " +
                    "ท่านต้องการแก้ไข production no. นีใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                Facade.pdsetupBL()
                    .Update(_pdno,
                    _packedgrade,
                    _mode,
                    user_setting.security.uname,
                    _remark,
                    _remarkparent,
                    _date,
                    _def);

                ClearForm();
                MessageBoxHelper.Info("แก้ไขข้อมูลสำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(Clear)); }
            set { _onClearCommand = value; }
        }

        private void Clear(object obj)
        {
            ClearForm();
        }

        private ICommand _onEditSelectedCommand;

        public ICommand OnEditSelectedCommand
        {
            get { return _onEditSelectedCommand ?? (_onEditSelectedCommand = new RelayCommand(EditSelected)); }
            set { _onEditSelectedCommand = value; }
        }

        private void EditSelected(object obj)
        {
            try
            {
                _selectedEditItem = (m_pdsetup)obj;
                if (_selectedEditItem == null)
                    return;

                _pdno = _selectedEditItem.pdno;
                _packedgrade = _selectedEditItem.packedgrade;
                _type = _selectedEditItem.type;
                _customer = _selectedEditItem.customer;
                _form = _selectedEditItem.form;
                _packingmat = _selectedEditItem.packingmat;
                _netdef = _selectedEditItem.netdef;
                _taredef = _selectedEditItem.taredef;
                _grossdef = _selectedEditItem.grossdef;
                _date = (DateTime)_selectedEditItem.date;
                _def = (bool)_selectedEditItem.def;
                _remark = _selectedEditItem.pdremark;
                _remarkparent = _selectedEditItem.PdRemarkParent;
                _mode = _selectedEditItem.mode;
                _addbuttonVisibility = Visibility.Collapsed;
                _editButtonVisibility = Visibility.Visible;
                _pdnoStatusBGColor = Brushes.Ivory;

                RaisePropertyChangedEvent(nameof(pdno));
                RaisePropertyChangedEvent(nameof(packedgrade));
                RaisePropertyChangedEvent(nameof(type));
                RaisePropertyChangedEvent(nameof(customer));
                RaisePropertyChangedEvent(nameof(form));
                RaisePropertyChangedEvent(nameof(packingmat));
                RaisePropertyChangedEvent(nameof(netdef));
                RaisePropertyChangedEvent(nameof(taredef));
                RaisePropertyChangedEvent(nameof(grossdef));
                RaisePropertyChangedEvent(nameof(def));
                RaisePropertyChangedEvent(nameof(remark));
                RaisePropertyChangedEvent(nameof(remarkparent));
                RaisePropertyChangedEvent(nameof(mode));
                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
                RaisePropertyChangedEvent(nameof(EditButtonVisibility));
                RaisePropertyChangedEvent(nameof(pdnoStatusBGColor));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onDeleteSelectedCommand;

        public ICommand OnDeleteSelectedCommand
        {
            get { return _onDeleteSelectedCommand ?? (_onDeleteSelectedCommand = new RelayCommand(DeleteSelected)); }
            set { _onDeleteSelectedCommand = value; }
        }

        private void DeleteSelected(object obj)
        {
            try
            {
                var selectedItem = (m_pdsetup)obj;
                if (selectedItem == null)
                    return;

                if (MessageBoxHelper.Question("ก่อนลบข้อมูลจะส่งผลกระทบต่อข้อมูลการผลิตในปัจจุบันและก่อนหน้านี้ " +
                    "ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                Facade.pdsetupBL().Delete(selectedItem.pdno);
                ClearForm();
                MessageBoxHelper.Info("ลบข้อมูลสำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSetDefaultCommand;

        public ICommand OnSetDefaultCommand
        {
            get { return _onSetDefaultCommand ?? (_onSetDefaultCommand = new RelayCommand(SetDefault)); }
            set { _onSetDefaultCommand = value; }
        }

        private void SetDefault(object obj)
        {
            try
            {
                var selectedItem = (m_pdsetup)obj;
                if (selectedItem == null)
                    return;

                if (MessageBoxHelper.Question("การเปลี่ยน default นี้จะส่งผลกระทบต่อหน้างานของกระบวนการผลิต" +
                    " ท่านแน่ใจว่าจะให้ pdno นี้เป็น default ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                Facade.pdsetupBL().SetDefault(selectedItem.pdno, user_setting.security.uname);
                ClearForm();
                MessageBoxHelper.Info("เปลี่ยน default สำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSetByProductCommand;

        public ICommand OnSetByProductCommand
        {
            get { return _onSetByProductCommand ?? (_onSetByProductCommand = new RelayCommand(SetByProduct)); }
            set { _onSetByProductCommand = value; }
        }

        private void SetByProduct(object obj)
        {
            try
            {
                if (_selectedEditItem == null)
                    throw new ArgumentException("โปรดเลือกเกรดแม่ที่ต้องการบันทึก ByProduct โดยการกดปุ่ม Edit ได้จากตารางด้านล่าง");

                var vm = new vm_ByProductSetup();
                var window = new ByProductSetup();
                vm.pdsetup = _selectedEditItem;
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion



        #region Function
        private void ClearForm()
        {
            _pdno = Facade.pdsetupBL().GetMaxProductionNo();
            _packedgrade = null;
            _type = null;
            _customer = null;
            _form = null;
            _packingmat = null;
            _netdef = (decimal)0.0;
            _taredef = (decimal)0.0;
            _grossdef = (decimal)0.0;
            _date = DateTime.Now;
            _def = true;
            _remark = null;
            _remarkparent = null;
            _mode = "Normal";
            _addbuttonVisibility = Visibility.Visible;
            _editButtonVisibility = Visibility.Collapsed;
            _pdnoStatusBGColor = Brushes.Honeydew;
            _selectedEditItem = null;

            RaisePropertyChangedEvent(nameof(packedgrade));
            RaisePropertyChangedEvent(nameof(type));
            RaisePropertyChangedEvent(nameof(customer));
            RaisePropertyChangedEvent(nameof(form));
            RaisePropertyChangedEvent(nameof(packingmat));
            RaisePropertyChangedEvent(nameof(netdef));
            RaisePropertyChangedEvent(nameof(taredef));
            RaisePropertyChangedEvent(nameof(grossdef));
            RaisePropertyChangedEvent(nameof(def));
            RaisePropertyChangedEvent(nameof(remark));
            RaisePropertyChangedEvent(nameof(remarkparent));
            RaisePropertyChangedEvent(nameof(mode));
            RaisePropertyChangedEvent(nameof(AddButtonVisibility));
            RaisePropertyChangedEvent(nameof(EditButtonVisibility));
            RaisePropertyChangedEvent(nameof(pdnoStatusBGColor));
            RaisePropertyChangedEvent(nameof(SelectedEditItem));

            pdsetupListBinding();
        }

        private void pdsetupListBinding()
        {
            try
            {
                var pdsetupList = Facade.pdsetupBL().GetByCrop(_crop);
                _pdsetupList = (from s in pdsetupList
                                from g in _packedgradeList
                                where s.packedgrade == g.packedgrade1
                                select new m_pdsetup
                                {
                                    def = s.def,
                                    date = s.date,
                                    pdno = s.pdno,
                                    packedgrade = g.packedgrade1,
                                    type = g.type,
                                    customer = g.customer,
                                    form = g.form,
                                    netdef = (decimal)g.netdef,
                                    taredef = (decimal)g.taredef,
                                    grossdef = (decimal)g.grossdef,
                                    packingmat = g.packingmat,
                                    mode = s.mode,
                                    pdremark = s.pdremark,
                                    PdRemarkParent = s.PdRemarkParent
                                })
                              .OrderByDescending(x => x.pdno)
                              .ToList();

                RaisePropertyChangedEvent(nameof(pdsetupList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void packedgradeChanged()
        {
            try
            {
                var packedgrade = Facade.packedgradeBL().GetSingle(_packedgrade);
                if (packedgrade == null)
                    throw new ArgumentException("ไม่พบ packedgrade นี้ในระบบ");
                _type = packedgrade.type;
                _customer = packedgrade.customer;
                _form = packedgrade.form;
                _netdef = (decimal)packedgrade.netdef;
                _taredef = (decimal)packedgrade.taredef;
                _grossdef = (decimal)packedgrade.grossdef;
                _packingmat = packedgrade.packingmat;

                RaisePropertyChangedEvent(nameof(type));
                RaisePropertyChangedEvent(nameof(customer));
                RaisePropertyChangedEvent(nameof(form));
                RaisePropertyChangedEvent(nameof(netdef));
                RaisePropertyChangedEvent(nameof(taredef));
                RaisePropertyChangedEvent(nameof(grossdef));
                RaisePropertyChangedEvent(nameof(packingmat));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
