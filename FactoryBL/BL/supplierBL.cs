﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IsupplierBL
    {
        List<supplier> GetAll();
    }

    public class supplierBL : IsupplierBL
    {
        IStecDBMSUnitOfWork uow;
        public supplierBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public List<supplier> GetAll()
        {
            return uow.supplierRepo.Get();
        }
    }
}
