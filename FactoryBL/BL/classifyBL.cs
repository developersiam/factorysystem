﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IclassifyBL
    {
        List<classify> GetByType(string type, bool laos);
    }

    public class classifyBL : IclassifyBL
    {
        IStecDBMSUnitOfWork uow;
        public classifyBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public List<classify> GetByType(string type, bool laos)
        {
            return uow.classifyRepo.Get(x => x.type == type && x.Laos == laos);
        }
    }
}
