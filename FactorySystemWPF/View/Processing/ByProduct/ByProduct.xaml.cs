﻿using FactorySystemWPF.MVVM;
using FactorySystemWPF.ViewModel.Processing.ByProduct;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FactorySystemWPF.View.Processing.ByProduct
{
    /// <summary>
    /// Interaction logic for ByProduct.xaml
    /// </summary>
    public partial class ByProduct : Page
    {
        public ByProduct()
        {
            InitializeComponent();
            DataContext = new vm_ByProduct();
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_ByProduct)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.gross):
                    grossTextBox.SelectAll();
                    grossTextBox.Focus();
                    break;
                case nameof(vm.boxtare):
                    boxtareTextBox.SelectAll();
                    boxtareTextBox.Focus();
                    break;
                default:
                    break;
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
            grossTextBox.Focus();
        }
    }
}
