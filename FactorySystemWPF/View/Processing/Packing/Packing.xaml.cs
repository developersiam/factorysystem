﻿using FactorySystemWPF.MVVM;
using FactorySystemWPF.ViewModel.Processing.Packing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FactorySystemWPF.View.Processing.Packing
{
    /// <summary>
    /// Interaction logic for Packing.xaml
    /// </summary>
    public partial class Packing : Page
    {
        public Packing()
        {
            InitializeComponent();
            DataContext = new vm_Packing();
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_Packing)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.gross):
                    grossTextBox.SelectAll();
                    grossTextBox.Focus();
                    break;
                case nameof(vm.boxtare):
                    boxtareTextBox.SelectAll();
                    boxtareTextBox.Focus();
                    break;
                default:
                    break;
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
            grossTextBox.Focus();
        }
    }
}
