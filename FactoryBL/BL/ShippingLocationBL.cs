﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IShippingLocationBL
    {
        List<ShippingLocation> GetAll();
        ShippingLocation GetSingle(string locationNo);
    }

    public class ShippingLocationBL : IShippingLocationBL
    {
        IStecDBMSUnitOfWork uow;
        public ShippingLocationBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public List<ShippingLocation> GetAll()
        {
            return uow.ShippingLocationRepo.Get();
        }

        public ShippingLocation GetSingle(string locationNo)
        {
            return uow.ShippingLocationRepo.GetSingle(x => x.LocNo == locationNo);
        }
    }
}
