﻿Option Explicit On
Dim packingHr As Integer
Public Sub ListNew()
    Dim PdHour_Mat As Integer 'for green
    Dim PdHour_Pd As Integer  'for repack

    'PD Control
    'Set rs = DataEnv.rsPdcontrol_Read
    'If rs.State Then rs.Close
    'rs.Open
    'lblPdDate = rs!PdDate

    'Get pdhour from mat (green) with Date
    If DataEnv.rsFeeding_Period_Count.State Then DataEnv.rsFeeding_Period_Count.Close
    DataEnv.Feeding_Period_Count lblPdDate, lblPdNo
            If DataEnv.rsFeeding_Period_Count.RecordCount > 0 Then
        PdHour_Mat = DataEnv.rsFeeding_Period_Count!topdhourmax
    Else
        PdHour_Mat = 0
    End If
    'Get pdhour from pd (repack) with Date
    If DataEnv.rsGetPdHour_Repack.State Then DataEnv.rsGetPdHour_Repack.Close
    DataEnv.GetPdHour_Repack lblPdDate, lblPdNo
            If DataEnv.rsGetPdHour_Repack.RecordCount > 0 Then
        PdHour_Pd = DataEnv.rsGetPdHour_Repack!topdhourmax
    Else
        PdHour_Pd = 0
    End If

    'get pdhour from mat (green) with PD No.
    If DataEnv.rsFeeding_Period_List.State Then DataEnv.rsFeeding_Period_List.Close
    DataEnv.Feeding_Period_List lblPdNo
        Set rs = DataEnv.rsFeeding_Period_List
        
        cmbPeriod.Clear
    If rs.RecordCount > 0 Then
        For i = 1 To rs.RecordCount
            cmbPeriod.AddItem rs!topdhour
                        rs.MoveNext
        Next
    Else

        If PdHour_Pd > PdHour_Mat Then
            '24/09/2019 For Cutrag Repack
            If PdHour_Pd > 0 Then
                'Looping for all repack hours
                For i = 1 To PdHour_Pd
                    cmbPeriod.AddItem i
                        Next
            End If
            cmbPeriod.AddItem PdHour_Pd + 1
                ElseIf PdHour_Mat > 0 Then
            cmbPeriod.AddItem PdHour_Mat + 1
                Else
            cmbPeriod.AddItem 1
                    cmbPeriod.ListIndex = 0
        End If
    End If

    If cmbPeriod.ListCount > 0 Then
        cmbPeriod.ListIndex = 0
    End If
    Tpacking_Timer()

    '---Get Packing amount By grade
    If DataEnv.rsPd_CountByGrade.State Then DataEnv.rsPd_CountByGrade.Close
    DataEnv.Pd_CountByGrade lblPackedGrade
        Set rs = DataEnv.rsPd_CountByGrade
            If DataEnv.rsPd_CountByGrade.EOF = False Then
        lblpackingGradeAmt.Caption = CStr(DataEnv.rsPd_CountByGrade!bcamt)
    End If

    '--- Get Packing amount By pdno and PdHr
    If DataEnv.rsPd_CountByPdnoHour.State Then DataEnv.rsPd_CountByPdnoHour.Close
    DataEnv.Pd_CountByPdnoHour lblPdNo, cmbPeriod
        Set rs = DataEnv.rsPd_CountByPdnoHour
            If DataEnv.rsPd_CountByPdnoHour.EOF = False Then
        lblpackingPdnoAmt.Caption = CStr(DataEnv.rsPd_CountByPdnoHour!bcamt)
    End If
    'cmdAdd_Click

End Sub

Private Sub List()
    Dim WT As Currency

    lblBT = ""
    lblWT = ""
    lblStartTime = ""
    lblFinishTime = ""
    ListView.ListItems.Clear

    If DataEnv.rsMat_Feeding_List.State Then DataEnv.rsMat_Feeding_List.Close
    DataEnv.Mat_Feeding_List lblPdNo, cmbPeriod
        Set rs = DataEnv.rsMat_Feeding_List
        If Not rs.RecordCount = 0 Then
        lblBT = rs.RecordCount
        With rs
            .MoveLast
            lblStartTime = !feedingtime
            .MoveFirst
            lblFinishTime = !feedingtime
            WT = 0
            X = 1
            While Not .EOF
                ListView.ListItems.Add X, , !bc
                                ListView.ListItems(X).SubItems(1) = !classify
                ListView.ListItems(X).SubItems(2) = IIf(IsNull(!Mark), "", !Mark)
                ListView.ListItems(X).SubItems(3) = !supplier
                ListView.ListItems(X).SubItems(4) = !baleno
                If (!crop >= 2019) And (!company = 1 Or !company = 2) Then
                    ListView.ListItems(X).SubItems(5) = Format(!Weightbuy, "#,##0.00")
                    WT = WT + !Weightbuy
                Else
                    ListView.ListItems(X).SubItems(5) = Format(!Weight, "#,##0.00")
                    WT = WT + !Weight
                End If
                ListView.ListItems(X).SubItems(6) = !feedingtime
                ListView.ListItems(X).SubItems(7) = !bz
                ListView.ListItems(X).SubItems(8) = !picking
                .MoveNext
                X = X + 1
                        Wend
                        lblWT = Format(WT, "#,##0.00")
        End With
    End If

    If DataEnv.rsBlendingStatus_Find.State Then DataEnv.rsBlendingStatus_Find.Close
    DataEnv.BlendingStatus_Find lblPdNo, cmbPeriod
        Set rs = DataEnv.rsBlendingStatus_Find
        
        lblPLocked = "UnLock"
    lblPLocked.BackColor = &HC0FFC0

    With rs
        If .RecordCount > 0 Then
            If !Locked = True Then
                lblPLocked = "Locked"
                lblPLocked.BackColor = &H8080FF
            End If
        End If
    End With

    If lblPLocked = "Locked" Then
        cmdUpdate.Enabled = False
    Else
        cmdUpdate.Enabled = True
    End If

    '---Get Packing amount By grade
    If DataEnv.rsPd_CountByGrade.State Then DataEnv.rsPd_CountByGrade.Close
    DataEnv.Pd_CountByGrade lblPackedGrade
        Set rs = DataEnv.rsPd_CountByGrade
            If DataEnv.rsPd_CountByGrade.EOF = False Then
        lblpackingGradeAmt.Caption = CStr(DataEnv.rsPd_CountByGrade!bcamt)
    End If

    '--- Get Packing amount By pdno and PdHr
    If DataEnv.rsPd_CountByPdnoHour.State Then DataEnv.rsPd_CountByPdnoHour.Close
    DataEnv.Pd_CountByPdnoHour lblPdNo, cmbPeriod
        Set rs = DataEnv.rsPd_CountByPdnoHour
            If DataEnv.rsPd_CountByPdnoHour.EOF = False Then
        lblpackingPdnoAmt.Caption = CStr(DataEnv.rsPd_CountByPdnoHour!bcamt)
    End If

End Sub

Private Sub Clear()
    '== Clear Heder
    lblCrop = FCrop
    lblPdNo = ""
    lblType = ""
    lblPackedGrade = ""
    lblCustomer = ""
    lblBT = ""
    lblWT = ""
    lblStartTime = ""
    lblFinishTime = ""
    lblPLocked = "UnLock"
    lblPLocked.BackColor = &H80FF80
    cmbPeriod.Clear
    lsPacking.ListItems.Clear

    '== Clear Green
    txtBC = ""
    lblClassify = ""
    lblMark = ""
    lblSupplier = ""
    lblBaleNo = ""
    lblWeight = ""
    chkBZ.Value = 0
    chkPK.Value = 0
    ListView.ListItems.Clear

    ' Clear Packed
    txtBC = ""
    lblGradeP = ""
    lblCaseNoP = ""
    lblNetP = ""
    lblNetRealP = ""
    lblpackingGradeAmt.Caption = ""
    lblpackingPdnoAmt.Caption = ""
    ListViewP.ListItems.Clear

End Sub

Public Sub ListP()
    Dim WT As Currency
    'Clear List
    lblBT = ""
    ListViewP.ListItems.Clear
    'Insert into List
    If DataEnv.rsPd_Feeding_List.State Then DataEnv.rsPd_Feeding_List.Close
    DataEnv.dbServer.CommandTimeout = 60
    DataEnv.pd_Feeding_List lblPdNo, cmbPeriod
        Set rs = DataEnv.rsPd_Feeding_List
    
        If Not rs.RecordCount = 0 Then
        lblBT = rs.RecordCount
        'If rs.RecordCount > 0 Then cmdDelete.Enabled = True
        X = 1
        With rs

            .Sort = "feedingtime desc"

            '.MoveLast
            'lblFinishTime = !feedingtime
            .MoveFirst
            'lblStartTime = !feedingtime
            WT = 0
            While Not .EOF
                ListViewP.ListItems.Add X, , !bc
                                ListViewP.ListItems(X).SubItems(1) = !grade
                ListViewP.ListItems(X).SubItems(2) = !caseno
                ListViewP.ListItems(X).SubItems(3) = !taredef
                ListViewP.ListItems(X).SubItems(4) = Format(!netreal, "#,##0.00")
                ListViewP.ListItems(X).SubItems(5) = !feedingtime
                WT = WT + !netreal
                .MoveNext
                X = X + 1
                        Wend
                        'lblWT = Format(WT, "#,##0.00")
                End With
    Else
        'cmdDelete.Enabled = False
    End If

    If lblPLocked = "Locked" Then
        'cmdAdd.Enabled = False
        'cmdDelete.Enabled = False
        cmdUpdateP.Enabled = True
        'cmdDeleteP.Enabled = False
        'cmdFinish.Enabled = False
        'cmdNewPeriod.Enabled = False
    Else
        cmdUpdateP.Enabled = True
        cmdDeleteP.Enabled = True
    End If

    '---Get Packing amount By grade
    If DataEnv.rsPd_CountByGrade.State Then DataEnv.rsPd_CountByGrade.Close
    DataEnv.Pd_CountByGrade lblPackedGrade
        Set rs = DataEnv.rsPd_CountByGrade
            If DataEnv.rsPd_CountByGrade.EOF = False Then
        lblpackingGradeAmt.Caption = CStr(DataEnv.rsPd_CountByGrade!bcamt)
    End If

    '--- Get Packing amount By pdno and PdHr
    If DataEnv.rsPd_CountByPdnoHour.State Then DataEnv.rsPd_CountByPdnoHour.Close
    DataEnv.Pd_CountByPdnoHour lblPdNo, cmbPeriod
        Set rs = DataEnv.rsPd_CountByPdnoHour
            If DataEnv.rsPd_CountByPdnoHour.EOF = False Then
        lblpackingPdnoAmt.Caption = CStr(DataEnv.rsPd_CountByPdnoHour!bcamt)
    End If

End Sub

Private Sub cmbPeriod_Click()
    List()
    ListP()
End Sub

Private Sub cmbPeriod_KeyPress(KeyAscii As Integer)
    Dim CB As Long
    Dim FindString As String

    If KeyAscii < 32 Or KeyAscii > 127 Then Exit Sub

    If cmbPeriod.SelLength = 0 Then
        FindString = cmbPeriod.Text & Chr$(KeyAscii)
    Else
        FindString = Left$(cmbPeriod.Text, cmbPeriod.SelStart) & Chr$(KeyAscii)
    End If

    CB = SendMessage(cmbPeriod.hwnd, CB_SHOWDROPDOWN, True, 0&)
    CB = SendMessage(cmbPeriod.hwnd, CB_FINDSTRING, -1, ByVal FindString)

    If CB <> CB_ERR Then
        cmbPeriod.ListIndex = CB
        cmbPeriod.SelStart = Len(FindString)
        cmbPeriod.SelLength = Len(cmbPeriod.Text) - cmbPeriod.SelStart
    End If
    KeyAscii = 0

End Sub

Private Sub cmdAddP_Click()
    If cmdAddP.Enabled = False Then Exit Sub

    'Flag2 = "Add"
    txtBCp = ""
    lblGradeP = ""
    lblCaseNoP = ""
    lblNetP = ""
    lblNetRealP = ""
    FF = True
    txtBCp.Locked = False
    txtBCp.SetFocus
End Sub

Private Sub cmdDailyReport_Click()
    Dim rsRP As ADODB.Recordset
    Dim rsCUPackedgrade As ADODB.Recordset

    Dim xlApp As Excel.Application
    Dim xlBook As Excel.Workbook
    Dim xls As Excel.Worksheet
    Dim Cell As Integer

    Dim WT As Currency

    If lsPacking.ListItems.Count = 0 Then Exit Sub

    WT = 0
        
        Set xlApp = New Excel.Application
        'Set xlBook = xlApp.Workbooks.Open("C:\My Developing\STEC-Thailand\2004 New System\Production Management System\1_Blending System\Version 2.0\Report\PeriodReport.xlt")
        Set xlBook = xlApp.Workbooks.Open("C:\Program Files\Common Files\DailyReport.xlt")
        Set xls = xlBook.Worksheets.Application.ActiveSheet
        
        'Green In
        'SELECT mat.topddate, mat.toprgrade, mat.classify, COUNT(mat.bc) AS bt, CASE WHEN (mat.crop >= 2019 AND mat.company IN (1,2)) THEN SUM(mat.weightbuy) ELSE SUM(mat.weight) END AS wt, mat.picking, 
		'pdsetup.packedgrade, packedgrade.type, packedgrade.customer, packedgrade.form, packedgrade.packingmat 
		'FROM mat INNER JOIN pdsetup ON mat.topdno = pdsetup.pdno INNER JOIN packedgrade ON pdsetup.packedgrade = packedgrade.packedgrade 
		'GROUP BY mat.toprgrade, mat.classify, mat.picking, mat.topddate, pdsetup.packedgrade, packedgrade.type, packedgrade.customer, packedgrade.form, packedgrade.packingmat,mat.crop,mat.company
		'HAVING (mat.picking = 0) AND (mat.topddate = ?) AND (mat.toprgrade = ?)
        If DataEnv.rsReport_DailyGreen.State Then DataEnv.rsReport_DailyGreen.Close
    DataEnv.Report_dailyGreen lblPdDate, lblPackedGradeSelect             ' lsPacking.SelectedItem
        Set rsRP = DataEnv.rsReport_DailyGreen
        
        '19/02/2016   add  cu_packedgrade  for  get  packedgrade,type,customer,form,packingmat
        'SELECT PD.frompdhour,PD.frompdno,
		'pdsetup.packedgrade,packedgrade.type,packedgrade.customer,packedgrade.form,packedgrade.packingmat
		'FROM pd INNER JOIN  pdsetup  ON PD.frompdno = pdsetup.pdno   
		'INNER JOIN packedgrade  ON  pdsetup.packedgrade = packedgrade.packedgrade  
		'WHERE (PD.fromprgrade = ?) AND (PD.frompddate = ?) AND  (PD.frompdhour = ?) AND PD.frompdno = ?
        If DataEnv.rsCU_Pakedgrade.State Then DataEnv.rsCU_Pakedgrade.Close
    DataEnv.CU_Pakedgrade lblPackedGradeSelect, lblPdDate, lblHourSelect, lblPdNo
        Set rsCUPackedgrade = DataEnv.rsCU_Pakedgrade
        
        With xls
        '.Range("D5") = ": " & rsRP!toprgrade
        '.Range("D6") = ": " & rsRP!Type
        '.Range("D7") = ": " & rsRP!Customer
        '.Range("D8") = ": " & rsRP!Form
        '.Range("D9") = ": " & rsRP!packingmat

        .Range("D5") = ": " & rsCUPackedgrade!packedgrade
        .Range("D6") = ": " & rsCUPackedgrade!Type
        .Range("D7") = ": " & rsCUPackedgrade!customer
        .Range("D8") = ": " & rsCUPackedgrade!Form
        .Range("D9") = ": " & rsCUPackedgrade!packingmat

        .Range("I5") = ": " & Format(frmBlending.lblPdDate, "dd-mm-yyyy")
        '.Range("I4") = rsRP!topdno

        '                .Range("I6") = ": " & lsPacking.SelectedItem
        '                .Range("I7") = ": " & frmBlending.lblBT
        '                .Range("I8") = ": " & frmBlending.lblWT

        Cell = 11
        '=== Green In
        For i = 1 To rsRP.RecordCount
            Cell = Cell + 1
            .Range("B" & Cell) = i
            .Range("D" & Cell) = rsRP!classify
            .Range("F" & Cell) = rsRP!bt
            .Range("H" & Cell) = rsRP!WT
            rsRP.MoveNext
        Next
        '=== Picking In
        'SELECT mat.topddate, mat.toprgrade, mat.topdno, COUNT(mat.bc) AS bt, CASE WHEN (mat.crop >= 2019 AND mat.company IN (1,2)) THEN SUM(mat.weightbuy) ELSE SUM(mat.weight) END AS wt, 
        'mat.picking, pdsetup.packedgrade, packedgrade.type, packedgrade.customer, 
        'packedgrade.form, packedgrade.packingmat FROM mat INNER JOIN pdsetup ON mat.topdno = pdsetup.pdno INNER JOIN packedgrade ON pdsetup.packedgrade = packedgrade.packedgrade 
        'GROUP BY mat.topdno, mat.toprgrade, mat.picking, mat.topddate, pdsetup.packedgrade, packedgrade.type, packedgrade.customer, packedgrade.form, packedgrade.packingmat,mat.crop, mat.company
        'HAVING (mat.picking = 1) AND (mat.topddate = ?) AND (mat.toprgrade = ?)
        If DataEnv.rsReport_DailyPicking.State Then DataEnv.rsReport_DailyPicking.Close
        DataEnv.report_Dailypicking lblPdDate, lblPackedGradeSelect
                If Not DataEnv.rsReport_DailyPicking.RecordCount = 0 Then
            .Range("F1002") = Format(DataEnv.rsReport_DailyPicking!bt, "#,##0.00")
            .Range("H1002") = Format(DataEnv.rsReport_DailyPicking!WT, "#,##0.00")
        End If
        '=== Repacked In
        'SELECT topddate, toprgrade, COUNT(bc) AS ct, SUM(netreal) AS nt, pdtype FROM pd GROUP BY topddate, toprgrade, pdtype HAVING (topddate = ?) AND (pdtype = N'Lamina') AND (toprgrade = ?)
        If DataEnv.rsReport_DailyRepacked.State Then DataEnv.rsReport_DailyRepacked.Close
        DataEnv.report_Dailyrepacked lblPdDate, lblPackedGradeSelect
                If Not DataEnv.rsReport_DailyRepacked.RecordCount = 0 Then
            .Range("F1003") = Format(DataEnv.rsReport_DailyRepacked!ct, "#,##0.00")
            .Range("H1003") = Format(DataEnv.rsReport_DailyRepacked!nt, "#,##0.00")
        End If
        '=== Remnant In
        'SELECT topddate, toprgrade, COUNT(bc) AS ct, SUM(netreal) AS nt, pdtype FROM pd GROUP BY topddate, toprgrade, pdtype HAVING (topddate = ?) AND (pdtype = N'Remnant') AND (toprgrade = ?)
        If DataEnv.rsReport_DailyRemnant.State Then DataEnv.rsReport_DailyRemnant.Close
        DataEnv.report_Dailyremnant lblPdDate, lblPackedGradeSelect
                If Not DataEnv.rsReport_DailyRemnant.RecordCount = 0 Then
            .Range("F1004") = Format(DataEnv.rsReport_DailyRemnant!ct, "#,##0.00")
            .Range("H1004") = Format(DataEnv.rsReport_DailyRemnant!nt, "#,##0.00")
        End If


        '=== Lamina Out
        'SELECT frompddate, fromprgrade, COUNT(bc) AS ct, SUM(netdef) AS nt, pdtype FROM pd GROUP BY frompddate, fromprgrade, pdtype HAVING (frompddate = ?) AND (pdtype = N'Lamina') AND (fromprgrade = ?)
        If DataEnv.rsReport_DailyPacked.State Then DataEnv.rsReport_DailyPacked.Close
        DataEnv.report_DailyPacked lblPdDate, lblPackedGradeSelect
                If Not DataEnv.rsReport_DailyPacked.RecordCount = 0 Then
            .Range("F1006") = Format(DataEnv.rsReport_DailyPacked!ct, "#,##0.00")
            .Range("H1006") = Format(DataEnv.rsReport_DailyPacked!nt, "#,##0.00")
        End If

        .Range("A" & Cell + 2 & ":K1000").Delete 2

        End With
    xlApp.Visible = True
        'xlApp.Application.Worksheets.PrintOut , , cmbCopies
'
        'xlBook.Application.Workbooks(1).Close 0
        
        Set xlApp = Nothing
        Set xlBook = Nothing
        Set xls = Nothing

        MsgBox "พิมพ์รายงานเรียบร้อยแล้ว", vbInformation, T

End Sub

Private Sub cmdDelete_Click()
    If txtBC.Text <> "" Then
        '=== PdNo.
        If lblPdNo = "" Then
            MsgBox "Please select Production No.", vbCritical, T
                Exit Sub
        End If
        '=== Period
        If cmbPeriod = "" Then
            MsgBox "Please Select Period", vbCritical, T
                cmbPeriod.SetFocus
            Exit Sub
        End If

        'Check this barcode already issued = 1
        If DataEnv.rsMat_Find.State Then DataEnv.rsMat_Find.Close
        DataEnv.mat_Find txtBC
                Set rs = DataEnv.rsMat_Find
                With rs
            If .RecordCount = 0 Then
                MsgBox "ไม่พบข้อมูลที่ต้องการ", , T
                                cmdAdd_Click()
                Exit Sub
            End If
            If Not !issued Then
                MsgBox "ข้อมูลนี้ยังไม่ได้ถูกนำมาใช้ ,กรุณาตรวจสอบ", vbOKOnly + vbCritical
                                cmdAdd_Click()
                Exit Sub
            Else
                DataEnv.Mat_Feeding_Delete txtBC
                                List()
                'clear all text
                cmdAdd_Click()
            End If
        End With
    End If
End Sub

Private Sub cmdDeleteP_Click()

    If txtBCp.Text <> "" Then
        XX = False
        'Check everything not Null
        If lblPdNo = "" Then
            MsgBox "Please select Production No.", vbCritical, T
                Exit Sub
        End If
        '=== Period
        If cmbPeriod = "" Then
            MsgBox "Please check Period", vbCritical, T
                    cmbPeriod.SetFocus
            Exit Sub
        End If

        'เพิ่มเช็คว่า หากยา Repacked จะต้องได้รับการ Received จากโปรแกรม shipping ก่อน
        If DataEnv.rsShippingReceived.State Then DataEnv.rsShippingReceived.Close
        DataEnv.ShippingReceived txtBCp.Text
            Dim rsSR As New ADODB.Recordset
            Set rsSR = DataEnv.rsShippingReceived
            With rsSR
            If !movementno <> "" Then
                If !movementstatusid = 0 Then
                    MsgBox "ระบบShippingยังไม่ได้Received เลข Movement No. " & !movementno & " กรุณาแจ้งเลข Barcodeและ Movement No. นี้กับแผนก Processing"
                        txtBCp.Text = ""
                    txtBCp.SetFocus
                    Exit Sub
                End If
            End If
        End With

        If DataEnv.rsPdBC_Find.State Then DataEnv.rsPdBC_Find.Close
        DataEnv.pdBC_Find txtBCp
            Set rs = DataEnv.rsPdBC_Find
            With rs
            If .RecordCount = 0 Then
                MsgBox "ไม่พบข้อมูลที่ต้องการ", , T
                        Exit Sub
            End If
            If Not !issued Then
                MsgBox "Packing ห่อนี้ยังไม่ถูกใช้ใน " & !issuedto & " นี้"
                        Exit Sub
            Else
                ' Set value back to default
                DataEnv.pd_Feeding_delete txtBCp
                        'Update list and Clear all text
                ListP()
                cmdAddP_Click()
            End If
        End With
    End If

End Sub

Private Sub cmdDetailReport_Click()
    Dim rs1 As ADODB.Recordset
    Dim rs2 As ADODB.Recordset
    Dim rsRP As ADODB.Recordset
    Dim rsCUPackedgrade As ADODB.Recordset
    Dim rsFeedIn_Repack As ADODB.Recordset

    Dim xlApp As Excel.Application
    Dim xlBook As Excel.Workbook
    Dim xls As Excel.Worksheet
    Dim Cell As Integer

    If lsPacking.ListItems.Count = 0 Then Exit Sub
        
        Set xlApp = New Excel.Application
        'Set xlBook = xlApp.Workbooks.Open("C:\My Developing\STEC-Thailand\2004 New System\Production Management System\1_Blending System\Version 2.0\Report\DetailReport.xlt")
        Set xlBook = xlApp.Workbooks.Open("C:\Program Files\Common Files\DetailReport.xlt")
        Set xls = xlBook.Worksheets.Application.ActiveSheet
        
        'INPUT
        'SELECT * FROM mat WHERE (topddate = ?) AND (topdhour = ?)  AND (topdno = ?)
        If DataEnv.rsReport_HourDetail.State Then DataEnv.rsReport_HourDetail.Close
    DataEnv.Report_Hourdetail lblPdDate, lblHourSelect, lblPdNo
        Set rs1 = DataEnv.rsReport_HourDetail
        
        'sp_Sel_PERIOD_MAT_CUTRAG
        If DataEnv.rsReport_PeriodGreen.State Then DataEnv.rsReport_PeriodGreen.Close
    DataEnv.Report_PeriodGreen lblPdDate, lblHourSelect, lblPdNo                    'lsPacking.SelectedItem
        Set rsRP = DataEnv.rsReport_PeriodGreen
        
        '19/02/2016   add  cu_packedgrade  for  get  packedgrade,type,customer,form,packingmat
        'SELECT PD.frompdhour,PD.frompdno,
		'pdsetup.packedgrade,packedgrade.type,packedgrade.customer,packedgrade.form,packedgrade.packingmat
		'FROM pd INNER JOIN  pdsetup  ON PD.frompdno = pdsetup.pdno   
		'INNER JOIN packedgrade  ON  pdsetup.packedgrade = packedgrade.packedgrade  
		'WHERE (PD.fromprgrade = ?) AND (PD.frompddate = ?) AND  (PD.frompdhour = ?) AND PD.frompdno = ?
        If DataEnv.rsCU_Pakedgrade.State Then DataEnv.rsCU_Pakedgrade.Close
    DataEnv.CU_Pakedgrade lblPackedGradeSelect, lblPdDate, lblHourSelect, lblPdNo
        Set rsCUPackedgrade = DataEnv.rsCU_Pakedgrade
        
        '20/2/2016  add feedin_repack for get  data from  repacked only
        'SELECT * FROM pd  WHERE (topdno = ?) AND (topdhour = ?) order by  feedingtime
        If DataEnv.rsFeedIn_Repack.State Then DataEnv.rsFeedIn_Repack.Close
    DataEnv.FeedIn_Repack lblPdNo.Caption, lblHourSelect.Caption
        Set rsFeedIn_Repack = DataEnv.rsFeedIn_Repack
        
        With xls
        .Range("D5") = ": " & rsCUPackedgrade!packedgrade
        .Range("D6") = ": " & rsCUPackedgrade!Type
        .Range("D7") = ": " & rsCUPackedgrade!customer
        .Range("D8") = ": " & rsCUPackedgrade!Form
        .Range("D9") = ": " & rsCUPackedgrade!packingmat

        .Range("H5") = ": " & Format(frmBlending.lblPdDate, "dd-mm-yyyy")
        .Range("I4") = rsCUPackedgrade!frompdno

        .Range("H7") = ": " & lblHourSelect
        '                .Range("H8") = ": " & frmBlending.lblBT
        '                .Range("H9") = ": " & frmBlending.lblWT

        Cell = 11
        rs1.Sort = "feedingtime"
        '                If lblHour = "1" Then
        i = 0
        If rs1.EOF = False Then
            For i = 1 To rs1.RecordCount
                Cell = Cell + 1
                .Range("B" & Cell) = i
                .Range("C" & Cell) = rs1!bc
                .Range("E" & Cell) = rs1!supplier
                .Range("F" & Cell) = rs1!baleno
                .Range("G" & Cell) = rs1!classify
                If (rs1!company = 1 Or rs1!company = 2) Then
                    .Range("H" & Cell) = rs1!Weightbuy
                Else
                    .Range("H" & Cell) = rs1!Weight
                End If
                .Range("J" & Cell) = "'" & rs1!feedingtime
                rs1.MoveNext
            Next
        End If

        Dim I2 As Integer
        I2 = i
        If rs1.EOF = True Then
            For i = 1 To rsFeedIn_Repack.RecordCount
                Cell = Cell + 1
                I2 = I2 + 1
                .Range("B" & Cell) = I2
                .Range("C" & Cell) = rsFeedIn_Repack!bc
                .Range("E" & Cell) = ""
                .Range("F" & Cell) = rsFeedIn_Repack!caseno
                .Range("G" & Cell) = ""
                .Range("H" & Cell) = rsFeedIn_Repack!netreal
                .Range("J" & Cell) = "'" & rsFeedIn_Repack!feedingtime
                rsFeedIn_Repack.MoveNext
            Next
        End If

        .Range("A" & Cell + 2 & ":K1000").Delete 2

        End With
    xlApp.Visible = True
        'xlApp.Application.Worksheets.PrintOut , , cmbCopies

        'xlBook.Application.Workbooks(1).Close 0
        
        Set xlApp = Nothing
        Set xlBook = Nothing
        Set xls = Nothing
        
        MsgBox "พิมพ์รายงานเรียบร้อยแล้ว", vbInformation, T

End Sub

Private Sub cmdFinishGrade_Click()
    With frmFinishGrade
        PdDate = lblPdDate
        .Show 1
        End With
End Sub

Private Sub cmdListAll_Click()
    List()
End Sub

Private Sub cmdNew_Click()
    frmPD.Show 1
End Sub

Private Sub cmdAdd_Click()
    txtBC = ""
    lblClassify = ""
    lblMark = ""
    lblSupplier = ""
    lblBaleNo = ""
    lblWeight = ""
    chkBZ.Value = 0
    chkPK.Value = 0
    FF = True
    txtBC.SetFocus
End Sub

Private Sub cmdExit_Click()
    Unload Me
    frmMain.Show
End Sub

Private Sub cmdNewPeriod_Click()
    If cmbPeriod = "" Then Exit Sub
    If UCase(lblPLocked) = "UNLOCK" And (Not ListView.ListItems.Count = 0 Or Not ListViewP.ListItems.Count = 0) Then
        DataEnv.Blendingstatus_Locked lblPdNo, cmbPeriod, lblPdDate
                cmbPeriod.AddItem cmbPeriod.List(cmbPeriod.ListCount - 1) + 1
                cmbPeriod.ListIndex = cmbPeriod.ListCount - 1
    Else
        'If Not ListView.ListItems.Count = 0 Then
        ''                        cmbPeriod.AddItem cmbPeriod.List(cmbPeriod.ListCount - 1) + 1
        ''                        cmbPeriod.ListIndex = cmbPeriod.ListCount - 1
        'End If
        cmbPeriod.AddItem cmbPeriod.List(cmbPeriod.ListCount - 1) + 1
                        cmbPeriod.ListIndex = cmbPeriod.ListCount - 1
    End If
    List()
    'txtBC.SetFocus
End Sub

Private Sub cmdReport_Click()
    frmReport.lblPdNo = lblPdNo
    frmReport.lblHour = cmbPeriod
    frmReport.Show 1
End Sub

Private Sub cmdPeriodReport_Click()
    Dim rsRP As ADODB.Recordset
    Dim rsCU As ADODB.Recordset
    Dim rsCUPackedgrade As ADODB.Recordset

    Dim xlApp As Excel.Application
    Dim xlBook As Excel.Workbook
    Dim xls As Excel.Worksheet
    Dim Cell As Integer

    Dim rsCuPicking As ADODB.Recordset
    Dim rsCuRePacked As ADODB.Recordset
    Dim rsCuRemnant As ADODB.Recordset
    Dim rsCuPacked As ADODB.Recordset

    Dim WT As Currency

    If lsPacking.ListItems.Count = 0 Then Exit Sub
    If lblHourSelect.Caption = "" Then
        MsgBox "กรุณาระบุ ชั่วโมงการทำงานก่อน Print", vbInformation, T
            Exit Sub
    End If
    WT = 0
        
        Set xlApp = New Excel.Application
        'Set xlBook = xlApp.Workbooks.Open("C:\My Developing\STEC-Thailand\2004 New System\Production Management System\1_Blending System\Version 2.0\Report\PeriodReport.xlt")
        Set xlBook = xlApp.Workbooks.Open("C:\Program Files\Common Files\PeriodReport.xlt")
        Set xls = xlBook.Worksheets.Application.ActiveSheet
        
        'Green In
		'sp_Sel_PERIOD_MAT_CUTRAG
        If DataEnv.rsReport_PeriodGreen.State Then DataEnv.rsReport_PeriodGreen.Close
    DataEnv.Report_PeriodGreen lblPdDate, lblHourSelect, lblPdNo             ' lsPacking.SelectedItem
        Set rsRP = DataEnv.rsReport_PeriodGreen
        
        'Cu Green
		'sp_Sel_CLASSIFY_MAT
        If DataEnv.rsCu_Green.State Then DataEnv.rsCu_Green.Close
    DataEnv.Cu_Green lblPackedGradeSelect, lblPdDate, lblHourSelect, lblPdNo
        Set rsCU = DataEnv.rsCu_Green
        
        'Cu Picking
		'SELECT COUNT(bc) AS bt, 
		'case when crop >= 2019 and company in (1,2) then sum(weightbuy) 
		'else SUM(weight) end wt,SUM(feedingweight) AS rwt FROM mat 
		'WHERE (toprgrade = ?) AND (topddate = ?) AND (CONVERT(decimal(10), topdhour) <= ?) AND topdno = ? AND (picking = 1) group by crop, company
        If DataEnv.rsCu_Picking.State Then DataEnv.rsCu_Picking.Close
    DataEnv.Cu_Picking lblPackedGradeSelect, lblPdDate, lblHourSelect, lblPdNo
        Set rsCuPicking = DataEnv.rsCu_Picking
        
        'Cu Repacked
		'SELECT COUNT(bc) AS ct, SUM(netdef) AS nt FROM pd WHERE (pdtype = 'Lamina') AND (toprgrade = ?) AND (topddate = ?) AND (topdhour <= ?) AND topdno = ?
        If DataEnv.rsCu_Repacked.State Then DataEnv.rsCu_Repacked.Close
    DataEnv.Cu_Repacked lblPackedGradeSelect, lblPdDate, lblHourSelect, lblPdNo
        Set rsCuRePacked = DataEnv.rsCu_Repacked
        
        'Cu Remnant
		'SELECT COUNT(bc) AS ct, SUM(netreal) AS nt FROM pd WHERE (pdtype = 'Remnant') AND (toprgrade = ?) AND (topddate = ?) AND (topdhour <= ?) AND topdno = ?
        If DataEnv.rsCu_Remnant.State Then DataEnv.rsCu_Remnant.Close
    DataEnv.Cu_Remnant lblPackedGradeSelect, lblPdDate, lblHourSelect, lblPdNo
        Set rsCuRemnant = DataEnv.rsCu_Remnant
        
        'Cu Packed
		'SELECT COUNT(bc) AS ct, SUM(netdef) AS ntFROM pd
		'WHERE (fromprgrade = ?) AND (frompddate = ?) AND frompdhour <= ?) AND (pdtype = 'Lamina') AND frompdno = ?
        If DataEnv.rsCu_Packed.State Then DataEnv.rsCu_Packed.Close
    DataEnv.Cu_Packed lblPackedGradeSelect, lblPdDate, lblHourSelect, lblPdNo
        Set rsCuPacked = DataEnv.rsCu_Packed
        
        '19/02/2016   add  cu_packedgrade  for  get  packedgrade,type,customer,form,packingmat
		'SELECT PD.frompdhour,PD.frompdno,pdsetup.packedgrade,packedgrade.type,packedgrade.customer,packedgrade.form,packedgrade.packingmat
		'FROM pd INNER JOIN  pdsetup  ON PD.frompdno = pdsetup.pdno   
		'INNER JOIN packedgrade  ON  pdsetup.packedgrade = packedgrade.packedgrade  
		'WHERE (PD.fromprgrade = ?) AND (PD.frompddate = ?) AND  (PD.frompdhour = ?) AND PD.frompdno = ?
        If DataEnv.rsCU_Pakedgrade.State Then DataEnv.rsCU_Pakedgrade.Close
    DataEnv.CU_Pakedgrade lblPackedGradeSelect, lblPdDate, lblHourSelect, lblPdNo
        Set rsCUPackedgrade = DataEnv.rsCU_Pakedgrade
        
        With xls
        .Range("D5") = ": " & rsCUPackedgrade!packedgrade
        .Range("D6") = ": " & rsCUPackedgrade!Type
        .Range("D7") = ": " & rsCUPackedgrade!customer
        .Range("D8") = ": " & rsCUPackedgrade!Form
        .Range("D9") = ": " & rsCUPackedgrade!packingmat

        .Range("P4") = rsCUPackedgrade!frompdno

        '.Range("D5") = ": " & rsRP!packedgrade
        '.Range("D6") = ": " & rsRP!Type
        '.Range("D7") = ": " & rsRP!Customer
        '.Range("D8") = ": " & rsRP!Form
        ''.Range("D9") = ": " & rsRP!packingmat

        '.Range("P4") = rsRP!topdno

        .Range("O5") = ": " & Format(frmBlending.lblPdDate, "dd-mm-yyyy")
        .Range("O6") = ": " & lblHourSelect
        '                .Range("I7") = ": " & frmBlending.lblBT
        '                .Range("I8") = ": " & frmBlending.lblWT

        '===========
        If rsCuPicking.RecordCount > 0 Then
            .Range("O1003") = rsCuPicking!bt
            .Range("P1003") = rsCuPicking!WT
        End If
        If rsCuRePacked.RecordCount > 0 Then
            .Range("O1004") = rsCuRePacked!ct
            .Range("P1004") = rsCuRePacked!nt
        End If
        If rsCuRemnant.RecordCount > 0 Then
            .Range("O1005") = rsCuRemnant!ct
            .Range("P1005") = rsCuRemnant!nt
        End If

        If rsCuPacked.RecordCount > 0 Then
            .Range("O1007") = rsCuPacked!ct
            .Range("P1007") = rsCuPacked!nt
        End If
        '===========

        Cell = 12
        '=== Green In
        'For i = 1 To rsRP.RecordCount
        For i = 1 To rsCU.RecordCount
            Cell = Cell + 1
            .Range("B" & Cell) = i
            'Cumulative
            .Range("C" & Cell) = rsCU!classify
            .Range("O" & Cell) = rsCU!bt
            .Range("P" & Cell) = rsCU!WT
            'Actual Feed
            rsRP.Find "classify='" & rsCU!classify & "'", , adSearchForward, 1
                        If rsRP.AbsolutePosition > 0 Then
                .Range("G" & Cell) = rsRP!bt
                .Range("H" & Cell) = rsRP!WT
            End If

            '                        .Range("D" & Cell) = rsRP!classify
            '                        .Range("F" & Cell) = rsRP!bt
            '                        .Range("H" & Cell) = rsRP!WT
            '                        rsRP.MoveNext
            rsCU.MoveNext
        Next
        '=== Picking In
        'SELECT mat.topddate, mat.topdhour, mat.topdno, COUNT(mat.bc) AS bt, 
        'CASE WHEN (mat.crop >= 2019 AND mat.company IN (1,2)) THEN SUM(mat.weightbuy) ELSE SUM(mat.weight) END AS wt,  
        'SUM(feedingweight) AS rwt,mat.picking, 
        'pdsetup.packedgrade, packedgrade.type, packedgrade.customer, packedgrade.form, packedgrade.packingmat 
        'FROM mat INNER JOIN pdsetup ON mat.topdno = pdsetup.pdno INNER JOIN packedgrade ON pdsetup.packedgrade = packedgrade.packedgrade 
        'GROUP BY mat.topdno, mat.topdhour, mat.picking, mat.topddate, pdsetup.packedgrade, packedgrade.type, packedgrade.customer, packedgrade.form, packedgrade.packingmat ,mat.crop, mat.company
        'HAVING (mat.picking = 1) AND (mat.topddate = ?) AND (mat.topdhour = ?) AND (mat.topdno=?)
        If DataEnv.rsReport_PeriodPicking.State Then DataEnv.rsReport_PeriodPicking.Close
        DataEnv.report_periodpicking lblPdDate, lblHourSelect, lblPdNo
                If Not DataEnv.rsReport_PeriodPicking.RecordCount = 0 Then
            .Range("G1003") = Format(DataEnv.rsReport_PeriodPicking!bt, "#,##0.00")
            .Range("H1003") = Format(DataEnv.rsReport_PeriodPicking!WT, "#,##0.00")
        End If
        '=== Repacked In
        'SELECT topddate, topdhour, COUNT(bc) AS ct, SUM(netreal) AS nt, pdtype FROM pd GROUP BY topddate, topdhour, pdtype, topdno HAVING (topddate = ?) AND (topdhour = ?) AND (topdno = ?) AND (pdtype = N'Lamina') 
        If DataEnv.rsReport_PeriodRepacked.State Then DataEnv.rsReport_PeriodRepacked.Close
        DataEnv.report_periodrepacked lblPdDate, lblHourSelect, lblPdNo
                If Not DataEnv.rsReport_PeriodRepacked.RecordCount = 0 Then
            .Range("G1004") = Format(DataEnv.rsReport_PeriodRepacked!ct, "#,##0.00")
            .Range("H1004") = Format(DataEnv.rsReport_PeriodRepacked!nt, "#,##0.00")
        End If
        '=== Remnant In
        'SELECT topddate, topdhour, COUNT(bc) AS ct, SUM(netreal) AS nt, pdtype FROM pd GROUP BY topddate, topdhour, pdtype , topdno HAVING (topddate = ?) AND (topdhour = ?) AND (topdno = ?) AND (pdtype = N'Remnant')
        If DataEnv.rsReport_PeriodRemnant.State Then DataEnv.rsReport_PeriodRemnant.Close
        DataEnv.report_periodremnant lblPdDate, lblHourSelect, lblPdNo
                If Not DataEnv.rsReport_PeriodRemnant.RecordCount = 0 Then
            .Range("G1005") = Format(DataEnv.rsReport_PeriodRemnant!ct, "#,##0.00")
            .Range("H1005") = Format(DataEnv.rsReport_PeriodRemnant!nt, "#,##0.00")
        End If


        '=== Lamina Out
        'SELECT frompddate, frompdhour, COUNT(bc) AS ct, SUM(netdef) AS nt, pdtype FROM pd GROUP BY frompddate, frompdhour, pdtype, frompdno HAVING (frompddate = ?) AND (frompdhour = ?) AND (frompdno = ?) AND (pdtype = N'Lamina')
        If DataEnv.rsReport_PeriodPacked.State Then DataEnv.rsReport_PeriodPacked.Close
        DataEnv.report_periodPacked lblPdDate, lblHourSelect, lblPdNo
                If Not DataEnv.rsReport_PeriodPacked.RecordCount = 0 Then
            .Range("G1007") = Format(DataEnv.rsReport_PeriodPacked!ct, "#,##0.00")
            .Range("H1007") = Format(DataEnv.rsReport_PeriodPacked!nt, "#,##0.00")
        End If

        .Range("A" & Cell + 2 & ":Z1001").Delete 2

        End With
    xlApp.Visible = True
        'xlApp.Application.Worksheets.PrintOut , , cmbCopies
''
        'xlBook.Application.Workbooks(1).Close 0
        
        Set xlApp = Nothing
        Set xlBook = Nothing
        Set xls = Nothing

        MsgBox "พิมพ์รายงานเรียบร้อยแล้ว", vbInformation, T

End Sub

Private Sub cmdSummaryRepoty_Click()

End Sub

Private Sub cmdSummaryReport_Click()
    Dim rsRP As ADODB.Recordset
    Dim rsCU As ADODB.Recordset
    Dim rsCUPackedgrade As ADODB.Recordset

    Dim xlApp As Excel.Application
    Dim xlBook As Excel.Workbook
    Dim xls As Excel.Worksheet
    Dim Cell As Integer

    Dim rsCuPicking As ADODB.Recordset
    Dim rsCuRePacked As ADODB.Recordset
    Dim rsCuRemnant As ADODB.Recordset
    Dim rsCuPacked As ADODB.Recordset

    Dim WT As Currency

    If lsPacking.ListItems.Count = 0 Then Exit Sub

    WT = 0
        
        Set xlApp = New Excel.Application
        'Set xlBook = xlApp.Workbooks.Open("C:\My Developing\STEC-Thailand\2004 New System\Production Management System\1_Blending System\Version 2.0\Report\PeriodReport.xlt")
        Set xlBook = xlApp.Workbooks.Open("C:\Program Files\Common Files\GradeAccReport.xlt")
        Set xls = xlBook.Worksheets.Application.ActiveSheet
        
        'Green In
		'sp_Sel_PERIOD_MAT_CUTRAG
        If DataEnv.rsReport_PeriodGreen.State Then DataEnv.rsReport_PeriodGreen.Close
    DataEnv.Report_PeriodGreen lblPdDate, lblHourSelect, lblPdNo             ' lsPacking.SelectedItem
        Set rsRP = DataEnv.rsReport_PeriodGreen
        
        'Cu Green
		'SELECT classify, COUNT(bc) AS bt, CASE WHEN (mat.crop >= 2019 AND mat.company IN (1,2)) THEN SUM(mat.weightbuy) ELSE SUM(mat.weight) END AS wt FROM mat WHERE (toprgrade = ?)  GROUP BY classify,crop, company ORDER BY classify
        If DataEnv.rsCuAll_Green.State Then DataEnv.rsCuAll_Green.Close
    DataEnv.CuAll_Green lblPackedGradeSelect
        Set rsCU = DataEnv.rsCuAll_Green
        
        'Cu Picking
		'SELECT COUNT(bc) AS bt, 
		'CASE WHEN crop >= 2019 and company IN (1,2) THEN SUM(weightbuy) ELSE SUM(weight) END AS wt 
		'FROM mat WHERE (toprgrade = ?) AND (picking = 1)
		'GROUP BY crop, company
        If DataEnv.rsCuAll_Picking.State Then DataEnv.rsCuAll_Picking.Close
    DataEnv.CuAll_Picking lblPackedGradeSelect
        Set rsCuPicking = DataEnv.rsCuAll_Picking
        
        'Cu Repacked
		'SELECT COUNT(bc) AS ct, SUM(netdef) AS nt FROM pd WHERE (pdtype = 'Lamina') AND (toprgrade = ?)
        If DataEnv.rsCuAll_Repacked.State Then DataEnv.rsCuAll_Repacked.Close
    DataEnv.CuAll_Repacked lblPackedGradeSelect
        Set rsCuRePacked = DataEnv.rsCuAll_Repacked
        
        'Cu Remnant
		'SELECT COUNT(bc) AS ct, SUM(netreal) AS nt FROM pd WHERE (pdtype = 'Remnant') AND (toprgrade = ?) 
        If DataEnv.rsCuAll_Remnant.State Then DataEnv.rsCuAll_Remnant.Close
    DataEnv.CuAll_Remnant lblPackedGradeSelect
        Set rsCuRemnant = DataEnv.rsCuAll_Remnant
        
        'Cu Packed
		'SELECT COUNT(bc) AS ct, SUM(netdef) AS nt FROM pd WHERE (pdtype = 'Lamina') AND (grade = ?)
        If DataEnv.rsCuAll_Packed.State Then DataEnv.rsCuAll_Packed.Close
    DataEnv.CuAll_Packed lblPackedGradeSelect
        Set rsCuPacked = DataEnv.rsCuAll_Packed
        
         '19/02/2016   add  cu_packedgrade  for  get  packedgrade,type,customer,form,packingmat
		'SELECT PD.frompdhour,PD.frompdno, pdsetup.packedgrade,packedgrade.type,packedgrade.customer,packedgrade.form,packedgrade.packingmat
		'FROM pd INNER JOIN  pdsetup  ON PD.frompdno = pdsetup.pdno   
		'INNER JOIN packedgrade  ON  pdsetup.packedgrade = packedgrade.packedgrade  
		'WHERE (PD.fromprgrade = ?) AND (PD.frompddate = ?) AND  (PD.frompdhour = ?) AND PD.frompdno = ? 
        If DataEnv.rsCU_Pakedgrade.State Then DataEnv.rsCU_Pakedgrade.Close
    DataEnv.CU_Pakedgrade lblPackedGradeSelect, lblPdDate, lblHourSelect, lblPdNo
        Set rsCUPackedgrade = DataEnv.rsCU_Pakedgrade
        
        With xls
        '.Range("D5") = ": " & rsRP!packedgrade
        '.Range("D6") = ": " & rsRP!Type
        '.Range("D7") = ": " & rsRP!Customer
        '.Range("D8") = ": " & rsRP!Form
        '.Range("D9") = ": " & rsRP!packingmat
        .Range("D5") = ": " & rsCUPackedgrade!packedgrade
        .Range("D6") = ": " & rsCUPackedgrade!Type
        .Range("D7") = ": " & rsCUPackedgrade!customer
        .Range("D8") = ": " & rsCUPackedgrade!Form
        .Range("D9") = ": " & rsCUPackedgrade!packingmat

        '                .Range("P4") = rsRP!topdno
        '                .Range("O5") = ": " & Format(frmBlending.lblPdDate, "dd-mm-yyyy")
        '                .Range("O6") = ": " & lblHourSelect
        '                .Range("I7") = ": " & frmBlending.lblBT
        '                .Range("I8") = ": " & frmBlending.lblWT

        '===========
        If rsCuPicking.RecordCount > 0 Then
            .Range("G1003") = rsCuPicking!bt
            .Range("H1003") = rsCuPicking!WT
        End If
        If rsCuRePacked.RecordCount > 0 Then
            .Range("G1004") = rsCuRePacked!ct
            .Range("H1004") = rsCuRePacked!nt
        End If
        If rsCuRemnant.RecordCount > 0 Then
            .Range("G1005") = rsCuRemnant!ct
            .Range("H1005") = rsCuRemnant!nt
        End If

        If rsCuPacked.RecordCount > 0 Then
            .Range("G1007") = rsCuPacked!ct
            .Range("H1007") = rsCuPacked!nt
        End If
        '===========

        Cell = 12
        '=== Green In
        'For i = 1 To rsRP.RecordCount
        For i = 1 To rsCU.RecordCount
            Cell = Cell + 1
            .Range("B" & Cell) = i
            'Cumulative
            .Range("C" & Cell) = rsCU!classify
            .Range("G" & Cell) = rsCU!bt
            .Range("H" & Cell) = rsCU!WT

            rsCU.MoveNext
        Next

        .Range("A" & Cell + 2 & ":Z1001").Delete 2

        End With
    xlApp.Visible = True
        'xlApp.Application.Worksheets.PrintOut , , cmbCopies
''
        'xlBook.Application.Workbooks(1).Close 0
        
        Set xlApp = Nothing
        Set xlBook = Nothing
        Set xls = Nothing

        MsgBox "พิมพ์รายงานเรียบร้อยแล้ว", vbInformation, T


End Sub

Private Sub cmdUpdate_Click()
    On Error GoTo ErrHdl

    If cmdUpdate.Enabled = False Or txtBC = "" Or lblClassify = "" Then Exit Sub

    FF = False
    'Check pdno in packingstatus table (ท้ายเครื่อง) with Blendingstatus SHOULD BE THE SAME NO.

    If DataEnv.rsChkPDNoInPackingStatus.State Then DataEnv.rsChkPDNoInPackingStatus.Close
    DataEnv.ChkPDNoInPackingStatus lblPdDate, lblPdNo
        Set rsBlending = DataEnv.rsChkPDNoInPackingStatus
        With rsBlending
        If Not IsNull(!packinghour) Then
            If .RecordCount > 0 Then
                packingHr = !packinghour
                If packingHr > CInt(cmbPeriod) Then
                    MsgBox "ชั่วโมงการทำงานที่ท้ายเครื่อง คือ " & packingHr & " ซึ่งไม่ตรงกับชั่วโมงปัจจุบัน, กรุณากด Finish ก่อน Key ห่อต่อไป", , "กรุณาตรวจสอบกับท้ายเครื่อง"
                        Exit Sub
                End If
            End If
        End If
    End With

    '=== PdNo.
    If lblPdNo = "" Then
        MsgBox "Please select Production No.", vbCritical, T
                Exit Sub
    End If
    '=== Period
    If cmbPeriod = "" Then
        MsgBox "Please Select Period", vbCritical, T
                cmbPeriod.SetFocus
        Exit Sub
    End If
    'BC
    If txtBC = "" Then
        MsgBox "Invalid Barcode, Please Checking", vbCritical, T
                txtBC.SetFocus
        Exit Sub
    End If

    If txtBC.Text <> "" Then
        '30/03/2016  เพิ่มให้เช็คว่า Barcode  ฝั่งขาเข้าจะต้องได้รับการ Approve จาก Checker ก่อนทำการใดๆ
        If DataEnv.rsChkCheckerApprove.State Then DataEnv.rsChkCheckerApprove.Close
        DataEnv.ChkCheckerApprove txtBC.Text
                    Dim rsChkChecker As New ADODB.Recordset
                    Set rsChkChecker = DataEnv.rsChkCheckerApprove
                    With rsChkChecker
            If !checkerapproved = 0 Then
                MsgBox "Checker ยังไม่ได้ Approve Barcode นี้ กรุณาตรวจสอบข้อมูลกับChecker โดยแจ้งเลข Barcode นี้"
                            txtBC.Text = ""
                txtBC.SetFocus
                Exit Sub
            End If
        End With

        '12/05/2020 Check moisture before blending for RYO
        'If DataEnv.rsPdMositure_Pack.State Then DataEnv.rsPdMositure_PackClose
        'DataEnv.PdMositure_Pack txtBC.Text
        'Set rsMoisture = DataEnv.rsPdMositure_Pack
        'If Not rsMoisture.EOF Then
        '    With rsMoisture
        '        .MoveFirst
        '        If !Target < !feeding_moisture Then
        '                MsgBox "ยาห่อนี้ มีค่า Moisture สูงกว่า Target จึงไม่สามารถนำมาทำยา RYO ได้, กรุณาติดต่อแผนก QC ถ้ามีข้อสงสัย"
        '                txtBC.Text = ""
        '                txtBC.SetFocus
        '                Exit Sub
        '        End If
        '    End With
        'End If


        '06/06/2016 เพิ่มเช็คว่า หากยา Repacked จะต้องได้รับการ Received จากโปรแกรม shipping ก่อน
        'If DataEnv.rsShippingReceived.State Then DataEnv.rsShippingReceived.Close
        'DataEnv.ShippingReceived txtBC.Text
        'Dim rsSR As New ADODB.Recordset
        'Set rsSR = DataEnv.rsShippingReceived
        'With rsSR
        '    If !movementno <> "" Then
        '        If !movementstatusid = 0 Then
        '             MsgBox "ระบบShippingยังไม่ได้Received เลข Movement No. " & !movementno & " กรุณาแจ้งเลข Barcodeและ Movement No. นี้กับแผนก Processing"
        '            txtBC.Text = ""
        '            txtBC.SetFocus
        '        Exit Sub
        '        End If
        '    End If
        'End With

    End If

    '============================
    DataEnv.dbServer.BeginTrans
    DataEnv.Mat_Feeding_Update Date, Time, lblPdNo, cmbPeriod, Date, Time, User, lblPdDate, lblPackedGrade, txtBC
        DataEnv.dbServer.CommitTrans

    'Dim RD As Double
    'RD = Round((1 * Rnd) + 0.45, 2)
    'DataEnv.FeedingWeight_Update RD, txtBC
    DataEnv.dbServer.BeginTrans

    DataEnv.FeedingWeight_Update_Normal txtBC
        DataEnv.dbServer.CommitTrans

    '######### Special for Time out Problem
    If Val(lblBT) < 22 Then
        List()
    Else
        lblBT = Val(lblBT) + 1
        lblWT = Format(CDbl(lblWT) + CDbl(lblWeight), "#,##0.00")

        '                ListView.ListItems.Add X, , txtBC
        '                ListView.ListItems(X).SubItems(1) = lblClassify
        '                ListView.ListItems(X).SubItems(2) = lblMark
        '                ListView.ListItems(X).SubItems(3) = lblSupplier
        '                ListView.ListItems(X).SubItems(4) = lblBaleNo
        '                ListView.ListItems(X).SubItems(5) = lblWeight
        '                ListView.ListItems(X).SubItems(6) = Time
    End If
    '###################################

    cmdAdd_Click()
    Exit Sub
ErrHdl:
    DataEnv.dbServer.RollbackTrans
    ErrDP
End Sub

Private Sub cmdUpdateP_Click()
    On Error GoTo ErrHdl
    If cmdUpdateP.Enabled = False Then Exit Sub

    XX = False
    'Check pdno in packingstatus table (ท้ายเครื่อง) with Blendingstatus SHOULD BE THE SAME NO.

    If DataEnv.rsChkPDNoInPackingStatus.State Then DataEnv.rsChkPDNoInPackingStatus.Close
    DataEnv.ChkPDNoInPackingStatus lblPdDate, lblPdNo
                Set rsBlending = DataEnv.rsChkPDNoInPackingStatus
                With rsBlending
        If Not IsNull(!packinghour) Then
            If .RecordCount > 0 Then
                packingHr = !packinghour
                If packingHr > CInt(cmbPeriod) Then
                    MsgBox "ชั่วโมงการทำงานที่ท้ายเครื่อง คือ " & packingHr & " ซึ่งไม่ตรงกับชั่วโมงปัจจุบัน, กรุณากด Finish ก่อน Key ห่อต่อไป", , "กรุณาตรวจสอบกับท้ายเครื่อง"
                                Exit Sub
                End If
            End If
        End If
    End With

    If DataEnv.rsPdBC_Find.State Then DataEnv.rsPdBC_Find.Close
    DataEnv.pdBC_Find txtBCp
                Set rs = DataEnv.rsPdBC_Find
                With rs
        If .RecordCount = 0 Then
            MsgBox "ไม่พบข้อมูลที่ต้องการ", , T
                                Exit Sub
        End If
        '20/04/2021 : Multiple Type For Cut Rag
        If InStr(1, Trim(lblForm.Caption), "CTB", vbTextCompare) = 0 And InStr(1, Trim(lblForm.Caption), "RYO", vbTextCompare) = 0 Then
            If Not !Type = lblType Then
                MsgBox "ข้อมูล Type ไม่ถูกต้อง", , T
                                        Exit Sub
            End If
        End If

        If !issued Then
            MsgBox "ข้อมูลนี้ถูก " & !issuedto & " เรียบร้อยแล้ว"
                                Exit Sub
        End If

        'If Not !Type = lblType Then
        '        MsgBox "ข้อมูล Type ไม่ถูกต้อง", , T
        '        Exit Sub
        'ElseIf !issued Then
        '        MsgBox "ข้อมูลนี้ถูก " & !issuedto & " เรียบร้อยแล้ว"
        '        Exit Sub
        'End If

        lblGradeP = !grade
        lblCaseNoP = !caseno
        lblNetP = Format(!netdef, "#,##0.00")
        lblNetRealP = Format(!netreal, "#,##0.00")

        If txtBCp.Text <> "" Then
            '12/05/2020 : Check moisture before blending for RYO
            'If DataEnv.rsPdMositure_Pack.State Then DataEnv.rsPdMositure_Pack.Close
            'DataEnv.PdMositure_Pack txtBCp.Text
            'Set rsMoisture = DataEnv.rsPdMositure_Pack
            'If Not rsMoisture.EOF Then
            '    With rsMoisture
            '        .MoveFirst
            '        If !Target < !feeding_moisture Then
            '                MsgBox "ยาห่อนี้ มีค่า Moisture สูงกว่า Target จึงไม่สามารถนำมาทำยา RYO ได้, กรุณาติดต่อแผนก QC ถ้ามีข้อสงสัย"
            '                txtBCp.Text = ""
            '                txtBCp.SetFocus
            '                Exit Sub
            '        End If
            '    End With
            'End If

            '06/06/2016 เพิ่มเช็คว่า หากยา Repacked จะต้องได้รับการ Received จากโปรแกรม shipping ก่อน
            If DataEnv.rsShippingReceived.State Then DataEnv.rsShippingReceived.Close
            DataEnv.ShippingReceived txtBCp.Text
                            Dim rsSR As New ADODB.Recordset
                            Set rsSR = DataEnv.rsShippingReceived
                            With rsSR
                If !movementno <> "" Then
                    '30/03/2021 เนื่องจากยาเส้น ย้าย line ไปที่ BaanKru จึงจำเป็นต้องอนุญาตให้ AB4 ตัดที่ wh BaanKru2 ได้(เท่านั้น)
                    '                                    If !Form = "RYO" Then
                    '                                            If InStr(1, !wh, "BaanKru", vbTextCompare) = 0 Then
                    '                                                MsgBox "Location ของยาห่อนี้ ไม่ได้อยู่ที่ BaanKru กรุณาทำ Movement เพื่อย้ายยา ให้ถูกต้องก่อนนำมาใช้", vbCritical + vbOKOnly
                    '                                                txtBCp.Text = ""
                    '                                                txtBCp.SetFocus
                    '                                                Exit Sub
                    '                                            End If
                    '                                            If !movementstatusid = 0 Then
                    '                                                MsgBox "ระบบ Shipping ยังไม่ได้Received เลข Movement No. " & !movementno & " กรุณาแจ้งเลข Barcodeและ Movement No. นี้กับแผนก Processing"
                    '                                                txtBCp.Text = ""
                    '                                                txtBCp.SetFocus
                    '                                                Exit Sub
                    '                                            End If
                    '                                   Else
                    '04/08/2020 เช็ค Location ของห่อยา ก่อนนำมาใช้
                    If !wh <> "STEC" Then
                        MsgBox "Location ของยาห่อนี้ ไม่ได้อยู่ที่ STEC กรุณาทำ Movement เพื่อย้ายยา ให้ถูกต้องก่อนนำมาใช้", vbCritical + vbOKOnly
                                                txtBCp.Text = ""
                        txtBCp.SetFocus
                        Exit Sub
                    End If

                    If !movementstatusid = 0 Then
                        MsgBox "ระบบ Shipping ยังไม่ได้Received เลข Movement No. " & !movementno & " กรุณาแจ้งเลข Barcodeและ Movement No. นี้กับแผนก Processing"
                                                txtBCp.Text = ""
                        txtBCp.SetFocus
                        Exit Sub
                    End If
                    '                                   End If

                End If
            End With
        End If

    End With

    '=== PdNo.
    If lblPdNo = "" Then
        MsgBox "Please select Production No.", vbCritical, T
                Exit Sub
    End If
    '=== Period
    If cmbPeriod = "" Then
        MsgBox "Please check Period", vbCritical, T
                cmbPeriod.SetFocus
        Exit Sub
    End If
    'BC
    If txtBCp = "" Then
        MsgBox "Invalid Barcode, Please Checking", vbCritical, T
                txtBCp.SetFocus
        Exit Sub
    End If

    txtBCp = Trim(txtBCp)
    'Check in this PD
    '        If DataEnv.rsBC_Feeding_Find.State Then DataEnv.rsBC_Feeding_Find.Close
    '        DataEnv.BC_Feeding_Find lblPdNo, txtBC
    '        If DataEnv.rsBC_Feeding_Find.RecordCount > 0 Then
    '                MsgBox "This Barcode already have in this Production", vbCritical, T
    '                txtBC.SetFocus
    '                Exit Sub
    '        End If


    'Header ============================
    '        If Flag1 = "Add" Then
    If lblStartTime = "" Then lblStartTime = Time
    DataEnv.pd_Feeding_Update Date, Time, lblPdNo, cmbPeriod, Date, Time, User, lblPdDate, lblPackedGrade, txtBCp
'        End If


    '        Flag1 = "Edit"
    ListP()
    cmdAddP_Click()
    Exit Sub
ErrHdl:
    MsgBox Err.Number & " :: " & Err.Description

End Sub

Private Sub cmePeriodReport2_Click()
    Dim rsRP As ADODB.Recordset
    Dim rsCU As ADODB.Recordset

    Dim xlApp As Excel.Application
    Dim xlBook As Excel.Workbook
    Dim xls As Excel.Worksheet
    Dim Cell As Integer

    Dim rsCuPicking As ADODB.Recordset
    Dim rsCuRePacked As ADODB.Recordset
    Dim rsCuRemnant As ADODB.Recordset
    Dim rsCuPacked As ADODB.Recordset

    Dim WT As Currency

    If lsPacking.ListItems.Count = 0 Then Exit Sub

    WT = 0
        
        Set xlApp = New Excel.Application
        'Set xlBook = xlApp.Workbooks.Open("C:\My Developing\STEC-Thailand\2004 New System\Production Management System\1_Blending System\Version 2.0\Report\PeriodReport.xlt")
        Set xlBook = xlApp.Workbooks.Open("C:\Program Files\Common Files\PeriodReport_ReWeight.xlt")
        Set xls = xlBook.Worksheets.Application.ActiveSheet
        
        'Green In
        If DataEnv.rsReport_PeriodGreen.State Then DataEnv.rsReport_PeriodGreen.Close
    DataEnv.Report_PeriodGreen lblPdDate, lblHourSelect, lblPdNo           ' lsPacking.SelectedItem
        Set rsRP = DataEnv.rsReport_PeriodGreen
        
        'Cu Green
        If DataEnv.rsCu_Green.State Then DataEnv.rsCu_Green.Close
    DataEnv.Cu_Green lblPackedGradeSelect, lblPdDate, lblHourSelect, lblPdNo
        Set rsCU = DataEnv.rsCu_Green
        
        'Cu Picking
        If DataEnv.rsCu_Picking.State Then DataEnv.rsCu_Picking.Close
    DataEnv.Cu_Picking lblPackedGradeSelect, lblPdDate, lblHourSelect, lblPdNo
        Set rsCuPicking = DataEnv.rsCu_Picking
        
        'Cu Repacked
        If DataEnv.rsCu_Repacked.State Then DataEnv.rsCu_Repacked.Close
    DataEnv.Cu_Repacked lblPackedGradeSelect, lblPdDate, lblHourSelect, lblPdNo
        Set rsCuRePacked = DataEnv.rsCu_Repacked
        
        'Cu Remnant
        If DataEnv.rsCu_Remnant.State Then DataEnv.rsCu_Remnant.Close
    DataEnv.Cu_Remnant lblPackedGradeSelect, lblPdDate, lblHourSelect, lblPdNo
        Set rsCuRemnant = DataEnv.rsCu_Remnant
        
        'Cu Packed
        If DataEnv.rsCu_Packed.State Then DataEnv.rsCu_Packed.Close
    DataEnv.Cu_Packed lblPackedGradeSelect, lblPdDate, lblHourSelect, lblPdNo
        Set rsCuPacked = DataEnv.rsCu_Packed
        
        
        With xls
        .Range("D5") = ": " & rsRP!packedgrade
        .Range("D6") = ": " & rsRP!Type
        .Range("D7") = ": " & rsRP!customer
        .Range("D8") = ": " & rsRP!Form
        .Range("D9") = ": " & rsRP!packingmat

        .Range("N4") = rsRP!topdno
        .Range("L5") = ": " & Format(frmBlending.lblPdDate, "dd-mm-yyyy")
        .Range("L6") = ": " & lblHourSelect
        '                .Range("I7") = ": " & frmBlending.lblBT
        '                .Range("I8") = ": " & frmBlending.lblWT

        '===========
        If rsCuPicking.RecordCount > 0 Then
            '.Range("L1003") = rsCuPicking!bt
            '.Range("M1003") = rsCuPicking!WT
        End If
        If rsCuRePacked.RecordCount > 0 Then
            .Range("L1004") = rsCuRePacked!ct
            .Range("M1004") = rsCuRePacked!nt
        End If
        If rsCuRemnant.RecordCount > 0 Then
            .Range("L1005") = rsCuRemnant!ct
            .Range("M1005") = rsCuRemnant!nt
        End If

        If rsCuPacked.RecordCount > 0 Then
            .Range("L1007") = rsCuPacked!ct
            .Range("M1007") = rsCuPacked!nt
        End If
        '===========

        Cell = 12
        '=== Green In
        'For i = 1 To rsRP.RecordCount
        For i = 1 To rsCU.RecordCount
            Cell = Cell + 1
            .Range("B" & Cell) = i
            'Cumulative
            .Range("C" & Cell) = rsCU!classify
            .Range("L" & Cell) = rsCU!bt
            .Range("M" & Cell) = rsCU!WT
            .Range("N" & Cell) = rsCU!rwt
            'Actual Feed
            rsRP.Find "classify='" & rsCU!classify & "'", , adSearchForward, 1
                        If rsRP.AbsolutePosition > 0 Then
                .Range("F" & Cell) = rsRP!bt
                .Range("G" & Cell) = rsRP!WT
                .Range("H" & Cell) = rsRP!rwt
            End If

            '                        .Range("D" & Cell) = rsRP!classify
            '                        .Range("F" & Cell) = rsRP!bt
            '                        .Range("H" & Cell) = rsRP!WT
            '                        rsRP.MoveNext
            rsCU.MoveNext
        Next
        '=== Picking In
        If DataEnv.rsReport_PeriodPicking.State Then DataEnv.rsReport_PeriodPicking.Close
        DataEnv.report_periodpicking lblPdDate, lblHourSelect, lblPdNo
                If Not DataEnv.rsReport_PeriodPicking.RecordCount = 0 Then
            '.Range("F1003") = Format(DataEnv.rsReport_PeriodPicking!bt, "#,##0.00")
            '.Range("G1003") = Format(DataEnv.rsReport_PeriodPicking!WT, "#,##0.00")
            '.Range("H1003") = Format(DataEnv.rsReport_PeriodPicking!rwt, "#,##0.00")
        End If
        '=== Repacked In
        If DataEnv.rsReport_PeriodRepacked.State Then DataEnv.rsReport_PeriodRepacked.Close
        DataEnv.report_periodrepacked lblPdDate, lblHourSelect, lblPdNo
                If Not DataEnv.rsReport_PeriodRepacked.RecordCount = 0 Then
            .Range("F1004") = Format(DataEnv.rsReport_PeriodRepacked!ct, "#,##0.00")
            .Range("G1004") = Format(DataEnv.rsReport_PeriodRepacked!nt, "#,##0.00")
        End If
        '=== Remnant In
        If DataEnv.rsReport_PeriodRemnant.State Then DataEnv.rsReport_PeriodRemnant.Close
        DataEnv.report_periodremnant lblPdDate, lblHourSelect, lblPdNo
                If Not DataEnv.rsReport_PeriodRemnant.RecordCount = 0 Then
            .Range("F1005") = Format(DataEnv.rsReport_PeriodRemnant!ct, "#,##0.00")
            .Range("G1005") = Format(DataEnv.rsReport_PeriodRemnant!nt, "#,##0.00")
        End If


        '=== Lamina Out
        If DataEnv.rsReport_PeriodPacked.State Then DataEnv.rsReport_PeriodPacked.Close
        DataEnv.report_periodPacked lblPdDate, lblHourSelect, lblPdNo
                If Not DataEnv.rsReport_PeriodPacked.RecordCount = 0 Then
            .Range("F1007") = Format(DataEnv.rsReport_PeriodPacked!ct, "#,##0.00")
            .Range("G1007") = Format(DataEnv.rsReport_PeriodPacked!nt, "#,##0.00")
        End If

        .Range("A" & Cell + 2 & ":Z1001").Delete 2

        End With
    'xlApp.Visible = True
    xlApp.Application.Worksheets.PrintOut , , cmbCopies
        xlBook.Application.Workbooks(1).Close 0
        
        Set xlApp = Nothing
        Set xlBook = Nothing
        Set xls = Nothing

        MsgBox "พิมพ์รายงานเรียบร้อยแล้ว", vbInformation, T


End Sub

Private Sub Command1_Click()
    Dim rs1 As ADODB.Recordset
    Dim rs2 As ADODB.Recordset
    Dim rsRP As ADODB.Recordset

    Dim xlApp As Excel.Application
    Dim xlBook As Excel.Workbook
    Dim xls As Excel.Worksheet
    Dim Cell As Integer

    If lsPacking.ListItems.Count = 0 Then Exit Sub
        
        Set xlApp = New Excel.Application
        'Set xlBook = xlApp.Workbooks.Open("C:\My Developing\STEC-Thailand\2004 New System\Production Management System\1_Blending System\Version 2.0\Report\DetailReport.xlt")
        Set xlBook = xlApp.Workbooks.Open("C:\Program Files\Common Files\DetailReport-ReWeight.xlt")
        Set xls = xlBook.Worksheets.Application.ActiveSheet
        
        'INPUT
        If DataEnv.rsReport_HourDetail.State Then DataEnv.rsReport_HourDetail.Close
    DataEnv.Report_Hourdetail lblPdDate, lblHourSelect, lblPdNo
        Set rs1 = DataEnv.rsReport_HourDetail
        
        If DataEnv.rsReport_PeriodGreen.State Then DataEnv.rsReport_PeriodGreen.Close
    DataEnv.Report_PeriodGreen lblPdDate, lblHourSelect, lblPdNo                     'lsPacking.SelectedItem
        Set rsRP = DataEnv.rsReport_PeriodGreen
        
        With xls
        .Range("D5") = ": " & rsRP!packedgrade
        .Range("D6") = ": " & rsRP!Type
        .Range("D7") = ": " & rsRP!customer
        .Range("D8") = ": " & rsRP!Form
        .Range("D9") = ": " & rsRP!packingmat

        .Range("H5") = ": " & Format(frmBlending.lblPdDate, "dd-mm-yyyy")
        .Range("I4") = rsRP!topdno

        .Range("H7") = ": " & lblHourSelect
        '                .Range("H8") = ": " & frmBlending.lblBT
        '                .Range("H9") = ": " & frmBlending.lblWT

        Cell = 11
        rs1.Sort = "feedingtime"
        '                If lblHour = "1" Then
        For i = 1 To rs1.RecordCount
            Cell = Cell + 1
            .Range("B" & Cell) = i
            .Range("C" & Cell) = rs1!bc
            .Range("E" & Cell) = rs1!supplier
            .Range("F" & Cell) = rs1!baleno
            .Range("G" & Cell) = rs1!classify
            If rs!crop >= 2019 And (rs1!company = 1 Or rs1!company = 2) Then
                .Range("H" & Cell) = rs1!Weightbuy
            Else
                .Range("H" & Cell) = rs1!Weight
            End If
            .Range("I" & Cell) = rs1!feedingweight
            .Range("J" & Cell) = "'" & rs1!feedingtime
            rs1.MoveNext
        Next

        .Range("A" & Cell + 2 & ":K1000").Delete 2

        End With
    xlApp.Visible = True
    'xlApp.Application.Worksheets.PrintOut , , cmbCopies

    'xlBook.Application.Workbooks(1).Close 0

    'Set xlApp = Nothing
    'Set xlBook = Nothing
    'Set xls = Nothing

    'MsgBox "พิมพ์รายงานเรียบร้อยแล้ว", vbInformation, T

End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        Case vbKeyReturn, vbKeyDown
            If FF = True Then Exit Sub
            If XX = True Then Exit Sub
            SendKeys "{tab}"
            KeyCode = 0
        Case vbKeyUp
            SendKeys "+{tab}"
            KeyCode = 0
        Case vbKeyF1
            lblMode = "GREEN IN"
            frameGreenIn.Visible = True
            framePackedIn.Visible = False
        Case vbKeyF2
            lblMode = "REPACKED"
            frameGreenIn.Visible = False
            framePackedIn.Visible = True
            framePackedIn.Top = frameGreenIn.Top
            framePackedIn.Left = frameGreenIn.Left

        Case vbKeyF7
            Exit Sub

            If lblClassify.Visible = True Then
                lblClassifyH.Visible = False
                lblClassify.Visible = False
                lblMarkH.Visible = False
                lblMark.Visible = False
                lblSupplierH.Visible = False
                lblSupplier.Visible = False

                ListView.ColumnHeaders(2).Width = 0
                ListView.ColumnHeaders(3).Width = 0
                ListView.ColumnHeaders(4).Width = 0
                ListView.Width = 8500

                lblBaleNoH.Left = lblBaleNoH.Left - 4950
                lblBaleNo.Left = lblBaleNoH.Left
                lblWeightH.Left = lblWeightH.Left - 4950
                lblWeight.Left = lblWeightH.Left

                framePicking.Left = framePicking.Left - 4950
            Else
                lblClassifyH.Visible = True
                lblClassify.Visible = True
                lblMarkH.Visible = True
                lblMark.Visible = True
                lblSupplierH.Visible = True
                lblSupplier.Visible = True

                ListView.ColumnHeaders(2).Width = 2000
                ListView.ColumnHeaders(3).Width = 920
                ListView.ColumnHeaders(4).Width = 2000
                ListView.Width = 13290

                lblBaleNoH.Left = lblBaleNoH.Left + 4950
                lblBaleNo.Left = lblBaleNoH.Left
                lblWeightH.Left = lblWeightH.Left + 4950
                lblWeight.Left = lblWeightH.Left

                framePicking.Left = framePicking.Left + 4950
            End If

        Case vbKeyF8
            cmdAdd_Click()
        Case vbKeyF9
            'cmdPrint_Click
        Case vbKeyF12
            cmdUpdate_Click()
    End Select

End Sub

Private Sub Form_Load()
    'Center of Screen
    Me.Top = Screen.Height / 2 - Me.Height / 2
    Me.Left = Screen.Width / 2 - Me.Width / 2

    Clear()

    CropSelect = FCrop
    lblCrop = CropSelect
    lblType = TypeSelect
    lblUser = "User : " & User
    lblUser.Left = 12500
    lblUser.Top = frameGreenIn.Top + frameGreenIn.Height + 50

End Sub

Private Sub lblBaleNo_Click()
    If Not ListView.ListItems.Count = 0 Then
        ListSort = 3
        List()
    End If
End Sub

Private Sub lblMark_Change()
    If lblMark = "PK" Or lblMark = "PKL/L" Then
        chkPK.Value = 1
    Else
        chkPK.Value = 0
    End If
End Sub

Private Sub lblSupplier_Click()
    If Not ListView.ListItems.Count = 0 Then
        ListSort = 2
        List()
    End If

End Sub

Private Sub lblWeight_Click()
    If Not ListView.ListItems.Count = 0 Then
        ListSort = 6
        List()
    End If

End Sub

Private Sub ListView_Click()
    On Error Resume Next
    If ListView.ListItems.Count = 0 Then Exit Sub
    With ListView
        txtBC = .SelectedItem
        txtBC = .SelectedItem.ListSubItems(0)
        lblClassify = .SelectedItem.ListSubItems(1)
        lblMark = .SelectedItem.ListSubItems(2)
        lblSupplier = .SelectedItem.ListSubItems(3)
        lblBaleNo = .SelectedItem.ListSubItems(4)
        lblWeight = .SelectedItem.ListSubItems(5)
    End With

End Sub

Private Sub ListViewP_Click()
    On Error Resume Next

    If ListViewP.ListItems.Count = 0 Then Exit Sub
    With ListViewP
        txtBCp = .SelectedItem
        txtBCp = .SelectedItem.ListSubItems(0)
        lblGradeP = .SelectedItem.ListSubItems(1)
        lblCaseNoP = .SelectedItem.ListSubItems(2)
        lblNetP = .SelectedItem.ListSubItems(3)
        lblNetRealP = .SelectedItem.ListSubItems(4)
    End With
End Sub

Private Sub lsPacking_Click()
    lblHourSelect = lsPacking.SelectedItem
    lblPackedGradeSelect = lsPacking.SelectedItem.ListSubItems(1)
End Sub

Private Sub Tpacking_Timer()
    If lblPdDate = "" Then Exit Sub
    If DataEnv.rsPackingStatus_Find.State Then DataEnv.rsPackingStatus_Find.Close
    DataEnv.PackingStatus_Find lblPdDate, lblPdNo

        With DataEnv.rsPackingStatus_Find
        lsPacking.ListItems.Clear
        X = 1
        For i = 1 To .RecordCount
            lsPacking.ListItems.Add X, , !packinghour
                        lsPacking.ListItems(X).SubItems(1) = !packedgrade
            lsPacking.ListItems(X).SubItems(2) = IIf(!Locked = True, "Finish", "")
            .MoveNext
            X = X + 1
        Next

    End With


End Sub

Private Sub txtBC_GotFocus()
    FF = True
    HL
End Sub

Private Sub txtBC_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        If DataEnv.rsMat_Find.State Then DataEnv.rsMat_Find.Close
        DataEnv.mat_Find txtBC
                Set rs = DataEnv.rsMat_Find
                With rs
            If .RecordCount = 0 Then
                MsgBox "ไม่พบข้อมูลที่ต้องการ", , T
                                cmdAdd_Click()
                Exit Sub
            End If

            '============= CUT RAG ============
            If InStr(1, Trim(lblForm.Caption), "CTB", vbTextCompare) = 0 And InStr(1, Trim(lblForm.Caption), "RYO", vbTextCompare) = 0 Then
                If Not !Type = lblType Then
                    MsgBox "ข้อมูล Type ไม่ถูกต้อง", , T
                                    cmdAdd_Click()
                    Exit Sub
                End If
            End If

            If !issued Then
                MsgBox "ข้อมูลนี้ถูก " & !issuedto & " เรียบร้อยแล้ว"
                            cmdAdd_Click()
                Exit Sub
            End If
            lblClassify = !classify
            lblMark = IIf(IsNull(!Mark), "", !Mark)
            lblSupplier = !supplier
            lblBaleNo = !baleno
            'lblWeight = Format(!Weight, "#,##0.00")
            If (!company = 1 Or !company = 2) Then
                lblWeight = IIf(IsNull(!Weightbuy), Format(!Weight, "#,##0.00"), Format(!Weightbuy, "#,##0.00"))
            Else
                lblWeight = Format(!Weight, "#,##0.00")
            End If
            chkPK.Value = IIf(!picking, 1, 0)
            chkBZ.Value = IIf(!bz, 1, 0)


        End With

        If txtBC.Text <> "" Then
            '30/03/2016  เพิ่มให้เช็คว่า Barcode  ฝั่งขาเข้าจะต้องได้รับการ Approve จาก Checker ก่อนทำการใดๆ
            If DataEnv.rsChkCheckerApprove.State Then DataEnv.rsChkCheckerApprove.Close
            DataEnv.ChkCheckerApprove txtBC.Text
                    Dim rsChkChecker As New ADODB.Recordset
                    Set rsChkChecker = DataEnv.rsChkCheckerApprove
                    With rsChkChecker
                If !checkerapproved = 0 Then
                    MsgBox "Checker ยังไม่ได้ Approve Barcode นี้ กรุณาตรวจสอบข้อมูลกับChecker โดยแจ้งเลข Barcode นี้"
                            txtBC.Text = ""
                    txtBC.SetFocus
                    Exit Sub
                End If
            End With
        End If

        If chkAuto.Value = 1 Then cmdUpdate_Click()

    End If

End Sub

Private Sub txtBC_LostFocus()
    FF = False
End Sub

Private Sub txtBCp_GotFocus()
    XX = True
    HL

End Sub

Private Sub txtBCp_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        If DataEnv.rsPdBC_Find.State Then DataEnv.rsPdBC_Find.Close
        DataEnv.pdBC_Find txtBCp
                Set rs = DataEnv.rsPdBC_Find
                With rs
            If .RecordCount = 0 Then
                MsgBox "ไม่พบข้อมูลที่ต้องการ", , T
                                Exit Sub
            End If

            '============= CUT RAG ============
            If InStr(1, Trim(lblForm.Caption), "CTB", vbTextCompare) = 0 And InStr(1, Trim(lblForm.Caption), "RYO", vbTextCompare) = 0 Then
                If Not !Type = lblType Then
                    MsgBox "ข้อมูล Type ไม่ถูกต้อง", , T
                                    Exit Sub
                End If
            End If

            If !issued Then
                MsgBox "ข้อมูลนี้ถูก " & !issuedto & " เรียบร้อยแล้ว"
                            Exit Sub
            End If

            lblGradeP = !grade
            lblCaseNoP = !caseno
            lblNetP = Format(!netdef, "#,##0.00")
            lblNetRealP = Format(!netreal, "#,##0.00")

        End With

        If chkAuto.Value = 1 Then cmdUpdateP_Click()

    End If

End Sub

Private Sub txtBCp_LostFocus()
    XX = False

End Sub

