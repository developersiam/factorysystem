﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface ImatrcnoBL
    {
        string Add(int crop, string machine, string user);
        void DeleteByRcno(int crop,string rcno);
    }
    public class matrcnoBL : ImatrcnoBL
    {
        StecDBMSUnitOfWork uow;
        public matrcnoBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public string Add(int crop, string machine, string user)
        {
            try
            {
                if (crop != DateTime.Now.Year)
                    throw new ArgumentException("crop ที่ระบุไม่ตรงกับ crop ในปฏิทินของเครื่อง โปรดตรวจสอบ");

                if (string.IsNullOrEmpty(machine))
                    throw new ArgumentException("โปรดระบุ machine");

                if (string.IsNullOrEmpty(user))
                    throw new ArgumentException("โปรดระบุ user");

                var rcnoList = uow.matrcnoRepo.Get(x => x.crop == crop);
                var newRcno = 0;
                if (rcnoList.Count() < 1)
                    newRcno = 1;
                else
                    newRcno = rcnoList.Max(x => x.rcno) + 1;

                uow.matrcnoRepo.Add(new matrcno
                {
                    crop = crop,
                    rcno = newRcno,
                    machine = machine,
                    user = user,
                    dtrecord = DateTime.Now
                });
                uow.Save();
                return crop.ToString().Substring(2, 2) +
                    "-" + newRcno.ToString().PadLeft(4, '0');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public void DeleteByRcno(int crop,string rcno)
        {
            //trim for delete
            int tmprcno = Convert.ToInt32(rcno.ToString().PadRight(4, '0'));

            var matrcno = uow.matrcnoRepo.GetSingle(x => x.rcno == tmprcno && x.crop == crop);
            if (matrcno == null)
                throw new ArgumentException("ไม่พบ rcno " + rcno + " ในระบบ");

            uow.matrcnoRepo.Remove(matrcno);
            uow.Save();

        }
    }
}
