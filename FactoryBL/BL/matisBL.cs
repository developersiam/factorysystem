﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface ImatisBL
    {
        void Add(string rgno, string tfno, string hsno, string isno, int crop,
            string type, DateTime date, string place, string isto,
            string remark, string user);

        mati GetSingle(string isno);

        mati GetByRgno(string rgno);

        mati GetByTfno(string tfno);

        mati GetByHsno(string hsno);

        void UpdateRegradeRemark(string rgno, string remark, string editUser);
    }
    public class matisBL : ImatisBL
    {
        StecDBMSUnitOfWork uow;
        public matisBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public void Add(string rgno, string tfno, string hsno, string isno,
            int crop, string type, DateTime date, string place,
            string isto, string remark, string user)
        {
            try
            {
                if (string.IsNullOrEmpty(type))
                    throw new ArgumentException("type cannot be empty.");

                if (string.IsNullOrEmpty(place))
                    throw new ArgumentException("place cannot be empty.");

                if (string.IsNullOrEmpty(user))
                    throw new ArgumentException("user cannot be empty.");

                if (string.IsNullOrEmpty(isto))
                    throw new ArgumentException("isto cannot be empty. { Regrade, Transfer, Handstrip }");

                if (crop != DateTime.Now.Year)
                    throw new ArgumentException("crop ที่ระบุไม่ตรงกับ crop ในปฏิทินของเครื่อง");
                /// insert into matis(rgno, isno, crop, type, date, place, isto, remark, dtrecord, [user])
                /// values(?,?,?,?,?,?,?,?,?,?)
                /// 
                uow.matisRepo.Add(new mati
                {
                    rgno = rgno,
                    hsno = hsno,
                    tfno = tfno,
                    isno = isno,
                    crop = crop,
                    type = type,
                    date = date,
                    place = place,
                    isto = isto,
                    remark = remark,
                    dtrecord = DateTime.Now,
                    user = user
                });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public mati GetByHsno(string hsno)
        {
            return uow.matisRepo.GetSingle(x => x.hsno == hsno);
        }

        public mati GetByRgno(string rgno)
        {
            return uow.matisRepo.GetSingle(x => x.rgno == rgno);
        }

        public mati GetByTfno(string tfno)
        {
            return uow.matisRepo.GetSingle(x => x.tfno == tfno);
        }

        public mati GetSingle(string isno)
        {
            return uow.matisRepo.GetSingle(x => x.isno == isno);
        }

        public void UpdateRegradeRemark(string rgno, string remark, string editUser)
        {
            try
            {
                var model = uow.matisRepo.GetSingle(x => x.rgno == rgno);
                if (model == null)
                    throw new ArgumentException("ไม่พบ matis นี้ในระบบ");

                model.remark = remark;
                model.user = editUser;
                model.dtrecord = DateTime.Now;

                uow.matisRepo.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
