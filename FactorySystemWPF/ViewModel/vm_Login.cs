﻿using FactorySystemWPF.Helper;
using FactorySystemWPF.Model;
using FactorySystemWPF.MVVM;
using FactorySystemWPF.View;
using FactorySystemWPF.View.Shared;
using FactoryBL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FactorySystemWPF.ViewModel
{
    public class vm_Login : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Login()
        {

        }


        #region Properties
        private string _username;
        public string Username
        {
            get { return _username; }
            set { _username = value; }
        }

        private string _password;
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }

        private Login _loginPage;

        public Login LoginPage
        {
            get { return _loginPage; }
            set { _loginPage = value; }
        }
        #endregion


        #region Command
        private ICommand _onPasswordChangedCommand;

        public ICommand OnPasswordChangedCommand
        {
            get { return _onPasswordChangedCommand ?? (_onPasswordChangedCommand = new RelayCommand(OnPasswordChanged)); }
            set { _onPasswordChangedCommand = value; }
        }

        private void OnPasswordChanged(object obj)
        {
            _password = ((System.Windows.Controls.PasswordBox)obj).Password;
        }

        private ICommand _onLoginCommand;

        public ICommand OnLoginCommand
        {
            get { return _onLoginCommand ?? (_onLoginCommand = new RelayCommand(OnLogin)); }
            set { _onLoginCommand = value; }
        }

        private void OnLogin(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_username))
                {
                    MessageBoxHelper.Warning("โปรดกรอกชื่อผู้ใช้");
                    OnFocusRequested(nameof(Username));
                    return;
                }

                if (string.IsNullOrEmpty(_password))
                {
                    MessageBoxHelper.Warning("โปรดกรอกรหัสผ่าน");
                    OnFocusRequested(nameof(Password));
                    return;
                }

                var security = Facade.securityBL().GetSingle(_username);
                if (security == null)
                {
                    OnFocusRequested(nameof(Username));
                    throw new ArgumentException("ไม่พบชื่อผู้ใช้นี้ในระบบ");
                }
                    

                if (Facade.securityBL().DecodePassword(security.pwd) != _password)
                {
                    OnFocusRequested(nameof(Password));
                    throw new ArgumentException("รหัสผ่านไม่ถูกต้อง");
                }

                if(security.blending != true)
                    throw new ArgumentException("ไม่พบสิทธิ์ในการเข้าใช้งานระบบ blending โปรดติดต่อหัวหน้างานเพื่อขอเพิ่มสิทธิ์ในการเข้าใช้งานระบบ");

                user_setting.security = security;
                user_setting.machine = Environment.MachineName;

                _loginPage.NavigationService.Navigate(new Home());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(OnClear)); }
            set { _onClearCommand = value; }
        }

        private void OnClear(object obj)
        {
            _username = "";
            _password = "";

            RaisePropertyChangedEvent(nameof(Username));
            RaisePropertyChangedEvent(nameof(Password));
            OnFocusRequested(nameof(Username));
            OnFocusRequested(nameof(Password));
        }
        #endregion
    }
}
