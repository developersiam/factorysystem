﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface ImatrgnoBL
    {
        string Add(int crop, string machine, string user);
    }

    public class matrgnoBL : ImatrgnoBL
    {
        StecDBMSUnitOfWork uow;
        public matrgnoBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public string Add(int crop, string machine, string user)
        {
            try
            {
                if (crop != DateTime.Now.Year)
                    throw new ArgumentException("crop ที่ระบุไม่ตรงกับ crop ในปฏิทินของเครื่อง โปรดตรวจสอบ");

                if (string.IsNullOrEmpty(machine))
                    throw new ArgumentException("โปรดระบุ machine");

                if (string.IsNullOrEmpty(user))
                    throw new ArgumentException("โปรดระบุ user");

                var rgnoList = uow.matrgnoRepo.Get(x => x.crop == crop);
                var newRgno = 0;
                if (rgnoList.Count() < 1)
                    newRgno = 1;
                else
                    newRgno = rgnoList.Max(x => x.rgno) + 1;

                uow.matrgnoRepo.Add(new matrgno
                {
                    crop = crop,
                    rgno = newRgno,
                    machine = machine,
                    user = user,
                    dtrecord = DateTime.Now
                });
                uow.Save();
                return crop.ToString().Substring(2, 2) +
                    "-" + newRgno.ToString().PadLeft(4, '0');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
