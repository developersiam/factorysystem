﻿using FactoryBL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.Helper
{
    public static class h_packingstatus
    {
        public static List<m_packingstatus> GetByPdno(string pdno)
        {
            try
            {
                var pdsetup = Facade.pdsetupBL().GetSingle(pdno);
                var pdsetupList = Facade.pdsetupBL().GetByCrop(Convert.ToInt16(pdsetup.crop));
                var packingStatusList = Facade.packingstatusBL().GetByPdno(pdno);

                return (from a in pdsetupList
                        from b in packingStatusList
                        where a.pdno == b.pdno
                        select new m_packingstatus
                        {
                            crop = a.crop,
                            pdno = a.pdno,
                            grade = a.packedgrade,
                            pddate = b.pddate,
                            packinghour = b.packinghour,
                            locked = b.locked
                        })
                    .ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
