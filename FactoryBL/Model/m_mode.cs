﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.Model
{
    public class m_mode
    {
        public string Value { get; set; }
        public string Text { get; set; }
    }
}
