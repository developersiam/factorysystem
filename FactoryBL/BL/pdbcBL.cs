﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IpdbcBL
    {
        pdbc GetSingle(short crop);
    }
    public class pdbcBL : IpdbcBL
    {
        StecDBMSUnitOfWork uow;
        public pdbcBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public pdbc GetSingle(short crop)
        {
            return uow.pdbcRepo.GetSingle(x => x.crop == crop);
        }
    }
}
