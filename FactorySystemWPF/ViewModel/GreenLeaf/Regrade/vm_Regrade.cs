﻿using FactoryBL;
using FactoryBL.Helper;
using FactoryBL.Model;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.Model;
using FactorySystemWPF.MVVM;
using FactorySystemWPF.View.GreenLeaf.Regrade;
using FactorySystemWPF.View.Shared;
using FactorySystemWPF.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace FactorySystemWPF.ViewModel.GreenLeaf.Regrade
{
    public class vm_Regrade : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }

        public vm_Regrade()
        {
            _companyList = Facade.companyBL().GetAll();
            RaisePropertyChangedEvent(nameof(companyList));

            _printBarcode = false;
            RaisePropertyChangedEvent(nameof(printBarcode));
            ResetEnabledButton();
        }


        #region Properties

        private matrc _matrc;

        public matrc matrc
        {
            get { return _matrc; }
            set { _matrc = value; }
        }

        private string _company;

        public string company
        {
            get { return _company; }
            set { _company = value; }
        }

        private string _companyName;

        public string companyName
        {
            get { return _companyName; }
            set { _companyName = value; }
        }

        private string _documentStatus;

        public string DocumentStatus
        {
            get { return _documentStatus; }
            set { _documentStatus = value; }
        }

        private Brush _documentStatusBGColor;

        public Brush DocumentStatusBGColor
        {
            get { return _documentStatusBGColor; }
            set { _documentStatusBGColor = value; }
        }

        private int _totalBale;

        public int TotalBale
        {
            get { return _totalBale; }
            set { _totalBale = value; }
        }

        private decimal _totalWeight;

        public decimal TotalWeight
        {
            get { return _totalWeight; }
            set { _totalWeight = value; }
        }

        private mati _matis;

        public mati matis
        {
            get { return _matis; }
            set { _matis = value; }
        }

        private string _remark;

        public string remark
        {
            get { return _remark; }
            set { _remark = value; }
        }

        private mat _matInput;

        public mat matInput
        {
            get { return _matInput; }
            set
            {
                _matInput = value;
                if (_matInput == null)
                    _deleteInputEnabled = false;
                else
                    _deleteInputEnabled = true;
                RaisePropertyChangedEvent(nameof(DeleteInputEnabled));
            }
        }

        private mat _matOutput;

        public mat matOutput
        {
            get { return _matOutput; }
            set { _matOutput = value; }
        }

        private bool _bcInputEnabled;

        public bool bcInputEnabled
        {
            get { return _bcInputEnabled; }
            set { _bcInputEnabled = value; }
        }

        private bool _bcOutputEnabled;

        public bool bcOutputEnabled
        {
            get { return _bcOutputEnabled; }
            set
            {
                _bcOutputEnabled = value;

                if (!string.IsNullOrEmpty(_bcInput))
                    _outputControlEnabled = true;
                else
                    _outputControlEnabled = false;

                RaisePropertyChangedEvent(nameof(OutputControlEnabled));
            }
        }

        private bool _deleteInputEnabled;

        public bool DeleteInputEnabled
        {
            get { return _deleteInputEnabled; }
            set { _deleteInputEnabled = value; }
        }

        private bool _saveInputEnabled;

        public bool SaveInputEnabled
        {
            get { return _saveInputEnabled; }
            set { _saveInputEnabled = value; }
        }

        private bool _deleteOutputEnabled;

        public bool DeleteOutputEnabled
        {
            get { return _deleteOutputEnabled; }
            set { _deleteOutputEnabled = value; }
        }

        private bool _saveOutputEnabled;

        public bool SaveOutputEnabled
        {
            get { return _saveOutputEnabled; }
            set { _saveOutputEnabled = value; }
        }

        private bool _reprintEnabled;

        public bool ReprintEnabled
        {
            get { return _reprintEnabled; }
            set { _reprintEnabled = value; }
        }

        private bool _finishEnabled;

        public bool FinishEnabled
        {
            get { return _finishEnabled; }
            set { _finishEnabled = value; }
        }

        private bool _printBarcode;

        public bool printBarcode
        {
            get { return _printBarcode; }
            set
            {
                _printBarcode = value;
                RaisePropertyChangedEvent(nameof(printBarcode));
                AddOutput();
            }
        }

        private bool _printX2;

        public bool printX2
        {
            get { return _printX2; }
            set { _printX2 = value; }
        }

        private bool _autoSave;

        public bool autoSave
        {
            get { return _autoSave; }
            set { _autoSave = value; }
        }

        private bool _picking;

        public bool picking
        {
            get { return _picking; }
            set { _picking = value; }
        }

        private bool _useDigitalScale;

        public bool useDigitalScale
        {
            get { return _useDigitalScale; }
            set
            {
                try
                {
                    _useDigitalScale = value;
                    if (_useDigitalScale == true)
                    {
                        if (DigitalScaleHelper.CheckPort() == false)
                        {
                            _useDigitalScale = false;
                            MessageBoxHelper.Warning("ไม่พบ port เครื่องชั่งที่เชื่อมต่ออยู่กับเครื่องคอมพิวเตอร์");
                        }
                        else
                        {
                            _useDigitalScale = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBoxHelper.Exception(ex);
                }
            }
        }

        private bool _outputControlEnabled;

        public bool OutputControlEnabled
        {
            get { return _outputControlEnabled; }
            set { _outputControlEnabled = value; }
        }

        private string _bcInput;

        public string bcInput
        {
            get { return _bcInput; }
            set { _bcInput = value; }
        }

        private string _classifyInput;

        public string classifyInput
        {
            get { return _classifyInput; }
            set { _classifyInput = value; }
        }

        private decimal _weightbuyInput;

        public decimal weightbuyInput
        {
            get { return _weightbuyInput; }
            set { _weightbuyInput = value; }
        }

        private decimal _weightInput;

        public decimal weightInput
        {
            get { return _weightInput; }
            set { _weightInput = value; }
        }

        private string _bcOutput;

        public string bcOutput
        {
            get { return _bcOutput; }
            set { _bcOutput = value; }
        }

        private int _balenoOutput;

        public int balenoOutput
        {
            get { return _balenoOutput; }
            set { _balenoOutput = value; }
        }

        private string _classifyOutput;

        public string classifyOutput
        {
            get { return _classifyOutput; }
            set { _classifyOutput = value; }
        }

        private decimal _weightOutput;

        public decimal weightOutput
        {
            get { return _weightOutput; }
            set { _weightOutput = value; }
        }

        private string _mark;

        public string mark
        {
            get { return _mark; }
            set { _mark = value; }
        }

        private int _totalBaleInput;

        public int TotalBaleInput
        {
            get { return _totalBaleInput; }
            set { _totalBaleInput = value; }
        }

        private decimal _totalBuyingWeightInput;

        public decimal TotalBuyingWeightInput
        {
            get { return _totalBuyingWeightInput; }
            set { _totalBuyingWeightInput = value; }
        }

        private decimal _totalReceiveWeightInput;

        public decimal TotalReceiveWeightInput
        {
            get { return _totalReceiveWeightInput; }
            set { _totalReceiveWeightInput = value; }
        }

        private int _totalBaleOutput;

        public int TotalBaleOutput
        {
            get { return _totalBaleOutput; }
            set { _totalBaleOutput = value; }
        }

        private decimal _totalWeightOutput;

        public decimal TotalWeightOutput
        {
            get { return _totalWeightOutput; }
            set { _totalWeightOutput = value; }
        }
        #endregion


        #region List
        private List<classify> _classifyList;

        public List<classify> classifyList
        {
            get { return _classifyList; }
            set { _classifyList = value; }
        }

        private List<mat> _matInputList;

        public List<mat> matInputList
        {
            get { return _matInputList; }
            set { _matInputList = value; }
        }

        private List<mat> _matOutputList;

        public List<mat> matOutputList
        {
            get { return _matOutputList; }
            set { _matOutputList = value; }
        }

        private List<company> _companyList;

        public List<company> companyList
        {
            get { return _companyList; }
            set { _companyList = value; }
        }

        private ObservableCollection<string> _classifyObservableList;

        public ObservableCollection<string> ClassifyObservableList
        {
            get
            { return _classifyObservableList; }
            set { _classifyObservableList = value; }
        }
        #endregion


        #region Command
        private ICommand _newCommand;

        public ICommand NewCommand
        {
            get { return _newCommand ?? (_newCommand = new RelayCommand(New)); }
            set { _newCommand = value; }
        }

        private void New(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการสร้างเอกสาร regrade ใหม่ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                var window = new New();
                var vm = new vm_New();
                window.DataContext = vm;
                vm.Window = window;
                window.ShowDialog();

                if (vm.matrc == null)
                    return;

                _matrc = vm.matrc;
                HeaderBinding();
                InputDetailsBinding();
                OutputDetailsBinding();

                _matInput = null;
                _totalBale = 0;
                _totalWeight = (decimal)0.0;
                _remark = null;
                _bcInput = "";
                _classifyInput = "";
                _weightbuyInput = 0;
                _weightInput = 0;
                _bcOutput = "";
                _balenoOutput = 0;
                _classifyOutput = "";
                _weightOutput = 0;
                _mark = "";

                RaisePropertyChangedEvent(nameof(matrc));
                RaisePropertyChangedEvent(nameof(matInput));
                RaisePropertyChangedEvent(nameof(remark));
                RaisePropertyChangedEvent(nameof(bcInput));
                RaisePropertyChangedEvent(nameof(classifyInput));
                RaisePropertyChangedEvent(nameof(weightbuyInput));
                RaisePropertyChangedEvent(nameof(weightInput));
                RaisePropertyChangedEvent(nameof(bcOutput));
                RaisePropertyChangedEvent(nameof(classifyOutput));
                RaisePropertyChangedEvent(nameof(weightOutput));
                RaisePropertyChangedEvent(nameof(mark));

                ResetEnabledButton();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _callDataCommand;

        public ICommand CallDataCommand
        {
            get { return _callDataCommand ?? (_callDataCommand = new RelayCommand(CallData)); }
            set { _callDataCommand = value; }
        }

        private void CallData(object obj)
        {
            CallData();
        }

        private ICommand _finishCommand;

        public ICommand FinishCommand
        {
            get { return _finishCommand ?? (_finishCommand = new RelayCommand(Finish)); }
            set { _finishCommand = value; }
        }

        private void Finish(object obj)
        {
            try
            {
                if (_matrc == null)
                    throw new ArgumentException("โปรดกด New หรือ Call Data เพื่อเริ่มต้นการทำงาน");

                if (_matrc.finishtime != null)
                    throw new ArgumentException("ข้อมูบนี้ถูก finish ไปแล้วเมือ " + _matrc.finishtime);

                if (_matInputList.Count() <= 0)
                    throw new ArgumentException("จำนวนห่อยาฝั่ง input ควรมีอย่างน้อย 1 ห่อขึ้นไป");

                if (_matOutputList.Count() <= 0)
                    throw new ArgumentException("จำนวนห่อยาฝั่ง output ควรมีอย่างน้อย 1 ห่อขึ้นไป");

                if (MessageBoxHelper.Question("ข้อมูลชุดนี้จะถูกส่งต่อไปยังแผนก Leaf Account" + Environment.NewLine +
                    "ท่านต้องการ finish ข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                Facade.matrcBL().FinishRegrade(_matrc.rcno, user_setting.security.uname);
                _matrc = Facade.matrcBL().GetSingle(_matrc.rcno);
                RaisePropertyChangedEvent(nameof(matrc));
                HeaderBinding();
                InputDetailsBinding();
                OutputDetailsBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _reportCommand;

        public ICommand ReportCommand
        {
            get { return _reportCommand ?? (_reportCommand = new RelayCommand(Report)); }
            set { _reportCommand = value; }
        }

        private void Report(object obj)
        {
            PrintReport();
        }

        private ICommand _reloadCommand;

        public ICommand ReloadCommand
        {
            get { return _reloadCommand ?? (_reloadCommand = new RelayCommand(Reload)); }
            set { _reloadCommand = value; }
        }

        private void Reload(object obj)
        {
            if (_matrc == null)
                return;

            ReloadPage(_matrc.rcno);
        }

        private ICommand _addInputCommand;

        public ICommand AddInputCommand
        {
            get { return _addInputCommand ?? (_addInputCommand = new RelayCommand(AddInput)); }
            set { _addInputCommand = value; }
        }

        private void AddInput(object obj)
        {
            try
            {
                if (_matrc == null)
                    throw new ArgumentException("โปรดกด New หรือ Call Data เพื่อเริ่มต้นการทำงาน");

                _bcInputEnabled = true;
                _saveInputEnabled = false;
                RaisePropertyChangedEvent(nameof(bcInputEnabled));
                RaisePropertyChangedEvent(nameof(SaveInputEnabled));
                AddInput();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _deleteInputCommand;

        public ICommand DeleteInputCommand
        {
            get { return _deleteInputCommand ?? (_deleteInputCommand = new RelayCommand(DeleteInput)); }
            set { _deleteInputCommand = value; }
        }

        private void DeleteInput(object obj)
        {
            DeleteInput();
        }

        private ICommand _saveInputCommand;

        public ICommand SaveInputCommand
        {
            get { return _saveInputCommand ?? (_saveInputCommand = new RelayCommand(SaveInput)); }
            set { _saveInputCommand = value; }
        }

        private void SaveInput(object obj)
        {
            SaveInput();
        }

        private ICommand _addOutputCommand;

        public ICommand AddOutputCommand
        {
            get { return _addOutputCommand ?? (_addOutputCommand = new RelayCommand(AddOutput)); }
            set { _addOutputCommand = value; }
        }

        private void AddOutput(object obj)
        {
            AddOutput();
        }

        private ICommand _deleteOutputCommand;

        public ICommand DeleteOutputCommand
        {
            get { return _deleteOutputCommand ?? (_deleteOutputCommand = new RelayCommand(DeleteOutput)); }
            set { _deleteOutputCommand = value; }
        }

        private void DeleteOutput(object obj)
        {
            DeleteOutput();
        }

        private ICommand _saveOutputCommand;

        public ICommand SaveOutputCommand
        {
            get { return _saveOutputCommand ?? (_saveOutputCommand = new RelayCommand(SaveOutput)); }
            set { _saveOutputCommand = value; }
        }

        private void SaveOutput(object obj)
        {
            SaveOutput();
        }

        private ICommand _reprintCommand;

        public ICommand ReprintCommand
        {
            get { return _reprintCommand ?? (_reprintCommand = new RelayCommand(Reprint)); }
            set { _reprintCommand = value; }
        }

        private void Reprint(object obj)
        {
            try
            {
                ///Reprint function.
                ///
                Facade.matBL().PrintRegradeBarcode(_matOutput.bc, _printX2 == true ? 2 : 1);
                _reprintEnabled = false;
                RaisePropertyChangedEvent(nameof(ReprintEnabled));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _printTestCommand;

        public ICommand PrintTestCommand
        {
            get { return _printTestCommand ?? (_printTestCommand = new RelayCommand(PrintTest)); }
            set { _printTestCommand = value; }
        }

        private void PrintTest(object obj)
        {
            Facade.matBL().PrintTestRegradeBarcode(_printX2 == true ? 2 : 1);
        }

        private ICommand _f1Command;

        public ICommand F1Command
        {
            get { return _f1Command ?? (_f1Command = new RelayCommand(F1)); }
            set { _f1Command = value; }
        }

        private void F1(object obj)
        {
            if (_matrc == null)
                return;
            ReloadPage(_matrc.rcno);
        }

        private ICommand _f2Command;

        public ICommand F2Command
        {
            get { return _f2Command ?? (_f2Command = new RelayCommand(F2)); }
            set { _f2Command = value; }
        }

        private void F2(object obj)
        {
            CallData();
        }

        private ICommand _f3Command;

        public ICommand F3Command
        {
            get { return _f3Command ?? (_f3Command = new RelayCommand(F3)); }
            set { _f3Command = value; }
        }

        private void F3(object obj)
        {
            PrintReport();
        }

        private ICommand _f8Command;

        public ICommand F8Command
        {
            get { return _f8Command ?? (_f8Command = new RelayCommand(F8)); }
            set { _f8Command = value; }
        }

        private void F8(object obj)
        {
            AddOutput();
        }

        private ICommand _f12Command;

        public ICommand F12Command
        {
            get { return _f12Command ?? (_f12Command = new RelayCommand(F12)); }
            set { _f12Command = value; }
        }

        private void F12(object obj)
        {
            SaveOutput();
        }

        private ICommand _barcodeInputEnterCommand;

        public ICommand BarcodeInputEnterCommand
        {
            get { return _barcodeInputEnterCommand ?? (_barcodeInputEnterCommand = new RelayCommand(BarcodeInputEnter)); }
            set { _barcodeInputEnterCommand = value; }
        }

        private void BarcodeInputEnter(object obj)
        {
            try
            {
                if (_matrc == null)
                    throw new ArgumentException("โปรดกด New หรือ Call Data เพื่อเริ่มต้นการทำงาน");

                matInputBinding();
                if (_autoSave == true)
                    SaveInput();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _barcodeOutputEnterCommand;

        public ICommand BarcodeOutputEnterCommand
        {
            get { return _barcodeOutputEnterCommand ?? (_barcodeOutputEnterCommand = new RelayCommand(BarcodeOutputEnter)); }
            set { _barcodeOutputEnterCommand = value; }
        }

        private void BarcodeOutputEnter(object obj)
        {
            try
            {
                if (_matrc == null)
                    throw new ArgumentException("โปรดกดปุ่ม New หรือ Call Data เพื่อเริ่มต้นการทำงานก่อน");

                if (string.IsNullOrEmpty(_bcOutput))
                {
                    MessageBoxHelper.Warning("โปรดระบุ barcode");
                    OnFocusRequested(nameof(bcOutput));
                    return;
                }
                OnFocusRequested(nameof(balenoOutput));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _baleNoOutputEnterCommand;

        public ICommand BaleNoOutputEnterCommand
        {
            get { return _baleNoOutputEnterCommand ?? (_baleNoOutputEnterCommand = new RelayCommand(BaleNoOutputEnter)); }
            set { _baleNoOutputEnterCommand = value; }
        }

        private void BaleNoOutputEnter(object obj)
        {
            OnFocusRequested(nameof(classifyOutput));
        }

        private ICommand _classifyOutputEnterCommand;

        public ICommand ClassifyOutputEnterCommand
        {
            get { return _classifyOutputEnterCommand ?? (_classifyOutputEnterCommand = new RelayCommand(ClassifyOutputEnter)); }
            set { _classifyOutputEnterCommand = value; }
        }

        private void ClassifyOutputEnter(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_classifyOutput))
                    throw new ArgumentException("โปรดระบุเกรด classify");

                if (_classifyList.SingleOrDefault(x => x.classify1 == _classifyOutput) == null)
                {
                    MessageBoxHelper.Warning("ไม่พบเกรด classify นี้ในระบบ โปรดตรวจสอบข้อมูลกับแผนกไอที");
                    OnFocusRequested(nameof(classifyOutput));
                    return;
                }

                _saveOutputEnabled = true;
                RaisePropertyChangedEvent(nameof(SaveOutputEnabled));
                OnFocusRequested(nameof(weightOutput));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _weightOutputEnterCommand;

        public ICommand WeightOutputEnterCommand
        {
            get { return _weightOutputEnterCommand ?? (_weightOutputEnterCommand = new RelayCommand(WeightOutputEnter)); }
            set { _weightOutputEnterCommand = value; }
        }

        private void WeightOutputEnter(object obj)
        {
            try
            {
                if (_weightOutput < 1)
                    throw new ArgumentException("น้ำหนักห่อยาจะต้องมากกว่า 0");

                if (_weightOutput > 100)
                    if (MessageBoxHelper.Question("น้ำหนักห่อยาเกิน 100 กก. ท่านต้องการบันทึกห่อยานี้ใช่หรือไม่?")
                        == MessageBoxResult.No)
                        return;

                if (_autoSave == true)
                    SaveOutput();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _matInputSelectedRowCommand;

        public ICommand MatInputSelectedRowCommand
        {
            get { return _matInputSelectedRowCommand ?? (_matInputSelectedRowCommand = new RelayCommand(MatInputSelectedRow)); }
            set { _matInputSelectedRowCommand = value; }
        }

        private void MatInputSelectedRow(object obj)
        {
            try
            {
                if (obj == null)
                    return;

                _matInput = (mat)obj;
                _deleteInputEnabled = true;
                RaisePropertyChangedEvent(nameof(matInput));
                RaisePropertyChangedEvent(nameof(DeleteInputEnabled));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _matOutputSelectedRowCommand;

        public ICommand MatOutputSelectedRowCommand
        {
            get { return _matOutputSelectedRowCommand ?? (_matOutputSelectedRowCommand = new RelayCommand(MatOutputSelectedRow)); }
            set { _matOutputSelectedRowCommand = value; }
        }

        private void MatOutputSelectedRow(object obj)
        {
            try
            {
                if (obj == null)
                    return;

                _matOutput = (mat)obj;
                _deleteOutputEnabled = true;
                _reprintEnabled = true;
                RaisePropertyChangedEvent(nameof(matOutput));
                RaisePropertyChangedEvent(nameof(DeleteOutputEnabled));
                RaisePropertyChangedEvent(nameof(ReprintEnabled));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _weightOutputGotFocusCommand;

        public ICommand WeightOutputGotFocusCommand
        {
            get { return _weightOutputGotFocusCommand ?? (_weightOutputGotFocusCommand = new RelayCommand(WeightOutputGotFocus)); }
            set { _weightOutputGotFocusCommand = value; }
        }

        private void WeightOutputGotFocus(object obj)
        {
            if (_useDigitalScale == false)
                return;

            if (DigitalScaleHelper.CheckPort() == false)
            {
                MessageBoxHelper.Warning("ไม่พบพอร์ตเชื่อมต่อเครื่องชั่งดิจิตอลบนเครื่องคอมพิวเตอร์นี้");
                _useDigitalScale = false;
                RaisePropertyChangedEvent(nameof(useDigitalScale));
                return;
            }

            var window = new ScalePanel();
            var vm = new vm_ScalePanel();
            vm.Window = window;
            window.DataContext = vm;
            window.ShowDialog();

            if (vm.Weight == null)
                return;

            _weightOutput = (decimal)vm.Weight;
            RaisePropertyChangedEvent(nameof(weightOutput));
            OnFocusRequested(nameof(weightOutput));
        }

        private ICommand _saveRemarkCommand;

        public ICommand SaveRemarkCommand
        {
            get { return _saveRemarkCommand ?? (_saveRemarkCommand = new RelayCommand(SaveRemark)); }
            set { _saveRemarkCommand = value; }
        }

        private void SaveRemark(object obj)
        {
            try
            {
                if (_matrc == null)
                    throw new ArgumentException("โปรดกดปุ่ม New หรือ Call Data เพื่อเริ่มต้นการทำงานก่อน");

                if (_matrc.finishtime != null)
                    throw new ArgumentException("หน้าจอนี้ถูก finish ไปแล้วเมือ " + matrc.finishtime);

                if (string.IsNullOrEmpty(_remark))
                    throw new ArgumentException("โปรดระบุ remark");

                Facade.matrcBL().UpdateRegradeRemark(_matrc.rgno, _remark, user_setting.security.uname);
                Facade.matisBL().UpdateRegradeRemark(_matrc.rgno, _remark, user_setting.security.uname);

                MessageBoxHelper.Info("บันทึกสำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion


        #region Function
        private void CompanyToCompanyName()
        {
            try
            {
                if (string.IsNullOrEmpty(_company))
                    return;

                var company = _companyList.SingleOrDefault(x => x.code == _company);
                if (company == null)
                    throw new ArgumentException("ไม่พบ company " + _company + " ในระบบ");

                _companyName = company.name;
                RaisePropertyChangedEvent(nameof(companyName));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void HeaderBinding()
        {
            try
            {
                if (_matrc == null)
                    throw new ArgumentException("โปรดกดปุ่ม New หรือ Call Data เพื่อเริ่มต้นการทำงานก่อน");

                _matis = Facade.matisBL().GetByRgno(_matrc.rgno);
                if (_matis == null)
                    throw new ArgumentException("ไม่พบข้อมูล matis ในระบบ");

                _remark = _matrc.remark;
                _documentStatus = _matrc.finishtime == null ? "UnLocked" : "Locked";
                _documentStatusBGColor = _matrc.finishtime == null ? Brushes.LightGreen : Brushes.Salmon;
                _finishEnabled = _matrc.finishtime == null ? true : false;

                RaisePropertyChangedEvent(nameof(matrc));
                RaisePropertyChangedEvent(nameof(matis));
                RaisePropertyChangedEvent(nameof(remark));
                RaisePropertyChangedEvent(nameof(DocumentStatus));
                RaisePropertyChangedEvent(nameof(DocumentStatusBGColor));
                RaisePropertyChangedEvent(nameof(FinishEnabled));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void InputDetailsBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_matis.isno))
                    throw new ArgumentException("ไม่พบข้อมูล isno นี้ในระบบ");

                _matInputList = Facade.matBL().GetByIsno(_matis.isno)
                    .OrderByDescending(x => x.issueddate)
                    .ThenByDescending(x => x.issuedtime)
                    .ToList();
                _totalBaleInput = _matInputList.Count();
                _totalBuyingWeightInput = _matInputList.Count() > 0 ?
                    (decimal)_matInputList.Sum(x => x.weightbuy) : (decimal)0.0;
                _totalReceiveWeightInput = _matInputList.Count() > 0 ?
                    (decimal)_matInputList.Sum(x => x.weight) : (decimal)0.0;

                RaisePropertyChangedEvent(nameof(matInputList));
                RaisePropertyChangedEvent(nameof(TotalBaleInput));
                RaisePropertyChangedEvent(nameof(TotalBuyingWeightInput));
                RaisePropertyChangedEvent(nameof(TotalReceiveWeightInput));

                _company = "";
                _companyName = "";
                RaisePropertyChangedEvent(nameof(company));
                RaisePropertyChangedEvent(nameof(companyName));

                if (_matInputList.Count() > 0)
                {
                    _company = _matInputList.FirstOrDefault().company;
                    var company = _companyList.SingleOrDefault(x => x.code == _company);
                    if (company == null)
                        throw new ArgumentException("ไม่พบ company นี้ในระบบ โปรดติดต่อแผนกไอทีเพื่อตรวจสอบข้อมูล");

                    _companyName = company.name;
                    RaisePropertyChangedEvent(nameof(company));
                    RaisePropertyChangedEvent(nameof(companyName));
                    ClassifyListBinding();
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void OutputDetailsBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_matrc.rcno))
                    throw new ArgumentException("ไม่พบข้อมูล rcno นี้ในระบบ");

                _matOutputList = Facade.matBL().GetByRcno(_matrc.rcno)
                    .OrderByDescending(x => x.dtrecord)
                    .ToList();
                _totalBaleOutput = _matOutputList.Count();
                _totalWeightOutput = _matOutputList.Count() > 0 ?
                    (decimal)_matOutputList.Sum(x => x.weight) : (decimal)0.0;
                _totalBale = _totalBaleOutput;
                _totalWeight = _totalWeightOutput;

                RaisePropertyChangedEvent(nameof(matOutputList));
                RaisePropertyChangedEvent(nameof(TotalBaleOutput));
                RaisePropertyChangedEvent(nameof(TotalWeightOutput));
                RaisePropertyChangedEvent(nameof(TotalBale));
                RaisePropertyChangedEvent(nameof(TotalWeight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void matInputBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_bcInput))
                {
                    MessageBoxHelper.Warning("โปรดระบุ input barcode");
                    OnFocusRequested(nameof(bcInput));
                    return;
                }

                _matInput = Facade.matBL().GetSingle(_bcInput);
                if (_matInput == null)
                {
                    MessageBoxHelper.Warning("ไม่พบข้อมูลห่อยารหัสบาร์โค้ต " + _bcInput + " นี้ในระบบ");
                    _bcInput = "";
                    _saveInputEnabled = false;
                    RaisePropertyChangedEvent(nameof(bcInput));
                    RaisePropertyChangedEvent(nameof(SaveInputEnabled));
                    OnFocusRequested(nameof(bcInput));
                    return;
                }

                _saveInputEnabled = true;
                _classifyInput = _matInput.classify;
                _weightbuyInput = (decimal)_matInput.weightbuy;
                _weightInput = (decimal)_matInput.weight;
                if (_matInputList == null)
                {
                    ///company and companyName จะเปลี่ยนก็ต่อเมื่อเป็นยาห่อแรกเท่านั้น
                    ///เอาไว้แสดงตรง Header และใช้เช็คว่า
                    ///ยาที่จะเอามาทำ regrade ต้องเป็นยาจาก compay เดียวกันเท่านั้น
                    _company = _matInput.company;
                    CompanyToCompanyName();
                    RaisePropertyChangedEvent(nameof(company));
                    RaisePropertyChangedEvent(nameof(companyName));
                }

                RaisePropertyChangedEvent(nameof(SaveInputEnabled));
                RaisePropertyChangedEvent(nameof(classifyInput));
                RaisePropertyChangedEvent(nameof(weightbuyInput));
                RaisePropertyChangedEvent(nameof(weightInput));
                RaisePropertyChangedEvent(nameof(matInput));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddInput()
        {
            try
            {
                if (_matrc == null)
                    throw new ArgumentException("โปรดกดปุ่ม New หรือ Call Data เพื่อเริ่มต้นการทำงานก่อน");

                if (_matrc.finishtime != null)
                    throw new ArgumentException("หน้าจอนี้ถูก finish ไปแล้วเมือ " + matrc.finishtime);

                _bcInput = "";
                _company = null;
                _classifyInput = "";
                _weightbuyInput = (decimal)0.0;
                _weightInput = (decimal)0.0;
                _remark = "";
                _matInput = null;
                _saveInputEnabled = false;
                _deleteInputEnabled = false;

                RaisePropertyChangedEvent(nameof(bcInput));
                RaisePropertyChangedEvent(nameof(company));
                RaisePropertyChangedEvent(nameof(classifyInput));
                RaisePropertyChangedEvent(nameof(weightbuyInput));
                RaisePropertyChangedEvent(nameof(weightInput));
                RaisePropertyChangedEvent(nameof(remark));
                RaisePropertyChangedEvent(nameof(matInput));
                RaisePropertyChangedEvent(nameof(SaveInputEnabled));
                RaisePropertyChangedEvent(nameof(DeleteInputEnabled));
                OnFocusRequested(nameof(bcInput));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void AddOutput()
        {
            try
            {
                if (_matrc == null)
                    throw new ArgumentException("โปรดกดปุ่ม New หรือ Call Data เพื่อเริ่มต้นการทำงานก่อน");

                if (_matrc.finishtime != null)
                    throw new ArgumentException("หน้าจอนี้ถูก finish ไปแล้วเมือ " + matrc.finishtime);

                if (_matInputList == null)
                    throw new ArgumentException("โปรดบันทึกยาฝั่ง input ก่อนอย่างน้อย 1 ห่อ");

                _balenoOutput = 0;
                _classifyOutput = "";
                _weightOutput = (decimal)0.0;
                _mark = "";
                _matOutput = null;
                _saveOutputEnabled = false;
                _deleteOutputEnabled = false;
                _reprintEnabled = false;
                _outputControlEnabled = true;

                RaisePropertyChangedEvent(nameof(bcOutput));
                RaisePropertyChangedEvent(nameof(balenoOutput));
                RaisePropertyChangedEvent(nameof(classifyOutput));
                RaisePropertyChangedEvent(nameof(weightOutput));
                RaisePropertyChangedEvent(nameof(mark));
                RaisePropertyChangedEvent(nameof(matOutput));
                RaisePropertyChangedEvent(nameof(DeleteOutputEnabled));
                RaisePropertyChangedEvent(nameof(ReprintEnabled));
                RaisePropertyChangedEvent(nameof(OutputControlEnabled));

                if (_printBarcode == true)
                {
                    _bcOutput = Facade.matBL()
                        .GetAB4RegradeBarcode((int)_matrc.crop, _matrc.type);

                    if (string.IsNullOrEmpty(_bcOutput))
                        throw new ArgumentException("ระบบไม่สามารถ generate บาร์โค้ตได้ โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                    _bcOutputEnabled = false;
                    RaisePropertyChangedEvent(nameof(bcOutput));
                    RaisePropertyChangedEvent(nameof(bcOutputEnabled));
                    OnFocusRequested(nameof(balenoOutput));
                }
                else
                {
                    _bcOutput = "";
                    _bcOutputEnabled = true;
                    RaisePropertyChangedEvent(nameof(bcOutput));
                    RaisePropertyChangedEvent(nameof(bcOutputEnabled));
                    OnFocusRequested(nameof(bcOutput));
                }
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SaveInput()
        {
            try
            {
                if (_matInput == null)
                    throw new ArgumentException("โปรดระบุข้อมูลห่อยาฝั่ง input");

                Facade.matBL().RegradeIssued(_matis.isno, DateTime.Now, DateTime.Now, _bcInput);
                _saveInputEnabled = false;
                RaisePropertyChangedEvent(nameof(SaveInputEnabled));
                AddInput();
                HeaderBinding();
                InputDetailsBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteInput()
        {
            try
            {
                if (_matInput == null)
                    throw new ArgumentException("โปรดคลิกเลือกแถวข้อมูลที่ต้องการลบจากตาราง regrade input");

                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?" + Environment.NewLine +
                    "===============" + Environment.NewLine +
                    "bc: " + _matInput.bc + Environment.NewLine +
                    "company: " + _matInput.company + Environment.NewLine +
                    "green: " + _matInput.green + Environment.NewLine +
                    "classify: " + _matInput.classify + Environment.NewLine +
                    "weight: " + Convert.ToDecimal(_matInput.weight).ToString("N1") + Environment.NewLine +
                    "weightbuy: " + Convert.ToDecimal(_matInput.weightbuy).ToString("N1") + Environment.NewLine +
                    "supplier: " + _matInput.supplier + Environment.NewLine) == MessageBoxResult.No)
                    return;

                Facade.matBL().DeleteRegradeIssued(_matInput.bc, matis.isno);
                _deleteInputEnabled = false;
                RaisePropertyChangedEvent(nameof(DeleteInputEnabled));
                AddInput();
                InputDetailsBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SaveOutput()
        {
            try
            {
                if (_matInputList == null)
                    throw new ArgumentException("โปรดบันทึกยาฝั่ง Input ก่อนอย่างน้อย 1 ห่อ");

                if (_weightOutput > 100)
                    if (MessageBoxHelper.Question("น้ำหนักเกิน 100 Kg. ยืนยันกด Yes ยกเลิกกด No") == MessageBoxResult.No)
                        return;

                Facade.matBL().RegradeReceive(_matrc.rcno, _bcOutput, _balenoOutput, _classifyOutput, _mark,
                    _weightOutput, _weightOutput, _picking, user_setting.security.uname, _matrc.place);

                if (_printBarcode == true)
                    Facade.matBL().PrintRegradeBarcode(_bcOutput, _printX2 == true ? 2 : 1);

                AddOutput();
                OutputDetailsBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DeleteOutput()
        {
            try
            {
                if (_matOutput == null)
                    throw new ArgumentException("โปรดคลิกเลือกแถวข้อมูลที่ต้องการลบจากตาราง regrade output");

                if (MessageBoxHelper.Question("ท่านต้องการลบข้อมูลนี้ใช่หรือไม่?" + Environment.NewLine +
                    "===============" + Environment.NewLine +
                    "bc: " + _matOutput.bc + Environment.NewLine +
                    "crop: " + _matOutput.crop + Environment.NewLine +
                    "type: " + _matOutput.type + Environment.NewLine +
                    "company: " + _matOutput.company + Environment.NewLine +
                    "green: " + _matOutput.green + Environment.NewLine +
                    "classify: " + _matOutput.classify + Environment.NewLine +
                    "weight: " + Convert.ToDecimal(_matOutput.weight).ToString("N1") + Environment.NewLine +
                    "weightbuy: " + Convert.ToDecimal(_matOutput.weightbuy).ToString("N1") + Environment.NewLine +
                    "supplier: " + _matOutput.supplier + Environment.NewLine) == MessageBoxResult.No)
                    return;

                Facade.matBL().DeleteRegradeReceive(_matOutput.bc);
                AddOutput();
                OutputDetailsBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ResetEnabledButton()
        {
            _deleteInputEnabled = false;
            _deleteOutputEnabled = false;
            _saveInputEnabled = false;
            _saveOutputEnabled = false;
            _bcOutputEnabled = false;
            _bcInputEnabled = false;
            _outputControlEnabled = false;
            _reprintEnabled = false;

            RaisePropertyChangedEvent(nameof(DeleteInputEnabled));
            RaisePropertyChangedEvent(nameof(DeleteOutputEnabled));
            RaisePropertyChangedEvent(nameof(SaveInputEnabled));
            RaisePropertyChangedEvent(nameof(SaveOutputEnabled));
            RaisePropertyChangedEvent(nameof(bcInputEnabled));
            RaisePropertyChangedEvent(nameof(bcOutputEnabled));
            RaisePropertyChangedEvent(nameof(OutputControlEnabled));
            RaisePropertyChangedEvent(nameof(ReprintEnabled));
        }

        private void ClassifyListBinding()
        {
            try
            {
                if (string.IsNullOrEmpty(_company))
                    _classifyObservableList = new ObservableCollection<string>();

                if (string.IsNullOrEmpty(_matrc.type))
                    _classifyObservableList = new ObservableCollection<string>();

                var isLTL = false;
                if (_companyName.Contains("LTL"))
                    isLTL = true;

                _classifyList = Facade.classifyBL().GetByType(_matrc.type, isLTL);
                var list = new ObservableCollection<string>();
                foreach (var item in _classifyList)
                    list.Add(item.classify1);

                _classifyObservableList = list;
                RaisePropertyChangedEvent(nameof(ClassifyObservableList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ReloadPage(string rcno)
        {
            try
            {
                _matrc = Facade.matrcBL().GetSingle(rcno);
                RaisePropertyChangedEvent(nameof(matrc));

                if (_matrc == null)
                    return;

                _company = "";
                _companyName = "";
                _matInput = null;
                _matOutput = null;
                _bcInput = null;
                _classifyInput = null;
                _weightbuyInput = (decimal)0.0;
                _weightInput = (decimal)0.0;
                _bcOutput = null;
                _balenoOutput = 0;
                _classifyOutput = null;
                _weightOutput = (decimal)0.0;
                _mark = null;
                _matInputList = null;
                _matOutputList = null;

                RaisePropertyChangedEvent(nameof(company));
                RaisePropertyChangedEvent(nameof(companyName));
                RaisePropertyChangedEvent(nameof(matInput));
                RaisePropertyChangedEvent(nameof(matOutput));
                RaisePropertyChangedEvent(nameof(bcInput));
                RaisePropertyChangedEvent(nameof(classifyInput));
                RaisePropertyChangedEvent(nameof(weightbuyInput));
                RaisePropertyChangedEvent(nameof(weightInput));
                RaisePropertyChangedEvent(nameof(bcOutput));
                RaisePropertyChangedEvent(nameof(balenoOutput));
                RaisePropertyChangedEvent(nameof(weightOutput));
                RaisePropertyChangedEvent(nameof(mark));
                RaisePropertyChangedEvent(nameof(matInputList));
                RaisePropertyChangedEvent(nameof(matOutputList));

                HeaderBinding();
                InputDetailsBinding();
                OutputDetailsBinding();
                ResetEnabledButton();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void CallData()
        {
            try
            {
                var window = new CallData();
                var vm = new vm_CallData();
                window.DataContext = vm;
                vm.window = window;
                window.ShowDialog();
                if (vm.selectedRow == null)
                    return;

                _matrc = Facade.matrcBL().GetSingle(vm.selectedRow.rcno);
                RaisePropertyChangedEvent(nameof(matrc));
                ReloadPage(_matrc.rcno);
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PrintReport()
        {
            try
            {
                if (_matrc == null)
                    throw new ArgumentException("โปรดกดปุ่ม Call Data เพื่อระบุเอกสารที่ต้องการจะพิมพ์");

                if (_matrc.finishtime == null)
                    throw new ArgumentException("โปรด finish เอกสารก่อนพิมพ์");

                Facade.RegradeReportBL().RegradeReport(_matrc.rcno, _matis.isno);
                MessageBoxHelper.Info("พิมพ์รายงานสำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
