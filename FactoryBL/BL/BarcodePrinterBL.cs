﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IBarcodePrinterBL
    {
        void Add(string printerName, string createUser);
        void Update(int printerID, string printerName, string modifiedUser);
        void Delete(int printerID);
        BarcodePrinter GetSingle(int printerID);
        List<BarcodePrinter> GetAll();
    }

    public class BarcodePrinterBL : IBarcodePrinterBL
    {
        StecDBMSUnitOfWork uow;
        public BarcodePrinterBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public void Add(string printerName, string createUser)
        {
            try
            {
                if (string.IsNullOrEmpty(printerName))
                    throw new ArgumentException("โปรดระบุชื่อเครื่องพิมพ์ (Printer name cannot be empty.)");

                if (string.IsNullOrEmpty(createUser))
                    throw new ArgumentException("โปรดระบุชื่อผู้ใช้ (Create user cannot be empty.)");

                if (uow.barcodePrinterRepo.Get(x => x.PrinterName.Contains(printerName)).Count() > 0)
                    throw new ArgumentException("มีชื่อเครื่องพิมพ์นี้แล้วในระบบ (Dupplicate printer name.)");

                uow.barcodePrinterRepo.Add(new BarcodePrinter
                {
                    PrinterName = printerName,
                    CreateBy = createUser,
                    CreateDate = DateTime.Now,
                    ModifiedBy = createUser,
                    ModifiedDate = DateTime.Now
                });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(int printerID)
        {
            try
            {
                var model = GetSingle(printerID);
                if (model == null)
                    throw new ArgumentException("ไม่พบเครื่องพิมพ์นี้ในระบบ (Printer not found.)");

                uow.barcodePrinterRepo.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BarcodePrinter> GetAll()
        {
            return uow.barcodePrinterRepo.Get();
        }

        public BarcodePrinter GetSingle(int printerID)
        {
            return uow.barcodePrinterRepo.GetSingle(x => x.PrinterID == printerID);
        }

        public void Update(int printerID, string printerName, string modifiedUser)
        {
            try
            {
                if (string.IsNullOrEmpty(printerName))
                    throw new ArgumentException("โปรดระบุชื่อเครื่องพิมพ์ (Printer name cannot be empty.)");

                if (string.IsNullOrEmpty(modifiedUser))
                    throw new ArgumentException("โปรดระบุชื่อผู้ใช้ (Modified user cannot be empty.)");

                var model = GetSingle(printerID);
                if (model == null)
                    throw new ArgumentException("ไม่พบเครื่องพิมพ์นี้ในระบบ (Printer not found.)");

                if(model.PrinterName == printerName)
                    throw new ArgumentException("ชื่อเครื่องพิมพ์นี้ซ้ำกับที่มีในระบบ");

                model.PrinterName = printerName;
                model.ModifiedBy = modifiedUser;
                model.ModifiedDate = DateTime.Now;

                uow.barcodePrinterRepo.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
