﻿using FactoryBL.Helper;
using FactoryDAL.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.Report
{
    public interface IRegradeReportBL
    {
        void RegradeReport(string rcno, string isno);
    }

    public class RegradeReportBL : IRegradeReportBL
    {
        StecDBMSUnitOfWork uow;

        public RegradeReportBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public void RegradeReport(string rcno, string isno)
        {
            try
            {
                if (string.IsNullOrEmpty(rcno))
                    throw new ArgumentException("โปรดระบุ rcno");

                if (string.IsNullOrEmpty(isno))
                    throw new ArgumentException("โปรดระบุ isno");

                var matrc = Facade.matrcBL().GetSingle(rcno);
                if (matrc == null)
                    throw new ArgumentException("ไม่พบข้อมูล matrc ในระบบ");

                var matis = Facade.matisBL().GetByRgno(matrc.rgno);
                if (matis == null)
                    throw new ArgumentException("ไม่พบข้อมูล matis ในระบบ");

                if (matrc.finishtime == null)
                    throw new ArgumentException("โปรด finish เอกสารก่อนพิมพ์");

                var inputList = Facade.matBL().GetByIsno(isno);
                var outputList = Facade.matBL().GetByRcno(rcno);

                h_Excel.Open(@"C:\Program Files\Common Files\RgHsReport.xlt");
                h_Excel.PutDataToCell("C3", "Regrade Report");
                h_Excel.PutDataToCell("J3", matrc.rgno);
                h_Excel.PutDataToCell("L3", "RC-" + rcno);
                h_Excel.PutDataToCell("L4", "IS-" + isno);

                h_Excel.PutDataToCell("D6", ": " + ((DateTime)matrc.date).ToString("dd-MM-yyyy"));
                h_Excel.PutDataToCell("D7", ": " + matrc.type);
                h_Excel.PutDataToCell("D8", ": " + matrc.place);
                h_Excel.PutDataToCell("D9", ": " + matrc.classifier);
                h_Excel.PutDataToCell("D10", ": " + matrc.remark);

                h_Excel.PutDataToCell("K6", outputList.Count().ToString("N0"));
                h_Excel.PutDataToCell("K7", ((decimal)outputList.Sum(x => x.weight)).ToString("N1"));
                h_Excel.PutDataToCell("K8", ((DateTime)matrc.starttime).ToLongTimeString());
                h_Excel.PutDataToCell("K9", ((DateTime)matrc.finishtime).ToLongTimeString());

                var row = 14;
                var i = 1;
                foreach (var item in inputList)
                {
                    h_Excel.PutDataToCell("B" + row, i.ToString());
                    h_Excel.PutDataToCell("C" + row, item.bc);
                    h_Excel.PutDataToCell("D" + row, item.classify);
                    h_Excel.PutDataToCell("E" + row, ((decimal)(item.company == "1" ? item.weightbuy : item.weightbuy)).ToString("N1"));
                    h_Excel.PutDataToCell("F" + row, ((decimal)item.price).ToString("N1"));
                    i++;
                    row++;
                }

                row = 14;
                i = 1;
                foreach (var item in outputList)
                {
                    h_Excel.PutDataToCell("I" + row, i.ToString());
                    h_Excel.PutDataToCell("J" + row, item.bc);
                    h_Excel.PutDataToCell("K" + row, item.baleno.ToString());
                    h_Excel.PutDataToCell("L" + row, item.classify);
                    h_Excel.PutDataToCell("M" + row, ((decimal)item.weight).ToString("N1"));
                    h_Excel.PutDataToCell("N" + row, ((decimal)item.price).ToString("N1"));
                    i++;
                    row++;
                }

                var chooseList = inputList.Count() > outputList.Count() ? inputList : outputList;
                var endOfRow = 14 + chooseList.Count();
                h_Excel.DeleteRow("A" + endOfRow, "A1000");
                h_Excel.Print(1);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
