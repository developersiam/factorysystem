﻿using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactorySystemWPF.Model
{
    public static class user_setting
    {
        public static security security { get; set; }
        public static string machine { get; set; }
        public static int currentCrop { get { return DateTime.Now.Year; } set { currentCrop = value; } }
    }
}
