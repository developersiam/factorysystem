﻿using FactorySystemWPF.MVVM;
using FactorySystemWPF.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FactorySystemWPF.View
{
    /// <summary>
    /// Interaction logic for Login.xaml
    /// </summary>
    public partial class Login : Page
    {
        public Login()
        {
            InitializeComponent();

            var vm = new vm_Login();
            vm.LoginPage = this;
            DataContext = vm;
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
            UsernameTextBox.Focus();
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_Login)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.Username):
                    UsernameTextBox.Focus();
                    UsernameTextBox.SelectAll();
                    break;
                case nameof(vm.Password):
                    PasswordBox.Focus();
                    PasswordBox.SelectAll();
                    break;
            }
        }
    }
}
