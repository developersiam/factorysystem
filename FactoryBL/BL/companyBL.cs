﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IcompanyBL
    {
        List<company> GetAll();
    }
    public class companyBL : IcompanyBL
    {
        IStecDBMSUnitOfWork uow;
        public companyBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public List<company> GetAll()
        {
            return uow.companyRepo.Get();
        }
    }
}
