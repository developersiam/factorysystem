﻿using FactoryBL;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.Model;
using FactorySystemWPF.MVVM;
using FactorySystemWPF.View.GreenLeaf.Regrade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FactorySystemWPF.ViewModel.GreenLeaf.Regrade
{
    public class vm_New : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }
        public vm_New()
        {
            try
            {
                _crop = DateTime.Now.Year;
                _typeList = Facade.typeBL().GetAll();
                _classifierList = Facade.expertBL().GetAll();
                _placeList = Facade.matwhBL().GetAll();

                RaisePropertyChangedEvent(nameof(crop));
                RaisePropertyChangedEvent(nameof(TypeList));
                RaisePropertyChangedEvent(nameof(classifierList));
                RaisePropertyChangedEvent(nameof(placeList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }


        #region Properties
        private int _crop;

        public int crop
        {
            get { return _crop; }
            set { _crop = value; }
        }

        private string _type;

        public string type
        {
            get { return _type; }
            set { _type = value; }
        }

        private string _comapny;

        public string company
        {
            get { return _comapny; }
            set { _comapny = value; }
        }

        private string _place;

        public string place
        {
            get { return _place; }
            set { _place = value; }
        }

        private string _classifier;

        public string classifier
        {
            get { return _classifier; }
            set { _classifier = value; }
        }

        private string _remark;

        public string remark
        {
            get { return _remark; }
            set { _remark = value; }
        }

        private New _window;

        public New Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private matrc _matrc;

        public matrc matrc
        {
            get { return _matrc; }
            set { _matrc = value; }
        }

        #endregion



        #region List
        private List<type> _typeList;

        public List<type> TypeList
        {
            get { return _typeList; }
            set { _typeList = value; }
        }

        private List<matwh> _placeList;

        public List<matwh> placeList
        {
            get { return _placeList; }
            set { _placeList = value; }
        }

        private List<expert> _classifierList;

        public List<expert> classifierList
        {
            get { return _classifierList; }
            set { _classifierList = value; }
        }
        #endregion



        #region Command
        private ICommand _saveCommand;

        public ICommand SaveCommand
        {
            get { return _saveCommand ?? (_saveCommand = new RelayCommand(Save)); }
            set { _saveCommand = value; }
        }

        private void Save(object obj)
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการสร้างเอกสาร regrade ใหม่ใช่หรือไม่?") 
                    == System.Windows.MessageBoxResult.No)
                    return;

                if (string.IsNullOrEmpty(_type))
                {
                    MessageBoxHelper.Warning("โปรดระบุ type");
                    OnFocusRequested(nameof(type));
                    return;
                }

                if (string.IsNullOrEmpty(_place))
                {
                    MessageBoxHelper.Warning("โปรดระบุ place");
                    OnFocusRequested(nameof(place));
                    return;
                }

                if (string.IsNullOrEmpty(_classifier))
                {
                    MessageBoxHelper.Warning("โปรดระบุ classifier");
                    OnFocusRequested(nameof(classifier));
                    return;
                }

                if (string.IsNullOrEmpty(_remark))
                {
                    MessageBoxHelper.Warning("โปรดระบุ remark");
                    OnFocusRequested(nameof(remark));
                    return;
                }

                Save();

                if (_matrc == null)
                    throw new ArgumentException("matrc not found.");

                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion


        #region Function
        private void Save()
        {
            try
            {
                var rcno = Facade.matrcnoBL().Add(_crop, user_setting.machine, user_setting.security.uname);
                var rgno = Facade.matrgnoBL().Add(_crop, user_setting.machine, user_setting.security.uname);
                var isno = Facade.matisnoBL().Add(_crop, user_setting.machine, user_setting.security.uname);
                Facade.matrcBL().Add(rgno, null, null, rcno, _crop, _type, DateTime.Now,
                    "Regrade", _place, _classifier,_remark, user_setting.security.uname,"","");
                Facade.matisBL().Add(rgno, null, null, isno, _crop, _type, DateTime.Now,
                    _place, "Regrade",_remark, user_setting.security.uname);

                _matrc = Facade.matrcBL().GetSingle(rcno);
                RaisePropertyChangedEvent(nameof(matrc));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
