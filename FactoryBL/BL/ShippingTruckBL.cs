﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Diagnostics;

namespace FactoryBL.BL
{
    public interface IShippingTruckBL
    {
        void Add(string truckNo, string createBy);
        void Edit(string truckID, string truckNo, string modifiedBy);
        void ChangeStatus(string truckID, bool status, string modifiedBy);
        void Delete(string truckID);
        ShippingTruck GetSingle(string truckID);
        ShippingTruck GetSingleByTruckNo(string truckNo);
        List<ShippingTruck> GetAll();
    }

    public class ShippingTruckBL : IShippingTruckBL
    {
        IStecDBMSUnitOfWork uow;
        public ShippingTruckBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public void Add(string truckNo, string createBy)
        {
            try
            {
                if (string.IsNullOrEmpty(truckNo))
                    throw new Exception("Truck No cannot be empty.");

                if (string.IsNullOrEmpty(truckNo))
                    throw new Exception("Create By cannot be empty. Please login.");

                if (GetSingleByTruckNo(truckNo) != null)
                    throw new Exception("มีทะเบียนรถนี้แล้วในระบบ");

                var list = GetAll()
                    .Select(x => new
                    {
                        ID = Convert.ToInt16(x.TruckID.Replace('T', ' '))
                    })
                    .ToList();
                var max = list.Count() > 0 ? list.Max(x => x.ID) + 1 : 0;
                var truckID = "T" + max.ToString().PadLeft(3, '0');

                uow.ShippingTruckRepo
                    .Add(new ShippingTruck
                    {
                        TruckID = truckID,
                        TruckNo = truckNo,
                        Driver = "",
                        Status = true,
                        CreateBy = createBy,
                        CreateDate = DateTime.Now,
                        ModifiedBy = createBy,
                        ModifiedDate = DateTime.Now
                    });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ChangeStatus(string truckID, bool status, string modifiedBy)
        {
            try
            {
                if (string.IsNullOrEmpty(truckID))
                    throw new Exception("Truck ID cannot be empty.");

                if (string.IsNullOrEmpty(modifiedBy))
                    throw new Exception("Modified by cannot be empty. Please login.");

                var model = GetSingle(truckID);
                if (model == null)
                    throw new Exception("ไม่พบข้อมูลนี้ในระบบ");

                model.Status = status;
                model.ModifiedBy = modifiedBy;
                model.ModifiedDate = DateTime.Now;
                uow.ShippingTruckRepo.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string truckID)
        {
            try
            {
                if (string.IsNullOrEmpty(truckID))
                    throw new Exception("Truck ID cannot be empty.");

                var model = GetSingle(truckID);
                if (model == null)
                    throw new Exception("ไม่พบข้อมูลนี้ในระบบ");

                uow.ShippingTruckRepo.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(string truckID, string truckNo, string modifiedBy)
        {
            try
            {
                if (string.IsNullOrEmpty(truckID))
                    throw new Exception("Truck ID cannot be empty.");

                var model = GetSingle(truckID);
                if (model == null)
                    throw new Exception("ไม่พบข้อมูลนี้ในระบบ");

                if (uow.ShippingTruckRepo
                    .Get(x => x.TruckNo == truckNo)
                    .Count() > 0)
                    throw new Exception("มี truck no นี้แล้วในระบบ");

                model.TruckNo = truckNo;
                model.ModifiedBy = modifiedBy;
                model.ModifiedDate = DateTime.Now;
                uow.ShippingTruckRepo.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ShippingTruck> GetAll()
        {
            return uow.ShippingTruckRepo.Get();
        }

        public ShippingTruck GetSingle(string truckID)
        {
            return uow.ShippingTruckRepo.GetSingle(x => x.TruckID == truckID);
        }

        public ShippingTruck GetSingleByTruckNo(string truckNo)
        {
            return uow.ShippingTruckRepo.GetSingle(x => x.TruckNo == truckNo);
        }
    }
}
