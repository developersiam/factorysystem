﻿using FactoryDAL.StoreProcedure;
using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface ImatrcBL
    {
        void Add(string rgno, string hsno, string tfno, string rcno,
            int crop, string type, DateTime date, string rcfrom,
            string place, string classifier, string remark, string user,string truckno, string buyer);

        void FinishRegrade(string rcno, string checker);

        matrc GetSingle(string rcno);

        List<matrc> GetByCrop(int crop);

        List<matrc> GetByType(int crop, string type);

        void FinishPicking(string rcno, string user);

        void UserLockedPicking(string rcno);

        void DeleteByRcno(string rcno);

        void UpdateMatrcPicking(string rcno, string classifier, string remark, string receivedUser, string type);

        void UpdateRegradeRemark(string rgno, string remark, string editUser);
    }

    public class matrcBL : ImatrcBL
    {
        IStecDBMSUnitOfWork uow;
        public matrcBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public void Add(string rgno, string hsno, string tfno, string rcno,
            int crop, string type, DateTime date, string rcfrom,
            string place, string classifier, string remark, string user,string trucker, string buyer)
        {
            try
            {
                if (string.IsNullOrEmpty(rcfrom))
                    throw new ArgumentException("rcfrom cannot be empty.");

                if (string.IsNullOrEmpty(rgno) && rcfrom == "Regrade")
                    throw new ArgumentException("rgno cannot be empty.");

                if (string.IsNullOrEmpty(hsno) && rcfrom == "Handstrip")
                    throw new ArgumentException("rgno cannot be empty.");

                if (string.IsNullOrEmpty(tfno) && rcfrom == "Transfer")
                    throw new ArgumentException("rgno cannot be empty.");

                if (string.IsNullOrEmpty(type))
                    throw new ArgumentException("type cannot be empty.");

                if (string.IsNullOrEmpty(place))
                    throw new ArgumentException("place cannot be empty.");

                if (string.IsNullOrEmpty(classifier))
                    throw new ArgumentException("classifier cannot be empty.");

                if (string.IsNullOrEmpty(user))
                    throw new ArgumentException("user cannot be empty.");

                if (crop != DateTime.Now.Year)
                    throw new ArgumentException("crop ที่ระบุไม่ตรงกับ crop ในปฏิทินของเครื่อง");

                /// insert into matrc(rgno,rcno,crop,type,date,rcfrom,place,
                /// classifier,remark,starttime,dtrecord,[user]) 
                /// values(?,?,?,?,?,?,?,?,?,?,?,?)
                /// 
                uow.matrcRepo.Add(new matrc
                {
                    rcno = rcno,
                    rgno = rgno,
                    hsno = hsno,
                    tfno = tfno,
                    crop = crop,
                    type = type,
                    date = date,
                    rcfrom = rcfrom,
                    place = place,
                    classifier = classifier,
                    remark = remark,
                    starttime = DateTime.Now,
                    dtrecord = DateTime.Now,
                    user = user,
                    userlocked = false,
                    checkerapproved = false,
                    checkerlocked = false,
                    checkerlockedtimes = 0,
                    leaflocked = false,
                    xlocked  = false,
                    xlockedtimes = 0,
                    CancleRcNoStatus = false,
                    truckno = trucker,
                    buyer = buyer
                });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void FinishRegrade(string rcno, string checker)
        {
            try
            {
                if (string.IsNullOrEmpty(rcno))
                    throw new ArgumentException("rcno cannot be empty.");

                if (string.IsNullOrEmpty(checker))
                    throw new ArgumentException("checker cannot be empty.");

                var matrc = uow.matrcRepo.GetSingle(x=>x.rcno == rcno);
                if (matrc == null)
                    throw new ArgumentException("ไม่พบ rcno " + rcno + " ในระบบ");

                matrc.userlocked = true;
                matrc.finishtime = DateTime.Now;

                matrc.checker = checker;
                matrc.checkerapproved = true;
                matrc.checkerapproveddate = DateTime.Now;
                matrc.checkerapprovedtime = DateTime.Now;
                matrc.checkerlocked = true;
                matrc.checkerlockedtimes = matrc.checkerlockedtimes + 1;

                var matList = uow.matRepo.Get(x => x.rcno == rcno);
                matList.ForEach(x => x.locked = true);
                uow.Save();

                /*
                 *  BEGIN TRANSACTION
		                UPDATE	StecDBMS.dbo.matrc
		                SET		finishtime		= GETDATE()
				                ,userlocked		= 1
		                WHERE	rcno			= @rcno
    
		                UPDATE	StecDBMS.dbo.mat
		                SET		locked			= @locked
		                WHERE	rcno			= @rcno
                    COMMIT TRANSACTION



                 *  SET NOCOUNT ON;
	                DECLARE  @rcno1    NVARCHAR(50) 
	                SET  @rcno1 = ''
	                SET  @rcno1 = (SELECT rcno FROM matrc WHERE rcfrom IN( 'Blending','Free')  AND  rcno = @rcno)

                    -- Insert statements for procedure here
                    BEGIN TRANSACTION
		                UPDATE StecDBMS.dbo.matrc
		                SET  
			                [checker]				= @checker,
			                [checkerapproved]		= @checkerapproved,
			                [checkerapproveddate]	= GETDATE(),
			                [checkerapprovedtime]	= GETDATE(),
			                [checkerlocked]			= @checkerlocked,
			                [checkerlockedtimes]	= [checkerlockedtimes]+1
		                WHERE rcno = @rcno
	
		                IF  @rcno1 <>  '' 
			                BEGIN
				                UPDATE	StecDBMS.dbo.mat
				                SET		leaflocked		= 1
				                WHERE	rcno			= @rcno 
			                END
                    COMMIT TRANSACTION
                 */
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<matrc> GetByCrop(int crop)
        {
            return uow.matrcRepo.Get(x => x.crop == crop);
        }

        public List<matrc> GetByType(int crop, string type)
        {
            return uow.matrcRepo.Get(x => x.crop == crop && x.type == type);
        }

        public matrc GetSingle(string rcno)
        {
            return uow.matrcRepo.GetSingle(x => x.rcno == rcno);
        }

        public void FinishPicking(string rcno, string user)
        {
            try
            {
                if (string.IsNullOrEmpty(rcno))
                    throw new ArgumentException("Rcno cannot be empty.");

                var matrc = uow.matrcRepo.GetSingle(x => x.rcno == rcno);
                if (matrc == null)
                    throw new ArgumentException("ไม่พบ rcno " + rcno + " ในระบบ");

                matrc.finishtime = DateTime.Now;
                uow.Save();            
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteByRcno(string rcno)
        {
            var matrc = uow.matrcRepo.GetSingle(x => x.rcno == rcno);
            if (matrc == null)
                throw new ArgumentException("ไม่พบ rcno " + rcno + " ในระบบ");

            uow.matrcRepo.Remove(matrc);
            uow.Save();

        }

        public void UserLockedPicking(string rcno)
        {
            try
            {
                if (string.IsNullOrEmpty(rcno))
                    throw new ArgumentException("Rcno cannot be empty.");

                var matrc = uow.matrcRepo.GetSingle(x => x.rcno == rcno);
                if (matrc == null)
                    throw new ArgumentException("ไม่พบ Rcno " + rcno + " ในระบบ");

                matrc.userlocked = true;
                matrc.userlockedtime = DateTime.Now;
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateMatrcPicking(string rcno,string classifier, string remark, string receivedUser, string type)
        {
            if (string.IsNullOrEmpty(rcno)) throw new ArgumentException("Rcno cannot be empty.");

            var matrc = uow.matrcRepo.GetSingle(x => x.rcno == rcno);
            if (matrc == null)
                throw new ArgumentException("ไม่พบ Rcno " + rcno + " ในระบบ");

            matrc.classifier = classifier;
            matrc.date = DateTime.Now;
            matrc.remark = remark;
            matrc.starttime = DateTime.Now;
            matrc.machine = receivedUser;
            matrc.dtrecord = DateTime.Now;
            matrc.user = receivedUser;
            matrc.type = type;
            matrc.userlocked = false;

            uow.matrcRepo.Update(matrc);
            uow.Save();


        }

        public void UpdateRegradeRemark(string rgno, string remark, string editUser)
        {
            try
            {
                var model = uow.matrcRepo.GetSingle(x => x.rgno == rgno);
                if (model == null)
                    throw new ArgumentException("ไม่พบ matrc นี้ในระบบ");

                model.remark = remark;
                model.user = editUser;
                model.dtrecord = DateTime.Now;

                uow.matrcRepo.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
