﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IpickingBL
    {
        picking GetSingle(string pickingGrade, string pType);
        void UpdatePickingPrice(string pickingGrade, string pickingType,decimal pickingPrice, string user);
        List<picking> GetbyType(string type);

    }
    public class pickingBL : IpickingBL
    {
        StecDBMSUnitOfWork uow;
        public pickingBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public picking GetSingle(string pickingGrade, string pType)
        {
            return uow.pickingRepo.GetSingle(x => x.classify == pickingGrade && x.type == pType);
        }

        public void UpdatePickingPrice(string pGrade, string pType, decimal pPrice, string user)
        {
            try 
            {
                var expicking = Facade.pickingBL().GetSingle(pGrade,pType);
                if (expicking == null)
                    throw new ArgumentException("Picking grade not found");

                expicking.price = pPrice;
                expicking.user = user;
                expicking.dtrecord = DateTime.Now;

                uow.pickingRepo.Update(expicking);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        public List<picking> GetbyType(string t)
        {
            return uow.pickingRepo.Get(x => x.type == t).ToList();
        }
    }
}
