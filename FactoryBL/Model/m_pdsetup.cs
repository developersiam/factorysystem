﻿using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.Model
{
    public class m_pdsetup : pdsetup
    {
        public string customer { get; set; }
        public string type { get; set; }
        public string form { get; set; }
        public decimal netdef { get; set; }
        public decimal taredef { get; set; }
        public decimal grossdef { get; set; }
        public string packingmat { get; set; }
    }
}
