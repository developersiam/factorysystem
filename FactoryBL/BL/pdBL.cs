﻿using FactoryBL.Helper;
using FactoryDAL.StoreProcedure;
using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Printing;
using System.Text;
using System.Threading.Tasks;
using System.Management;
using FactoryBL.Model;

namespace FactoryBL.BL
{
    public interface IpdBL
    {
        void FeedIn(string bc, DateTime issueddate, DateTime issuedtime, string topdno, int topdhour,
            DateTime feedingdate, DateTime feedingtime, string repackeduser, DateTime topddate,
            string toprgrade);
        pd AddPacked(string bc, int caseno, decimal grossreal, string thr, string rdy, string pend,
            decimal boxtare, bool box, string pdtype, string pdno, int pdhour, string packinguser, bool checkNetReal);
        void ReWeightPacked(string bc, decimal grossreal, bool box, decimal boxtare,
            bool checkNetReal, string packinguser);
        void PrintSTECBarcode(pd pd, string printerName, short copy);
        void PrintCustomerBarcode(PDCustomerPackedPrint pdCusPackedPrint,
            pd pd, string printerName,
            short copy);
        void PrintBarcodeWithBartender(pd pd);
        List<pd> GetByToPdno(string topdno);
        List<pd> GetByToPdHour(string topdno, int topdhour);
        List<pd> GetByToPackedGrade(string toprgrade);
        List<pd> GetByFromPdno(string frompdno);
        List<pd> GetByFromPackedGrade(string fromprgrade);
        List<pd> GetByFromPdHour(string frompdno, int frompdhour);
        List<pd> GetByGrade(string grade);
        List<pd> GetByMovementNo(string movementNo);
        List<sp_PD_SEL_PackedCasesByGrade_Result> GetByGradeSP(string grade);
        pd GetSingle(string bc);
        string GetNewPackedBarcode(string pdtype, short crop, string form);
        int GetNewPackedCaseNo(string packedgrade);
        string GetNewByProductBarcode(string byproductType, short crop);
        int GetNewByProductCaseNo(string packedgrade);
        void AddByProduct(string bc, int caseno, decimal grossreal, string pend, string machine,
            decimal boxtare, bool box, string byProductGrade, bool checkNetReal,
            string printerName, short copy, string pdno, string packinguser);
        void ReWeightByProduct(string bc, decimal grossreal, bool box, decimal boxtare,
            bool checkNetReal, string packinguser);
    }

    /*
     UPDATE pd SET issued = 1, issuedto = 'Blending', issueddate = ?, issuedtime = ?, topdno = ?, topdhour = ?, feedingdate = ?, feedingtime = ?, repackeduser = ?, topddate = ?, toprgrade = ? WHERE (bc = ?)
         */
    public class pdBL : IpdBL
    {
        string cusLableTemplate = @"\\192.168.0.221\Log_setting\program_2015\processing\new version\Packing\CustomerFormat\";
        StecDBMSUnitOfWork uow;
        public pdBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public void FeedIn(string bc, DateTime issueddate, DateTime issuedtime, string topdno, int topdhour,
            DateTime feedingdate, DateTime feedingtime, string repackeduser, DateTime topddate, string toprgrade)
        {
            try
            {
                ///เงื่อนไขการนำยา green เข้า production 
                ///1.blendinglocked = false
                ///2.packinglocked = false
                ///3.bledingstatus = false
                ///4.is in pd
                ///5.issued = true
                ///6.blendinghour = packinghour
                ///7.pd type = packedgrade type (RYO,CTB,DIET can difference)
                ///8.wh = STEC or Rodchana
                ///9.movement = received
                ///

                if (string.IsNullOrEmpty(bc))
                    throw new ArgumentException("โปรดระบุเลขบาร์โค้ต");

                if (string.IsNullOrEmpty(topdno))
                    throw new ArgumentException("โปรดเลือก pdno ที่จะทำการบันทึกข้อมูล");

                var pd = GetSingle(bc);
                if (pd == null)
                    throw new ArgumentException("ไม่พบข้อมูลบาร์โค้ตยากล่อง รหัส " + bc + " นี้ในระบบ");

                if (pd.issued == true)
                    throw new ArgumentException("บาร์โค้ตนี้ถูกใช้ไปแล้วในการทำ " + pd.issuedto);

                if (pd.wh != "STEC" && pd.wh != "Rodchana")
                    throw new ArgumentException("Location ของยาห่อนี้ ไม่ได้อยู่ที่ STEC หรือ Rodchana" +
                        "กรุณาทำ Movement เพื่อย้ายกล่องยามายัง STEC หรือ Rodchana ก่อน");

                var movement = Facade.ShippingMovementBL().GetSingle(pd.MovementNo);
                if (movement != null)
                    if (movement.DateReceived == null)
                        throw new ArgumentException("Movement No. " + movement.MovementNo +
                            " ของยากล่องนี้ยังไม่ถูกระบุวันที่รับ โปรดตรวจสอบ");

                ///ตรวจสอบว่า กรณีห่อยาที่พึ่งอบเสร็จใหม่ๆ และยังไม่ได้ lock pdsetup ให้ครบถ้วน จะไม่ให้นำเข้าไปทำ repacked
                ///เนื่องจากกล่องยาจะต้องถูกนำเข้าโปรแกรม pricing เพื่อคำนวณมูลค่าของยาก่อน
                var packedStockpdsetup = Facade.pdsetupBL().GetSingle(pd.frompdno);
                if (packedStockpdsetup == null)
                    throw new ArgumentException("ไม่พบ pdsetup ของป้ายบาร์โค้ตนี้");

                if (packedStockpdsetup.packinglocked == false ||
                    packedStockpdsetup.blendinglocked == false ||
                    packedStockpdsetup.pickinglocked == false ||
                    packedStockpdsetup.byproductlocked == false)
                    throw new ArgumentException("ไม่อนุญาตให้นำป้ายบาร์โค้ตของ run ที่ยังอบไม่เสร็จมาทำการ feeding เข้าระบบ" +
                        " โปรดตรวจสอบสถานะ packinglocked,blendinglocked,pickinglocked หรือ byproductlocked " +
                        " ของ pdno " + packedStockpdsetup.pdno +
                        " วันที่ " + ((DateTime)packedStockpdsetup.date).ToString("dd/MM/yyyy") +
                        " กับแผนก Green Leaf Account");

                var pdsetup = Facade.pdsetupBL().GetSingle(topdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (pdsetup.blendinglocked == true)
                    throw new ArgumentException("หน้าเบลนได้ lock pdno นี้แล้ว ติดต่อหัวหน้างานหากต้องการปลดล็อค");

                if (pdsetup.packinglocked == true)
                    throw new ArgumentException("ท้ายเครื่องได้ lock pdno นี้แล้ว ติดต่อหัวหน้างานหากต้องการปลดล็อค");

                var blendingstatus = Facade.blendingstatusBL().GetSingle(topdno, topdhour);
                if (blendingstatus == null)
                    throw new ArgumentException("ไม่พบชั่วโมงการทำงาน " + topdhour + " ที่หน้าเบลน โปรดตรวจสอบ");

                if (blendingstatus.locked == true)
                    throw new ArgumentException("ชั่วโมงที่ " + topdhour + " ถูก finish แล้ว ไม่สามารถบันทึกข้อมูลได้");

                var packingstatus = Facade.packingstatusBL().GetSingle(topdno, topdhour);
                if (packingstatus == null)
                    throw new ArgumentException("ไม่พบชั่วโมงการทำงาน " + topdhour + " ที่ท้ายเครื่อง โปรดตรวจสอบ");

                if (blendingstatus.blendinghour != packingstatus.packinghour)
                    throw new ArgumentException("ชั่วโมงการทำงานที่ท้ายเครื่องไม่ตรงกับชั่วโมงการทำงานปัจจุบันที่หน้าเบลน โปรดตรวจสอบ" + Environment.NewLine +
                        "ชั่วโมงหน้าเบลน : " + blendingstatus.blendinghour + Environment.NewLine +
                        "ชั่วโมงท้ายเครื่อง : " + packingstatus.packinghour);

                var packedgrade = Facade.packedgradeBL().GetSingle(toprgrade);
                if (packedgrade == null)
                    throw new ArgumentException("ไม่พบ packedgrade ในระบบ");

                if (!packedgrade.form.Contains("RYO") &&
                    !packedgrade.form.Contains("CTB") &&
                    !packedgrade.form.Contains("DIET"))
                    if (packedgrade.type != pd.type)
                        throw new ArgumentException("ข้อมูล type ไม่ตรงกันไม่สามารถบันทึกข้อมูลได้ " + Environment.NewLine +
                            "(อนุญาตให้ต่าง type ได้เฉพาะ RYO, CTB และ DIET เท่านั้น)");

                pd.issued = true;
                pd.issuedto = "Blending";
                pd.issueddate = issueddate.Date;
                pd.issuedtime = issuedtime;
                pd.topdno = topdno;
                pd.topdhour = topdhour;
                pd.feedingdate = feedingdate.Date;
                pd.feedingtime = feedingtime;
                pd.repackeduser = repackeduser;
                pd.topddate = topddate;
                pd.toprgrade = toprgrade;

                uow.pdRepo.Update(pd);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd> GetByFromPackedGrade(string fromprgrade)
        {
            return uow.pdRepo.Get(x => x.fromprgrade == fromprgrade);
        }

        public List<pd> GetByFromPdHour(string frompdno, int frompdhour)
        {
            return uow.pdRepo.Get(x => x.frompdno == frompdno && x.frompdhour == frompdhour);
        }

        public List<pd> GetByFromPdno(string frompdno)
        {
            return uow.pdRepo.Get(x => x.frompdno == frompdno);
        }

        public List<pd> GetByGrade(string grade)
        {
            return uow.pdRepo.Get(x => x.grade == grade);
        }

        public List<sp_PD_SEL_PackedCasesByGrade_Result> GetByGradeSP(string grade)
        {
            return StoreProcedureRepository.sp_PD_SEL_PackedCasesByGrade(grade);
        }

        public List<pd> GetByToPackedGrade(string toprgrade)
        {
            return uow.pdRepo.Get(x => x.toprgrade == toprgrade);
        }

        public List<pd> GetByToPdno(string topdno)
        {
            return uow.pdRepo.Get(x => x.topdno == topdno);
        }

        public List<pd> GetByToPdHour(string topdno, int topdhour)
        {
            return uow.pdRepo.Get(x => x.topdno == topdno && x.topdhour == topdhour);
        }

        public pd GetSingle(string bc)
        {
            return uow.pdRepo.GetSingle(x => x.bc == bc);
        }

        public pd AddPacked(string bc, int caseno, decimal grossreal, string thr, string rdy, string pend,
            decimal boxtare, bool box, string pdtype, string pdno, int pdhour, string packinguser, bool checkNetReal)
        {
            try
            {
                if (string.IsNullOrEmpty(bc))
                    throw new ArgumentException("โปรดระบุ barcode");

                if (string.IsNullOrEmpty(thr))
                    throw new ArgumentException("โปรดระบุ Thresher");

                if (string.IsNullOrEmpty(rdy))
                    throw new ArgumentException("โปรดระบุ Redryer");

                if (string.IsNullOrEmpty(pend))
                    throw new ArgumentException("โปรดระบุ Packing End");

                if (string.IsNullOrEmpty(pdtype))
                    throw new ArgumentException("โปรดระบุ pdtype Lamina หรือ Remnant");

                if (pdtype == "Remnant" && caseno != 0)
                    throw new ArgumentException("ถ้าเป็นยา Remnant เลขกล่อง (caseno) จะต้องเป็น 0");

                if (pdtype == "Lamina" && caseno == 0)
                    throw new ArgumentException("ถ้าเป็นยา Lamina เลขกล่อง (caseno) จะต้องไม่เป็น 0");

                if (pdtype == "Lamina" && bc.Substring(0, 2) != "LN")
                    throw new ArgumentException("Prefix ของยา Lamina จะต้องขึ้นต้นด้วย LN เท่านั้น");

                if (pdtype == "Remnant" && bc.Substring(0, 2) != "RN")
                    throw new ArgumentException("Prefix ของยา Remnant จะต้องขึ้นต้นด้วย RN เท่านั้น");

                if (grossreal <= 0)
                    throw new ArgumentException("gross real ต้องมากกว่า 0");

                if (boxtare <= 0)
                    throw new ArgumentException("boxtare ต้องมากกว่า 0");

                var pdsetup = uow.pdsetupRepo.GetSingle(x => x.pdno == pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (pdsetup.packinglocked == true)
                    throw new ArgumentException("packinglocked = 1 (เกรดนี้ถูกล็อคแล้วไม่สามารถบันทึกข้อมูลได้)");

                var packingstatus = uow.packingstatusRepo
                    .GetSingle(x => x.pdno == pdsetup.pdno && x.packinghour == pdhour);
                if (packingstatus == null)
                    throw new ArgumentException("ไม่พบชั่วโมง " + pdhour + " ที่ท้ายเครื่อง");

                if (packingstatus.locked == true)
                    throw new ArgumentException("ชั่วโมงที่ " + packingstatus.packinghour + " ที่ท้ายเครื่องถูกล็อคแล้ว");

                var bledingstatus = uow.blendingstatusRepo
                    .GetSingle(x => x.pdno == pdsetup.pdno &&
                    x.blendinghour == pdhour);
                if (bledingstatus == null)
                    throw new ArgumentException("ไม่พบชั่วโมง " + pdhour + " ที่หน้าเบลน โปรดติดต่อหน้าเบลนหรือหัวหน้างานเพื่อตรวจสอบข้อมูล");

                var packedgrade = uow.packedgradeRepo
                    .GetSingle(x => x.packedgrade1 == pdsetup.packedgrade);
                if (packedgrade == null)
                    throw new ArgumentException("ไม่พบ packedgrade " + pdsetup.packedgrade + " ในระบบ");

                if (boxtare > packedgrade.taredef * 2)
                    throw new ArgumentException("Boxtare ไม่ควรเกิน " +
                        ((decimal)(packedgrade.taredef * 2)).ToString("N2"));

                var netreal = grossreal - boxtare;
                var diffWeight = (decimal)0.3;

                if (netreal <= 0)
                    throw new ArgumentException("น้ำหนักใบยา (netreal) ต้อง 1 กก.ขึ้นไป");

                if (pdtype == "Lamina")
                {
                    if (checkNetReal == true)
                    {
                        if (netreal > packedgrade.netdef + diffWeight)
                            throw new ArgumentException("กรุณาดึงยาออก น้ำหนักใบยาจะต้องไม่เกิน " +
                                ((decimal)(packedgrade.netdef + diffWeight)).ToString("N2") +
                                " โปรดดึงยาออกจำนวน " +
                                ((decimal)Math.Abs((decimal)(packedgrade.netdef + diffWeight - netreal))).ToString("N2") +
                                " กก.");

                        if (netreal < packedgrade.netdef - diffWeight)
                            throw new ArgumentException("กรุณาใส่ยาเพิ่ม น้ำหนักยาจะต้องไม่น้อยกว่า " +
                                ((decimal)(packedgrade.netdef - diffWeight)).ToString("N2") +
                                " โปรดใส่ยาเพิ่มอีก " +
                                ((decimal)Math.Abs((decimal)(packedgrade.netdef + diffWeight - netreal))).ToString("N2") +
                                " กก.");
                    }
                }

                var pdListByGrade = uow.pdRepo.Get(x => x.grade == packedgrade.packedgrade1);
                var pdListByPdno = pdListByGrade.Where(x => x.frompdno == pdsetup.pdno);
                var caseRunno = pdListByPdno.Count() > 0 ? pdListByPdno.Max(x => x.CaseRunno) + 1 : 1;
                var cusRunno = 1;
                if (pdListByPdno.Count() > 0)
                {
                    cusRunno = (int)pdListByPdno.FirstOrDefault().CusRunno;
                }
                else
                {
                    if (pdListByGrade.Count() > 0)
                    {
                        if (pdsetup.mode == "Continue")
                            cusRunno = (int)pdListByGrade.Max(x => x.CusRunno);
                        else
                            cusRunno = (int)pdListByGrade.Max(x => x.CusRunno) + 1;
                    }
                }

                var newpd = new pd
                {
                    crop = pdsetup.crop,
                    type = packedgrade.type,
                    bc = bc,
                    grade = packedgrade.packedgrade1,
                    customer = packedgrade.customer,
                    form = packedgrade.form,
                    packingmat = packedgrade.packingmat,
                    caseno = caseno,
                    grossdef = packedgrade.grossdef,
                    grossreal = grossreal,
                    taredef = packedgrade.taredef,
                    netdef = packedgrade.netdef,
                    netreal = grossreal - boxtare,
                    malcam = (decimal)0.00,
                    rcfrom = "Packing",
                    box = box,
                    pdtype = pdtype,
                    frompddate = pdsetup.date,
                    frompdno = pdsetup.pdno,
                    frompdhour = pdhour,
                    fromprgrade = packedgrade.packedgrade1,
                    wh = "STEC",
                    packingdate = pdsetup.date,
                    packingtime = DateTime.Now,
                    dtrecord = DateTime.Now,
                    packinguser = packinguser,
                    graders = packedgrade.packedgrade1,
                    customerrs = packedgrade.customer,
                    thr = thr,
                    rdy = rdy,
                    pend = pend,
                    boxtare = (double)boxtare,
                    issued = false,
                    CusRunno = cusRunno,
                    CaseRunno = caseRunno,
                    AccuRunno = caseno
                    //pdremark = pdremark,
                    //frompdremark = frompdremark,
                    //pdremarkrs = pdremarkrs,
                    //PdRemarkParent = pdremarkparent
                };

                var pdbc = Facade.pdbcBL().GetSingle((short)pdsetup.crop);
                if (pdbc == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdbc ใน crop " + pdsetup.crop + " โปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                uow.pdRepo.Add(newpd);
                uow.Save();

                pdbc.bc = pdbc.bc + 1;
                uow.pdbcRepo.Update(pdbc);
                uow.Save();

                return newpd;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ReWeightPacked(string bc, decimal grossreal, bool box, decimal boxtare,
            bool checkNetReal, string packinguser)
        {
            try
            {
                var pd = GetSingle(bc);
                if (pd == null)
                    throw new ArgumentException("ไม่พบข้อมูลรหัสบาร์โค้ต " + bc + " ในระบบ");

                var pdsetup = uow.pdsetupRepo
                    .GetSingle(x => x.pdno == pd.frompdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (pdsetup.packinglocked == true)
                    throw new ArgumentException("(packinglocked = 1) ท้ายเครื่องล็อคเกรดแล้วไม่สามารถบันทึกข้อมูลได้");

                var packedgrade = uow.packedgradeRepo
                    .GetSingle(x => x.packedgrade1 == pdsetup.packedgrade);
                if (packedgrade == null)
                    throw new ArgumentException("ไม่พบ packedgrade " + pdsetup.packedgrade + " ในระบบ");

                var packingstatus = uow.packingstatusRepo
                    .GetSingle(x => x.pdno == pdsetup.pdno &&
                    x.packinghour == pd.frompdhour);
                if (packingstatus == null)
                    throw new ArgumentException("ไม่พบชั่วโมง " + pd.frompdhour + " ที่ท้ายเครื่อง");

                if (packingstatus.locked == true)
                    throw new ArgumentException("ชั่วโมงที่ " + packingstatus.packinghour + " ที่ท้ายเครื่องถูกล็อคแล้ว");

                var bledingstatus = uow.blendingstatusRepo
                    .GetSingle(x => x.pdno == pdsetup.pdno &&
                    x.blendinghour == pd.frompdhour);
                if (bledingstatus == null)
                    throw new ArgumentException("ไม่พบชั่วโมง " + pd.frompdhour + " ที่หน้าเบลน โปรดติดต่อหน้าเบลนหรือหัวหน้างานเพื่อตรวจสอบข้อมูล");

                if (boxtare > packedgrade.taredef * 2)
                    throw new ArgumentException("Boxtare ไม่ควรเกิน " +
                        ((decimal)(packedgrade.taredef * 2)).ToString("N2"));

                var netreal = grossreal - boxtare;
                var accept = (decimal)0.3;

                if (netreal <= 0)
                    throw new ArgumentException("น้ำหนักใบยา (netreal) ต้อง 1 กก.ขึ้นไป");

                if (pd.pdtype == "Lamina")
                {
                    if (checkNetReal == true)
                    {
                        if (netreal > packedgrade.netdef + accept)
                            throw new ArgumentException("กรุณาดึงยาออก น้ำหนักใบยาจะต้องไม่เกิน " +
                                ((decimal)(packedgrade.netdef + accept)).ToString("N2") +
                                " โปรดดึงยาออกจำนวน " +
                                ((decimal)Math.Abs((decimal)(packedgrade.netdef + accept - netreal))).ToString("N2") +
                                " กก.");

                        if (netreal < packedgrade.netdef - accept)
                            throw new ArgumentException("กรุณาใส่ยาเพิ่ม น้ำหนักยาจะต้องไม่น้อยกว่า " +
                                ((decimal)(packedgrade.netdef - accept)).ToString("N2") +
                                " โปรดใส่ยาเพิ่มอีก " +
                                ((decimal)Math.Abs((decimal)(packedgrade.netdef + accept - netreal))).ToString("N2") +
                                " กก.");
                    }
                }

                pd.grossreal = grossreal;
                pd.netreal = netreal;
                pd.boxtare = (double)boxtare;
                pd.box = box;
                pd.dtrecord = DateTime.Now;
                pd.packinguser = packinguser;

                uow.pdRepo.Update(pd);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetNewPackedBarcode(string pdtype, short crop, string form)
        {
            try
            {
                var prefix = pdtype == "Lamina" ? "LN" : "RN";
                if (form == "RYO")
                    prefix = prefix + "A";

                var pdbc = Facade.pdbcBL().GetSingle(crop);
                if (pdbc == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdbc ใน crop " + crop + " โปรดติดต่อแผนกไอทีเพื่อทำการตรวจสอบ");

                return prefix + pdbc.crop.ToString().Substring(2, 2) + "-" +
                    (pdbc.bc + 1).ToString().PadLeft(6, '0');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetNewPackedCaseNo(string packedgrade)
        {
            try
            {
                var pdList = uow.pdRepo
                    .Get(x => x.fromprgrade == packedgrade)
                    .Where(x => x.pdtype == "Lamina")
                    .ToList();
                if (pdList.Count() <= 0)
                    return 1;
                else
                    return (int)pdList.Max(x => x.caseno) + 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PrintSTECBarcode(pd pd, string printerName, short copy)
        {
            try
            {
                if (copy == 0)
                    return;

                if (System.Drawing.Printing.PrinterSettings
                    .InstalledPrinters
                    .Cast<string>()
                    .Any(name => printerName
                    .ToUpper()
                    .Trim() == name.ToUpper()
                    .Trim()) == false)
                    throw new ArgumentException("ไม่พบเครื่องพิมพ์ " + printerName +
                        " บนเครื่องของท่าน กรุณาติดตั้งเครื่องพิมพ์และลองใหม่อีกครั้ง");

                if (IsPrinterOffline(printerName))
                    throw new ArgumentException("Printer " + printerName + " is offline.");

                if (pd == null)
                    throw new ArgumentException("pd is null. ไม่พบข้อมูลยากล่องนี้ในระบบ");

                TSCPrinter.openport(printerName);
                TSCPrinter.setup("103", "76", "2.0", "1", "0", "0", "0");
                TSCPrinter.sendcommand("GAP  3 mm,0");
                TSCPrinter.sendcommand("DIRECTION 1");
                TSCPrinter.clearbuffer();

                TSCPrinter.sendcommand("BAR 50,25,4,510"); //เส้นตั้ง หน้าสุด
                TSCPrinter.sendcommand("BAR 820,25,4,510"); //เส้นตั้ง ท้ายสุด
                TSCPrinter.sendcommand("BAR 150,210,4,325"); //เส้นตั้ง หน้า  123
                TSCPrinter.sendcommand("BAR 50,25,770,4"); //เส้นนอน บนสุด

                //เพิ่มเช็ค  len ของ  Grade เพื่อจัดตำแหน่งให้อยู่ตรงกลาง
                var gradeLen = pd.grade.Length;
                switch (gradeLen)
                {
                    case 4:
                        TSCPrinter.windowsfont(300, 50, 140, 0, 2, 0, "arial", pd.grade); //14-A
                        break;
                    case 5:
                        TSCPrinter.windowsfont(250, 50, 140, 0, 2, 0, "arial", pd.grade); //14-AB
                        break;
                    case 6:
                        TSCPrinter.windowsfont(220, 50, 140, 0, 2, 0, "arial", pd.grade); //14-ABC
                        break;
                    case 7:
                        TSCPrinter.windowsfont(180, 50, 140, 0, 2, 0, "arial", pd.grade); //14-ABCD
                        break;
                    case 8:
                        TSCPrinter.windowsfont(140, 50, 140, 0, 2, 0, "arial", pd.grade); //14-ABCDE
                        break;
                    case 9:
                        TSCPrinter.windowsfont(100, 50, 140, 0, 2, 0, "arial", pd.grade); //14-ABCDEF
                        break;
                    case 10:
                        TSCPrinter.windowsfont(90, 50, 130, 0, 2, 0, "arial", pd.grade); //19-ABCDEFG
                        break;
                    case 12:
                        TSCPrinter.windowsfont(80, 50, 110, 0, 2, 0, "arial", pd.grade); //19-ABCDEFG
                        break;
                    default:
                        TSCPrinter.windowsfont(60, 40, 90, 0, 2, 0, "arial", pd.grade); //14-ABCDEFGHIJK (> 12)
                        break;
                }

                TSCPrinter.barcode("60", "520", "128", "60", "0", "270", "2", "2", pd.bc); //barcode ตั้ง
                TSCPrinter.windowsfont(120, 460, 30, 90, 0, 0, "arial", pd.bc);
                TSCPrinter.sendcommand("BAR 50,210,770,4"); //เส้นนอน ล่าง Grade

                //เพิ่มเช็ค  len ของ  Caseno เพื่อจัดตำแหน่งให้อยู่ตรงกลาง
                var casenoLen = pd.caseno.ToString().Length;
                switch (casenoLen)
                {
                    case 1:
                        TSCPrinter.windowsfont(440, 212, 110, 0, 2, 0, "arial", pd.caseno.ToString()); //1
                        break;
                    case 2:
                        TSCPrinter.windowsfont(420, 212, 110, 0, 2, 0, "arial", pd.caseno.ToString()); //12
                        break;
                    case 3:
                        TSCPrinter.windowsfont(390, 212, 110, 0, 2, 0, "arial", pd.caseno.ToString()); //123
                        break;
                    case 4:
                        TSCPrinter.windowsfont(360, 212, 110, 0, 2, 0, "arial", pd.caseno.ToString()); //1234
                        break;
                    default:
                        TSCPrinter.windowsfont(330, 212, 110, 0, 2, 0, "arial", pd.caseno.ToString()); //12345
                        break;
                }

                TSCPrinter.sendcommand("BAR 150,315,670,4"); //เส้นนอน ล่าง Caseno
                TSCPrinter.windowsfont(190, 317, 110, 0, 2, 0, "arial", ((decimal)pd.grossdef).ToString("N1"));
                TSCPrinter.windowsfont(520, 317, 110, 0, 2, 0, "arial", ((decimal)pd.netdef).ToString("N1"));
                TSCPrinter.sendcommand("BAR 150,430,670,4"); //เส้นนอน ล่าง 215
                TSCPrinter.barcode("165", "450", "128", "50", "0", "0", "2", "2", pd.bc); //x,y,code type,hight,human readable ,rotation,narrow,wide,code
                TSCPrinter.windowsfont(220, 500, 30, 0, 0, 0, "arial", pd.bc);

                if (!string.IsNullOrEmpty(pd.pdremark))
                    TSCPrinter.windowsfont(480, 465, 40, 0, 2, 0, "arial", "T:" +
                        ((decimal)pd.boxtare).ToString("N2") + ",N:" +
                        ((decimal)pd.netreal).ToString("N2") + "," + pd.pdremark);
                else
                    TSCPrinter.windowsfont(480, 465, 40, 0, 2, 0, "arial", "T:" +
                        ((decimal)pd.boxtare).ToString("N2") + ",N:" +
                        ((decimal)pd.netreal).ToString("N2"));

                TSCPrinter.sendcommand("BAR 50,535,774,4"); //เส้นนอน ล่าง barcode
                TSCPrinter.sendcommand("BAR 470,315,4,220"); //เส้นตั้ง กลาง
                TSCPrinter.windowsfont(65, 540, 30, 0, 0, 0, "arial", "Effective : 28-10-2013");
                TSCPrinter.windowsfont(650, 545, 30, 0, 0, 0, "arial", "FM-PCS-17");
                TSCPrinter.windowsfont(65, 565, 30, 0, 0, 0, "arial", "D/M/Y");
                TSCPrinter.printlabel("1", copy.ToString());
                TSCPrinter.closeport();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PrintCustomerBarcode(PDCustomerPackedPrint pdCusPackedPrint,
            pd pd, string printerName, short copy)
        {
            try
            {
                if (copy == 0)
                    return;

                if (System.Drawing.Printing.PrinterSettings
                    .InstalledPrinters
                    .Cast<string>()
                    .Any(name => printerName
                    .ToUpper()
                    .Trim() == name.ToUpper()
                    .Trim()) == false)
                    throw new ArgumentException("ไม่พบเครื่องพิมพ์ " + printerName +
                        " บนเครื่องของท่าน กรุณาติดตั้งเครื่องพิมพ์และลองใหม่อีกครั้ง");

                if (IsPrinterOffline(printerName))
                    throw new ArgumentException("Printer " + printerName + " is offline.");

                if (pd == null)
                    throw new ArgumentException("pd is null. ไม่พบข้อมูลยากล่องนี้ในระบบ");

                if (pdCusPackedPrint == null)
                    throw new ArgumentException("pdCustomerPackedPrint is null." +
                        " ไม่พบข้อมูลการตั้งค่าบาร์โค้ตลูกค้า โปรดแจ้งแผนก QC เพื่อตรวจสอบข้อมูล");

                var pdCustomer = uow.pdCustomeRepo.GetSingle(x => x.BC == pd.bc);
                if (pdCustomer == null)
                    throw new ArgumentException("pdCustomer is null." +
                        " ไม่พบข้อมูลบาร์โค้ตลูกค้าในระบบ โปรดแจ้งแผนก IT เพื่อตรวจสอบข้อมูล");

                var path = cusLableTemplate + pdCusPackedPrint.CusPrintFormatName + ".btw";
                if (!File.Exists(path))
                    throw new ArgumentException("ไม่พบไฟล์เทมเพลตบาร์โค้ตลูกค้าบนเซิร์ฟเวอร์ (File not exists)");

                BarTender.Application btApp;
                BarTender.Format btFormat;
                btApp = new BarTender.Application();
                btApp.Visible = true;
                btFormat = btApp.Formats.Open(path, false, printerName);

                switch (pd.customerrs)
                {
                    case "LTL":
                        btFormat.SetNamedSubStringValue("LTLBarcode2", pdCustomer.BCCustomer);
                        btFormat.SetNamedSubStringValue("LTLGrade2", pdCusPackedPrint.CusGradeCode);
                        btFormat.SetNamedSubStringValue("NetDef2", pd.netdef + "  Kg");
                        btFormat.SetNamedSubStringValue("STECBarcode2", pd.bc);
                        break;
                    case "PMI":
                        btFormat.SetNamedSubStringValue("Lot", pdCusPackedPrint.CusLotNo);
                        btFormat.SetNamedSubStringValue("Accumulative", pd.AccuRunno.ToString());
                        btFormat.SetNamedSubStringValue("CaseNo", pd.CaseRunno.ToString());
                        btFormat.SetNamedSubStringValue("Run", pd.CusRunno.ToString());
                        btFormat.SetNamedSubStringValue("Moist", pdCusPackedPrint.CusMoistureDefault.ToString());
                        btFormat.SetNamedSubStringValue("GradeCode", pdCusPackedPrint.CusGradeCode);
                        btFormat.SetNamedSubStringValue("DealerBarCode", pd.bc);
                        btFormat.SetNamedSubStringValue("Net", pd.netdef.ToString());
                        btFormat.SetNamedSubStringValue("Gross", pdCusPackedPrint.CusGrossDefault.ToString());
                        break;
                    case "ITG":
                        btFormat.SetNamedSubStringValue("Accumulative", pd.AccuRunno.ToString());
                        btFormat.SetNamedSubStringValue("DealerBarCode", pd.bc);
                        break;
                    case "KT &  G":
                        btFormat.SetNamedSubStringValue("Grade", pdCusPackedPrint.CusGradeCode);
                        btFormat.SetNamedSubStringValue("ACCUMULATIVE", pd.AccuRunno.ToString());
                        btFormat.SetNamedSubStringValue("CUSGRADECODE", pdCusPackedPrint.CusGradeCode);
                        btFormat.SetNamedSubStringValue("STECBARCODE", pd.bc);
                        btFormat.SetNamedSubStringValue("GROSSNET", ((decimal)pdCusPackedPrint.CusGrossDefault).ToString("N1"));
                        btFormat.SetNamedSubStringValue("TARENET", ((decimal)pd.taredef).ToString("N1"));
                        btFormat.SetNamedSubStringValue("NET", ((decimal)pd.netdef).ToString("N0"));
                        break;
                    case "JMC":
                        var tmpWT = ",430,33,397,BU";
                        var customerBC = "";
                        var customerQRCode = "";
                        var hour = ((DateTime)pd.packingtime).Hour.ToString().PadLeft(2, '0');
                        var minute = ((DateTime)pd.packingtime).Minute.ToString().PadLeft(2, '0');
                        var day = ((DateTime)pd.packingdate).Day.ToString().PadLeft(2, '0');
                        var month = ((DateTime)pd.packingdate).Month.ToString().PadLeft(2, '0');
                        var year = ((DateTime)pd.packingdate).Year.ToString().Substring(2, 2).PadLeft(2, '0');

                        if (pd.graders.Contains("47P"))
                        {
                            customerBC = "000047P02020F000TH039700JL47P" + pdCustomer.BCCustomer;
                            customerQRCode = "47P02020F,4700000263," + pd.crop + "," +
                                pdCusPackedPrint.CusGradeCode + "," +
                                pdCustomer.BCCustomer + ",TH,SSTK," + pd.crop + "," +
                                pd.CusRunno.ToString().PadLeft(2, '0') +
                                hour + minute + "," +
                                month + day + year +
                                tmpWT;
                        }
                        else
                        {
                            customerBC = "0000BMX02020B000TH039700JLBMX" + pdCustomer.BCCustomer;
                            customerQRCode = "BMX02020B,4700000262," + pd.crop + "," +
                                pdCusPackedPrint.CusGradeCode + "," +
                                pdCustomer.BCCustomer + ",TH,SSTK," + pd.crop + "," +
                                pd.CusRunno.ToString().PadLeft(2, '0') +
                                hour + minute + "," +
                                month + day + year +
                                tmpWT;
                        }

                        btFormat.SetNamedSubStringValue("CaseRunning", pdCustomer.BCCustomer);
                        btFormat.SetNamedSubStringValue("CustomerBarcode2", customerBC);
                        btFormat.SetNamedSubStringValue("BCReserved", customerBC);
                        btFormat.SetNamedSubStringValue("Material", pdCusPackedPrint.CusGradeCode);
                        btFormat.SetNamedSubStringValue("STECBARCODE", pd.bc);
                        btFormat.SetNamedSubStringValue("QRCode", customerQRCode);
                        break;
                    default:
                        break;
                }

                btFormat.IdenticalCopiesOfLabel = (int)(pdCusPackedPrint.PrintCopy) * copy;
                btFormat.PrintOut(false, false);
                btFormat.Close(BarTender.BtSaveOptions.btDoNotSaveChanges);
                btApp.Quit(BarTender.BtSaveOptions.btDoNotSaveChanges);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool IsPrinterOffline(string printerName)
        {
            // Set management scope
            ManagementScope scope = new ManagementScope(@"\root\cimv2");
            scope.Connect();

            // Select Printers from WMI Object Collections
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_Printer");
            string printerName1 = "";
            foreach (ManagementObject printer in searcher.Get())
            {
                printerName1 = printer["Name"].ToString().ToLower();
                if (printerName1.Equals(printerName.ToLower()))
                    if (printer["WorkOffline"].ToString().ToLower().Equals("true"))
                        return true;

            }
            return false;
        }

        public string GetNewByProductBarcode(string byproductType, short crop)
        {
            try
            {
                if (string.IsNullOrEmpty(byproductType))
                    throw new ArgumentException("Type cannot by empty. " +
                        " โปรดระบุ by product type ก่อนได้แก่ (Stem Long, Stem Long 2, Stem Short, Fines Large, Fines Small)");

                if (byproductType != "SL" && byproductType != "SL2" && byproductType != "SS" && byproductType != "FL" && byproductType != "FS")
                    throw new ArgumentException("Please specify type e.g. " +
                        "Stem Long, Stem Long (2), Stem Short, Fine Large, Fine Small.");

                var prefix = byproductType == "SL2" ? "SL" : byproductType;
                var pdbc = Facade.pdbcBL().GetSingle(crop);
                if (pdbc == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdbc ใน crop " + crop +
                        " โปรดติดต่อแผนกไอทีเพื่อทำการตรวจสอบ");

                return prefix + pdbc.crop.ToString().Substring(2, 2) + "-" +
                    (pdbc.bc + 1).ToString().PadLeft(7, '0');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int GetNewByProductCaseNo(string packedgrade)
        {
            try
            {
                var pdList = uow.pdRepo.Get(x => x.grade == packedgrade);
                if (pdList.Count() <= 0)
                    return 1;
                else
                    return (int)pdList.Max(x => x.caseno) + 1;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void AddByProduct(string bc, int caseno, decimal grossreal, string pend, string machine,
            decimal boxtare, bool box, string byProductGrade, bool checkNetReal,
            string printerName, short copy, string pdno, string packinguser)
        {
            try
            {
                if (string.IsNullOrEmpty(pend))
                    throw new ArgumentException("โปรดระบุ machine");

                if (string.IsNullOrEmpty(pend))
                    throw new ArgumentException("โปรดระบุ packing end");

                if (string.IsNullOrEmpty(packinguser))
                    throw new ArgumentException("โปรดระบุ packing user");

                if (string.IsNullOrEmpty(bc))
                    throw new ArgumentException("โปรดระบุ barcode");

                if (grossreal <= 0)
                    throw new ArgumentException("gross ไม่ควรเป็น 0 หรือน้อยกว่า");

                if (boxtare <= 0)
                    throw new ArgumentException("boxtare ไม่ควรเป็น 0 หรือน้อยกว่า");

                var pdsetup = uow.pdsetupRepo.GetSingle(x => x.pdno == pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (pdsetup.byproductlocked == true)
                    throw new ArgumentException("รันนี้ถูกล็อคไปแล้ว (byproductlocked = true) โปรดติดดต่อแผนก Leaf Account เพื่อปลดล็อค");

                var packedgrade = uow.packedgradeRepo
                    .GetSingle(x => x.packedgrade1 == byProductGrade);
                if (packedgrade == null)
                    throw new ArgumentException("ไม่พบ packedgrade " + pdsetup.packedgrade + " ในระบบ");

                if (boxtare > packedgrade.taredef * 2)
                    throw new ArgumentException("Boxtare ไม่ควรเกิน " +
                        ((decimal)(packedgrade.taredef * 2)).ToString("N2"));

                var netreal = grossreal - boxtare;
                var accept = (decimal)0.3;

                if (netreal <= 0)
                    throw new ArgumentException("น้ำหนัก (netreal) ต้องมากว่า 0 กก. ขึ้นไป");

                if (caseno != 0)
                {
                    if (checkNetReal == true)
                    {
                        if (netreal > packedgrade.netdef + accept)
                            throw new ArgumentException("กรุณาดึงยาออก น้ำหนักใบยาจะต้องไม่เกิน " +
                                ((decimal)(packedgrade.netdef + accept)).ToString("N2") +
                                " โปรดดึงยาออกจำนวน " +
                                ((decimal)Math.Abs((decimal)(packedgrade.netdef + accept - netreal))).ToString("N2") +
                                " กก.");
                        if (netreal < packedgrade.netdef - accept)
                            throw new ArgumentException("กรุณาใส่ยาเพิ่ม น้ำหนักยาจะต้องไม่น้อยกว่า " +
                                ((decimal)(packedgrade.netdef - accept)).ToString("N2") +
                                " โปรดใส่ยาเพิ่มอีก " +
                                ((decimal)Math.Abs((decimal)(packedgrade.netdef + accept - netreal))).ToString("N2") +
                                " กก.");
                    }
                }

                var byproduct = new pd
                {
                    crop = packedgrade.crop,
                    bc = bc,
                    type = packedgrade.type,
                    customer = packedgrade.customer,
                    grade = packedgrade.packedgrade1,
                    form = packedgrade.form,
                    caseno = caseno,
                    grossdef = packedgrade.grossdef,
                    taredef = packedgrade.taredef,
                    netdef = packedgrade.netdef,
                    grossreal = grossreal,
                    netreal = netreal,
                    packingmat = packedgrade.packingmat,
                    malcam = 0,
                    wh = "STEC",
                    rcfrom = "Packing",
                    frompddate = pdsetup.date,
                    fromprgrade = pdsetup.packedgrade,/// Parent grade.
                    frompdno = pdsetup.pdno,
                    packingdate = DateTime.Now.Date,
                    packingtime = DateTime.Now,
                    packinguser = packinguser,
                    pdtype = bc.Contains("F") ? "Fines" : "Stem",
                    box = box,
                    graders = packedgrade.packedgrade1,
                    customerrs = packedgrade.customer,
                    boxtare = (double)boxtare,
                    pdremark = "",
                    frompdremark = "",
                    pdremarkrs = ""
                };
                uow.pdRepo.Add(byproduct);

                var pdbc = uow.pdbcRepo.GetSingle(x => x.crop == pdsetup.crop);
                if (pdbc == null)
                    throw new ArgumentException("pdbc cannot be empty. the system cannot update bc.");

                pdbc.bc = pdbc.bc + 1;
                uow.pdbcRepo.Update(pdbc);
                uow.Save();
                PrintSTECBarcode(byproduct, printerName, copy);

                /// *************************************
                /// Print customer label.
                /// *************************************
                var pdCusPackedPrint = uow.pdCusPackedPrintRepo
                    .GetSingle(x => x.STECPackedGrade == packedgrade.packedgrade1);
                if (pdCusPackedPrint == null)
                    return;

                var byProductList = uow.pdRepo
                    .Get(x => x.grade == packedgrade.packedgrade1 &&
                    x.caseno != 0);

                var caseRunnoList = byProductList.Where(x => x.frompdno == pdsetup.pdno);
                var caseRunno = caseRunnoList.Count() > 0 ? caseRunnoList.Max(x => x.CaseRunno) + 1 : 1;
                var accuRunno = byProductList.Count() > 0 ? byProductList.Max(x => x.AccuRunno) + 1 : 1;

                var cusRunnoList = byProductList
                    .GroupBy(x => new { x.CusRunno, x.frompdno })
                    .Select(x => new
                    {
                        pdno = x.Key.frompdno,
                        CusRunno = x.Key.CusRunno
                    }).ToList();

                var cusRunno = 1;
                if (cusRunnoList.Count() > 0)
                {
                    if (caseRunnoList.Count() > 0)
                        /// Not first box of this pdno.
                        cusRunno = (int)caseRunnoList.FirstOrDefault().CusRunno;
                    else
                        /// First box of this pdno.
                        cusRunno = (int)caseRunnoList.Max(x => x.CusRunno) + 1;
                }

                var pdCus = uow.pdRepo.GetSingle(x => x.bc == byproduct.bc);
                if (pdCus == null)
                    throw new ArgumentException("ไม่พบข้อมูลกล่องยารหัส " + byproduct.bc + " นี้ในระบบ");

                pdCus.CaseRunno = caseRunno;
                pdCus.AccuRunno = accuRunno;
                pdCus.CusRunno = cusRunno;
                pdCus.FromCusLotNo = pdCusPackedPrint.CusLotNo;
                pdCus.ParentGrade = pdsetup.packedgrade;

                uow.pdRepo.Update(pdCus);
                uow.Save();
                PrintCustomerBarcode(pdCusPackedPrint, pdCus, printerName, copy);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void ReWeightByProduct(string bc, decimal grossreal, bool box, decimal boxtare,
            bool checkNetReal, string packinguser)
        {
            try
            {
                var pd = GetSingle(bc);
                if (pd == null)
                    throw new ArgumentException("ไม่พบข้อมูลรหัสบาร์โค้ต " + bc + " ในระบบ");

                var pdsetup = uow.pdsetupRepo
                    .GetSingle(x => x.pdno == pd.frompdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (pdsetup.byproductlocked == true)
                    throw new ArgumentException("ท้ายเครื่องล็อคเกรดนี้ไปแล้ว (packinglocked = 1) โปรดติดดต่อแผนก Leaf Account เพื่อปลดล็อค");

                var packedgrade = uow.packedgradeRepo
                    .GetSingle(x => x.packedgrade1 == pd.grade);
                if (packedgrade == null)
                    throw new ArgumentException("ไม่พบ packedgrade " + pdsetup.packedgrade + " ในระบบ");

                if (boxtare > packedgrade.taredef * 2)
                    throw new ArgumentException("Boxtare ไม่ควรเกิน " +
                        ((decimal)(packedgrade.taredef * 2)).ToString("N2"));

                var netreal = grossreal - boxtare;
                var accept = (decimal)0.3;

                if (netreal <= 0)
                    throw new ArgumentException("น้ำหนัก (netreal) ต้องมากว่า 0 กก. ขึ้นไป");

                if (pd.caseno != 0)
                {
                    if (checkNetReal == true)
                    {
                        if (netreal > packedgrade.netdef + accept)
                            throw new ArgumentException("กรุณาดึงยาออก น้ำหนักใบยาจะต้องไม่เกิน " +
                                ((decimal)(packedgrade.netdef + accept)).ToString("N2") +
                                " โปรดดึงยาออกจำนวน " +
                                ((decimal)Math.Abs((decimal)(packedgrade.netdef + accept - netreal))).ToString("N2") +
                                " กก.");
                        if (netreal < packedgrade.netdef - accept)
                            throw new ArgumentException("กรุณาใส่ยาเพิ่ม น้ำหนักยาจะต้องไม่น้อยกว่า " +
                                ((decimal)(packedgrade.netdef - accept)).ToString("N2") +
                                " โปรดใส่ยาเพิ่มอีก " +
                                ((decimal)Math.Abs((decimal)(packedgrade.netdef + accept - netreal))).ToString("N2") +
                                " กก.");
                    }
                }

                pd.grossreal = grossreal;
                pd.netreal = netreal;
                pd.boxtare = (double)boxtare;
                pd.box = box;
                pd.dtrecord = DateTime.Now;
                pd.packinguser = packinguser;

                uow.pdRepo.Update(pd);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PrintBarcodeWithBartender(pd pd)
        {
            try
            {
                var barcode = Facade.BarcodeSettingBL().GetByCurrentCaseNo(pd.grade, (int)pd.caseno);
                if (barcode == null)
                    throw new ArgumentException("ไม่พบการตั้งค่าการพิมพ์บาร์โค้ต โปรดติดต่อแผนก QC เพื่อตั้งค่าดังกล่าว");

                if (barcode.Copies == 0)
                    throw new ArgumentException("โปรดระบุจำนวนที่จะพิมพ์");

                if (!(pd.caseno >= barcode.FromCaseNo && pd.caseno <= barcode.ToCaseNo))
                    throw new ArgumentException("caseno ของกล่องยานี้ไม่อยู่ในช่วงที่ทาง QC ระบุ โปรดตรวจสอบกับแผนก QC อีกครั้ง");

                if (System.Drawing.Printing.PrinterSettings
                    .InstalledPrinters
                    .Cast<string>()
                    .Any(name => barcode.BarcodePrinter.PrinterName
                    .ToUpper()
                    .Trim() == name.ToUpper()
                    .Trim()) == false)
                    throw new ArgumentException("ไม่พบเครื่องพิมพ์ " +
                        barcode.BarcodePrinter.PrinterName +
                        " บนเครื่องของท่าน กรุณาติดตั้งเครื่องพิมพ์และลองใหม่อีกครั้ง");

                if (IsPrinterOffline(barcode.BarcodePrinter.PrinterName))
                    throw new ArgumentException("Printer " +
                        barcode.BarcodePrinter.PrinterName +
                        " is offline.");

                if (pd == null)
                    throw new ArgumentException("pd is null. ไม่พบข้อมูลยากล่องนี้ในระบบ");

                var path = barcode.BarcodeTemplate.FilePath + barcode.BarcodeTemplate.FileName;
                if (!File.Exists(path))
                    throw new ArgumentException("ไม่พบไฟล์เทมเพลตบาร์โค้ตลูกค้าบนเซิร์ฟเวอร์ (File not exists)");

                BarTender.Application btApp;
                BarTender.Format btFormat;
                btApp = new BarTender.Application();
                btApp.Visible = true;
                btFormat = btApp.Formats.Open(path, false, barcode.BarcodePrinter.PrinterName);

                btFormat.SetNamedSubStringValue("dbbc", pd.bc);
                btFormat.SetNamedSubStringValue("dbcrop", pd.crop.ToString());
                btFormat.SetNamedSubStringValue("dbtype", pd.type);
                btFormat.SetNamedSubStringValue("dbgrade", pd.grade);
                btFormat.SetNamedSubStringValue("dbform", pd.form);
                btFormat.SetNamedSubStringValue("dbcustomer", pd.customer);
                btFormat.SetNamedSubStringValue("dbcaseno", pd.caseno.ToString());
                btFormat.SetNamedSubStringValue("dbgrossdef", pd.grossdef.ToString());
                btFormat.SetNamedSubStringValue("dbtaredef", pd.taredef.ToString());
                btFormat.SetNamedSubStringValue("dbnetdef", pd.netdef.ToString());
                btFormat.SetNamedSubStringValue("dbgrossreal", pd.grossreal.ToString());
                btFormat.SetNamedSubStringValue("dbnetreal", pd.netreal.ToString());
                btFormat.SetNamedSubStringValue("dbpackingdate", pd.packingdate.ToString());
                btFormat.SetNamedSubStringValue("dbpackingtime", pd.packingtime.ToString());
                btFormat.SetNamedSubStringValue("dbfrompdno", pd.frompdno);
                btFormat.SetNamedSubStringValue("dbfrompdhour", pd.frompdhour.ToString());
                btFormat.SetNamedSubStringValue("dbboxtare", pd.boxtare.ToString());
                btFormat.SetNamedSubStringValue("dbgraders", pd.graders.ToString());
                btFormat.SetNamedSubStringValue("dbcustomerrs", pd.customerrs.ToString());
                btFormat.SetNamedSubStringValue("dbCaseRunno", pd.CaseRunno.ToString());
                btFormat.SetNamedSubStringValue("dbAccuRunno", pd.AccuRunno.ToString());
                btFormat.SetNamedSubStringValue("dbCusRunno", pd.CusRunno.ToString());

                btFormat.IdenticalCopiesOfLabel = barcode.Copies;
                //btFormat.PrintOut(false, false);
                btFormat.Close(BarTender.BtSaveOptions.btDoNotSaveChanges);
                btApp.Quit(BarTender.BtSaveOptions.btDoNotSaveChanges);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pd> GetByMovementNo(string movementNo)
        {
            return uow.pdRepo.Get(x => x.MovementNo == movementNo);
        }
    }
}
