﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IBarcodeTemplateBL
    {
        void Add(string filePath, string fileName, string createUser);
        void Edit(Guid id, string filePath, string fileName, string editUser);
        void Delete(Guid id);
        BarcodeTemplate GetSingle(Guid id);
        List<BarcodeTemplate> GetAll();
    }

    public class BarcodeTemplateBL : IBarcodeTemplateBL
    {
        StecDBMSUnitOfWork uow;

        public BarcodeTemplateBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public void Add(string filePath, string fileName, string createUser)
        {
            try
            {
                if (string.IsNullOrEmpty(filePath))
                    throw new ArgumentException("โปรดระบุที่อยู่ไฟล์ข้อมูล file path is null.");

                if (string.IsNullOrEmpty(createUser))
                    throw new ArgumentException("create user is null.");

                var model = uow.barcodeTemplateRepo.GetSingle(x => x.FileName == fileName);
                if (model != null)
                    throw new ArgumentException("มีไฟล์นี้อยู่แล้วในระบบ");

                uow.barcodeTemplateRepo.Add(new BarcodeTemplate
                {
                    TemplateID = Guid.NewGuid(),
                    FilePath = filePath,
                    FileName = fileName,
                    CreateBy = createUser,
                    CreateDate = DateTime.Now,
                    ModifiedBy = createUser,
                    ModifiedDate = DateTime.Now
                });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(Guid id)
        {
            try
            {
                var model = GetSingle(id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (uow.barcodeSettingRepo.Get(x => x.TemplateID == model.TemplateID).Count() > 0)
                    throw new ArgumentException("มีการนำ template นี้ไปใช้ในการตั้งค่าการพิมพ์บาร์โค้ต " +
                        "ต้องลบออกจากการตั้งค่าการพิมพ์บาร์โค้ตก่อน");

                uow.barcodeTemplateRepo.Remove(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Edit(Guid id, string filePath, string fileName, string editUser)
        {
            try
            {
                var model = GetSingle(id);
                if (model == null)
                    throw new ArgumentException("ไม่พบข้อมูลในระบบ");

                if (uow.barcodeTemplateRepo.Get(x => x.FileName == fileName).Count() > 0)
                    throw new ArgumentException("มีการใช้ชื่อไฟล์นี้แล้วในระบบ");

                model.FilePath = filePath;
                model.FileName = fileName;
                model.ModifiedDate = DateTime.Now;
                model.ModifiedBy = editUser;

                uow.barcodeTemplateRepo.Update(model);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<BarcodeTemplate> GetAll()
        {
            return uow.barcodeTemplateRepo.Get();
        }

        public BarcodeTemplate GetSingle(Guid id)
        {
            return uow.barcodeTemplateRepo.GetSingle(x => x.TemplateID == id);
        }
    }
}
