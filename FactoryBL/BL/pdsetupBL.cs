﻿using FactoryBL.Model;
using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IpdsetupBL
    {
        void FinishBlending(string pdno);
        void FinishPacking(string pdno);
        void ChangedLockedStatus(string pdno, bool blendinglocked, bool packinglocked,
            bool byproductlocked, bool pickinglocked, string user);
        void Add(string packedgrade, string mode, string user, string pdremark, string pdremarkparent, DateTime date, bool def);
        void Update(string pdno, string packedgrade, string mode, string user, string pdremark, string pdremarkparent, DateTime date, bool def);
        void Delete(string pdno);
        void SetDefault(string pdno, string user);
        string GetMaxProductionNo();
        List<pdsetup> GetByCrop(int crop);
        pdsetup GetSingle(string pdno);
        pdsetup GetByDefault();
        List<pdsetup> GetByDateAndDefault(DateTime date);
        List<pdsetup> GetByPackedGrade(string packedgrade);
        List<pdsetup> GetByDate(DateTime date);
    }

    public class pdsetupBL : IpdsetupBL
    {
        StecDBMSUnitOfWork uow;
        public pdsetupBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public void FinishBlending(string pdno)
        {
            try
            {
                var pdsetup = GetSingle(pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (pdsetup.blendinglocked == true)
                    throw new ArgumentException("เกรดนี้ถูก finish ไปแล้ว");

                var blendingstatusList = uow.blendingstatusRepo.Get(x => x.pdno == pdno);
                if (blendingstatusList
                    .Where(x => x.locked == false)
                    .Count() > 0)
                    throw new ArgumentException("มีชั่วโมงที่ยังไม่ได้เปลี่ยนสถานะเป็น locked");

                var packingstatusList = uow.packingstatusRepo.Get(x => x.pdno == pdno);
                if (blendingstatusList.Count() < packingstatusList.Count())
                    throw new ArgumentException("จำนวนชั่วโมงที่ท้ายเครื่องมากกว่าจำนวนชั่วโมงที่หน้าเบลน โปรดตรวจสอบข้อมูล");

                pdsetup.blendinglocked = true;
                uow.pdsetupRepo.Update(pdsetup);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void FinishPacking(string pdno)
        {
            try
            {
                var pdsetup = GetSingle(pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (pdsetup.packinglocked == true)
                    throw new ArgumentException("เกรดนี้ถูก finish ไปแล้ว");

                var packingstatusList = uow.packingstatusRepo.Get(x => x.pdno == pdno);
                if (packingstatusList
                    .Where(x => x.locked == false)
                    .Count() >= 1)
                    throw new ArgumentException("มีชั่วโมงที่ยังไม่ได้เปลี่ยนสถานะเป็น locked");

                pdsetup.packinglocked = true;
                uow.pdsetupRepo.Update(pdsetup);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<pdsetup> GetByCrop(int crop)
        {
            return uow.pdsetupRepo.Get(x => x.crop == crop);
        }

        public List<pdsetup> GetByDateAndDefault(DateTime date)
        {
            var listA = uow.pdsetupRepo
                .Get(x => x.date == date || x.def == true).ToList();

            return listA;


            //var crop = listA.FirstOrDefault().crop;
            //var listB = uow.packedgradeRepo
            //    .Get(x => x.crop == crop);

            //return (from a in listA
            //        from b in listB
            //        where a.packedgrade == b.packedgrade1
            //        select new m_pdsetup
            //        {
            //            crop = a.crop,
            //            date = a.date,
            //            pdno = a.pdno,
            //            packedgrade = a.packedgrade,
            //            def = a.def,
            //            locked = a.locked,
            //            blendinglocked = a.blendinglocked,
            //            packinglocked = a.packinglocked,
            //            byproductlocked = a.byproductlocked,
            //            pickinglocked = a.pickinglocked,
            //            dtrecord = a.dtrecord,
            //            user = a.user,
            //            mode = a.mode,
            //            casenost = a.casenost,
            //            pdremark = a.pdremark,
            //            PdRemarkParent = a.PdRemarkParent,
            //            CusRunID = a.CusRunID,
            //            customer = b.customer
            //        }).ToList();
        }

        public pdsetup GetByDefault()
        {
            return uow.pdsetupRepo.GetSingle(x => x.def == true);
        }

        public List<pdsetup> GetByPackedGrade(string packedgrade)
        {
            return uow.pdsetupRepo.Get(x => x.packedgrade == packedgrade);
        }

        public pdsetup GetSingle(string pdno)
        {
            return uow.pdsetupRepo.GetSingle(x => x.pdno == pdno);
        }
        public List<pdsetup> GetByDate(DateTime date)
        {
            var listA = uow.pdsetupRepo
                .Get(x => x.date == date).ToList();

            List<pdsetup> t = new List<pdsetup>();

            if (listA.Count() > 0)
            {
                var crop = (int)listA.FirstOrDefault().crop;
                var listB = uow.packedgradeRepo
                    .Get(x => x.crop == crop);

                t = (from a in listA
                     from b in listB
                     where a.packedgrade == b.packedgrade1
                     select new pdsetup
                     {
                         crop = a.crop,
                         date = a.date,
                         pdno = a.pdno,
                         packedgrade = a.packedgrade,
                         def = a.def,
                         locked = a.locked,
                         blendinglocked = a.blendinglocked,
                         packinglocked = a.packinglocked,
                         byproductlocked = a.byproductlocked,
                         pickinglocked = a.pickinglocked,
                         dtrecord = a.dtrecord,
                         user = a.user,
                         mode = a.mode,
                         casenost = a.casenost,
                         pdremark = a.pdremark,
                         PdRemarkParent = a.PdRemarkParent,
                         CusRunID = a.CusRunID
                     }).ToList();
            }

            return t;

        }

        public void ChangedLockedStatus(string pdno, bool blendinglocked, bool packinglocked,
            bool byproductlocked, bool pickinglocked, string user)
        {
            try
            {
                if (string.IsNullOrEmpty(pdno))
                    throw new ArgumentException("pdno can not be empty.");

                if (string.IsNullOrEmpty(user))
                    throw new ArgumentException("user can not be empty.");

                var security = uow.securityRepo.GetSingle(x => x.uname == user);
                if (security == null)
                    throw new ArgumentException("ไม่พบ user นี้ในระบบ");

                if (security.costing == false)
                    throw new ArgumentException("ผู้ที่สามารถจัดการข้อมูลส่วนนี้ได้จะต้องได้รับสิทธิ์อยู่ในกลุ่ม costing");

                var pdsetup = GetSingle(pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบ");

                pdsetup.blendinglocked = blendinglocked;
                pdsetup.packinglocked = packinglocked;
                pdsetup.byproductlocked = byproductlocked;
                pdsetup.pickinglocked = pickinglocked;
                pdsetup.dtrecord = DateTime.Now;
                pdsetup.user = user;

                uow.pdsetupRepo.Update(pdsetup);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Update(string pdno, string packedgrade, string mode, string user, string pdremark, string pdremarkparent, DateTime date, bool def)
        {
            try
            {
                var pdsetup = GetSingle(pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบ pdno นี้ในระบบ");

                var packedOutList = Facade.pdBL().GetByFromPdno(pdno);
                if (packedOutList.Count() > 0)
                    if (packedOutList.FirstOrDefault().grade != packedgrade)
                        throw new ArgumentException("มีการผลิตไปแล้วใน pdno " + pdno +
                            " จำนวน " + packedOutList.Count().ToString("N0") + " กล่อง" +
                            " เป็น packedgrade " + packedOutList.FirstOrDefault().grade +
                            " ระบบไม่สามารถเปลี่ยนเป็น packedgrade " + packedgrade +
                            " ได้เพราะอาจทำให้ข้อมูลการผลิตไม่สอดคล้องกัน");

                if (packedOutList.Count() > 0)
                    if (pdsetup.mode != mode)
                        throw new ArgumentException("มีการอบยาไปแล้วใน pdno นี้จำนวน " + packedOutList.Count().ToString("N0") + " กล่อง" +
                            " ระบบไม่สามารถเปลี่ยน mode ได้ (สามารถเปลี่ยน mode ได้ก็ต่อเมื่อยังไม่ได้เริ่มทำการผลิตใน pdno นี้)");

                var repackedList = Facade.pdBL().GetByToPdno(pdno);
                if (repackedList.Count() > 0)
                    if (pdsetup.packedgrade != packedgrade)
                        throw new ArgumentException("มีการป้อนยา repacked เข้า packedgrade นี้แล้ว ไม่สามารถเปลี่ยน packedgrade ของ pdno นี้ได้");

                var matList = Facade.matBL().GetByToPdno(pdno);
                if (matList.Count() > 0)
                    if (pdsetup.packedgrade != packedgrade)
                        throw new ArgumentException("มีการป้อนยา green เข้า packedgrade นี้แล้ว ไม่สามารถเปลี่ยน packedgrade ของ pdno นี้ได้");

                pdsetup.packedgrade = packedgrade;
                pdsetup.mode = mode;
                pdsetup.date = date;
                pdsetup.pdremark = pdremark;
                pdsetup.PdRemarkParent = pdremarkparent;
                pdsetup.user = user;
                pdsetup.dtrecord = DateTime.Now;
                uow.pdsetupRepo.Update(pdsetup);
                uow.Save();

                if (def == true)
                    SetDefault(pdno,user);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string pdno)
        {
            try
            {
                var pdsetup = GetSingle(pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบ pdno นี้ในระบบ");

                var pdList = Facade.pdBL().GetByFromPdno(pdno);
                if (pdList.Count() > 0)
                    throw new ArgumentException("มีการผลิตไปแล้วใน pdno " + pdno +
                        " จำนวน " + pdList.Count().ToString("N0") +
                        " กล่อง จึงไม่สามารถลบข้อมูล pdno นี้ได้");

                var repackedList = Facade.pdBL().GetByToPdno(pdno);
                if (repackedList.Count() > 0)
                    throw new ArgumentException("มีการป้อนยา repacked เข้า packedgrade นี้แล้ว ไม่สามารถลบ pdno นี้ได้");

                var matList = Facade.matBL().GetByToPdno(pdno);
                if (matList.Count() > 0)
                    throw new ArgumentException("มีการป้อนยา green เข้า packedgrade นี้แล้ว ไม่สามารลบ pdno นี้ได้");

                uow.pdsetupRepo.Remove(pdsetup);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void SetDefault(string pdno, string user)
        {
            try
            {
                var pdsetup = GetSingle(pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบ pdno นี้ในระบบ");

                var pdsetupList = uow.pdsetupRepo.Get(x => x.def == true);
                if (pdsetupList.Count() > 0)
                {
                    foreach (var item in pdsetupList)
                        item.def = false;

                    uow.pdsetupRepo.UpdateRange(pdsetupList);
                    uow.Save();
                }

                pdsetup.def = true;
                pdsetup.user = user;
                pdsetup.dtrecord = DateTime.Now;
                uow.pdsetupRepo.Update(pdsetup);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Add(string packedgrade, string mode, string user, string pdremark, string pdremarkparent, DateTime date, bool def)
        {
            try
            {
                if (string.IsNullOrEmpty(packedgrade))
                    throw new ArgumentException("โปรดระบุ packedgrade");

                if (string.IsNullOrEmpty(mode))
                    throw new ArgumentException("โปรดระบุ mode");

                if (string.IsNullOrEmpty(user))
                    throw new ArgumentException("โปรดระบุ user");

                if (date.Year != DateTime.Now.Year)
                    throw new ArgumentException("วันที่อบยา (production date) ที่ระบุ ไม่ตรงกับปีปัจจุบัน");

                uow.pdsetupRepo.Add(new pdsetup
                {
                    pdno = GetMaxProductionNo(),
                    crop = date.Year,
                    date = date,
                    mode = mode,
                    packedgrade = packedgrade,
                    user = user,
                    pdremark = pdremark,
                    PdRemarkParent = pdremarkparent,
                    def = def,
                    dtrecord = DateTime.Now,
                    blendinglocked = false,
                    packinglocked = false,
                    byproductlocked = false,
                    pickinglocked = false,
                    locked = false
                });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetMaxProductionNo()
        {
            var max = 0;
            var crop = DateTime.Now.Year;
            var pdsetupList = GetByCrop(crop);
            if (pdsetupList.Count() > 0)
            {
                var maxpdno = pdsetupList.Max(x => x.pdno);
                max = Convert.ToInt16(maxpdno.Substring(maxpdno.Length - 3, 3));
            }

            return crop.ToString().Substring(2, 2) + "-" + (max + 1).ToString().PadLeft(3, '0');
        }
    }
}
