﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IpdCustomerBL
    {
        PDCustomer GetSingleByStecBarcode(string stecBarcode);
        PDCustomer GetSingleByCustomerBarcode(string customerBarcode);
    }

    public class pdCustomerBL : IpdCustomerBL
    {
        StecDBMSUnitOfWork uow;
        public pdCustomerBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public PDCustomer GetSingleByCustomerBarcode(string customerBarcode)
        {
            return uow.pdCustomeRepo.GetSingle(x => x.BCCustomer == customerBarcode);
        }

        public PDCustomer GetSingleByStecBarcode(string stecBarcode)
        {
            return uow.pdCustomeRepo.GetSingle(x => x.BC == stecBarcode);
        }
    }
}
