﻿using FactorySystemWPF.MVVM;
using FactorySystemWPF.ViewModel.Processing.Packing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FactorySystemWPF.View.Processing.Packing
{
    /// <summary>
    /// Interaction logic for ConfirmToPrintInternalBarcode.xaml
    /// </summary>
    public partial class ConfirmToPrintInternalBarcode : Window
    {
        public ConfirmToPrintInternalBarcode()
        {
            InitializeComponent();
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_ConfirmToPrintInternalBarcode)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.username):
                    UsernameTextBox.SelectAll();
                    PasswordBox.Focus();
                    break;
                case nameof(vm.password):
                    PasswordBox.SelectAll();
                    PasswordBox.Focus();
                    break;
                default:
                    break;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
            UsernameTextBox.Focus();
        }
    }
}
