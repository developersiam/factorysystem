﻿using FactoryDAL.StoreProcedure;
using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IsecurityBL
    {
        security GetSingle(string username);
        string DecodePassword(string pw);
    }

    public class securityBL : IsecurityBL
    {
        StecDBMSUnitOfWork uow;
        public securityBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public string DecodePassword(string password)
        {
            string firstDecode = "";
            var loopNum = new List<int> { 0, 1 };

            firstDecode = StoreProcedureRepository
                .sp_Receiving_DecodePassword(password);

            foreach (int i in loopNum)
                firstDecode = StoreProcedureRepository
                    .sp_Receiving_DecodePassword(firstDecode);

            return firstDecode;
        }

        public security GetSingle(string username)
        {
            return uow.securityRepo.GetSingle(x => x.uname == username);
        }
    }
}
