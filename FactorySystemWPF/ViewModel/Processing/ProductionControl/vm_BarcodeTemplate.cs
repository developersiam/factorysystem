﻿using FactoryBL;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.Model;
using FactorySystemWPF.MVVM;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace FactorySystemWPF.ViewModel.Processing.ProductionControl
{
    public class vm_BarcodeTemplate : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_BarcodeTemplate()
        {
            Clear();
        }



        #region Properties
        private string _filePath;

        public string FileParh
        {
            get { return _filePath; }
            set { _filePath = value; }
        }

        private string _fileName;

        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        private BarcodeTemplate _dataGridSelectedRow;

        public BarcodeTemplate DataGridSelectedRow
        {
            get { return _dataGridSelectedRow; }
            set { _dataGridSelectedRow = value; }
        }

        private Visibility _addButtonVisibility;

        public Visibility AddButtonVisibility
        {
            get { return _addButtonVisibility; }
            set { _addButtonVisibility = value; }
        }

        private Visibility _editButtonVisibility;

        public Visibility EditButtonVisibility
        {
            get { return _editButtonVisibility; }
            set { _editButtonVisibility = value; }
        }

        #endregion



        #region List
        private List<BarcodeTemplate> _templateList;

        public List<BarcodeTemplate> TemplateList
        {
            get { return _templateList; }
            set { _templateList = value; }
        }
        #endregion



        #region Command
        private ICommand addCommand;

        public ICommand AddCommand
        {
            get { return addCommand ?? (addCommand = new RelayCommand(Add)); }
            set { addCommand = value; }
        }

        private void Add(object obj)
        {
            Add();
        }

        private ICommand editCommand;

        public ICommand EditCommand
        {
            get { return editCommand ?? (editCommand = new RelayCommand(Edit)); }
            set { editCommand = value; }
        }

        private void Edit(object obj)
        {
            if (_dataGridSelectedRow == null)
                throw new ArgumentException("โปรดคลิกเลือกเครื่องพิมพ์ที่ต้องการลบจากตารางด้านล่าง");

            Edit();
        }

        private ICommand clearFormCommand;

        public ICommand ClearFormCommand
        {
            get { return clearFormCommand ?? (clearFormCommand = new RelayCommand(ClearForm)); }
            set { clearFormCommand = value; }
        }

        private void ClearForm(object obj)
        {
            Clear();
        }

        private ICommand selectedEditCommand;

        public ICommand SelectedEditCommand
        {
            get { return selectedEditCommand ?? (selectedEditCommand = new RelayCommand(SelectedEdit)); }
            set { selectedEditCommand = value; }
        }

        private void SelectedEdit(object obj)
        {
            try
            {
                if (obj == null)
                    return;

                _dataGridSelectedRow = (BarcodeTemplate)obj;
                _fileName = _dataGridSelectedRow.FileName;
                _editButtonVisibility = Visibility.Visible;
                _addButtonVisibility = Visibility.Collapsed;
                RaisePropertyChangedEvent(nameof(DataGridSelectedRow));
                RaisePropertyChangedEvent(nameof(FileName));
                RaisePropertyChangedEvent(nameof(EditButtonVisibility));
                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand deleteCommand;

        public ICommand DeleteCommand
        {
            get { return deleteCommand ?? (deleteCommand = new RelayCommand(Delete)); }
            set { deleteCommand = value; }
        }

        private void Delete(object obj)
        {
            if (obj == null)
                return;

            _dataGridSelectedRow = (BarcodeTemplate)obj;
            Delete();
        }

        private ICommand _browseFileCommand;

        public ICommand BrowseFileCommand
        {
            get { return _browseFileCommand ?? (_browseFileCommand = new RelayCommand(BrowseFile)); }
            set { _browseFileCommand = value; }
        }

        private void BrowseFile(object obj)
        {
            try
            {
                OpenFileDialog fd = new OpenFileDialog();
                fd.InitialDirectory = @"\\192.168.0.221\Share\Bartender";
                fd.RestoreDirectory = true;
                fd.Title = "Browse Bartender Template File.";
                fd.Filter = "BarTender Document (*.btw)|*.btw";
                fd.Multiselect = false;
                fd.ShowDialog();

                _filePath = Path.GetDirectoryName(fd.FileName) + @"\";
                _fileName = Path.GetFileName(fd.FileName);
                RaisePropertyChangedEvent(nameof(FileName));

                if (string.IsNullOrEmpty(_filePath))
                {
                    MessageBoxHelper.Warning("โปรดระบุชื่อไฟล์ก่อนจึงจะสามารถบันทึกข้อมูลได้");
                    OnFocusRequested(nameof(FileParh));
                    return;
                }

                if (_dataGridSelectedRow == null)
                    _addButtonVisibility = Visibility.Visible;

                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        #endregion



        #region Function
        private void Add()
        {
            try
            {
                if (string.IsNullOrEmpty(_filePath))
                {
                    MessageBoxHelper.Warning("โปรดใส่ชื่อเครื่องพิมพ์บาร์โค้ต");
                    OnFocusRequested(nameof(FileParh));
                    return;
                }

                Facade.BarcodeTemplateBL().Add(_filePath, _fileName, user_setting.security.uname);
                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Edit()
        {
            try
            {
                if (MessageBoxHelper.Question("ท่านต้องการแก้ไขชื่อเครื่องพิมพ์นี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                Facade.BarcodeTemplateBL()
                    .Edit(_dataGridSelectedRow.TemplateID,
                    _filePath, _fileName,
                    user_setting.security.uname);
                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Delete()
        {
            try
            {
                if (_dataGridSelectedRow == null)
                    throw new ArgumentException("โปรดคลิกเลือกเครื่องพิมพ์ที่ต้องการลบจากตารางด้านล่าง");

                if (MessageBoxHelper.Question("ท่านต้องการลบเครื่องพิมพ์นี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                Facade.BarcodeTemplateBL().Delete(_dataGridSelectedRow.TemplateID);
                MessageBoxHelper.Info("ลบข้อมูลสำเร็จ");
                Clear();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void Clear()
        {
            try
            {
                _filePath = null;
                _fileName = null;
                _dataGridSelectedRow = null;
                _addButtonVisibility = Visibility.Collapsed;
                _editButtonVisibility = Visibility.Collapsed;
                RaisePropertyChangedEvent(nameof(FileParh));
                RaisePropertyChangedEvent(nameof(FileName));
                RaisePropertyChangedEvent(nameof(_dataGridSelectedRow));
                RaisePropertyChangedEvent(nameof(AddButtonVisibility));
                RaisePropertyChangedEvent(nameof(EditButtonVisibility));
                OnFocusRequested(nameof(FileParh));
                TemplateListBinding();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void TemplateListBinding()
        {
            try
            {
                _templateList = Facade.BarcodeTemplateBL()
                    .GetAll()
                    .OrderBy(x => x.FilePath)
                    .ToList();
                RaisePropertyChangedEvent(nameof(TemplateList));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
