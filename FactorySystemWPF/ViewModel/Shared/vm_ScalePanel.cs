﻿using FactorySystemWPF.Helper;
using FactorySystemWPF.MVVM;
using FactorySystemWPF.View.Shared;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FactorySystemWPF.ViewModel.Shared
{
    public class vm_ScalePanel : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_ScalePanel()
        {
        }


        #region Properties
        private ScalePanel _window;

        public ScalePanel Window
        {
            get { return _window; }
            set { _window = value; }
        }

        private decimal? _weight;

        public decimal? Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }

        #endregion



        #region Command
        private ICommand _formEnterCommand;

        public ICommand FormEnterCommand
        {
            get { return _formEnterCommand ?? (_formEnterCommand = new RelayCommand(FormEnter)); }
            set { _formEnterCommand = value; }
        }

        private void FormEnter(object obj)
        {
            try
            {
                if (_weight <= 0)
                    throw new ArgumentException("น้ำหนักไม่ควรน้อยกว่า 1");

                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _windowClosedCommand;

        public ICommand WindowClosedCommand
        {
            get { return _windowClosedCommand ?? (_windowClosedCommand = new RelayCommand(WindowClosed)); }
            set { _windowClosedCommand = value; }
        }

        private void WindowClosed(object obj)
        {
            CloseSerialOnExit();
        }

        private ICommand _windowLoadedCommand;

        public ICommand WindowLoadedCommand
        {
            get { return _windowLoadedCommand ?? (_windowLoadedCommand = new RelayCommand(WindowLoaded)); }
            set { _windowLoadedCommand = value; }
        }

        private void WindowLoaded(object obj)
        {
            DigitalScaleConnect();
        }

        #endregion



        #region DigitalScale
        private delegate void preventCrossThreading(string str);
        //private preventCrossThreading accessControlFromCentralThread;

        private void displayTextReadIn(string str)
        {
            try
            {
                decimal value;
                if (!Decimal.TryParse(str, out value))
                {
                    _weight = Convert.ToDecimal(str);
                    return;
                }

                _weight = Convert.ToDecimal(str);
                RaisePropertyChangedEvent(nameof(Weight));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            var model = Properties.Settings.Default.Model.ToLower();

            if (model.Contains("ind221"))
                displayTextReadIn(DigitalScaleHelper.GetWeightFromMETTLER_TOLEDO_IND221());
            else if (model.Contains("spider2"))
                displayTextReadIn(DigitalScaleHelper.GetWeightFromMETTLER_TOLEDO_Spider2());
            else if (model.Contains("tiger"))
                displayTextReadIn(DigitalScaleHelper.GetWeightFromTiger_Commando());
            else
                displayTextReadIn(DigitalScaleHelper.GetWeightFromTiger_Commando());
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (DigitalScaleHelper.serialPort.IsOpen)
            {
                e.Cancel = true; //cancel the fom closing
                Thread CloseDown = new Thread(new ThreadStart(CloseSerialOnExit)); //close port in new thread to avoid hang
                CloseDown.Start(); //close port in new thread to avoid hang
            }
        }

        private void CloseSerialOnExit()
        {
            try
            {
                DigitalScaleHelper.serialPort.DataReceived -= port_DataReceived;
                DigitalScaleHelper.serialPort.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void DigitalScaleConnect()
        {
            try
            {
                DigitalScaleHelper.Setup();
                DigitalScaleHelper.serialPort.Open();
                DigitalScaleHelper.serialPort.DataReceived += port_DataReceived;
            }
            catch (Exception ex)
            {
                Thread CloseDown = new Thread(new ThreadStart(CloseSerialOnExit)); //close port in new thread to avoid hang
                CloseDown.Start(); //close port in new thread to avoid hang
                MessageBoxHelper.Exception(ex);
                _window.Close();
            }
        }
        #endregion
    }
}
