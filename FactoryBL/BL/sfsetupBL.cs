﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IsfsetupBL
    {
        void Save(sfsetup model);
        sfsetup GetSingle();
    }

    public class sfsetupBL : IsfsetupBL
    {
        IStecDBMSUnitOfWork uow;
        public sfsetupBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public sfsetup GetSingle()
        {
            return uow.sfsetupRepo.Get().FirstOrDefault();
        }

        public void Save(sfsetup model)
        {
            try
            {
                var sfsetup = GetSingle();
                if (sfsetup == null)
                    throw new ArgumentException("sfsetup is null.");

                sfsetup.crop = model.crop;
                sfsetup.slgrade = model.slgrade;
                sfsetup.sl2grade = model.sl2grade;
                sfsetup.ssgrade = model.ssgrade;
                sfsetup.flgrade = model.flgrade;
                sfsetup.fsgrade = model.fsgrade;

                uow.sfsetupRepo.Update(sfsetup);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
