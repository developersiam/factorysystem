﻿using FactoryBL.Helper;
using FactoryDAL.StoreProcedure;
using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface ImatBL
    {
        void FeedIn(string bc, DateTime issueddate, DateTime issuedtime,
            string topdno, int topdhour, DateTime feedingdate, DateTime feedingtime,
            string feedinguser, DateTime topddate, string toprgrade);
        void RegradeIssued(string isno, DateTime issueddate, DateTime issuedtime, string bc);
        void RegradeReceive(string rcno, string bc, int baleno, string classify, string mark,
            decimal weight, decimal weightbuy, bool picking, string receivedUser, string wh);
        void DeleteRegradeIssued(string bc, string isno);
        void DeleteRegradeReceive(string bc);
        string GetAB4RegradeBarcode(int crop, string type);
        void PrintRegradeBarcode(string bc, int copy);
        void PrintTestRegradeBarcode(int copy);

        void IssuedTo(string isno, string issuedto, DateTime issueddate, DateTime issuedtime, string bc);
        mat GetSingle(string bc);
        List<mat> GetByRcno(string rcno);
        List<mat> GetByIsno(string isno);
        List<mat> GetByToPdno(string topdno);
        List<mat> GetByToPdHour(string topdno, int topdhour);
        List<mat> GetByToPackedGrade(string toprgrade);
        List<mat> GetByType(int crop, string type);
        void PickingReceive(string rcno, string bc, int baleno, string classify,
            decimal weight, string receivedUser, string mark, DateTime pdDate, string pdNo, string pack_grade, decimal price, decimal priceunit, string subtype, string company, bool bz);
        string GetPickingBarcode(int crop, string type);
        void PrintPickingBarcode(string bc, int copy);
        void DeletePicking(string bc);
        void UpdateDocno(int crop, string rcno, string supplier, string subT, string company);
        void UpdateCheckerLocked(string rcno, string user);

    }

    public class matBL : ImatBL
    {
        StecDBMSUnitOfWork uow;

        public matBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public void FeedIn(string bc, DateTime issueddate, DateTime issuedtime,
            string topdno, int topdhour, DateTime feedingdate, DateTime feedingtime,
            string feedinguser, DateTime topddate, string toprgrade)
        {
            try
            {
                ///เงื่อนไขการนำยา green เข้า production 
                ///1.blendinglocked = false
                ///2.packinglocked = false
                ///3.bledingstatus = false
                ///4.is in mat
                ///5.issued = true
                ///6.blendinghour = packinghour
                ///7.mat type = packedgrade type (RYO,CTB,DIET can difference)
                ///8.matrc.checkerapproved = true
                ///

                if (string.IsNullOrEmpty(bc))
                    throw new ArgumentException("โปรดระบุเลขบาร์โค้ต");

                if (string.IsNullOrEmpty(topdno))
                    throw new ArgumentException("โปรดเลือก pdno ที่จะทำการบันทึกข้อมูล");

                var mat = GetSingle(bc);
                if (mat == null)
                    throw new ArgumentException("ไม่พบข้อมูลป้ายบาร์โค้ตยา green รหัส " + bc + " นี้ในระบบ");

                if (mat.issued == true)
                    throw new ArgumentException("บาร์โค้ตนี้ถูกใช้ไปแล้วในการทำ " + mat.issuedto);

                var pdsetup = uow.pdsetupRepo
                    .GetSingle(x => x.pdno == topdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (pdsetup.blendinglocked == true)
                    throw new ArgumentException("หน้าเบลนได้ lock pdno นี้แล้ว ติดต่อหัวหน้างานหากต้องการปลดล็อค");

                if (pdsetup.packinglocked == true)
                    throw new ArgumentException("ท้ายเครื่องได้ lock pdno นี้แล้ว ติดต่อหัวหน้างานหากต้องการปลดล็อค");

                var blendingstatus = Facade.blendingstatusBL().GetSingle(topdno, topdhour);
                if (blendingstatus == null)
                    throw new ArgumentException("ไม่พบชั่วโมงการทำงาน " + topdhour + " ที่หน้าเบลน โปรดตรวจสอบ");

                if (blendingstatus.locked == true)
                    throw new ArgumentException("ชั่วโมงที่ " + topdhour + " ถูก finish แล้ว ไม่สามารถบันทึกข้อมูลได้");

                var packingstatus = Facade.packingstatusBL().GetSingle(topdno, topdhour);
                if (packingstatus == null)
                    throw new ArgumentException("ไม่พบชั่วโมงการทำงาน " + topdhour + " ที่ท้ายเครื่อง โปรดตรวจสอบ");

                if (blendingstatus.blendinghour != packingstatus.packinghour)
                    throw new ArgumentException("ชั่วโมงการทำงานที่ท้ายเครื่องไม่ตรงกับชั่วโมงการทำงานปัจจุบันที่หน้าเบลน โปรดตรวจสอบ" + Environment.NewLine +
                        "ชั่วโมงหน้าเบลน : " + blendingstatus.blendinghour + Environment.NewLine +
                        "ชั่วโมงท้ายเครื่อง : " + packingstatus.packinghour);

                var packedgrade = Facade.packedgradeBL().GetSingle(toprgrade);
                if (packedgrade == null)
                    throw new ArgumentException("ไม่พบ packedgrade ในระบบ");

                if (!packedgrade.form.Contains("RYO") &&
                    !packedgrade.form.Contains("CTB") &&
                    !packedgrade.form.Contains("DIET"))
                    if (packedgrade.type != mat.type)
                        throw new ArgumentException("ข้อมูล type ไม่ตรงกันไม่สามารถบันทึกข้อมูลได้ " + Environment.NewLine +
                            "(อนุญาตให้ต่าง type ได้เฉพาะ RYO, CTB และ DIET เท่านั้น)");

                var matrc = Facade.matrcBL().GetSingle(mat.rcno);
                if (matrc == null)
                    throw new ArgumentException("ไม่พบข้อมูล rcno " + mat.rcno + " นี้ในระบบ");

                if (matrc.checkerapproved == false)
                    throw new ArgumentException("Checker ยังไม่ได้ approve บาร์โค้ตนี้โปรดเช็คข้อมูลกับทีม checker อีกครั้ง");

                var feedingweight = mat.weight;
                if (mat.company == "1" || mat.company == "2")
                    feedingweight = mat.weightbuy == null ? mat.weight : mat.weightbuy;

                mat.issued = true;
                mat.issuedto = "Blending";
                mat.issueddate = issueddate;
                mat.issuedtime = issuedtime;
                mat.topdno = topdno;
                mat.topdhour = topdhour;
                mat.topddate = topddate;
                mat.toprgrade = toprgrade;
                mat.feedingdate = feedingdate;
                mat.feedingtime = feedingtime;
                mat.feedinguser = feedinguser;
                mat.feedingweight = (double)feedingweight;

                uow.matRepo.Update(mat);
                uow.Save();

                /*UPDATE mat 
                 * SET issued = 1, 
                 * issuedto = 'Blending', 
                 * issueddate = ?, 
                 * issuedtime = ?, 
                 * topdno = ?, 
                 * topdhour = ?, 
                 * feedingdate = ?, 
                 * feedingtime = ?, 
                 * feedinguser = ?, 
                 * topddate = ?, 
                 * toprgrade = ? 
                 * WHERE (bc = ?)*/
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<mat> GetByToPackedGrade(string toprgrade)
        {
            return uow.matRepo.Get(x => x.toprgrade == toprgrade);
        }

        public List<mat> GetByToPdno(string topdno)
        {
            return uow.matRepo.Get(x => x.topdno == topdno);
        }

        public List<mat> GetByToPdHour(string topdno, int topdhour)
        {
            return uow.matRepo.Get(x => x.topdno == topdno && x.topdhour == topdhour);
        }

        public mat GetSingle(string bc)
        {
            return uow.matRepo.GetSingle(x => x.bc == bc);
        }

        public List<mat> GetByType(int crop, string type)
        {
            return uow.matRepo.Get(x => x.crop == crop && x.type == type);
        }

        public List<mat> GetByIsno(string isno)
        {
            return uow.matRepo.Get(x => x.isno == isno);
        }

        public List<mat> GetByRcno(string rcno)
        {
            return uow.matRepo.Get(x => x.rcno == rcno);
        }

        public void RegradeIssued(string isno, DateTime issueddate, DateTime issuedtime, string bc)
        {
            try
            {
                ///update mat set isno =?, issued = 1, issuedto =?, issueddate =?, issuedtime =? 
                ///where bc =?
                ///

                if (string.IsNullOrEmpty(isno))
                    throw new ArgumentException("isno cannot be empty.");

                if (string.IsNullOrEmpty(bc))
                    throw new ArgumentException("barcode cannot be empty.");

                var mat = GetSingle(bc);
                if (mat == null)
                    throw new ArgumentException("ไม่พบข้อมูลยาห่อนี้ในระบบ (barcode:" + bc + ")");

                if (mat.issued == true)
                    throw new ArgumentException("Barcode นี้ถูกใช้นำไปใช้แล้ว โปรดตรวจสอบข้อมูลกับทาง checker อีกครั้ง" + Environment.NewLine +
                        "รายละเอียด" + Environment.NewLine +
                        "bc: " + mat.bc + Environment.NewLine +
                        "crop: " + mat.crop + Environment.NewLine +
                        "type: " + mat.type + Environment.NewLine +
                        "classify: " + mat.classify + Environment.NewLine +
                        "weight: " + Convert.ToDecimal(mat.weight).ToString("N1") + Environment.NewLine +
                        "rcfrom: " + mat.rcfrom + Environment.NewLine +
                        "issuedTo: " + mat.issuedto + Environment.NewLine +
                        "issuedDate: " + mat.issueddate + Environment.NewLine +
                        "isno: " + mat.isno);

                var matis = uow.matisRepo.GetSingle(x => x.isno == isno);
                if (matis == null)
                    throw new ArgumentException("ไม่พบ isno นี้ในระบบ");

                var matrc = uow.matrcRepo.GetSingle(x => x.rgno == matis.rgno);
                if (matrc == null)
                    throw new ArgumentException("ไม่พบข้อมูล rgno นี้ในระบบ");

                if (matrc.finishtime != null)
                    throw new ArgumentException("rgno นี้ถูก finish ไปแล้วเมื่อ " + matrc.finishtime);

                if (matrc.checkerapproved == false)
                    throw new ArgumentException("Chceker ยังไม่ได้ approve ข้อมูลของ rcno นี้ โปรดแจ้ง checker เพื่อทำการตรวจสอบ");

                if (matrc.place != mat.wh)
                    throw new ArgumentException("place ของยาห่อนี้คือ " + mat.wh +
                        " ซึ่งไม่ตรงกับ place ที่ระบุไว้ด้านบนคือ " + matrc.place);

                if (matrc.type != mat.type)
                    throw new ArgumentException("type ของยาที่จะเอามาทำ regrade จะต้องเป็น type เดียวกันทั้งหมด");

                var matIssuedList = uow.matRepo.Get(x => x.isno == matrc.rcno).ToList();
                if (matIssuedList.Count() > 0)
                    if (mat.company != matIssuedList.FirstOrDefault().company)
                        throw new ArgumentException("ยาที่จะนำมาทำ regrade จะต้องเป็นของ company เดียวกันเท่านั้น");

                mat.isno = isno;
                mat.issued = true;
                mat.issuedto = "Regrade";
                mat.issueddate = issueddate;
                mat.issuedtime = issuedtime;

                uow.matRepo.Update(mat);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteRegradeIssued(string bc, string isno)
        {
            try
            {
                var mat = GetSingle(bc);
                if (mat == null)
                    throw new ArgumentException("ไม่พบห่อยารหัสบาร์โค้ต " + bc + " นี้ในระบบ");

                if (string.IsNullOrEmpty(mat.isno))
                    throw new ArgumentException("ห่อยานี้ไม่มีข้อมูล isno");

                ///จาก isno ของห่อยา regrade input ดังกล่าว จะต้องนำไปหา matrc หลักของการทำ regrade
                ///เพื่อตรวจสอบว่าถ้าหากต้องการลบข้อมูลการ issued ออกจาก rcno นั้น matrc ดังกล่าวจะต้องยังไม่ถูก finish
                ///การหา matrc หลักของห่อยา regrade input จะต้องหา isno ที่เชื่อมโยงไปยัง rcno ให้ได้
                ///เมื่อได้ matis แล้วจะพบคอลัมน์ rgno เราจะใช้ rgno เพื่อค้นหา rcno ของ matrc หลักต่อไป
                ///ห่อยาที่จะลบข้อมูลออกจาก regrade input คอลัมน์ issuedto จะต้องเป็น regrade เท่านั้น
                ///
                if (mat.issuedto != "Regrade")
                    throw new ArgumentException("ยาห่อนี้ไม่ไม่ได้ถูกนำไปทำ regrade");

                var matis = Facade.matisBL().GetSingle(isno);
                if (matis == null)
                    throw new ArgumentException("matis not found.");

                var matrc = uow.matrcRepo.GetSingle(x => x.rgno == matis.rgno);
                if (matrc == null)
                    throw new ArgumentException("ไม่พบ matrc จากการค้นหาด้วย rgno : " + matis.rgno);

                if (matrc.finishtime != null)
                    throw new ArgumentException("rcno นี้ถูก finish ไปแล้วเมื่อ " + matrc.finishtime);

                ///ห่อยานั้นจะต้องไม่ใช่ห่อยาที่มีค่า issuedto = regrade เท่านั้น
                if (mat.issued == true &&
                    mat.issuedto != "Regrade")
                    throw new ArgumentException("Barcode นี้ถูกใช้นำไปใช้แล้ว " +
                        "โปรดตรวจสอบข้อมูลกับทาง checker อีกครั้ง" + Environment.NewLine +
                        "รายละเอียด" + Environment.NewLine +
                        "bc: " + mat.bc + Environment.NewLine +
                        "crop: " + mat.crop + Environment.NewLine +
                        "type: " + mat.type + Environment.NewLine +
                        "classify: " + mat.classify + Environment.NewLine +
                        "weight: " + mat.weight + Environment.NewLine +
                        "rcfrom: " + mat.rcfrom + Environment.NewLine +
                        "issuedTo: " + mat.issuedto + Environment.NewLine +
                        "issuedDate: " + mat.issueddate + Environment.NewLine +
                        "isno: " + mat.isno);

                ///isno ของห่อยาจะต้องตรงกันกับ isno ของหัวเอกสาร (เป็นห่อยาที่ถูก issued ในเอกสารเดียวกัน)
                if (mat.isno != isno)
                    throw new ArgumentException("ห่อยาที่จะลบออกจาก regrade input นี้ไม่ตรงกันกับหัวเอกสาร" + Environment.NewLine +
                        "isno ของห่อยา : " + mat.isno + Environment.NewLine +
                        "isno ของเอกสาร : " + isno);

                mat.isno = null;
                mat.issued = false;
                mat.issuedto = null;
                mat.issueddate = null;
                mat.issuedtime = null;

                uow.matRepo.Update(mat);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void RegradeReceive(string rcno, string bc, int baleno, string classify, string mark,
            decimal weight, decimal weightbuy, bool picking, string receivedUser, string wh)
        {
            try
            {
                if (string.IsNullOrEmpty(rcno))
                    throw new ArgumentException("โปรดระบุ rcno");

                if (string.IsNullOrEmpty(classify))
                    throw new ArgumentException("โปรดระบุ classify");

                if (string.IsNullOrEmpty(receivedUser))
                    throw new ArgumentException("โปรดระบุุ receiving user");

                if (string.IsNullOrEmpty(wh))
                    throw new ArgumentException("โปรดระบุุ wh");

                if (string.IsNullOrEmpty(bc))
                    throw new ArgumentException("โปรดระบุ output barcode");

                var bcPrefix = bc.Substring(0, 2);
                if (bcPrefix != "05" && bcPrefix != "08" && bcPrefix != "CY")
                    throw new ArgumentException("รูปแบบรหัส Barcode ไม่ถูกต้อง จะต้องใช้ป้ายที่ขึ้นต้นด้วย 05 08 หรือ CY เท่านั้น");

                ///17 หลักคือป้าย 05 และ 08 
                ///13 หลักคือป้ายสำหรับทำยา AB4
                if (bc.Length != 17 && bc.Length != 11)
                    throw new ArgumentException("Barcode จะต้องมี 17 หลักกรณีป้าย 05 และ 08 " + Environment.NewLine +
                        "ส่วนป้าย AB4 จะต้องมี 11 หลัก");

                var mat = Facade.matBL().GetSingle(bc);
                if (mat != null)
                    throw new ArgumentException("Barcode นี้ซ้ำกับที่มีในระบบ โปรดตรวจสอบข้อมูลกับทาง checker อีกครั้ง" + Environment.NewLine +
                        "รายละเอียด" + Environment.NewLine +
                        "bc: " + mat.bc + Environment.NewLine +
                        "type: " + mat.type + Environment.NewLine +
                        "classify: " + mat.classify + Environment.NewLine +
                        "weight: " + Convert.ToDecimal(mat.weight).ToString("N1") + Environment.NewLine +
                        "rcfrom: " + mat.rcfrom + Environment.NewLine +
                        "rcno: " + mat.rcno);

                var matrc = uow.matrcRepo.GetSingle(x => x.rcno == rcno);
                if (matrc == null)
                    throw new ArgumentException("matrc cannot be null.");

                if (matrc.finishtime != null)
                    throw new ArgumentException("matrc นี้ถูก finish ไปแล้วเมื่อ " + matrc.finishtime);

                if (matrc.place != wh)
                    throw new ArgumentException("place ของยาห่อนี้คือ " + mat.wh +
                        " ซึ่งไม่ตรงกับ place ที่ระบุไว้ด้านบนคือ " + matrc.place);

                var matList = uow.matRepo.Get(x => x.rcno == matrc.rcno);
                if (matList.Count() > 0)
                    if (matList.FirstOrDefault().bc.Substring(0, 2) != bcPrefix)
                        throw new ArgumentException("รหัส barcode output 2 หลักแรกใน rcno เดียวกันจะต้องเหมือนกันทั้งหมด");

                /// <summary>
                /// INSERT INTO mat(rcno, crop, type, rcfrom, bc, supplier, baleno, 
                /// classify, mark, weight, weightbuy, bz, picking, dtrecord, 
                /// receiveduser, wh,cparesult,cparesultdate,cparesultuser) 
                /// VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?)
                /// </summary>
                /// 
                uow.matRepo.Add(new mat
                {
                    rcno = rcno,
                    crop = matrc.crop,
                    type = matrc.type,
                    rcfrom = "Regrade",
                    bc = bc,
                    supplier = "RG",
                    baleno = baleno,
                    classify = classify,
                    mark = mark,
                    weight = weight,
                    weightbuy = weightbuy,
                    bz = false,
                    picking = picking,
                    dtrecord = DateTime.Now,
                    receiveduser = receivedUser,
                    wh = wh,

                    issued = false,
                    priceunit = 0,
                    price = 0,
                    pvpriceunit = 0,
                    pvprice = 0,
                    firstprice = 0,
                    adjtimes = 0,
                    paid = false,
                    rcstatus = false,
                    leafstatus = false,
                    leaflocked = false,
                    blendingprice = 0,
                    pkvalue = false,
                    price2 = 0,
                    price3 = 0,
                    cropdef = 2011,
                    moisture_status = false,
                    wet = false,
                    handle_charge = 0,
                    price_wo_handle_charge = 0,
                    priceunit_wo_handle_charge = 0,
                    ReplaceBales = false
                });
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeleteRegradeReceive(string bc)
        {
            try
            {
                var mat = GetSingle(bc);
                if (mat == null)
                    throw new ArgumentException("ไม่พบห่อยารหัสบาร์โค้ต " + bc + " นี้ในระบบ");

                if (mat.issued == true)
                    throw new ArgumentException("Barcode นี้ถูกใช้นำไปใช้แล้ว โปรดตรวจสอบข้อมูลกับทาง checker อีกครั้ง" + Environment.NewLine +
                        "รายละเอียด" + Environment.NewLine +
                        "bc: " + mat.bc + Environment.NewLine +
                        "type: " + mat.type + Environment.NewLine +
                        "classify: " + mat.classify + Environment.NewLine +
                        "weight: " + mat.weight + Environment.NewLine +
                        "rcfrom: " + mat.rcfrom + Environment.NewLine +
                        "issuedTo: " + mat.issuedto + Environment.NewLine +
                        "issuedDate: " + mat.issueddate + Environment.NewLine +
                        "isno: " + mat.isno + Environment.NewLine +
                        "topdno: " + mat.topdno);

                var matrc = uow.matrcRepo.GetSingle(x => x.rcno == mat.rcno);
                if (matrc == null)
                    throw new ArgumentException("ไม่พบ rcno " + mat.rcno + " นี้ในระบบ");

                if (matrc.finishtime != null)
                    throw new ArgumentException("rcno นี้ถูก finish ไปแล้วเมื่อ " + matrc.finishtime);

                uow.matRepo.Remove(mat);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetAB4RegradeBarcode(int crop, string type)
        {
            try
            {
                ///SELECT TOP 1 'CY' + 
                ///substring(CAST(crop AS NVARCHAR), 3, 2) + 
                ///'-RG' + 
                ///FORMAT(CAST(substring(bc, 8, 4) AS INT) + 
                ///1, '0000') AS barcode
                ///FROM mat
                ///WHERE bc LIKE 'CY%'
                ///AND crop = ?
                ///AND type = ?
                ///ORDER BY bc DESC
                ///e.g. CY20-RG2345
                ///
                var list = uow.matRepo
                    .Get(x => x.crop == crop &&
                    x.type == type &&
                    x.bc.Contains("CY"))
                    .ToList();
                var max = 0;
                if (list.Count() > 0)
                    max = list.Max(x =>
                   Convert.ToInt16(x.bc.Substring(x.bc.Length - 4, 4)));

                max = max + 1;
                return "CY" + crop.ToString().Substring(2, 2) +
                    "-RG" + max.ToString().PadLeft(4, '0');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void IssuedTo(string isno, string issuedto, DateTime issueddate, DateTime issuedtime, string bc)
        {
            throw new NotImplementedException();
        }

        public void PrintRegradeBarcode(string bc, int copy)
        {
            try
            {
                if (copy < 1)
                    throw new ArgumentException("โปรดระบุจำนวนที่จะพิมพ์");

                var mat = uow.matRepo.GetSingle(x => x.bc == bc);
                if (mat == null)
                    throw new ArgumentException("ไม่พบห่อยารหัสบาร์โค้ต " + bc + " นี้ในระบบ");

                var matrc = uow.matrcRepo.GetSingle(x => x.rcno == mat.rcno);
                if (matrc == null)
                    throw new ArgumentException("ไม่พบ rcno นี้ในระบบ(" + mat.rcno + ") โปรดติดต่อแผนกไอทีเพื่อตรวจสอบข้อมูล");

                TSCPrinter.openport("TSC TTP-247");
                TSCPrinter.setup("100", "65", "3.0", "2", "0", "0", "0");
                TSCPrinter.sendcommand("GAP 4 mm,0");
                TSCPrinter.sendcommand("DIRECTION 1");
                TSCPrinter.clearbuffer();

                //---------------------
                //Pallet Tag
                //TSCPrinter.windowsfont(40, 5, 120, 0, 2, 0, "arial", Classify)
                //TSCPrinter.barcode("490", "10", "128", "40", "0", "0", "2", "2", BC)
                //TSCPrinter.windowsfont(525, 50, 40, 0, 2, 0, "arial", BC)
                //TSCPrinter.windowsfont(550, 85, 30, 0, 2, 0, "arial", Supplier & " , " & BaleNo)
                //---------------------
                TSCPrinter.barcode("30", "350", "128", "60", "0", "270", "2", "2", mat.bc); //Barcode แนวตั้งซ้าย
                TSCPrinter.barcode("745", "350", "128", "60", "0", "270", "2", "2", mat.bc); //Barcode แนวตั้งขวา
                TSCPrinter.windowsfont(120, 8, 40, 0, 2, 0, "arial", "STEC");
                TSCPrinter.windowsfont(370, 8, 40, 0, 2, 0, "arial", mat.bc);
                TSCPrinter.sendcommand("BAR 100,5,630,4"); //เส้น Header บน
                TSCPrinter.sendcommand("BAR 100,45,630,4"); //เส้น Header ล่าง
                TSCPrinter.sendcommand("BAR 100,5,4,180"); //เส้นซ้าย
                TSCPrinter.sendcommand("BAR 730,5,4,180"); //เส้นขวา
                TSCPrinter.windowsfont(110, 48, 35, 0, 0, 0, "tahoma", "Type:");
                TSCPrinter.windowsfont(110, 83, 35, 0, 0, 0, "tahoma", "Supp:");
                TSCPrinter.windowsfont(110, 118, 35, 0, 0, 0, "tahoma", "Green:");
                TSCPrinter.windowsfont(110, 153, 25, 0, 0, 0, "tahoma", "buyer:");
                TSCPrinter.windowsfont(420, 48, 35, 0, 0, 0, "tahoma", "Date:");
                TSCPrinter.windowsfont(420, 83, 35, 0, 0, 0, "tahoma", "Bale:");
                TSCPrinter.windowsfont(420, 118, 35, 0, 0, 0, "tahoma", "Weight:");
                TSCPrinter.windowsfont(420, 153, 25, 0, 0, 0, "tahoma", "classifier:");
                TSCPrinter.sendcommand("BAR 100,185,634,4"); //เส้นล่าง
                TSCPrinter.windowsfont(200, 48, 35, 0, 2, 0, "tahoma", mat.type);
                TSCPrinter.windowsfont(200, 83, 35, 0, 2, 0, "tahoma", mat.supplier);
                TSCPrinter.windowsfont(200, 118, 35, 0, 2, 0, "tahoma", mat.green == null ? "" : mat.green);
                TSCPrinter.windowsfont(200, 153, 25, 0, 0, 0, "tahoma", matrc.buyer == null ? "" : matrc.buyer);
                TSCPrinter.windowsfont(540, 48, 35, 0, 2, 0, "tahoma", Convert.ToDateTime(mat.dtrecord).Date.ToShortDateString());
                TSCPrinter.windowsfont(540, 83, 35, 0, 2, 0, "tahoma", mat.baleno.ToString());
                TSCPrinter.windowsfont(540, 118, 35, 0, 2, 0, "tahoma", ((decimal)mat.weight).ToString("N1"));
                TSCPrinter.windowsfont(540, 153, 25, 0, 0, 0, "tahoma", matrc.classifier);

                var classifyLen = mat.classify.Length;
                switch (classifyLen)
                {
                    case 3:
                        TSCPrinter.windowsfont(240, 173, 200, 0, 2, 0, "arial", mat.classify); //3 digit
                        break;
                    case 4:
                        TSCPrinter.windowsfont(190, 173, 200, 0, 2, 0, "arial", mat.classify); //4 digit
                        break;
                    case 5:
                        TSCPrinter.windowsfont(140, 173, 200, 0, 2, 0, "arial", mat.classify); //5 digit
                        break;
                    case 6:
                        TSCPrinter.windowsfont(90, 173, 200, 0, 2, 0, "arial", mat.classify); //6 digit
                        break;
                    default:
                        break;
                }

                if (classifyLen < 3)
                    TSCPrinter.windowsfont(290, 173, 200, 0, 2, 0, "arial", mat.classify); //<3 digit
                else if (classifyLen > 6)
                    TSCPrinter.windowsfont(90, 173, 130, 0, 2, 0, "arial", mat.classify); //>6 digit

                TSCPrinter.windowsfont(100, 333, 25, 0, 0, 0, "tahoma", "Effective : 09-09-2019                                          FM-LEF-35");
                TSCPrinter.windowsfont(100, 353, 25, 0, 0, 0, "tahoma", "D/M/Y");
                TSCPrinter.printlabel("1", copy.ToString());
                TSCPrinter.closeport();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PrintTestRegradeBarcode(int copy)
        {
            try
            {
                if (copy < 1)
                    throw new ArgumentException("โปรดระบุจำนวนที่จะพิมพ์");

                var mat = new mat
                {
                    bc = "0X00000000000-99",
                    type = "Type",
                    supplier = "Supplier",
                    green = "Green",
                    classify = "Classify",
                    dtrecord = DateTime.Now,
                    baleno = 99,
                    weight = (decimal)99.9
                };
                var matrc = new matrc
                {
                    buyer = "Buyer",
                    classifier = "Classifier"
                };

                TSCPrinter.openport("TSC TTP-247");
                TSCPrinter.setup("100", "65", "3.0", "2", "0", "0", "0");
                TSCPrinter.sendcommand("GAP 4 mm,0");
                TSCPrinter.sendcommand("DIRECTION 1");
                TSCPrinter.clearbuffer();

                //---------------------
                //Pallet Tag
                //TSCPrinter.windowsfont(40, 5, 120, 0, 2, 0, "arial", Classify)
                //TSCPrinter.barcode("490", "10", "128", "40", "0", "0", "2", "2", BC)
                //TSCPrinter.windowsfont(525, 50, 40, 0, 2, 0, "arial", BC)
                //TSCPrinter.windowsfont(550, 85, 30, 0, 2, 0, "arial", Supplier & " , " & BaleNo)

                //---------------------
                TSCPrinter.barcode("30", "350", "128", "60", "0", "270", "2", "2", mat.bc); //Barcode แนวตั้งซ้าย
                TSCPrinter.barcode("745", "350", "128", "60", "0", "270", "2", "2", mat.bc); //Barcode แนวตั้งขวา
                TSCPrinter.windowsfont(120, 8, 40, 0, 2, 0, "arial", "STEC");
                TSCPrinter.windowsfont(370, 8, 40, 0, 2, 0, "arial", mat.bc);
                TSCPrinter.sendcommand("BAR 100,5,630,4"); //เส้น Header บน
                TSCPrinter.sendcommand("BAR 100,45,630,4"); //เส้น Header ล่าง
                TSCPrinter.sendcommand("BAR 100,5,4,180"); //เส้นซ้าย
                TSCPrinter.sendcommand("BAR 730,5,4,180"); //เส้นขวา
                TSCPrinter.windowsfont(110, 48, 35, 0, 0, 0, "tahoma", "Type:");
                TSCPrinter.windowsfont(110, 83, 35, 0, 0, 0, "tahoma", "Supp:");
                TSCPrinter.windowsfont(110, 118, 35, 0, 0, 0, "tahoma", "Green:");
                TSCPrinter.windowsfont(110, 153, 25, 0, 0, 0, "tahoma", "buyer:");
                TSCPrinter.windowsfont(420, 48, 35, 0, 0, 0, "tahoma", "Date:");
                TSCPrinter.windowsfont(420, 83, 35, 0, 0, 0, "tahoma", "Bale:");
                TSCPrinter.windowsfont(420, 118, 35, 0, 0, 0, "tahoma", "Weight:");
                TSCPrinter.windowsfont(420, 153, 25, 0, 0, 0, "tahoma", "classifier:");
                TSCPrinter.sendcommand("BAR 100,185,634,4"); //เส้นล่าง
                TSCPrinter.windowsfont(200, 48, 35, 0, 2, 0, "tahoma", mat.type);
                TSCPrinter.windowsfont(200, 83, 35, 0, 2, 0, "tahoma", mat.supplier);
                TSCPrinter.windowsfont(200, 118, 35, 0, 2, 0, "tahoma", mat.green);
                TSCPrinter.windowsfont(200, 153, 25, 0, 0, 0, "tahoma", matrc.buyer);
                TSCPrinter.windowsfont(540, 48, 35, 0, 2, 0, "tahoma", Convert.ToDateTime(mat.dtrecord).Date.ToShortDateString());
                TSCPrinter.windowsfont(540, 83, 35, 0, 2, 0, "tahoma", mat.baleno.ToString());
                TSCPrinter.windowsfont(540, 118, 35, 0, 2, 0, "tahoma", ((decimal)mat.weight).ToString("N1"));
                TSCPrinter.windowsfont(540, 153, 25, 0, 0, 0, "tahoma", matrc.classifier);

                var classifyLen = mat.classify.Length;
                switch (classifyLen)
                {
                    case 3:
                        TSCPrinter.windowsfont(240, 173, 200, 0, 2, 0, "arial", mat.classify); //3 digit
                        break;
                    case 4:
                        TSCPrinter.windowsfont(190, 173, 200, 0, 2, 0, "arial", mat.classify); //4 digit
                        break;
                    case 5:
                        TSCPrinter.windowsfont(140, 173, 200, 0, 2, 0, "arial", mat.classify); //5 digit
                        break;
                    case 6:
                        TSCPrinter.windowsfont(90, 173, 200, 0, 2, 0, "arial", mat.classify); //6 digit
                        break;
                    default:
                        break;
                }

                if (classifyLen < 3)
                    TSCPrinter.windowsfont(290, 173, 200, 0, 2, 0, "arial", mat.classify); //<3 digit
                else if (classifyLen > 6)
                    TSCPrinter.windowsfont(90, 173, 130, 0, 2, 0, "arial", mat.classify); //>6 digit

                TSCPrinter.windowsfont(100, 333, 25, 0, 0, 0, "tahoma", "Effective : 09-09-2019                                          FM-LEF-35");
                TSCPrinter.windowsfont(100, 353, 25, 0, 0, 0, "tahoma", "D/M/Y");
                TSCPrinter.printlabel("1", copy.ToString());
                TSCPrinter.closeport();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PickingReceive(string rcno, string bc, int baleno, string classify,
            decimal weight, string receivedUser, string mark, DateTime pdDate, string pdNo, string pack_grade, decimal price, decimal priceunit, string subtype, string company, bool bz)
        {
            try
            {
                if (string.IsNullOrEmpty(rcno))
                    throw new ArgumentException("โปรดระบุ rcno");

                if (string.IsNullOrEmpty(classify))
                    throw new ArgumentException("โปรดระบุ classify");

                if (string.IsNullOrEmpty(receivedUser))
                    throw new ArgumentException("โปรดระบุุ receiving user");

                if (string.IsNullOrEmpty(bc))
                    throw new ArgumentException("โปรดระบุ output barcode");

                var bcPrefix = bc.Substring(0, 2);
                if (bcPrefix != "05" && bcPrefix != "08" && bcPrefix != "CY")
                    throw new ArgumentException("รูปแบบรหัส Barcode ไม่ถูกต้อง จะต้องใช้ป้ายที่ขึ้นต้นด้วย 05 08 หรือ CY เท่านั้น");

                ///17 หลักคือป้าย 05 และ 08 
                ///11 หลักคือป้ายสำหรับทำยา AB4
                if (((bcPrefix == "05" || bcPrefix == "08") && bc.Length != 17) && (bcPrefix == "CY" && bc.Length != 11))
                    throw new ArgumentException("Barcode จะต้องมี 17 หลักกรณีป้าย 05 และ 08 " + Environment.NewLine +
                        "และป้าย RYO จะต้องมี 11 หลัก");

                var matrc = uow.matrcRepo.GetSingle(x => x.rcno == rcno);
                if (matrc == null)
                    throw new ArgumentException("matrc cannot be null.");

                if ((bool)matrc.userlocked)
                    throw new ArgumentException("Picking no. นี้ถูก Send Data ไปแล้ว ถ้าต้องการแก้ไขโปรดติดต่อบัญชี เพื่อทำการปลดล็อค");


                var mat = Facade.matBL().GetSingle(bc);
                if (mat != null)
                {
                    //----- Update mat -----
                    mat.baleno = baleno;
                    mat.classify = classify;
                    mat.mark = mark;
                    mat.weight = weight;
                    mat.weightbuy = weight;
                    mat.dtrecord = DateTime.Now;
                    mat.picking = true;
                    mat.receiveduser = receivedUser;
                    mat.price = Convert.ToDouble(price);
                    mat.priceunit = Convert.ToDouble(priceunit);
                    mat.subtype = subtype;
                    mat.company = company;
                    mat.bz = bz;

                    uow.matRepo.Update(mat);
                    uow.Save();
                }
                else
                {
                    uow.matRepo.Add(new mat
                    {
                        rcno = rcno,
                        crop = matrc.crop,
                        type = matrc.type,
                        subtype = subtype,
                        rcfrom = "Blending",
                        bc = bc,
                        supplier = "BD",
                        baleno = baleno,
                        green = "",
                        classify = classify,
                        weight = weight,
                        weightbuy = weight,
                        bz = bz,
                        picking = true,
                        dtrecord = DateTime.Now,
                        receiveduser = receivedUser,
                        wh = "Leaf(BD,RG,HS)",
                        mark = "",
                        issuedreason = "CUT RAG",
                        frompddate = pdDate,
                        frompdno = pdNo,
                        fromprgrade = pack_grade,
                        price = Convert.ToDouble(price),
                        priceunit = Convert.ToDouble(priceunit),
                        company = company,
                        cropdef = DateTime.Now.Year,
                        pkvalue = true,
                        issued = false,
                        leaflocked = false,
                        blendingprice = 0,
                        price2 = 0,
                        price3 = 0,
                        pvpriceunit = 0,
                        pvprice = 0,
                        firstprice = 0,
                        adjtimes = 0,
                        paid = false,
                        rcstatus = false,
                        leafstatus = false,
                        locked = false,
                        moisture_status = false,
                        wet = false,
                        handle_charge = 0,
                        price_wo_handle_charge = 0,
                        priceunit_wo_handle_charge = 0,
                        ReplaceBales = false

                    });
                    uow.Save();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetPickingBarcode(int crop, string type)
        {
            try
            {
                var list = uow.matRepo
                    .Get(x => x.crop == crop &&
                    x.rcfrom == "Blending" &&
                    x.bc.Contains("PK"))
                    .ToList();

                var max = 0;
                if (list.Count() > 0)
                    max = list.Max(x =>
                   Convert.ToInt16(x.bc.Substring(x.bc.Length - 4, 4)));

                max = max + 1;
                return "CY" + crop.ToString().Substring(2, 2) +
                    "-PK" + max.ToString().PadLeft(4, '0');


            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PrintPickingBarcode(string bc, int copy)
        {
            try
            {
                if (copy < 1)
                    throw new ArgumentException("โปรดระบุจำนวนที่จะพิมพ์");

                var mat = uow.matRepo.GetSingle(x => x.bc == bc);
                if (mat == null)
                    throw new ArgumentException("ไม่พบห่อยารหัสบาร์โค้ต " + bc + " นี้ในระบบ");

                var matrc = uow.matrcRepo.GetSingle(x => x.rcno == mat.rcno);
                if (matrc == null)
                    throw new ArgumentException("ไม่พบ rcno นี้ในระบบ(" + mat.rcno + ") โปรดติดต่อแผนกไอทีเพื่อตรวจสอบข้อมูล");

                TSCPrinter.openport("TSC TTP-247");
                TSCPrinter.setup("100", "65", "3.0", "2", "0", "0", "0");
                TSCPrinter.sendcommand("GAP 4 mm,0");
                TSCPrinter.sendcommand("DIRECTION 1");
                TSCPrinter.clearbuffer();

                TSCPrinter.barcode("30", "350", "128", "60", "0", "270", "2", "2", mat.bc); //Barcode แนวตั้งซ้าย
                TSCPrinter.barcode("745", "350", "128", "60", "0", "270", "2", "2", mat.bc); //Barcode แนวตั้งขวา
                TSCPrinter.windowsfont(470, 8, 40, 0, 2, 0, "arial", mat.bc);
                TSCPrinter.sendcommand("BAR 100,5,630,4"); //เส้น Header บน
                TSCPrinter.sendcommand("BAR 100,45,630,4"); //เส้น Header ล่าง
                TSCPrinter.sendcommand("BAR 100,5,4,180"); //เส้นซ้าย
                TSCPrinter.sendcommand("BAR 730,5,4,180"); //เส้นขวา
                TSCPrinter.windowsfont(110, 48, 35, 0, 0, 0, "tahoma", "Type:");
                TSCPrinter.windowsfont(110, 83, 35, 0, 0, 0, "tahoma", "Supp:");
                TSCPrinter.windowsfont(110, 118, 35, 0, 0, 0, "tahoma", "Green:");
                TSCPrinter.windowsfont(110, 153, 25, 0, 0, 0, "tahoma", "buyer:");
                TSCPrinter.windowsfont(420, 48, 35, 0, 0, 0, "tahoma", "Date:");
                TSCPrinter.windowsfont(420, 83, 35, 0, 0, 0, "tahoma", "Bale:");
                TSCPrinter.windowsfont(420, 118, 35, 0, 0, 0, "tahoma", "Weight:");
                TSCPrinter.windowsfont(420, 153, 25, 0, 0, 0, "tahoma", "classifier:");
                TSCPrinter.sendcommand("BAR 100,185,634,4"); //เส้นล่าง
                TSCPrinter.windowsfont(200, 48, 35, 0, 2, 0, "tahoma", mat.type);
                TSCPrinter.windowsfont(200, 83, 35, 0, 2, 0, "tahoma", mat.supplier);
                TSCPrinter.windowsfont(200, 118, 35, 0, 2, 0, "tahoma", mat.green == null ? "" : mat.green);
                TSCPrinter.windowsfont(200, 153, 25, 0, 0, 0, "tahoma", matrc.buyer == null ? "" : matrc.buyer);
                TSCPrinter.windowsfont(540, 48, 35, 0, 2, 0, "tahoma", Convert.ToDateTime(mat.dtrecord).Date.ToShortDateString());
                TSCPrinter.windowsfont(540, 83, 35, 0, 2, 0, "tahoma", mat.baleno.ToString());
                TSCPrinter.windowsfont(540, 118, 35, 0, 2, 0, "tahoma", ((decimal)mat.weight).ToString("N1"));
                TSCPrinter.windowsfont(540, 153, 25, 0, 0, 0, "tahoma", matrc.classifier);

                var classifyLen = mat.classify.Length;
                switch (classifyLen)
                {
                    case 3:
                        TSCPrinter.windowsfont(240, 173, 200, 0, 2, 0, "arial", mat.classify); //3 digit
                        break;
                    case 4:
                        TSCPrinter.windowsfont(190, 173, 200, 0, 2, 0, "arial", mat.classify); //4 digit
                        break;
                    case 5:
                        TSCPrinter.windowsfont(140, 173, 200, 0, 2, 0, "arial", mat.classify); //5 digit
                        break;
                    case 6:
                        TSCPrinter.windowsfont(90, 173, 200, 0, 2, 0, "arial", mat.classify); //6 digit
                        break;
                    default:
                        break;
                }

                if (classifyLen < 3)
                    TSCPrinter.windowsfont(290, 163, 200, 0, 2, 0, "arial", mat.classify); //<3 digit
                else if (classifyLen > 6)
                    TSCPrinter.windowsfont(90, 163, 130, 0, 2, 0, "arial", mat.classify); //>6 digit

                TSCPrinter.windowsfont(100, 333, 25, 0, 0, 0, "tahoma", "Effective : 09-09-2019                                          FM-LEF-35");
                TSCPrinter.windowsfont(100, 353, 25, 0, 0, 0, "tahoma", "D/M/Y");
                TSCPrinter.printlabel("1", copy.ToString());
                TSCPrinter.closeport();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DeletePicking(string bc)
        {
            try
            {
                var mat = GetSingle(bc);
                if (mat == null)
                    throw new ArgumentException("ไม่พบห่อยารหัสบาร์โค้ต " + bc + " นี้ในระบบ");

                if (mat.issued == true)
                    throw new ArgumentException("Barcode นี้ถูกใช้นำไปใช้แล้ว โปรดตรวจสอบข้อมูลกับทาง checker อีกครั้ง" + Environment.NewLine +
                        "รายละเอียด" + Environment.NewLine +
                        "bc: " + mat.bc + Environment.NewLine +
                        "type: " + mat.type + Environment.NewLine +
                        "classify: " + mat.classify + Environment.NewLine +
                        "weight: " + mat.weight + Environment.NewLine +
                        "rcfrom: " + mat.rcfrom + Environment.NewLine +
                        "issuedTo: " + mat.issuedto + Environment.NewLine +
                        "issuedDate: " + mat.issueddate + Environment.NewLine +
                        "isno: " + mat.isno + Environment.NewLine +
                        "topdno: " + mat.topdno);

                var matrc = uow.matrcRepo.GetSingle(x => x.rcno == mat.rcno);
                if (matrc == null)
                    throw new ArgumentException("ไม่พบ rcno " + mat.rcno + " นี้ในระบบ");

                if ((bool)matrc.userlocked)
                    throw new ArgumentException("Picking No. นี้ send data ไปแล้ว ถ้าต้องการแก้ไขข้อมูล ติดต่อบัญชีเพื่อ ปลดล็อค" + matrc.finishtime);

                uow.matRepo.Remove(mat);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void UpdateDocno(int crop, string rcno, string supplier, string subT, string company)
        {
            try
            {
                StoreProcedureRepository.sp_Receiving_UPD_DocNo(crop, rcno, supplier, subT, company);
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public void UpdateCheckerLocked(string rcno, string user)
        {
            try
            {
                StoreProcedureRepository.sp_Receiving_UPD_CheckerLocked(rcno, user, true, true);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
