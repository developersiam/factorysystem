﻿using FactorySystemWPF.MVVM;
using FactorySystemWPF.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FactorySystemWPF.View.Shared
{
    /// <summary>
    /// Interaction logic for BarcodePrinter.xaml
    /// </summary>
    public partial class BarcodePrinter : Page
    {
        public BarcodePrinter()
        {
            InitializeComponent();
            DataContext = new vm_BarcodePrinter();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_BarcodePrinter)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.PrinterName):
                    PrinterNameTextBox.Focus();
                    PrinterNameTextBox.SelectAll();
                    break;
            }
        }
    }
}
