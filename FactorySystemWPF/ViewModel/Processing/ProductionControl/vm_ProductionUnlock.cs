﻿using FactoryBL;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.Model;
using FactorySystemWPF.MVVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace FactorySystemWPF.ViewModel.Processing.ProductionControl
{
    public class vm_ProductionUnlock : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }


        public vm_ProductionUnlock()
        {
            _crop = DateTime.Now.Year;
            _subBorderVisibility = Visibility.Collapsed;
            RaisePropertyChangedEvent(nameof(crop));
            RaisePropertyChangedEvent(nameof(SubBorderVisibility));
            pdsetupListBinding();
        }


        #region Properties
        private pdsetup _pdsetup;

        public pdsetup pdsetup
        {
            get { return _pdsetup; }
            set { _pdsetup = value; }
        }

        private int _crop;

        public int crop
        {
            get { return _crop; }
            set { _crop = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private Visibility _subBorderVisibility;

        public Visibility SubBorderVisibility
        {
            get { return _subBorderVisibility; }
            set { _subBorderVisibility = value; }
        }
        #endregion



        #region List
        private List<pdsetup> _pdsetupList;

        public List<pdsetup> pdsetupList
        {
            get { return _pdsetupList; }
            set { _pdsetupList = value; }
        }
        #endregion



        #region Command
        private ICommand _onDataGridSelectedCommand;

        public ICommand OnDataGridSelectedCommand
        {
            get { return _onDataGridSelectedCommand ?? (_onDataGridSelectedCommand = new RelayCommand(DataGridSelected)); }
            set { _onDataGridSelectedCommand = value; }
        }

        private void DataGridSelected(object obj)
        {
            try
            {
                _pdsetup = (pdsetup)obj;
                if (_pdsetup == null)
                    return;

                _subBorderVisibility = Visibility.Visible;
                RaisePropertyChangedEvent(nameof(pdsetup));
                RaisePropertyChangedEvent(nameof(SubBorderVisibility));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onSaveCommand;

        public ICommand OnSaveCommand
        {
            get { return _onSaveCommand ?? (_onSaveCommand = new RelayCommand(Save)); }
            set { _onSaveCommand = value; }
        }

        private void Save(object obj)
        {
            try
            {
                if (_pdsetup == null)
                    throw new ArgumentException("โปรดเลือกแถวข้อมูลที่ต้องการแก้ไข โดยคลิกจากแถวข้อมูลในตาราง");

                if (MessageBoxHelper.Question("การเปลี่ยนแปลงข้อมูลนี้ อาจส่งผลกระทบต่อการผลิตดังกล่าว " +
                    "ท่านต้องการแก้ไขข้อมูลนี้ใช่หรือไม่?") == MessageBoxResult.No)
                    return;

                Facade.pdsetupBL().ChangedLockedStatus(_pdsetup.pdno,
                    (bool)_pdsetup.blendinglocked,
                    (bool)_pdsetup.packinglocked,
                    (bool)_pdsetup.byproductlocked,
                    (bool)_pdsetup.pickinglocked,
                    user_setting.security.uname);

                pdsetupListBinding();
                ClearForm();
                MessageBoxHelper.Info("บันทึกข้อมูลสำเร็จ");
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private ICommand _onClearCommand;

        public ICommand OnClearCommand
        {
            get { return _onClearCommand ?? (_onClearCommand = new RelayCommand(Clear)); }
            set { _onClearCommand = value; }
        }

        private void Clear(object obj)
        {
            ClearForm();
        }

        private ICommand _onRefreshListCommand;

        public ICommand OnRefreshListCommand
        {
            get { return _onRefreshListCommand ?? (_onRefreshListCommand = new RelayCommand(Refresh)); }
            set { _onRefreshListCommand = value; }
        }

        private void Refresh(object obj)
        {
            pdsetupListBinding();
        }
        #endregion



        #region Function
        private void pdsetupListBinding()
        {
            try
            {
                _pdsetupList = Facade.pdsetupBL()
                    .GetByCrop(_crop)
                    .OrderByDescending(x => x.pdno)
                    .ToList();
                _totalRecord = _pdsetupList.Count();
                RaisePropertyChangedEvent(nameof(pdsetupList));
                RaisePropertyChangedEvent(nameof(TotalRecord));
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ClearForm()
        {
            _pdsetup = null;
            _subBorderVisibility = Visibility.Collapsed;
            RaisePropertyChangedEvent(nameof(pdsetup));
            RaisePropertyChangedEvent(nameof(SubBorderVisibility));
            pdsetupListBinding();
        }
        #endregion
    }
}
