﻿using FactorySystemWPF.MVVM;
using FactorySystemWPF.ViewModel.Processing.ProductionControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FactorySystemWPF.View.Processing.ProductionControl
{
    /// <summary>
    /// Interaction logic for ProductionUnlock.xaml
    /// </summary>
    public partial class ProductionUnlock : Page
    {
        public ProductionUnlock()
        {
            InitializeComponent();
            DataContext = new vm_ProductionUnlock();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_ProductionUnlock)DataContext;
            switch (e.PropertyName)
            {
                //case nameof(vm.pdsetup):
                //    PrinterNameTextBox.Focus();
                //    PrinterNameTextBox.SelectAll();
                //    break;
                default:
                    break;
            }
        }
    }
}
