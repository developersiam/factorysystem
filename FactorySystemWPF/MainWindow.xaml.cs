﻿using FactoryBL;
using FactorySystemWPF.Helper;
using FactorySystemWPF.Model;
using FactorySystemWPF.View;
using FactorySystemWPF.View.GreenLeaf.Regrade;
using FactorySystemWPF.View.Processing.Blending;
using FactorySystemWPF.View.Processing.ByProduct;
using FactorySystemWPF.View.Processing.Packing;
using FactorySystemWPF.View.Processing.PickingOut;
using FactorySystemWPF.View.Processing.ProductionControl;
using FactorySystemWPF.View.Shared;
using FactorySystemWPF.ViewModel.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FactorySystemWPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            user_setting.security = Facade.securityBL().GetSingle("IT");
            user_setting.machine = Environment.MachineName;
            MainFrame.NavigationService.Navigate(new ProductionSetup());
        }

        private void LogOffMenu_Click(object sender, RoutedEventArgs e)
        {
            user_setting.security = null;
            MainFrame.NavigationService.Navigate(new Login());
        }

        private void HomeMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new Home());
        }

        private void BlendingMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.security == null)
                    throw new ArgumentException("โปรดทำการล็อคอินก่อนเข้าใช้งานระบบ");

                if (user_setting.security.blending == false)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานระบบนี้ได้ " +
                        "โปรดแจ้งหัวหน้างานเพื่อให้แผนกไอทีเพิ่มสิทธิ์ให้");

                MainFrame.NavigationService.Navigate(new Blending());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PackingMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.security == null)
                    throw new ArgumentException("โปรดทำการล็อคอินก่อนเข้าใช้งานระบบ");

                if (user_setting.security.blending == false)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานระบบนี้ได้ " +
                        "โปรดแจ้งหัวหน้างานเพื่อให้แผนกไอทีเพิ่มสิทธิ์ให้");

                MainFrame.NavigationService.Navigate(new PackingV2());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void SetupDigitalScaleMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var window = new SetupDigitalScale();
                var vm = new vm_SetupDigitalScale(window);
                window.DataContext = vm;
                window.ShowDialog();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void ByProductMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.security == null)
                    throw new ArgumentException("โปรดทำการล็อคอินก่อนเข้าใช้งานระบบ");

                if (user_setting.security.stem == false)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานระบบนี้ได้ " +
                        "โปรดแจ้งหัวหน้างานเพื่อให้แผนกไอทีเพิ่มสิทธิ์ให้");

                MainFrame.NavigationService.Navigate(new ByProduct());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void RegradeMenu_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.NavigationService.Navigate(new Regrade());
        }

        private void PickingOutMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.security == null)
                    throw new ArgumentException("โปรดทำการล็อคอินก่อนเข้าใช้งานระบบ");

                if (user_setting.security.pickingout == false)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานระบบนี้ได้ " +
                        "โปรดแจ้งหัวหน้างานเพื่อให้แผนกไอทีเพิ่มสิทธิ์ให้");

                MainFrame.NavigationService.Navigate(new PickingOut());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void ProductionControlMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BarcodeSettingMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.security == null)
                    throw new ArgumentException("โปรดทำการล็อคอินก่อนเข้าใช้งานระบบ");

                if (user_setting.security.productioncontrol == false)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานระบบนี้ได้ " +
                        "โปรดแจ้งหัวหน้างานเพื่อให้แผนกไอทีเพิ่มสิทธิ์ให้");

                MainFrame.NavigationService.Navigate(new BarcodeSetting());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }

        private void PackedGradeMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void BoxTareMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void PrintLabelMenu_Click(object sender, RoutedEventArgs e)
        {

        }

        private void ProductionUnlockMenu_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (user_setting.security == null)
                    throw new ArgumentException("โปรดทำการล็อคอินก่อนเข้าใช้งานระบบ");

                if (user_setting.security.costing == false)
                    throw new ArgumentException("บัญชีผู้ใช้ของท่านไม่สามารถเข้าใช้งานระบบนี้ได้ " +
                        "โปรดแจ้งหัวหน้างานเพื่อให้แผนกไอทีเพิ่มสิทธิ์ให้");

                MainFrame.NavigationService.Navigate(new ProductionUnlock());
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
    }
}
