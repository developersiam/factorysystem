﻿using FactoryBL;
using FactoryEntities;
using FactorySystemWPF.Helper;
using FactorySystemWPF.MVVM;
using FactorySystemWPF.View.Processing.PickingOut;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace FactorySystemWPF.ViewModel.Processing.PickingOut
{
    public class vm_SelectType : ObservableObject, IRequestFocus
    {
        public event EventHandler<FocusRequestedEventArgs> FocusRequested;
        protected virtual void OnFocusRequested(string propertyName)
        {
            FocusRequested?.Invoke(this, new FocusRequestedEventArgs(propertyName));
        }
        public vm_SelectType()
        {
            _crop = DateTime.Now.Year;
            _typeList = Facade.typeBL().GetAll();
            RaisePropertyChangedEvent(nameof(crop));
            RaisePropertyChangedEvent(nameof(TypeList));
        }

        #region Properties
        private int _crop;

        public int crop
        {
            get { return _crop; }
            set { _crop = value; }
        }

        private string _type;

        public string type
        {
            get { return _type; }
            set { _type = value; }
        }

        private int _totalRecord;

        public int TotalRecord
        {
            get { return _totalRecord; }
            set { _totalRecord = value; }
        }

        private SelectType _window;

        public SelectType Window
        {
            get { return _window; }
            set { _window = value; }
        }

        #endregion



        #region List
        private List<type> _typeList;

        public List<type> TypeList
        {
            get { return _typeList; }
            set { _typeList = value; }
        }
        #endregion



        #region Command
        private ICommand _okCommand;

        public ICommand OKCommand
        {
            get { return _okCommand ?? (_okCommand = new RelayCommand(OK)); }
            set { _okCommand = value; }
        }

        private void OK(object obj)
        {
            try
            {
                if (string.IsNullOrEmpty(_type))
                    throw new ArgumentException("โปรดระบุ type");

                _window.Close();
            }
            catch (Exception ex)
            {
                MessageBoxHelper.Exception(ex);
            }
        }
        #endregion
    }
}
