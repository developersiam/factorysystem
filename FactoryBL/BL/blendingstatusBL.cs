﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IblendingstatusBL
    {
        int Add(string pdno);
        void Finish(string pdno, int blendinghour);
        void Delete(string pdno, int blendinghour);
        List<blendingstatu> GetByPdno(string pdno);
        blendingstatu GetSingle(string pdno, int blendinghour);
    }

    public class blendingstatusBL : IblendingstatusBL
    {
        StecDBMSUnitOfWork uow;
        public blendingstatusBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public int Add(string pdno)
        {
            try
            {
                var pdsetup = uow.pdsetupRepo.GetSingle(x => x.pdno == pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (pdsetup.blendinglocked == true)
                    throw new ArgumentException("(blendinglocked) เกรดนี้หน้าเบลน locked แล้ว" +
                        " หากต้องการปลดล็อคให้ติดต่อหัวหน้างานเพื่อขอปลดล็อค");

                if (pdsetup.packinglocked == true)
                    throw new ArgumentException("(packinglocked) เกรดนี้ท้ายเครื่อง locked แล้ว" +
                        " หากต้องการปลดล็อคให้ติดต่อหัวหน้างานเพื่อขอปลดล็อค");

                var blendingststusList = GetByPdno(pdno);
                if (blendingststusList.Where(x => x.locked == false).Count() > 0)
                    throw new ArgumentException("ชั่วโมงที่ " +
                        blendingststusList.FirstOrDefault(x => x.locked == false).blendinghour +
                        " ยังไม่ถูกล็อค ไม่สามารถเพิ่มชั่วโมงใหม่ได้");

                var nextHour = 1;
                if (blendingststusList.Count() > 0)
                    nextHour = blendingststusList.Max(x => x.blendinghour) + 1;

                uow.blendingstatusRepo.Add(new blendingstatu
                {
                    crop = pdsetup.crop,
                    pdno = pdno,
                    pddate = (DateTime)pdsetup.date,
                    blendinghour = nextHour,
                    locked = false
                });
                uow.Save();

                return nextHour;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Delete(string pdno, int blendinghour)
        {
            try
            {
                if (string.IsNullOrEmpty(pdno))
                    throw new ArgumentException("โปรดเลือกเกรดที่จะลบชั่วโมง โดยคลิกที่ปุ่ม New Grade");

                var pdsetup = uow.pdsetupRepo.GetSingle(x => x.pdno == pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (pdsetup.blendinglocked == true)
                    throw new ArgumentException("(blendinglocked) เกรดนี้หน้าเบลน locked แล้ว" +
                        "หากต้องการปลดล็อคให้ติดต่อหัวหน้างานเพื่อขอปลดล็อค");

                var blendingstatus = GetSingle(pdno, blendinghour);
                if (blendingstatus == null)
                    throw new ArgumentException("ไม่พบชั่วโมงดังกล่าวในระบบ");

                if (blendingstatus.locked == true)
                    throw new ArgumentException("ชั่วโมงนี้อยู่ในสถานะ locked แล้ว ไม่สามารถลบได้");

                var greenList = Facade.matBL().GetByToPdHour(pdno, blendinghour);
                var pdInList = Facade.pdBL().GetByToPdHour(pdno, blendinghour);
                var pdOutList = Facade.pdBL().GetByFromPdHour(pdno, blendinghour);

                if (greenList.Count() >= 1 || pdInList.Count() >= 1)
                    throw new ArgumentException("มีข้อมูลยา green หรือ packed stock ภายในชั่วโมง ไม่สามารถลบชั่วโมงได้");

                if (pdOutList.Count() >= 1)
                    throw new ArgumentException("ชั่วโมงนี้มียาออกแล้วที่ท้ายเครื่องแล้ว ไม่สามารถลบชั่วโมงได้ โปรดตรวจสอบกับท้ายเครื่อง");

                uow.blendingstatusRepo.Remove(blendingstatus);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Finish(string pdno, int blendinghour)
        {
            try
            {
                var blendingstatus = GetSingle(pdno, blendinghour);
                if (blendingstatus == null)
                    throw new ArgumentException("ไม่พบชั่วโมงดังกล่าวในระบบ");

                var pdsetup = uow.pdsetupRepo.GetSingle(x => x.pdno == pdno);
                if (pdsetup == null)
                    throw new ArgumentException("ไม่พบข้อมูล pdsetup ในระบบโปรดติดต่อแผนกไอทีเพื่อตรวจสอบ");

                if (pdsetup.blendinglocked == true)
                    throw new ArgumentException("(blendinglocked) เกรดนี้หน้าเบลน locked แล้ว" +
                        "หากต้องการปลดล็อคให้ติดต่อหัวหน้างานเพื่อขอปลดล็อค");

                var blendingstatusList = uow.blendingstatusRepo.Get(x => x.pdno == pdno);
                if (blendingstatusList.Where(x => x.locked == false &&
                 x.blendinghour != blendinghour).Count() >= 1)
                    throw new ArgumentException("มีบางชั่วโมงที่ยังไม่ได้ locked โปรดตรวจสอบ");

                var greenList = Facade.matBL().GetByToPdHour(pdno, blendinghour);
                var pdInList = Facade.pdBL().GetByToPdHour(pdno, blendinghour);
                var pdOutList = Facade.pdBL().GetByFromPdHour(pdno, blendinghour);

                if (greenList.Count() < 1 && pdInList.Count() < 1)
                    throw new ArgumentException("ชั่วโมงนี้จะต้องมียา green หรือ packed stock อย่างน้อย 1 ป้าย");

                if (pdOutList.Count() < 1)
                    throw new ArgumentException("ชั่วโมงนี้จะต้องมียากล่องออกที่ท้ายเครื่องอย่างน้อย 1 กล่อง โปรดตรวจสอบกับท้ายเครื่อง");

                blendingstatus.locked = true;
                uow.blendingstatusRepo.Update(blendingstatus);
                uow.Save();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<blendingstatu> GetByPdno(string pdno)
        {
            return uow.blendingstatusRepo.Get(x => x.pdno == pdno);
        }

        public blendingstatu GetSingle(string pdno, int blendinghour)
        {
            return uow.blendingstatusRepo
                .GetSingle(x => x.pdno == pdno && x.blendinghour == blendinghour);
        }
    }
}
