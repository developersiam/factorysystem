﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IpdCustomerPackedPrintBL
    {
        List<PDCustomerPackedPrint> GetByPackedGradeAndCustomer(string packedgrade, string customer);
        PDCustomerPackedPrint GetSingle(int cusLotID);
    }

    public class pdCustomerPackedPrintBL : IpdCustomerPackedPrintBL
    {
        StecDBMSUnitOfWork uow;
        public pdCustomerPackedPrintBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public List<PDCustomerPackedPrint> GetByPackedGradeAndCustomer(string packedgrade, string customer)
        {
            return uow.pdCusPackedPrintRepo
                .Get(x => x.STECPackedGrade == packedgrade &&
                x.STECCustomer == customer);
        }

        public PDCustomerPackedPrint GetSingle(int cusLotID)
        {
            return uow.pdCusPackedPrintRepo.GetSingle(x => x.CusLotID == cusLotID);
        }
    }
}
