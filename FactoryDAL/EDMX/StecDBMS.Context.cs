﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FactoryDAL.EDMX
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    using FactoryEntities;

    public partial class StecDBMSEntities : DbContext
    {
        public StecDBMSEntities()
            : base("name=StecDBMSEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<BarcodePrinter> BarcodePrinters { get; set; }
        public virtual DbSet<blendingstatu> blendingstatus { get; set; }
        public virtual DbSet<company> companies { get; set; }
        public virtual DbSet<mat> mats { get; set; }
        public virtual DbSet<matrc> matrcs { get; set; }
        public virtual DbSet<PDCustomer> PDCustomers { get; set; }
        public virtual DbSet<PDCustomerPackedPrint> PDCustomerPackedPrints { get; set; }
        public virtual DbSet<security> securities { get; set; }
        public virtual DbSet<subtype> subtypes { get; set; }
        public virtual DbSet<type> types { get; set; }
        public virtual DbSet<ShippingMovement> ShippingMovements { get; set; }
        public virtual DbSet<classify> classifies { get; set; }
        public virtual DbSet<expert> experts { get; set; }
        public virtual DbSet<matwh> matwhs { get; set; }
        public virtual DbSet<supplier> suppliers { get; set; }
        public virtual DbSet<picking> pickings { get; set; }
        public virtual DbSet<pdbc> pdbcs { get; set; }
        public virtual DbSet<packedgrade> packedgrades { get; set; }
        public virtual DbSet<packingstatu> packingstatus { get; set; }
        public virtual DbSet<pd> pds { get; set; }
        public virtual DbSet<mati> matis { get; set; }
        public virtual DbSet<matrgno> matrgnoes { get; set; }
        public virtual DbSet<matisno> matisnoes { get; set; }
        public virtual DbSet<sfsetup> sfsetups { get; set; }
        public virtual DbSet<matrcno> matrcnoes { get; set; }
        public virtual DbSet<pdsetup> pdsetups { get; set; }
        public virtual DbSet<BarcodeTemplate> BarcodeTemplates { get; set; }
        public virtual DbSet<BarcodeSetting> BarcodeSettings { get; set; }
        public virtual DbSet<pdsp> pdsps { get; set; }
        public virtual DbSet<ShippingOrder> ShippingOrders { get; set; }
        public virtual DbSet<ShippingBay> ShippingBays { get; set; }
        public virtual DbSet<ShippingCustomerOrder> ShippingCustomerOrders { get; set; }
        public virtual DbSet<ShippingLocation> ShippingLocations { get; set; }
        public virtual DbSet<ShippingMovementTransaction> ShippingMovementTransactions { get; set; }
        public virtual DbSet<ShippingTruck> ShippingTrucks { get; set; }
        public virtual DbSet<ShippingTruckDriver> ShippingTruckDrivers { get; set; }
    
        public virtual ObjectResult<string> sp_Receiving_DecodePassword(string pwd)
        {
            var pwdParameter = pwd != null ?
                new ObjectParameter("pwd", pwd) :
                new ObjectParameter("pwd", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("sp_Receiving_DecodePassword", pwdParameter);
        }
    
        public virtual int sp_Receiving_UPD_CheckerLocked(string rcno, string checker, Nullable<bool> checkerapproved, Nullable<bool> checkerlocked)
        {
            var rcnoParameter = rcno != null ?
                new ObjectParameter("rcno", rcno) :
                new ObjectParameter("rcno", typeof(string));
    
            var checkerParameter = checker != null ?
                new ObjectParameter("checker", checker) :
                new ObjectParameter("checker", typeof(string));
    
            var checkerapprovedParameter = checkerapproved.HasValue ?
                new ObjectParameter("checkerapproved", checkerapproved) :
                new ObjectParameter("checkerapproved", typeof(bool));
    
            var checkerlockedParameter = checkerlocked.HasValue ?
                new ObjectParameter("checkerlocked", checkerlocked) :
                new ObjectParameter("checkerlocked", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Receiving_UPD_CheckerLocked", rcnoParameter, checkerParameter, checkerapprovedParameter, checkerlockedParameter);
        }
    
        public virtual int sp_Receiving_UPD_RCBFinished(string rcno, Nullable<bool> locked)
        {
            var rcnoParameter = rcno != null ?
                new ObjectParameter("rcno", rcno) :
                new ObjectParameter("rcno", typeof(string));
    
            var lockedParameter = locked.HasValue ?
                new ObjectParameter("locked", locked) :
                new ObjectParameter("locked", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Receiving_UPD_RCBFinished", rcnoParameter, lockedParameter);
        }
    
        public virtual int sp_Receiving_UPD_DocNo(Nullable<int> crop, string rcno, string supplier, string subtype, string company)
        {
            var cropParameter = crop.HasValue ?
                new ObjectParameter("crop", crop) :
                new ObjectParameter("crop", typeof(int));
    
            var rcnoParameter = rcno != null ?
                new ObjectParameter("rcno", rcno) :
                new ObjectParameter("rcno", typeof(string));
    
            var supplierParameter = supplier != null ?
                new ObjectParameter("supplier", supplier) :
                new ObjectParameter("supplier", typeof(string));
    
            var subtypeParameter = subtype != null ?
                new ObjectParameter("subtype", subtype) :
                new ObjectParameter("subtype", typeof(string));
    
            var companyParameter = company != null ?
                new ObjectParameter("company", company) :
                new ObjectParameter("company", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("sp_Receiving_UPD_DocNo", cropParameter, rcnoParameter, supplierParameter, subtypeParameter, companyParameter);
        }
    
        public virtual ObjectResult<sp_PD_SEL_PackedCasesByGrade_Result> sp_PD_SEL_PackedCasesByGrade(string grade)
        {
            var gradeParameter = grade != null ?
                new ObjectParameter("grade", grade) :
                new ObjectParameter("grade", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_PD_SEL_PackedCasesByGrade_Result>("sp_PD_SEL_PackedCasesByGrade", gradeParameter);
        }
    
        public virtual ObjectResult<sp_Shipping_SEL_ShippingMovementV2023_Result> sp_Shipping_SEL_ShippingMovementV2023(Nullable<int> crop, string fromLocation)
        {
            var cropParameter = crop.HasValue ?
                new ObjectParameter("crop", crop) :
                new ObjectParameter("crop", typeof(int));
    
            var fromLocationParameter = fromLocation != null ?
                new ObjectParameter("fromLocation", fromLocation) :
                new ObjectParameter("fromLocation", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_Shipping_SEL_ShippingMovementV2023_Result>("sp_Shipping_SEL_ShippingMovementV2023", cropParameter, fromLocationParameter);
        }
    
        public virtual ObjectResult<sp_Shipping_SEL_ShippingMovementByMovementNo_Result> sp_Shipping_SEL_ShippingMovementByMovementNo(string movementNo)
        {
            var movementNoParameter = movementNo != null ?
                new ObjectParameter("MovementNo", movementNo) :
                new ObjectParameter("MovementNo", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_Shipping_SEL_ShippingMovementByMovementNo_Result>("sp_Shipping_SEL_ShippingMovementByMovementNo", movementNoParameter);
        }
    
        public virtual ObjectResult<sp_Shipping_SEL_BarcodeDetailFromPC_Result> sp_Shipping_SEL_BarcodeDetailFromPC(string bc)
        {
            var bcParameter = bc != null ?
                new ObjectParameter("bc", bc) :
                new ObjectParameter("bc", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<sp_Shipping_SEL_BarcodeDetailFromPC_Result>("sp_Shipping_SEL_BarcodeDetailFromPC", bcParameter);
        }
    }
}
