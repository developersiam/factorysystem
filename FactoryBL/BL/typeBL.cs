﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface ItypeBL
    {
        List<type> GetAll();
    }

    public class typeBL : ItypeBL
    {
        IStecDBMSUnitOfWork uow;
        public typeBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public List<type> GetAll()
        {
            return uow.typeRepo.Get();
        }
    }
}
