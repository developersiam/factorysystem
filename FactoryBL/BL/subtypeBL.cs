﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface IsubtypeBL
    {
        List<subtype> GetAll();
    }

    public class subtypeBL : IsubtypeBL
    {
        IStecDBMSUnitOfWork uow;
        public subtypeBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public List<subtype> GetAll()
        {
            return uow.subtypeRepo.Get();
        }
    }
}
