﻿using FactorySystemWPF.MVVM;
using FactorySystemWPF.ViewModel.GreenLeaf.Regrade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace FactorySystemWPF.View.GreenLeaf.Regrade
{
    /// <summary>
    /// Interaction logic for SelectType.xaml
    /// </summary>
    public partial class New : Window
    {
        public New()
        {
            InitializeComponent();
        }

        private void Focus_FocusRequested(object sender, FocusRequestedEventArgs e)
        {
            var vm = (vm_New)DataContext;
            switch (e.PropertyName)
            {
                case nameof(vm.type):
                    typeComboBox.Focus();
                    break;
                case nameof(vm.place):
                    placeComboBox.Focus();
                    break;
                case nameof(vm.classifier):
                    classifierComboBox.Focus();
                    break;
                case nameof(vm.remark):
                    classifierComboBox.Focus();
                    break;
                default:
                    break;
            }
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            IRequestFocus focus = (IRequestFocus)DataContext;
            focus.FocusRequested += Focus_FocusRequested;
        }
    }
}
