﻿using FactoryDAL.UnitOfWork;
using FactoryEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryBL.BL
{
    public interface ImatisnoBL
    {
        string Add(int crop, string machine, string user);
    }

    public class matisnoBL : ImatisnoBL
    {
        StecDBMSUnitOfWork uow;
        public matisnoBL()
        {
            uow = new StecDBMSUnitOfWork();
        }

        public string Add(int crop, string machine, string user)
        {
            try
            {
                if (crop != DateTime.Now.Year)
                    throw new ArgumentException("crop ที่ระบุไม่ตรงกับ crop ในปฏิทินของเครื่อง โปรดตรวจสอบ");

                if (string.IsNullOrEmpty(machine))
                    throw new ArgumentException("โปรดระบุ machine");

                if (string.IsNullOrEmpty(user))
                    throw new ArgumentException("โปรดระบุ user");

                var isnoList = uow.matisnoRepo.Get(x => x.crop == crop);
                var newIsno = 0;
                if (isnoList.Count() < 1)
                    newIsno = 1;
                else
                    newIsno = isnoList.Max(x => x.isno) + 1;

                uow.matisnoRepo.Add(new matisno
                {
                    crop = crop,
                    isno = newIsno,
                    machine = machine,
                    user = user,
                    dtrecord = DateTime.Now
                });
                uow.Save();
                return crop.ToString().Substring(2, 2) +
                    "-" + newIsno.ToString().PadLeft(4, '0');
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
